# Instructions:

## Build docker image

~~~
docker build . -t kernel-dev
~~~

It will build a docker image labeled `kernel-dev`.

## Prepare a projects folder

You can prepare a folder named `projects` for example, and put inside source code
projects like Linux kernel sources, LKM, xv6-unrc, etc.

To download the Linux kernel source needed to compiling it or compiling Linux
kernel modules, run the `get-kernel-version.sh` script, which will show the
corresponding url. It use wget as download command. You can change the script to
using curl instead.

## Run docker container: 

The following script runs a container instance assuming you have the `projects`
folder in current directory and mounts it on /home of container instance.

~~~
./run-container.sh
~~~

## Editing files on projects folder

The `projects` folder is _shared_ by the running container instance and the host
system, so you can edit files from your host environment with your favorite
developing environment and then compile and run commands inside the container
instance without need of exiting of this.

