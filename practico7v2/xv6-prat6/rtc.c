/*
This simple module shows a basic Linux Kernem Module device driver
Usage:
  1. Create device special file /dev/myrtc as a character device with major
     number equal to DEFAULT_MAJOR (see man mknod)
  2. Compile this module (make)
  3. Load the module (sudo insmod ./rtc.ko)
  4. You should see "Loading module myrtc" in system log (dmesg)
  5. Write a user program which read from /dev/myrtc

Dependencies: you will need kernel-headers package and gcc

Author: Marcelo Arroyo - Universidad Nacional de Río Cuarto. 2014-2016
*/

#include <linux/module.h>
#include <linux/ioport.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include <linux/fs.h>		/* for file_operations */

#define DEFAULT_MAJOR	121
#define RTC_CMDPORT     0x70
#define RTC_DATAPORT    0x71
#define RTC_SECONDS	    0
#define RTC_MINUTES	    2
#define RTC_HOURS       4
#define RTC_WEEKDAY 	6

MODULE_AUTHOR("The great LKM programmer");
MODULE_DESCRIPTION("Just a dummy RTC module to play with");
MODULE_LICENSE("Dual BSD/GPL");

/* MODULE PARAMETERS */
#define DEV_NAME "myrtc"
static int dev = DEFAULT_MAJOR;
/*
MODULE_PARM(dev);
MODULE_PARM_DESC(dev,"Major device number");
*/

static int rtc_open(struct inode *i_node, struct file *f);
static ssize_t rtc_read(struct file *f, char __user *buffer, size_t count, loff_t *fpos);

static unsigned char get_rtc_register(unsigned char addr);

/* FILE OPERATIONS */
static struct file_operations rtc_fops = {
    open: rtc_open,
    read: rtc_read
};

int init_module(void)
{
    printk("Loading module myrtc with major=%d\n", dev);

    /* reserve i/o ports */
    request_region(0x70, 2, "rtc_ports");

    /* Register the device driver */
    return register_chrdev(dev, DEV_NAME, &rtc_fops);
}

void cleanup_module(void)
{
    printk("Unloading module rtc\n");

    release_region(0x70, 2);

    /* Unregister the device driver */
    unregister_chrdev(dev, DEV_NAME);
}

static int rtc_open(struct inode *i_node, struct file *f)
{
    return 0;
}

static ssize_t rtc_read(struct file *f, char __user *buffer, size_t count, loff_t *fpos)
{
    unsigned char s;

    /* Get the current time from RTC */
    s = get_rtc_register(RTC_SECONDS);

    /* copy to user space */
    if (copy_to_user(buffer, &s, count))
        return -EFAULT;
    return 1;
}

static unsigned char get_rtc_register(unsigned char addr)
{
    unsigned char result;

	outb_p(0x80 | addr, RTC_CMDPORT);
	result = inb_p(RTC_DATAPORT);

    // result is in Binary Coded Decimal (BCD): d1d0, where each di, 0<=i<=1, 
    // has four bits with values (in decimal) in [0..9]. 
    // So 00010011 in BCD, is 13 in decimal = 1101 in binary
    // To do: return value in plain binary
    return result;
}

