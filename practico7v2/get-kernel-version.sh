#!/bin/bash

# kernel version
v="$(docker run kernel-dev uname -r)"
n="$(echo $v | sed 's/-.*//')"

# kernel version main line
m="$(echo $n | sed 's/\..*//')"

# You can use wget or curl as download command
wget https://cdn.kernel.org/pub/linux/kernel/v$m.x/linux-$n.tar.xz
