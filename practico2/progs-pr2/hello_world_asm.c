// compile with gcc -m32 -nostdlib -nostdinc -static -O2 hello_world_asm.c -o hello-asm

char *hello = "Hello world\n";
void _start()
{
	// write(1, hello, 12);
	// Linux trap: int 0x80
	// args: eax=4 (write syscall number)
	//       ebx=1 (file descriptor: standard output)
	//       ecx=buffer pointer (hello)
	//       edx=buffer size (12 bytes)
	asm volatile (
		"mov $4, %eax;"
		"mov $1, %ebx;"
		"mov hello, %ecx;"
		"mov $12, %edx;"
		"int $0x80"
	);

}
