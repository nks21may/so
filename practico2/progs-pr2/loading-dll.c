#include <stdio.h>
#include <dlfcn.h>

int main()
{
	void    *handle;
	int     (*f2)(int);
	int result;

	/* open the needed object */
	handle = dlopen("./mylib.so", RTLD_LOCAL | RTLD_LAZY);

	/* find the address of function and data objects */
	f2 = dlsym(handle, "f2");

	/* invoke function, passing value of integer as a parameter */
	result = f2(10);

	printf("f2(10) = %d\n", result);
	return 0;
}
