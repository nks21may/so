#ifndef SHM_H
#define SHM_H

struct shmblock {
	int key;
	int refcount;	// how many processes are using this block
	char *pa;		// physical address
};

struct shmproc {
	struct shmblock *block;
	uint va; // virtual address
};

#endif
