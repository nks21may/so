#include "types.h"
#include "defs.h"
#include "param.h"
#include "spinlock.h"
#include "mmu.h"
#include "proc.h"

struct sem {
  int refcounter;
  int value;
  int key;
};

struct {
	struct spinlock lock;
	struct sem sems[MAXNSEM];
} semtable;

void seminit(void){
  initlock(&semtable.lock, "semtable");
  int i;
  for(i=0; i < MAXNSEM; i++) semtable.sems[i].refcounter = 0;
}

int semget(int key, int init_value){
	struct proc* p = myproc();
	int slot, i;

	for(slot=0; slot < NSEM && p->psem[slot] != 0; slot++); //find free place
	if (slot == NSEM) return -1; //no free place

	acquire(&semtable.lock);

	for (i=0; i < MAXNSEM && key != semtable.sems[i].key; i++); //find existing key
	if (i < MAXNSEM){ //found existing key
		(semtable.sems[i]).refcounter++; 
	}else{
		for (i=0; i < MAXNSEM && semtable.sems[i].refcounter; i++); //find existing key
		if (i < MAXNSEM){
			semtable.sems[i].refcounter++;
			semtable.sems[i].key = key;
			semtable.sems[i].value = init_value;
		}else{
			slot = -1;
		}
	}
	release(&semtable.lock);

	if (slot != -1)	{
		p->psem[slot] = &semtable.sems[i];
	}
	
	return slot;
}

int semdown(int semid){

	struct proc* p = myproc();
	if (semid < 0 || semid >= NSEM || !p->psem[semid]){
		return -1;
	}
	acquire(&semtable.lock);
	struct sem *s=&semtable.sems[semid];
	for(;;){
		if (s->value > 0){
			 s->value--;
			 release(&semtable.lock);
			 break;
		}else
		  sleep(s,&semtable.lock);
	}
	return 0;

}

int semup(int semid){
	struct proc* p = myproc();
  	if (semid < 0 || semid >= NSEM || !p->psem[semid]){
  		return -1;
  	}
	acquire(&semtable.lock);
	struct sem * s= &semtable.sems[semid];	
	s->value++;
	release(&semtable.lock);
	wakeup(s);
	return 0;

}

int semclose(int semid){
  struct proc* p = myproc();
  if (semid < 0 || semid >= NSEM || !p->psem[semid]){
  	return -1;
  }
  
  acquire(&semtable.lock);
  if (--semtable.sems[semid].refcounter <= 0){
	semtable.sems[semid].key = 0;
  }
  release(&semtable.lock);
 
  return 0;
}