#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/file.h>
#include <fcntl.h>

#define N 10000

void error(const char* msg)
{
	write(2, msg, strlen(msg));
	write(2, "\n", 1);
	exit(-1);
}

int main(void)
{
	int fd = open("counter", O_RDWR),
		i;

	if (fd < 0) {
		error("File open error");
	}

	for (i=0; i<N; i++) {
		int v, n;
		char buffer[256];

		/* rewind, then read text file */
		lseek(fd, 0, SEEK_SET);
		n = read(fd, buffer, 255);
		if (n <= 0) {
			error("File read error");
		}
		buffer[n] = 0;
		v = atoi(buffer);
		sprintf(buffer, "%d", v+1);
		lseek(fd, 0, SEEK_SET);
		write(fd, buffer, strlen(buffer));
	}
	close(fd);
	return 0;
}
