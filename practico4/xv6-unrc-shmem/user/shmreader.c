#include "types.h"
#include "user.h"

int main(void)
{
	int* ptr = (int *)shmget(123456);

	printf(1,"reader: ptr=%p\n", ptr);
	printf(1,"reader: *ptr: %d %d %d\n", ptr[0], ptr[1], ptr[2]);
	exit();
}
