#include "types.h"
#include "user.h"
#include "fcntl.h"
#include "user.h"

int readnumber(int fd)
{
  int v = 0;

  fd = open("counter", O_RDONLY);
  read(fd, &v, sizeof(int));
  close(fd);
  return v;
}

void writenumber(int fd, int n)
{
  fd = open("counter", O_WRONLY);
  write(fd, &n, sizeof(int));
  close(fd);
}

int main(void)
{
  int fd, i, pid, v = 0;
  int s = semget(89765, 1); /* create semaphore initialized with 1 (mutex) */
  /* create file with integer value=0 */
  fd = open("counter", O_WRONLY | O_CREATE);
  write(fd, &v, sizeof(int));
  close(fd);
 
  pid = fork();
  printf(2, "%d forkeado", 2);
  for (i=0; i<1000; i++) {
    if(i%100 == 0) printf(2,"En el ciclo: %d \n", i);
    semdown(s);
    v = readnumber(fd);
    v++;
    writenumber(fd, v);
    semup(s); 
  }

  semclose(s);
  
  if (pid > 0) {
    /* parent process: wait for child exit */
    wait();
  }
  printf(2,"Termino \n");
  exit();
}
