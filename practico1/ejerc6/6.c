#include <stdio.h>
#include <unistd.h> 
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/wait.h> 

/*
La biblioteca estándar de C ofrece la función int system(const char* string) , la cual crea un
subproceso (child) generado por el comando pasado en su argumento. Definir system() en
términos de las llamadas al sistema fork() , exec() y wait() (ver páginas del manual).
*/

int main(int argc, char const *argv[]){
	int pid = fork(); /* create a copy of this process */
	pid_t cpid; 
	if (pid != 0) {
		int status; /* in parent process ... */
		cpid = wait(&status);
	} else {
		execv(argv[1], argv+1);
		exit(0);
	}
	printf("Padre %d \n", getpid());
	printf("Hijo %d \n", cpid);
	exit(0);
}