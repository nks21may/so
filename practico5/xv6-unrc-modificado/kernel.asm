
kernel:     formato del fichero elf32-i386


Desensamblado de la sección .text:

80100000 <multiboot_header>:
80100000:	02 b0 ad 1b 00 00    	add    0x1bad(%eax),%dh
80100006:	00 00                	add    %al,(%eax)
80100008:	fe 4f 52             	decb   0x52(%edi)
8010000b:	e4                   	.byte 0xe4

8010000c <entry>:

# Entering xv6 on boot processor, with paging off.
.globl entry
entry:
  # Turn on page size extension for 4Mbyte pages
  movl    %cr4, %eax
8010000c:	0f 20 e0             	mov    %cr4,%eax
  orl     $(CR4_PSE), %eax
8010000f:	83 c8 10             	or     $0x10,%eax
  movl    %eax, %cr4
80100012:	0f 22 e0             	mov    %eax,%cr4
  # Set page directory
  movl    $(V2P_WO(entrypgdir)), %eax
80100015:	b8 00 a0 10 00       	mov    $0x10a000,%eax
  movl    %eax, %cr3
8010001a:	0f 22 d8             	mov    %eax,%cr3
  # Turn on paging.
  movl    %cr0, %eax
8010001d:	0f 20 c0             	mov    %cr0,%eax
  orl     $(CR0_PG|CR0_WP), %eax
80100020:	0d 00 00 01 80       	or     $0x80010000,%eax
  movl    %eax, %cr0
80100025:	0f 22 c0             	mov    %eax,%cr0

  # Set up the stack pointer.
  movl $(stack + KSTACKSIZE), %esp
80100028:	bc 70 c6 10 80       	mov    $0x8010c670,%esp

  # Jump to main(), and switch to executing at
  # high addresses. The indirect call is needed because
  # the assembler produces a PC-relative instruction
  # for a direct jump.
  mov $main, %eax
8010002d:	b8 e0 2f 10 80       	mov    $0x80102fe0,%eax
  jmp *%eax
80100032:	ff e0                	jmp    *%eax
80100034:	66 90                	xchg   %ax,%ax
80100036:	66 90                	xchg   %ax,%ax
80100038:	66 90                	xchg   %ax,%ax
8010003a:	66 90                	xchg   %ax,%ax
8010003c:	66 90                	xchg   %ax,%ax
8010003e:	66 90                	xchg   %ax,%ax

80100040 <binit>:
  struct buf head;
} bcache;

void
binit(void)
{
80100040:	55                   	push   %ebp
80100041:	89 e5                	mov    %esp,%ebp
80100043:	53                   	push   %ebx

//PAGEBREAK!
  // Create linked list of buffers
  bcache.head.prev = &bcache.head;
  bcache.head.next = &bcache.head;
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100044:	bb b4 c6 10 80       	mov    $0x8010c6b4,%ebx
{
80100049:	83 ec 0c             	sub    $0xc,%esp
  initlock(&bcache.lock, "bcache");
8010004c:	68 80 78 10 80       	push   $0x80107880
80100051:	68 80 c6 10 80       	push   $0x8010c680
80100056:	e8 85 43 00 00       	call   801043e0 <initlock>
  bcache.head.next = &bcache.head;
8010005b:	83 c4 10             	add    $0x10,%esp
8010005e:	ba 7c 0d 11 80       	mov    $0x80110d7c,%edx
  bcache.head.prev = &bcache.head;
80100063:	c7 05 cc 0d 11 80 7c 	movl   $0x80110d7c,0x80110dcc
8010006a:	0d 11 80 
  bcache.head.next = &bcache.head;
8010006d:	c7 05 d0 0d 11 80 7c 	movl   $0x80110d7c,0x80110dd0
80100074:	0d 11 80 
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
80100077:	eb 09                	jmp    80100082 <binit+0x42>
80100079:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100080:	89 c3                	mov    %eax,%ebx
    b->next = bcache.head.next;
    b->prev = &bcache.head;
    initsleeplock(&b->lock, "buffer");
80100082:	83 ec 08             	sub    $0x8,%esp
80100085:	8d 43 0c             	lea    0xc(%ebx),%eax
    b->next = bcache.head.next;
80100088:	89 53 54             	mov    %edx,0x54(%ebx)
    b->prev = &bcache.head;
8010008b:	c7 43 50 7c 0d 11 80 	movl   $0x80110d7c,0x50(%ebx)
    initsleeplock(&b->lock, "buffer");
80100092:	68 87 78 10 80       	push   $0x80107887
80100097:	50                   	push   %eax
80100098:	e8 13 42 00 00       	call   801042b0 <initsleeplock>
    bcache.head.next->prev = b;
8010009d:	a1 d0 0d 11 80       	mov    0x80110dd0,%eax
801000a2:	89 da                	mov    %ebx,%edx
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000a4:	83 c4 10             	add    $0x10,%esp
    bcache.head.next->prev = b;
801000a7:	89 58 50             	mov    %ebx,0x50(%eax)
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000aa:	8d 83 5c 02 00 00    	lea    0x25c(%ebx),%eax
    bcache.head.next = b;
801000b0:	89 1d d0 0d 11 80    	mov    %ebx,0x80110dd0
  for(b = bcache.buf; b < bcache.buf+NBUF; b++){
801000b6:	3d 7c 0d 11 80       	cmp    $0x80110d7c,%eax
801000bb:	75 c3                	jne    80100080 <binit+0x40>
  }
}
801000bd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801000c0:	c9                   	leave  
801000c1:	c3                   	ret    
801000c2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801000c9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801000d0 <bread>:
}

// Return a locked buf with the contents of the indicated block.
struct buf*
bread(uint dev, uint blockno)
{
801000d0:	55                   	push   %ebp
801000d1:	89 e5                	mov    %esp,%ebp
801000d3:	57                   	push   %edi
801000d4:	56                   	push   %esi
801000d5:	53                   	push   %ebx
801000d6:	83 ec 18             	sub    $0x18,%esp
801000d9:	8b 7d 08             	mov    0x8(%ebp),%edi
801000dc:	8b 75 0c             	mov    0xc(%ebp),%esi
  acquire(&bcache.lock);
801000df:	68 80 c6 10 80       	push   $0x8010c680
801000e4:	e8 57 44 00 00       	call   80104540 <acquire>
  for(b = bcache.head.next; b != &bcache.head; b = b->next){
801000e9:	8b 1d d0 0d 11 80    	mov    0x80110dd0,%ebx
801000ef:	83 c4 10             	add    $0x10,%esp
801000f2:	81 fb 7c 0d 11 80    	cmp    $0x80110d7c,%ebx
801000f8:	75 11                	jne    8010010b <bread+0x3b>
801000fa:	eb 24                	jmp    80100120 <bread+0x50>
801000fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100100:	8b 5b 54             	mov    0x54(%ebx),%ebx
80100103:	81 fb 7c 0d 11 80    	cmp    $0x80110d7c,%ebx
80100109:	74 15                	je     80100120 <bread+0x50>
    if(b->dev == dev && b->blockno == blockno){
8010010b:	3b 7b 04             	cmp    0x4(%ebx),%edi
8010010e:	75 f0                	jne    80100100 <bread+0x30>
80100110:	3b 73 08             	cmp    0x8(%ebx),%esi
80100113:	75 eb                	jne    80100100 <bread+0x30>
      b->refcnt++;
80100115:	83 43 4c 01          	addl   $0x1,0x4c(%ebx)
      release(&bcache.lock);
80100119:	eb 3f                	jmp    8010015a <bread+0x8a>
8010011b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010011f:	90                   	nop
  for(b = bcache.head.prev; b != &bcache.head; b = b->prev){
80100120:	8b 1d cc 0d 11 80    	mov    0x80110dcc,%ebx
80100126:	81 fb 7c 0d 11 80    	cmp    $0x80110d7c,%ebx
8010012c:	75 0d                	jne    8010013b <bread+0x6b>
8010012e:	eb 70                	jmp    801001a0 <bread+0xd0>
80100130:	8b 5b 50             	mov    0x50(%ebx),%ebx
80100133:	81 fb 7c 0d 11 80    	cmp    $0x80110d7c,%ebx
80100139:	74 65                	je     801001a0 <bread+0xd0>
    if(b->refcnt == 0 && (b->flags & B_DIRTY) == 0) {
8010013b:	8b 43 4c             	mov    0x4c(%ebx),%eax
8010013e:	85 c0                	test   %eax,%eax
80100140:	75 ee                	jne    80100130 <bread+0x60>
80100142:	f6 03 04             	testb  $0x4,(%ebx)
80100145:	75 e9                	jne    80100130 <bread+0x60>
      b->dev = dev;
80100147:	89 7b 04             	mov    %edi,0x4(%ebx)
      b->blockno = blockno;
8010014a:	89 73 08             	mov    %esi,0x8(%ebx)
      b->flags = 0;
8010014d:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
      b->refcnt = 1;
80100153:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
      release(&bcache.lock);
8010015a:	83 ec 0c             	sub    $0xc,%esp
8010015d:	68 80 c6 10 80       	push   $0x8010c680
80100162:	e8 99 44 00 00       	call   80104600 <release>
      acquiresleep(&b->lock);
80100167:	8d 43 0c             	lea    0xc(%ebx),%eax
8010016a:	89 04 24             	mov    %eax,(%esp)
8010016d:	e8 7e 41 00 00       	call   801042f0 <acquiresleep>
80100172:	83 c4 10             	add    $0x10,%esp
  struct buf *b;

  b = bget(dev, blockno);
  if((b->flags & B_VALID) == 0) {
80100175:	f6 03 02             	testb  $0x2,(%ebx)
80100178:	74 0e                	je     80100188 <bread+0xb8>
    iderw(b);
  }
  return b;
}
8010017a:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010017d:	89 d8                	mov    %ebx,%eax
8010017f:	5b                   	pop    %ebx
80100180:	5e                   	pop    %esi
80100181:	5f                   	pop    %edi
80100182:	5d                   	pop    %ebp
80100183:	c3                   	ret    
80100184:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    iderw(b);
80100188:	83 ec 0c             	sub    $0xc,%esp
8010018b:	53                   	push   %ebx
8010018c:	e8 9f 20 00 00       	call   80102230 <iderw>
80100191:	83 c4 10             	add    $0x10,%esp
}
80100194:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100197:	89 d8                	mov    %ebx,%eax
80100199:	5b                   	pop    %ebx
8010019a:	5e                   	pop    %esi
8010019b:	5f                   	pop    %edi
8010019c:	5d                   	pop    %ebp
8010019d:	c3                   	ret    
8010019e:	66 90                	xchg   %ax,%ax
  panic("bget: no buffers");
801001a0:	83 ec 0c             	sub    $0xc,%esp
801001a3:	68 8e 78 10 80       	push   $0x8010788e
801001a8:	e8 e3 01 00 00       	call   80100390 <panic>
801001ad:	8d 76 00             	lea    0x0(%esi),%esi

801001b0 <bwrite>:

// Write b's contents to disk.  Must be locked.
void
bwrite(struct buf *b)
{
801001b0:	55                   	push   %ebp
801001b1:	89 e5                	mov    %esp,%ebp
801001b3:	53                   	push   %ebx
801001b4:	83 ec 10             	sub    $0x10,%esp
801001b7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001ba:	8d 43 0c             	lea    0xc(%ebx),%eax
801001bd:	50                   	push   %eax
801001be:	e8 cd 41 00 00       	call   80104390 <holdingsleep>
801001c3:	83 c4 10             	add    $0x10,%esp
801001c6:	85 c0                	test   %eax,%eax
801001c8:	74 0f                	je     801001d9 <bwrite+0x29>
    panic("bwrite");
  b->flags |= B_DIRTY;
801001ca:	83 0b 04             	orl    $0x4,(%ebx)
  iderw(b);
801001cd:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
801001d0:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801001d3:	c9                   	leave  
  iderw(b);
801001d4:	e9 57 20 00 00       	jmp    80102230 <iderw>
    panic("bwrite");
801001d9:	83 ec 0c             	sub    $0xc,%esp
801001dc:	68 9f 78 10 80       	push   $0x8010789f
801001e1:	e8 aa 01 00 00       	call   80100390 <panic>
801001e6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801001ed:	8d 76 00             	lea    0x0(%esi),%esi

801001f0 <brelse>:

// Release a locked buffer.
// Move to the head of the MRU list.
void
brelse(struct buf *b)
{
801001f0:	55                   	push   %ebp
801001f1:	89 e5                	mov    %esp,%ebp
801001f3:	56                   	push   %esi
801001f4:	53                   	push   %ebx
801001f5:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holdingsleep(&b->lock))
801001f8:	8d 73 0c             	lea    0xc(%ebx),%esi
801001fb:	83 ec 0c             	sub    $0xc,%esp
801001fe:	56                   	push   %esi
801001ff:	e8 8c 41 00 00       	call   80104390 <holdingsleep>
80100204:	83 c4 10             	add    $0x10,%esp
80100207:	85 c0                	test   %eax,%eax
80100209:	74 66                	je     80100271 <brelse+0x81>
    panic("brelse");

  releasesleep(&b->lock);
8010020b:	83 ec 0c             	sub    $0xc,%esp
8010020e:	56                   	push   %esi
8010020f:	e8 3c 41 00 00       	call   80104350 <releasesleep>

  acquire(&bcache.lock);
80100214:	c7 04 24 80 c6 10 80 	movl   $0x8010c680,(%esp)
8010021b:	e8 20 43 00 00       	call   80104540 <acquire>
  b->refcnt--;
80100220:	8b 43 4c             	mov    0x4c(%ebx),%eax
  if (b->refcnt == 0) {
80100223:	83 c4 10             	add    $0x10,%esp
  b->refcnt--;
80100226:	83 e8 01             	sub    $0x1,%eax
80100229:	89 43 4c             	mov    %eax,0x4c(%ebx)
  if (b->refcnt == 0) {
8010022c:	85 c0                	test   %eax,%eax
8010022e:	75 2f                	jne    8010025f <brelse+0x6f>
    // no one is waiting for it.
    b->next->prev = b->prev;
80100230:	8b 43 54             	mov    0x54(%ebx),%eax
80100233:	8b 53 50             	mov    0x50(%ebx),%edx
80100236:	89 50 50             	mov    %edx,0x50(%eax)
    b->prev->next = b->next;
80100239:	8b 43 50             	mov    0x50(%ebx),%eax
8010023c:	8b 53 54             	mov    0x54(%ebx),%edx
8010023f:	89 50 54             	mov    %edx,0x54(%eax)
    b->next = bcache.head.next;
80100242:	a1 d0 0d 11 80       	mov    0x80110dd0,%eax
    b->prev = &bcache.head;
80100247:	c7 43 50 7c 0d 11 80 	movl   $0x80110d7c,0x50(%ebx)
    b->next = bcache.head.next;
8010024e:	89 43 54             	mov    %eax,0x54(%ebx)
    bcache.head.next->prev = b;
80100251:	a1 d0 0d 11 80       	mov    0x80110dd0,%eax
80100256:	89 58 50             	mov    %ebx,0x50(%eax)
    bcache.head.next = b;
80100259:	89 1d d0 0d 11 80    	mov    %ebx,0x80110dd0
  }
  
  release(&bcache.lock);
8010025f:	c7 45 08 80 c6 10 80 	movl   $0x8010c680,0x8(%ebp)
}
80100266:	8d 65 f8             	lea    -0x8(%ebp),%esp
80100269:	5b                   	pop    %ebx
8010026a:	5e                   	pop    %esi
8010026b:	5d                   	pop    %ebp
  release(&bcache.lock);
8010026c:	e9 8f 43 00 00       	jmp    80104600 <release>
    panic("brelse");
80100271:	83 ec 0c             	sub    $0xc,%esp
80100274:	68 a6 78 10 80       	push   $0x801078a6
80100279:	e8 12 01 00 00       	call   80100390 <panic>
8010027e:	66 90                	xchg   %ax,%ax

80100280 <consoleread>:
  }
}

int
consoleread(struct inode *ip, char *dst, int n)
{
80100280:	55                   	push   %ebp
80100281:	89 e5                	mov    %esp,%ebp
80100283:	57                   	push   %edi
80100284:	56                   	push   %esi
80100285:	53                   	push   %ebx
80100286:	83 ec 28             	sub    $0x28,%esp
  uint target;
  int c;

  iunlock(ip);
80100289:	ff 75 08             	pushl  0x8(%ebp)
{
8010028c:	8b 75 10             	mov    0x10(%ebp),%esi
  iunlock(ip);
8010028f:	e8 9c 15 00 00       	call   80101830 <iunlock>
  target = n;
  acquire(&cons.lock);
80100294:	c7 04 24 20 b5 10 80 	movl   $0x8010b520,(%esp)
8010029b:	e8 a0 42 00 00       	call   80104540 <acquire>
        // caller gets a 0-byte result.
        input.r--;
      }
      break;
    }
    *dst++ = c;
801002a0:	8b 7d 0c             	mov    0xc(%ebp),%edi
  while(n > 0){
801002a3:	83 c4 10             	add    $0x10,%esp
801002a6:	31 c0                	xor    %eax,%eax
    *dst++ = c;
801002a8:	01 f7                	add    %esi,%edi
  while(n > 0){
801002aa:	85 f6                	test   %esi,%esi
801002ac:	0f 8e a0 00 00 00    	jle    80100352 <consoleread+0xd2>
801002b2:	89 f3                	mov    %esi,%ebx
    while(input.r == input.w){
801002b4:	8b 15 60 10 11 80    	mov    0x80111060,%edx
801002ba:	39 15 64 10 11 80    	cmp    %edx,0x80111064
801002c0:	74 29                	je     801002eb <consoleread+0x6b>
801002c2:	eb 5c                	jmp    80100320 <consoleread+0xa0>
801002c4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      sleep(&input.r, &cons.lock);
801002c8:	83 ec 08             	sub    $0x8,%esp
801002cb:	68 20 b5 10 80       	push   $0x8010b520
801002d0:	68 60 10 11 80       	push   $0x80111060
801002d5:	e8 56 3c 00 00       	call   80103f30 <sleep>
    while(input.r == input.w){
801002da:	8b 15 60 10 11 80    	mov    0x80111060,%edx
801002e0:	83 c4 10             	add    $0x10,%esp
801002e3:	3b 15 64 10 11 80    	cmp    0x80111064,%edx
801002e9:	75 35                	jne    80100320 <consoleread+0xa0>
      if(myproc()->killed){
801002eb:	e8 30 36 00 00       	call   80103920 <myproc>
801002f0:	8b 48 24             	mov    0x24(%eax),%ecx
801002f3:	85 c9                	test   %ecx,%ecx
801002f5:	74 d1                	je     801002c8 <consoleread+0x48>
        release(&cons.lock);
801002f7:	83 ec 0c             	sub    $0xc,%esp
801002fa:	68 20 b5 10 80       	push   $0x8010b520
801002ff:	e8 fc 42 00 00       	call   80104600 <release>
        ilock(ip);
80100304:	5a                   	pop    %edx
80100305:	ff 75 08             	pushl  0x8(%ebp)
80100308:	e8 43 14 00 00       	call   80101750 <ilock>
        return -1;
8010030d:	83 c4 10             	add    $0x10,%esp
  }
  release(&cons.lock);
  ilock(ip);

  return target - n;
}
80100310:	8d 65 f4             	lea    -0xc(%ebp),%esp
        return -1;
80100313:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100318:	5b                   	pop    %ebx
80100319:	5e                   	pop    %esi
8010031a:	5f                   	pop    %edi
8010031b:	5d                   	pop    %ebp
8010031c:	c3                   	ret    
8010031d:	8d 76 00             	lea    0x0(%esi),%esi
    c = input.buf[input.r++ % INPUT_BUF];
80100320:	8d 42 01             	lea    0x1(%edx),%eax
80100323:	a3 60 10 11 80       	mov    %eax,0x80111060
80100328:	89 d0                	mov    %edx,%eax
8010032a:	83 e0 7f             	and    $0x7f,%eax
8010032d:	0f be 80 e0 0f 11 80 	movsbl -0x7feef020(%eax),%eax
    if(c == C('D')){  // EOF
80100334:	83 f8 04             	cmp    $0x4,%eax
80100337:	74 46                	je     8010037f <consoleread+0xff>
    *dst++ = c;
80100339:	89 da                	mov    %ebx,%edx
    --n;
8010033b:	83 eb 01             	sub    $0x1,%ebx
    *dst++ = c;
8010033e:	f7 da                	neg    %edx
80100340:	88 04 17             	mov    %al,(%edi,%edx,1)
    if(c == '\n')
80100343:	83 f8 0a             	cmp    $0xa,%eax
80100346:	74 31                	je     80100379 <consoleread+0xf9>
  while(n > 0){
80100348:	85 db                	test   %ebx,%ebx
8010034a:	0f 85 64 ff ff ff    	jne    801002b4 <consoleread+0x34>
80100350:	89 f0                	mov    %esi,%eax
  release(&cons.lock);
80100352:	83 ec 0c             	sub    $0xc,%esp
80100355:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80100358:	68 20 b5 10 80       	push   $0x8010b520
8010035d:	e8 9e 42 00 00       	call   80104600 <release>
  ilock(ip);
80100362:	58                   	pop    %eax
80100363:	ff 75 08             	pushl  0x8(%ebp)
80100366:	e8 e5 13 00 00       	call   80101750 <ilock>
  return target - n;
8010036b:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010036e:	83 c4 10             	add    $0x10,%esp
}
80100371:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100374:	5b                   	pop    %ebx
80100375:	5e                   	pop    %esi
80100376:	5f                   	pop    %edi
80100377:	5d                   	pop    %ebp
80100378:	c3                   	ret    
80100379:	89 f0                	mov    %esi,%eax
8010037b:	29 d8                	sub    %ebx,%eax
8010037d:	eb d3                	jmp    80100352 <consoleread+0xd2>
      if(n < target){
8010037f:	89 f0                	mov    %esi,%eax
80100381:	29 d8                	sub    %ebx,%eax
80100383:	39 f3                	cmp    %esi,%ebx
80100385:	73 cb                	jae    80100352 <consoleread+0xd2>
        input.r--;
80100387:	89 15 60 10 11 80    	mov    %edx,0x80111060
8010038d:	eb c3                	jmp    80100352 <consoleread+0xd2>
8010038f:	90                   	nop

80100390 <panic>:
{
80100390:	55                   	push   %ebp
80100391:	89 e5                	mov    %esp,%ebp
80100393:	56                   	push   %esi
80100394:	53                   	push   %ebx
80100395:	83 ec 30             	sub    $0x30,%esp
}

static inline void
cli(void)
{
  asm volatile("cli");
80100398:	fa                   	cli    
  cons.locking = 0;
80100399:	c7 05 54 b5 10 80 00 	movl   $0x0,0x8010b554
801003a0:	00 00 00 
  getcallerpcs(&s, pcs);
801003a3:	8d 5d d0             	lea    -0x30(%ebp),%ebx
801003a6:	8d 75 f8             	lea    -0x8(%ebp),%esi
  cprintf("lapicid %d: panic: ", lapicid());
801003a9:	e8 b2 24 00 00       	call   80102860 <lapicid>
801003ae:	83 ec 08             	sub    $0x8,%esp
801003b1:	50                   	push   %eax
801003b2:	68 ad 78 10 80       	push   $0x801078ad
801003b7:	e8 f4 02 00 00       	call   801006b0 <cprintf>
  cprintf(s);
801003bc:	58                   	pop    %eax
801003bd:	ff 75 08             	pushl  0x8(%ebp)
801003c0:	e8 eb 02 00 00       	call   801006b0 <cprintf>
  cprintf("\n");
801003c5:	c7 04 24 6d 82 10 80 	movl   $0x8010826d,(%esp)
801003cc:	e8 df 02 00 00       	call   801006b0 <cprintf>
  getcallerpcs(&s, pcs);
801003d1:	8d 45 08             	lea    0x8(%ebp),%eax
801003d4:	5a                   	pop    %edx
801003d5:	59                   	pop    %ecx
801003d6:	53                   	push   %ebx
801003d7:	50                   	push   %eax
801003d8:	e8 23 40 00 00       	call   80104400 <getcallerpcs>
  for(i=0; i<10; i++)
801003dd:	83 c4 10             	add    $0x10,%esp
    cprintf(" %p", pcs[i]);
801003e0:	83 ec 08             	sub    $0x8,%esp
801003e3:	ff 33                	pushl  (%ebx)
801003e5:	83 c3 04             	add    $0x4,%ebx
801003e8:	68 c1 78 10 80       	push   $0x801078c1
801003ed:	e8 be 02 00 00       	call   801006b0 <cprintf>
  for(i=0; i<10; i++)
801003f2:	83 c4 10             	add    $0x10,%esp
801003f5:	39 f3                	cmp    %esi,%ebx
801003f7:	75 e7                	jne    801003e0 <panic+0x50>
  panicked = 1; // freeze other CPU
801003f9:	c7 05 58 b5 10 80 01 	movl   $0x1,0x8010b558
80100400:	00 00 00 
    ;
80100403:	eb fe                	jmp    80100403 <panic+0x73>
80100405:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010040c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80100410 <consputc.part.0>:
consputc(int c)
80100410:	55                   	push   %ebp
80100411:	89 e5                	mov    %esp,%ebp
80100413:	57                   	push   %edi
80100414:	56                   	push   %esi
80100415:	53                   	push   %ebx
80100416:	89 c3                	mov    %eax,%ebx
80100418:	83 ec 1c             	sub    $0x1c,%esp
  if(c == BACKSPACE){
8010041b:	3d 00 01 00 00       	cmp    $0x100,%eax
80100420:	0f 84 ea 00 00 00    	je     80100510 <consputc.part.0+0x100>
    uartputc(c);
80100426:	83 ec 0c             	sub    $0xc,%esp
80100429:	50                   	push   %eax
8010042a:	e8 81 5a 00 00       	call   80105eb0 <uartputc>
8010042f:	83 c4 10             	add    $0x10,%esp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80100432:	bf d4 03 00 00       	mov    $0x3d4,%edi
80100437:	b8 0e 00 00 00       	mov    $0xe,%eax
8010043c:	89 fa                	mov    %edi,%edx
8010043e:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010043f:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
80100444:	89 ca                	mov    %ecx,%edx
80100446:	ec                   	in     (%dx),%al
  pos = inb(CRTPORT+1) << 8;
80100447:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010044a:	89 fa                	mov    %edi,%edx
8010044c:	c1 e0 08             	shl    $0x8,%eax
8010044f:	89 c6                	mov    %eax,%esi
80100451:	b8 0f 00 00 00       	mov    $0xf,%eax
80100456:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80100457:	89 ca                	mov    %ecx,%edx
80100459:	ec                   	in     (%dx),%al
  pos |= inb(CRTPORT+1);
8010045a:	0f b6 c0             	movzbl %al,%eax
8010045d:	09 f0                	or     %esi,%eax
  if(c == '\n')
8010045f:	83 fb 0a             	cmp    $0xa,%ebx
80100462:	0f 84 90 00 00 00    	je     801004f8 <consputc.part.0+0xe8>
  else if(c == BACKSPACE){
80100468:	81 fb 00 01 00 00    	cmp    $0x100,%ebx
8010046e:	74 70                	je     801004e0 <consputc.part.0+0xd0>
    crt[pos++] = (c&0xff) | 0x0700;  // black on white
80100470:	0f b6 db             	movzbl %bl,%ebx
80100473:	8d 70 01             	lea    0x1(%eax),%esi
80100476:	80 cf 07             	or     $0x7,%bh
80100479:	66 89 9c 00 00 80 0b 	mov    %bx,-0x7ff48000(%eax,%eax,1)
80100480:	80 
  if(pos < 0 || pos > 25*80)
80100481:	81 fe d0 07 00 00    	cmp    $0x7d0,%esi
80100487:	0f 8f f9 00 00 00    	jg     80100586 <consputc.part.0+0x176>
  if((pos/80) >= 24){  // Scroll up.
8010048d:	81 fe 7f 07 00 00    	cmp    $0x77f,%esi
80100493:	0f 8f a7 00 00 00    	jg     80100540 <consputc.part.0+0x130>
80100499:	89 f0                	mov    %esi,%eax
8010049b:	8d b4 36 00 80 0b 80 	lea    -0x7ff48000(%esi,%esi,1),%esi
801004a2:	88 45 e7             	mov    %al,-0x19(%ebp)
801004a5:	0f b6 fc             	movzbl %ah,%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801004a8:	bb d4 03 00 00       	mov    $0x3d4,%ebx
801004ad:	b8 0e 00 00 00       	mov    $0xe,%eax
801004b2:	89 da                	mov    %ebx,%edx
801004b4:	ee                   	out    %al,(%dx)
801004b5:	b9 d5 03 00 00       	mov    $0x3d5,%ecx
801004ba:	89 f8                	mov    %edi,%eax
801004bc:	89 ca                	mov    %ecx,%edx
801004be:	ee                   	out    %al,(%dx)
801004bf:	b8 0f 00 00 00       	mov    $0xf,%eax
801004c4:	89 da                	mov    %ebx,%edx
801004c6:	ee                   	out    %al,(%dx)
801004c7:	0f b6 45 e7          	movzbl -0x19(%ebp),%eax
801004cb:	89 ca                	mov    %ecx,%edx
801004cd:	ee                   	out    %al,(%dx)
  crt[pos] = ' ' | 0x0700;
801004ce:	b8 20 07 00 00       	mov    $0x720,%eax
801004d3:	66 89 06             	mov    %ax,(%esi)
}
801004d6:	8d 65 f4             	lea    -0xc(%ebp),%esp
801004d9:	5b                   	pop    %ebx
801004da:	5e                   	pop    %esi
801004db:	5f                   	pop    %edi
801004dc:	5d                   	pop    %ebp
801004dd:	c3                   	ret    
801004de:	66 90                	xchg   %ax,%ax
    if(pos > 0) --pos;
801004e0:	8d 70 ff             	lea    -0x1(%eax),%esi
801004e3:	85 c0                	test   %eax,%eax
801004e5:	75 9a                	jne    80100481 <consputc.part.0+0x71>
801004e7:	be 00 80 0b 80       	mov    $0x800b8000,%esi
801004ec:	c6 45 e7 00          	movb   $0x0,-0x19(%ebp)
801004f0:	31 ff                	xor    %edi,%edi
801004f2:	eb b4                	jmp    801004a8 <consputc.part.0+0x98>
801004f4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    pos += 80 - pos%80;
801004f8:	ba cd cc cc cc       	mov    $0xcccccccd,%edx
801004fd:	f7 e2                	mul    %edx
801004ff:	c1 ea 06             	shr    $0x6,%edx
80100502:	8d 04 92             	lea    (%edx,%edx,4),%eax
80100505:	c1 e0 04             	shl    $0x4,%eax
80100508:	8d 70 50             	lea    0x50(%eax),%esi
8010050b:	e9 71 ff ff ff       	jmp    80100481 <consputc.part.0+0x71>
    uartputc('\b'); uartputc(' '); uartputc('\b');
80100510:	83 ec 0c             	sub    $0xc,%esp
80100513:	6a 08                	push   $0x8
80100515:	e8 96 59 00 00       	call   80105eb0 <uartputc>
8010051a:	c7 04 24 20 00 00 00 	movl   $0x20,(%esp)
80100521:	e8 8a 59 00 00       	call   80105eb0 <uartputc>
80100526:	c7 04 24 08 00 00 00 	movl   $0x8,(%esp)
8010052d:	e8 7e 59 00 00       	call   80105eb0 <uartputc>
80100532:	83 c4 10             	add    $0x10,%esp
80100535:	e9 f8 fe ff ff       	jmp    80100432 <consputc.part.0+0x22>
8010053a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
80100540:	83 ec 04             	sub    $0x4,%esp
    pos -= 80;
80100543:	8d 5e b0             	lea    -0x50(%esi),%ebx
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100546:	bf 07 00 00 00       	mov    $0x7,%edi
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
8010054b:	68 60 0e 00 00       	push   $0xe60
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100550:	8d b4 1b 00 80 0b 80 	lea    -0x7ff48000(%ebx,%ebx,1),%esi
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
80100557:	68 a0 80 0b 80       	push   $0x800b80a0
8010055c:	68 00 80 0b 80       	push   $0x800b8000
80100561:	e8 8a 41 00 00       	call   801046f0 <memmove>
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
80100566:	b8 80 07 00 00       	mov    $0x780,%eax
8010056b:	83 c4 0c             	add    $0xc,%esp
8010056e:	29 d8                	sub    %ebx,%eax
80100570:	01 c0                	add    %eax,%eax
80100572:	50                   	push   %eax
80100573:	6a 00                	push   $0x0
80100575:	56                   	push   %esi
80100576:	e8 d5 40 00 00       	call   80104650 <memset>
8010057b:	88 5d e7             	mov    %bl,-0x19(%ebp)
8010057e:	83 c4 10             	add    $0x10,%esp
80100581:	e9 22 ff ff ff       	jmp    801004a8 <consputc.part.0+0x98>
    panic("pos under/overflow");
80100586:	83 ec 0c             	sub    $0xc,%esp
80100589:	68 c5 78 10 80       	push   $0x801078c5
8010058e:	e8 fd fd ff ff       	call   80100390 <panic>
80100593:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010059a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801005a0 <printint>:
{
801005a0:	55                   	push   %ebp
801005a1:	89 e5                	mov    %esp,%ebp
801005a3:	57                   	push   %edi
801005a4:	56                   	push   %esi
801005a5:	53                   	push   %ebx
801005a6:	83 ec 2c             	sub    $0x2c,%esp
801005a9:	89 55 d4             	mov    %edx,-0x2c(%ebp)
  if(sign && (sign = xx < 0))
801005ac:	85 c9                	test   %ecx,%ecx
801005ae:	74 04                	je     801005b4 <printint+0x14>
801005b0:	85 c0                	test   %eax,%eax
801005b2:	78 68                	js     8010061c <printint+0x7c>
    x = xx;
801005b4:	89 c1                	mov    %eax,%ecx
801005b6:	31 f6                	xor    %esi,%esi
  i = 0;
801005b8:	31 db                	xor    %ebx,%ebx
801005ba:	eb 04                	jmp    801005c0 <printint+0x20>
  }while((x /= base) != 0);
801005bc:	89 c1                	mov    %eax,%ecx
    buf[i++] = digits[x % base];
801005be:	89 fb                	mov    %edi,%ebx
801005c0:	89 c8                	mov    %ecx,%eax
801005c2:	31 d2                	xor    %edx,%edx
801005c4:	8d 7b 01             	lea    0x1(%ebx),%edi
801005c7:	f7 75 d4             	divl   -0x2c(%ebp)
801005ca:	0f b6 92 f0 78 10 80 	movzbl -0x7fef8710(%edx),%edx
801005d1:	88 54 3d d7          	mov    %dl,-0x29(%ebp,%edi,1)
  }while((x /= base) != 0);
801005d5:	39 4d d4             	cmp    %ecx,-0x2c(%ebp)
801005d8:	76 e2                	jbe    801005bc <printint+0x1c>
  if(sign)
801005da:	85 f6                	test   %esi,%esi
801005dc:	75 32                	jne    80100610 <printint+0x70>
801005de:	0f be c2             	movsbl %dl,%eax
801005e1:	89 df                	mov    %ebx,%edi
  if(panicked){
801005e3:	8b 0d 58 b5 10 80    	mov    0x8010b558,%ecx
801005e9:	85 c9                	test   %ecx,%ecx
801005eb:	75 20                	jne    8010060d <printint+0x6d>
801005ed:	8d 5c 3d d7          	lea    -0x29(%ebp,%edi,1),%ebx
801005f1:	e8 1a fe ff ff       	call   80100410 <consputc.part.0>
  while(--i >= 0)
801005f6:	8d 45 d7             	lea    -0x29(%ebp),%eax
801005f9:	39 d8                	cmp    %ebx,%eax
801005fb:	74 27                	je     80100624 <printint+0x84>
  if(panicked){
801005fd:	8b 15 58 b5 10 80    	mov    0x8010b558,%edx
    consputc(buf[i]);
80100603:	0f be 03             	movsbl (%ebx),%eax
  if(panicked){
80100606:	83 eb 01             	sub    $0x1,%ebx
80100609:	85 d2                	test   %edx,%edx
8010060b:	74 e4                	je     801005f1 <printint+0x51>
  asm volatile("cli");
8010060d:	fa                   	cli    
      ;
8010060e:	eb fe                	jmp    8010060e <printint+0x6e>
    buf[i++] = '-';
80100610:	c6 44 3d d8 2d       	movb   $0x2d,-0x28(%ebp,%edi,1)
80100615:	b8 2d 00 00 00       	mov    $0x2d,%eax
8010061a:	eb c7                	jmp    801005e3 <printint+0x43>
    x = -xx;
8010061c:	f7 d8                	neg    %eax
8010061e:	89 ce                	mov    %ecx,%esi
80100620:	89 c1                	mov    %eax,%ecx
80100622:	eb 94                	jmp    801005b8 <printint+0x18>
}
80100624:	83 c4 2c             	add    $0x2c,%esp
80100627:	5b                   	pop    %ebx
80100628:	5e                   	pop    %esi
80100629:	5f                   	pop    %edi
8010062a:	5d                   	pop    %ebp
8010062b:	c3                   	ret    
8010062c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80100630 <consolewrite>:

int
consolewrite(struct inode *ip, char *buf, int n)
{
80100630:	55                   	push   %ebp
80100631:	89 e5                	mov    %esp,%ebp
80100633:	57                   	push   %edi
80100634:	56                   	push   %esi
80100635:	53                   	push   %ebx
80100636:	83 ec 18             	sub    $0x18,%esp
80100639:	8b 7d 10             	mov    0x10(%ebp),%edi
8010063c:	8b 5d 0c             	mov    0xc(%ebp),%ebx
  int i;

  iunlock(ip);
8010063f:	ff 75 08             	pushl  0x8(%ebp)
80100642:	e8 e9 11 00 00       	call   80101830 <iunlock>
  acquire(&cons.lock);
80100647:	c7 04 24 20 b5 10 80 	movl   $0x8010b520,(%esp)
8010064e:	e8 ed 3e 00 00       	call   80104540 <acquire>
  for(i = 0; i < n; i++)
80100653:	83 c4 10             	add    $0x10,%esp
80100656:	85 ff                	test   %edi,%edi
80100658:	7e 36                	jle    80100690 <consolewrite+0x60>
  if(panicked){
8010065a:	8b 0d 58 b5 10 80    	mov    0x8010b558,%ecx
80100660:	85 c9                	test   %ecx,%ecx
80100662:	75 21                	jne    80100685 <consolewrite+0x55>
    consputc(buf[i] & 0xff);
80100664:	0f b6 03             	movzbl (%ebx),%eax
80100667:	8d 73 01             	lea    0x1(%ebx),%esi
8010066a:	01 fb                	add    %edi,%ebx
8010066c:	e8 9f fd ff ff       	call   80100410 <consputc.part.0>
  for(i = 0; i < n; i++)
80100671:	39 de                	cmp    %ebx,%esi
80100673:	74 1b                	je     80100690 <consolewrite+0x60>
  if(panicked){
80100675:	8b 15 58 b5 10 80    	mov    0x8010b558,%edx
    consputc(buf[i] & 0xff);
8010067b:	0f b6 06             	movzbl (%esi),%eax
  if(panicked){
8010067e:	83 c6 01             	add    $0x1,%esi
80100681:	85 d2                	test   %edx,%edx
80100683:	74 e7                	je     8010066c <consolewrite+0x3c>
80100685:	fa                   	cli    
      ;
80100686:	eb fe                	jmp    80100686 <consolewrite+0x56>
80100688:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010068f:	90                   	nop
  release(&cons.lock);
80100690:	83 ec 0c             	sub    $0xc,%esp
80100693:	68 20 b5 10 80       	push   $0x8010b520
80100698:	e8 63 3f 00 00       	call   80104600 <release>
  ilock(ip);
8010069d:	58                   	pop    %eax
8010069e:	ff 75 08             	pushl  0x8(%ebp)
801006a1:	e8 aa 10 00 00       	call   80101750 <ilock>

  return n;
}
801006a6:	8d 65 f4             	lea    -0xc(%ebp),%esp
801006a9:	89 f8                	mov    %edi,%eax
801006ab:	5b                   	pop    %ebx
801006ac:	5e                   	pop    %esi
801006ad:	5f                   	pop    %edi
801006ae:	5d                   	pop    %ebp
801006af:	c3                   	ret    

801006b0 <cprintf>:
{
801006b0:	55                   	push   %ebp
801006b1:	89 e5                	mov    %esp,%ebp
801006b3:	57                   	push   %edi
801006b4:	56                   	push   %esi
801006b5:	53                   	push   %ebx
801006b6:	83 ec 1c             	sub    $0x1c,%esp
  locking = cons.locking;
801006b9:	a1 54 b5 10 80       	mov    0x8010b554,%eax
801006be:	89 45 e0             	mov    %eax,-0x20(%ebp)
  if(locking)
801006c1:	85 c0                	test   %eax,%eax
801006c3:	0f 85 df 00 00 00    	jne    801007a8 <cprintf+0xf8>
  if (fmt == 0)
801006c9:	8b 45 08             	mov    0x8(%ebp),%eax
801006cc:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801006cf:	85 c0                	test   %eax,%eax
801006d1:	0f 84 5e 01 00 00    	je     80100835 <cprintf+0x185>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801006d7:	0f b6 00             	movzbl (%eax),%eax
801006da:	85 c0                	test   %eax,%eax
801006dc:	74 32                	je     80100710 <cprintf+0x60>
  argp = (uint*)(void*)(&fmt + 1);
801006de:	8d 5d 0c             	lea    0xc(%ebp),%ebx
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
801006e1:	31 f6                	xor    %esi,%esi
    if(c != '%'){
801006e3:	83 f8 25             	cmp    $0x25,%eax
801006e6:	74 40                	je     80100728 <cprintf+0x78>
  if(panicked){
801006e8:	8b 0d 58 b5 10 80    	mov    0x8010b558,%ecx
801006ee:	85 c9                	test   %ecx,%ecx
801006f0:	74 0b                	je     801006fd <cprintf+0x4d>
801006f2:	fa                   	cli    
      ;
801006f3:	eb fe                	jmp    801006f3 <cprintf+0x43>
801006f5:	8d 76 00             	lea    0x0(%esi),%esi
801006f8:	b8 25 00 00 00       	mov    $0x25,%eax
801006fd:	e8 0e fd ff ff       	call   80100410 <consputc.part.0>
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
80100702:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80100705:	83 c6 01             	add    $0x1,%esi
80100708:	0f b6 04 30          	movzbl (%eax,%esi,1),%eax
8010070c:	85 c0                	test   %eax,%eax
8010070e:	75 d3                	jne    801006e3 <cprintf+0x33>
  if(locking)
80100710:	8b 5d e0             	mov    -0x20(%ebp),%ebx
80100713:	85 db                	test   %ebx,%ebx
80100715:	0f 85 05 01 00 00    	jne    80100820 <cprintf+0x170>
}
8010071b:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010071e:	5b                   	pop    %ebx
8010071f:	5e                   	pop    %esi
80100720:	5f                   	pop    %edi
80100721:	5d                   	pop    %ebp
80100722:	c3                   	ret    
80100723:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100727:	90                   	nop
    c = fmt[++i] & 0xff;
80100728:	8b 45 e4             	mov    -0x1c(%ebp),%eax
8010072b:	83 c6 01             	add    $0x1,%esi
8010072e:	0f b6 3c 30          	movzbl (%eax,%esi,1),%edi
    if(c == 0)
80100732:	85 ff                	test   %edi,%edi
80100734:	74 da                	je     80100710 <cprintf+0x60>
    switch(c){
80100736:	83 ff 70             	cmp    $0x70,%edi
80100739:	0f 84 7e 00 00 00    	je     801007bd <cprintf+0x10d>
8010073f:	7f 26                	jg     80100767 <cprintf+0xb7>
80100741:	83 ff 25             	cmp    $0x25,%edi
80100744:	0f 84 be 00 00 00    	je     80100808 <cprintf+0x158>
8010074a:	83 ff 64             	cmp    $0x64,%edi
8010074d:	75 46                	jne    80100795 <cprintf+0xe5>
      printint(*argp++, 10, 1);
8010074f:	8b 03                	mov    (%ebx),%eax
80100751:	8d 7b 04             	lea    0x4(%ebx),%edi
80100754:	b9 01 00 00 00       	mov    $0x1,%ecx
80100759:	ba 0a 00 00 00       	mov    $0xa,%edx
8010075e:	89 fb                	mov    %edi,%ebx
80100760:	e8 3b fe ff ff       	call   801005a0 <printint>
      break;
80100765:	eb 9b                	jmp    80100702 <cprintf+0x52>
    switch(c){
80100767:	83 ff 73             	cmp    $0x73,%edi
8010076a:	75 24                	jne    80100790 <cprintf+0xe0>
      if((s = (char*)*argp++) == 0)
8010076c:	8d 7b 04             	lea    0x4(%ebx),%edi
8010076f:	8b 1b                	mov    (%ebx),%ebx
80100771:	85 db                	test   %ebx,%ebx
80100773:	75 68                	jne    801007dd <cprintf+0x12d>
80100775:	b8 28 00 00 00       	mov    $0x28,%eax
        s = "(null)";
8010077a:	bb d8 78 10 80       	mov    $0x801078d8,%ebx
  if(panicked){
8010077f:	8b 15 58 b5 10 80    	mov    0x8010b558,%edx
80100785:	85 d2                	test   %edx,%edx
80100787:	74 4c                	je     801007d5 <cprintf+0x125>
80100789:	fa                   	cli    
      ;
8010078a:	eb fe                	jmp    8010078a <cprintf+0xda>
8010078c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    switch(c){
80100790:	83 ff 78             	cmp    $0x78,%edi
80100793:	74 28                	je     801007bd <cprintf+0x10d>
  if(panicked){
80100795:	8b 15 58 b5 10 80    	mov    0x8010b558,%edx
8010079b:	85 d2                	test   %edx,%edx
8010079d:	74 4c                	je     801007eb <cprintf+0x13b>
8010079f:	fa                   	cli    
      ;
801007a0:	eb fe                	jmp    801007a0 <cprintf+0xf0>
801007a2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    acquire(&cons.lock);
801007a8:	83 ec 0c             	sub    $0xc,%esp
801007ab:	68 20 b5 10 80       	push   $0x8010b520
801007b0:	e8 8b 3d 00 00       	call   80104540 <acquire>
801007b5:	83 c4 10             	add    $0x10,%esp
801007b8:	e9 0c ff ff ff       	jmp    801006c9 <cprintf+0x19>
      printint(*argp++, 16, 0);
801007bd:	8b 03                	mov    (%ebx),%eax
801007bf:	8d 7b 04             	lea    0x4(%ebx),%edi
801007c2:	31 c9                	xor    %ecx,%ecx
801007c4:	ba 10 00 00 00       	mov    $0x10,%edx
801007c9:	89 fb                	mov    %edi,%ebx
801007cb:	e8 d0 fd ff ff       	call   801005a0 <printint>
      break;
801007d0:	e9 2d ff ff ff       	jmp    80100702 <cprintf+0x52>
801007d5:	e8 36 fc ff ff       	call   80100410 <consputc.part.0>
      for(; *s; s++)
801007da:	83 c3 01             	add    $0x1,%ebx
801007dd:	0f be 03             	movsbl (%ebx),%eax
801007e0:	84 c0                	test   %al,%al
801007e2:	75 9b                	jne    8010077f <cprintf+0xcf>
      if((s = (char*)*argp++) == 0)
801007e4:	89 fb                	mov    %edi,%ebx
801007e6:	e9 17 ff ff ff       	jmp    80100702 <cprintf+0x52>
801007eb:	b8 25 00 00 00       	mov    $0x25,%eax
801007f0:	e8 1b fc ff ff       	call   80100410 <consputc.part.0>
  if(panicked){
801007f5:	a1 58 b5 10 80       	mov    0x8010b558,%eax
801007fa:	85 c0                	test   %eax,%eax
801007fc:	74 4a                	je     80100848 <cprintf+0x198>
801007fe:	fa                   	cli    
      ;
801007ff:	eb fe                	jmp    801007ff <cprintf+0x14f>
80100801:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  if(panicked){
80100808:	8b 0d 58 b5 10 80    	mov    0x8010b558,%ecx
8010080e:	85 c9                	test   %ecx,%ecx
80100810:	0f 84 e2 fe ff ff    	je     801006f8 <cprintf+0x48>
80100816:	fa                   	cli    
      ;
80100817:	eb fe                	jmp    80100817 <cprintf+0x167>
80100819:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    release(&cons.lock);
80100820:	83 ec 0c             	sub    $0xc,%esp
80100823:	68 20 b5 10 80       	push   $0x8010b520
80100828:	e8 d3 3d 00 00       	call   80104600 <release>
8010082d:	83 c4 10             	add    $0x10,%esp
}
80100830:	e9 e6 fe ff ff       	jmp    8010071b <cprintf+0x6b>
    panic("null fmt");
80100835:	83 ec 0c             	sub    $0xc,%esp
80100838:	68 df 78 10 80       	push   $0x801078df
8010083d:	e8 4e fb ff ff       	call   80100390 <panic>
80100842:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80100848:	89 f8                	mov    %edi,%eax
8010084a:	e8 c1 fb ff ff       	call   80100410 <consputc.part.0>
8010084f:	e9 ae fe ff ff       	jmp    80100702 <cprintf+0x52>
80100854:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010085b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010085f:	90                   	nop

80100860 <consoleintr>:
{
80100860:	55                   	push   %ebp
80100861:	89 e5                	mov    %esp,%ebp
80100863:	57                   	push   %edi
80100864:	56                   	push   %esi
  int c, doprocdump = 0;
80100865:	31 f6                	xor    %esi,%esi
{
80100867:	53                   	push   %ebx
80100868:	83 ec 18             	sub    $0x18,%esp
8010086b:	8b 7d 08             	mov    0x8(%ebp),%edi
  acquire(&cons.lock);
8010086e:	68 20 b5 10 80       	push   $0x8010b520
80100873:	e8 c8 3c 00 00       	call   80104540 <acquire>
  while((c = getc()) >= 0){
80100878:	83 c4 10             	add    $0x10,%esp
8010087b:	ff d7                	call   *%edi
8010087d:	89 c3                	mov    %eax,%ebx
8010087f:	85 c0                	test   %eax,%eax
80100881:	0f 88 38 01 00 00    	js     801009bf <consoleintr+0x15f>
    switch(c){
80100887:	83 fb 10             	cmp    $0x10,%ebx
8010088a:	0f 84 f0 00 00 00    	je     80100980 <consoleintr+0x120>
80100890:	0f 8e ba 00 00 00    	jle    80100950 <consoleintr+0xf0>
80100896:	83 fb 15             	cmp    $0x15,%ebx
80100899:	75 35                	jne    801008d0 <consoleintr+0x70>
      while(input.e != input.w &&
8010089b:	a1 68 10 11 80       	mov    0x80111068,%eax
801008a0:	39 05 64 10 11 80    	cmp    %eax,0x80111064
801008a6:	74 d3                	je     8010087b <consoleintr+0x1b>
            input.buf[(input.e-1) % INPUT_BUF] != '\n'){
801008a8:	83 e8 01             	sub    $0x1,%eax
801008ab:	89 c2                	mov    %eax,%edx
801008ad:	83 e2 7f             	and    $0x7f,%edx
      while(input.e != input.w &&
801008b0:	80 ba e0 0f 11 80 0a 	cmpb   $0xa,-0x7feef020(%edx)
801008b7:	74 c2                	je     8010087b <consoleintr+0x1b>
  if(panicked){
801008b9:	8b 15 58 b5 10 80    	mov    0x8010b558,%edx
        input.e--;
801008bf:	a3 68 10 11 80       	mov    %eax,0x80111068
  if(panicked){
801008c4:	85 d2                	test   %edx,%edx
801008c6:	0f 84 be 00 00 00    	je     8010098a <consoleintr+0x12a>
801008cc:	fa                   	cli    
      ;
801008cd:	eb fe                	jmp    801008cd <consoleintr+0x6d>
801008cf:	90                   	nop
    switch(c){
801008d0:	83 fb 7f             	cmp    $0x7f,%ebx
801008d3:	0f 84 7c 00 00 00    	je     80100955 <consoleintr+0xf5>
      if(c != 0 && input.e-input.r < INPUT_BUF){
801008d9:	85 db                	test   %ebx,%ebx
801008db:	74 9e                	je     8010087b <consoleintr+0x1b>
801008dd:	a1 68 10 11 80       	mov    0x80111068,%eax
801008e2:	89 c2                	mov    %eax,%edx
801008e4:	2b 15 60 10 11 80    	sub    0x80111060,%edx
801008ea:	83 fa 7f             	cmp    $0x7f,%edx
801008ed:	77 8c                	ja     8010087b <consoleintr+0x1b>
        c = (c == '\r') ? '\n' : c;
801008ef:	8d 48 01             	lea    0x1(%eax),%ecx
801008f2:	8b 15 58 b5 10 80    	mov    0x8010b558,%edx
801008f8:	83 e0 7f             	and    $0x7f,%eax
        input.buf[input.e++ % INPUT_BUF] = c;
801008fb:	89 0d 68 10 11 80    	mov    %ecx,0x80111068
        c = (c == '\r') ? '\n' : c;
80100901:	83 fb 0d             	cmp    $0xd,%ebx
80100904:	0f 84 d1 00 00 00    	je     801009db <consoleintr+0x17b>
        input.buf[input.e++ % INPUT_BUF] = c;
8010090a:	88 98 e0 0f 11 80    	mov    %bl,-0x7feef020(%eax)
  if(panicked){
80100910:	85 d2                	test   %edx,%edx
80100912:	0f 85 ce 00 00 00    	jne    801009e6 <consoleintr+0x186>
80100918:	89 d8                	mov    %ebx,%eax
8010091a:	e8 f1 fa ff ff       	call   80100410 <consputc.part.0>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
8010091f:	83 fb 0a             	cmp    $0xa,%ebx
80100922:	0f 84 d2 00 00 00    	je     801009fa <consoleintr+0x19a>
80100928:	83 fb 04             	cmp    $0x4,%ebx
8010092b:	0f 84 c9 00 00 00    	je     801009fa <consoleintr+0x19a>
80100931:	a1 60 10 11 80       	mov    0x80111060,%eax
80100936:	83 e8 80             	sub    $0xffffff80,%eax
80100939:	39 05 68 10 11 80    	cmp    %eax,0x80111068
8010093f:	0f 85 36 ff ff ff    	jne    8010087b <consoleintr+0x1b>
80100945:	e9 b5 00 00 00       	jmp    801009ff <consoleintr+0x19f>
8010094a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    switch(c){
80100950:	83 fb 08             	cmp    $0x8,%ebx
80100953:	75 84                	jne    801008d9 <consoleintr+0x79>
      if(input.e != input.w){
80100955:	a1 68 10 11 80       	mov    0x80111068,%eax
8010095a:	3b 05 64 10 11 80    	cmp    0x80111064,%eax
80100960:	0f 84 15 ff ff ff    	je     8010087b <consoleintr+0x1b>
        input.e--;
80100966:	83 e8 01             	sub    $0x1,%eax
80100969:	a3 68 10 11 80       	mov    %eax,0x80111068
  if(panicked){
8010096e:	a1 58 b5 10 80       	mov    0x8010b558,%eax
80100973:	85 c0                	test   %eax,%eax
80100975:	74 39                	je     801009b0 <consoleintr+0x150>
80100977:	fa                   	cli    
      ;
80100978:	eb fe                	jmp    80100978 <consoleintr+0x118>
8010097a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      doprocdump = 1;
80100980:	be 01 00 00 00       	mov    $0x1,%esi
80100985:	e9 f1 fe ff ff       	jmp    8010087b <consoleintr+0x1b>
8010098a:	b8 00 01 00 00       	mov    $0x100,%eax
8010098f:	e8 7c fa ff ff       	call   80100410 <consputc.part.0>
      while(input.e != input.w &&
80100994:	a1 68 10 11 80       	mov    0x80111068,%eax
80100999:	3b 05 64 10 11 80    	cmp    0x80111064,%eax
8010099f:	0f 85 03 ff ff ff    	jne    801008a8 <consoleintr+0x48>
801009a5:	e9 d1 fe ff ff       	jmp    8010087b <consoleintr+0x1b>
801009aa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801009b0:	b8 00 01 00 00       	mov    $0x100,%eax
801009b5:	e8 56 fa ff ff       	call   80100410 <consputc.part.0>
801009ba:	e9 bc fe ff ff       	jmp    8010087b <consoleintr+0x1b>
  release(&cons.lock);
801009bf:	83 ec 0c             	sub    $0xc,%esp
801009c2:	68 20 b5 10 80       	push   $0x8010b520
801009c7:	e8 34 3c 00 00       	call   80104600 <release>
  if(doprocdump) {
801009cc:	83 c4 10             	add    $0x10,%esp
801009cf:	85 f6                	test   %esi,%esi
801009d1:	75 46                	jne    80100a19 <consoleintr+0x1b9>
}
801009d3:	8d 65 f4             	lea    -0xc(%ebp),%esp
801009d6:	5b                   	pop    %ebx
801009d7:	5e                   	pop    %esi
801009d8:	5f                   	pop    %edi
801009d9:	5d                   	pop    %ebp
801009da:	c3                   	ret    
        input.buf[input.e++ % INPUT_BUF] = c;
801009db:	c6 80 e0 0f 11 80 0a 	movb   $0xa,-0x7feef020(%eax)
  if(panicked){
801009e2:	85 d2                	test   %edx,%edx
801009e4:	74 0a                	je     801009f0 <consoleintr+0x190>
801009e6:	fa                   	cli    
      ;
801009e7:	eb fe                	jmp    801009e7 <consoleintr+0x187>
801009e9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801009f0:	b8 0a 00 00 00       	mov    $0xa,%eax
801009f5:	e8 16 fa ff ff       	call   80100410 <consputc.part.0>
        if(c == '\n' || c == C('D') || input.e == input.r+INPUT_BUF){
801009fa:	a1 68 10 11 80       	mov    0x80111068,%eax
          wakeup(&input.r);
801009ff:	83 ec 0c             	sub    $0xc,%esp
          input.w = input.e;
80100a02:	a3 64 10 11 80       	mov    %eax,0x80111064
          wakeup(&input.r);
80100a07:	68 60 10 11 80       	push   $0x80111060
80100a0c:	e8 df 36 00 00       	call   801040f0 <wakeup>
80100a11:	83 c4 10             	add    $0x10,%esp
80100a14:	e9 62 fe ff ff       	jmp    8010087b <consoleintr+0x1b>
}
80100a19:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100a1c:	5b                   	pop    %ebx
80100a1d:	5e                   	pop    %esi
80100a1e:	5f                   	pop    %edi
80100a1f:	5d                   	pop    %ebp
    procdump();  // now call procdump() wo. cons.lock held
80100a20:	e9 ab 37 00 00       	jmp    801041d0 <procdump>
80100a25:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100a2c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80100a30 <consoleinit>:

void
consoleinit(void)
{
80100a30:	55                   	push   %ebp
80100a31:	89 e5                	mov    %esp,%ebp
80100a33:	83 ec 10             	sub    $0x10,%esp
  initlock(&cons.lock, "console");
80100a36:	68 e8 78 10 80       	push   $0x801078e8
80100a3b:	68 20 b5 10 80       	push   $0x8010b520
80100a40:	e8 9b 39 00 00       	call   801043e0 <initlock>

  devsw[CONSOLE].write = consolewrite;
  devsw[CONSOLE].read = consoleread;
  cons.locking = 1;

  ioapicenable(IRQ_KBD, 0);
80100a45:	58                   	pop    %eax
80100a46:	5a                   	pop    %edx
80100a47:	6a 00                	push   $0x0
80100a49:	6a 01                	push   $0x1
  devsw[CONSOLE].write = consolewrite;
80100a4b:	c7 05 2c 1a 11 80 30 	movl   $0x80100630,0x80111a2c
80100a52:	06 10 80 
  devsw[CONSOLE].read = consoleread;
80100a55:	c7 05 28 1a 11 80 80 	movl   $0x80100280,0x80111a28
80100a5c:	02 10 80 
  cons.locking = 1;
80100a5f:	c7 05 54 b5 10 80 01 	movl   $0x1,0x8010b554
80100a66:	00 00 00 
  ioapicenable(IRQ_KBD, 0);
80100a69:	e8 72 19 00 00       	call   801023e0 <ioapicenable>
}
80100a6e:	83 c4 10             	add    $0x10,%esp
80100a71:	c9                   	leave  
80100a72:	c3                   	ret    
80100a73:	66 90                	xchg   %ax,%ax
80100a75:	66 90                	xchg   %ax,%ax
80100a77:	66 90                	xchg   %ax,%ax
80100a79:	66 90                	xchg   %ax,%ax
80100a7b:	66 90                	xchg   %ax,%ax
80100a7d:	66 90                	xchg   %ax,%ax
80100a7f:	90                   	nop

80100a80 <exec>:
#include "x86.h"
#include "elf.h"

int
exec(char *path, char **argv)
{
80100a80:	55                   	push   %ebp
80100a81:	89 e5                	mov    %esp,%ebp
80100a83:	57                   	push   %edi
80100a84:	56                   	push   %esi
80100a85:	53                   	push   %ebx
80100a86:	81 ec 0c 01 00 00    	sub    $0x10c,%esp
  uint argc, sz, sp, ustack[3+MAXARG+1];
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pde_t *pgdir, *oldpgdir;
  struct proc *curproc = myproc();
80100a8c:	e8 8f 2e 00 00       	call   80103920 <myproc>
80100a91:	89 85 ec fe ff ff    	mov    %eax,-0x114(%ebp)

  begin_op();
80100a97:	e8 34 22 00 00       	call   80102cd0 <begin_op>

  if((ip = namei(path)) == 0){
80100a9c:	83 ec 0c             	sub    $0xc,%esp
80100a9f:	ff 75 08             	pushl  0x8(%ebp)
80100aa2:	e8 49 15 00 00       	call   80101ff0 <namei>
80100aa7:	83 c4 10             	add    $0x10,%esp
80100aaa:	85 c0                	test   %eax,%eax
80100aac:	0f 84 2c 03 00 00    	je     80100dde <exec+0x35e>
    end_op();
    cprintf("exec: fail\n");
    return -1;
  }
  ilock(ip);
80100ab2:	83 ec 0c             	sub    $0xc,%esp
80100ab5:	89 c3                	mov    %eax,%ebx
80100ab7:	50                   	push   %eax
80100ab8:	e8 93 0c 00 00       	call   80101750 <ilock>
  pgdir = 0;

  // Check ELF header
  if(readi(ip, (char*)&elf, 0, sizeof(elf)) != sizeof(elf))
80100abd:	8d 85 24 ff ff ff    	lea    -0xdc(%ebp),%eax
80100ac3:	6a 34                	push   $0x34
80100ac5:	6a 00                	push   $0x0
80100ac7:	50                   	push   %eax
80100ac8:	53                   	push   %ebx
80100ac9:	e8 62 0f 00 00       	call   80101a30 <readi>
80100ace:	83 c4 20             	add    $0x20,%esp
80100ad1:	83 f8 34             	cmp    $0x34,%eax
80100ad4:	74 22                	je     80100af8 <exec+0x78>

 bad:
  if(pgdir)
    freevm(pgdir);
  if(ip){
    iunlockput(ip);
80100ad6:	83 ec 0c             	sub    $0xc,%esp
80100ad9:	53                   	push   %ebx
80100ada:	e8 01 0f 00 00       	call   801019e0 <iunlockput>
    end_op();
80100adf:	e8 5c 22 00 00       	call   80102d40 <end_op>
80100ae4:	83 c4 10             	add    $0x10,%esp
  }
  return -1;
80100ae7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80100aec:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100aef:	5b                   	pop    %ebx
80100af0:	5e                   	pop    %esi
80100af1:	5f                   	pop    %edi
80100af2:	5d                   	pop    %ebp
80100af3:	c3                   	ret    
80100af4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if(elf.magic != ELF_MAGIC)
80100af8:	81 bd 24 ff ff ff 7f 	cmpl   $0x464c457f,-0xdc(%ebp)
80100aff:	45 4c 46 
80100b02:	75 d2                	jne    80100ad6 <exec+0x56>
  if((pgdir = setupkvm()) == 0)
80100b04:	e8 67 65 00 00       	call   80107070 <setupkvm>
80100b09:	89 85 f4 fe ff ff    	mov    %eax,-0x10c(%ebp)
80100b0f:	85 c0                	test   %eax,%eax
80100b11:	74 c3                	je     80100ad6 <exec+0x56>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100b13:	66 83 bd 50 ff ff ff 	cmpw   $0x0,-0xb0(%ebp)
80100b1a:	00 
80100b1b:	8b b5 40 ff ff ff    	mov    -0xc0(%ebp),%esi
80100b21:	0f 84 d6 02 00 00    	je     80100dfd <exec+0x37d>
  sz = 0;
80100b27:	c7 85 f0 fe ff ff 00 	movl   $0x0,-0x110(%ebp)
80100b2e:	00 00 00 
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100b31:	31 ff                	xor    %edi,%edi
80100b33:	e9 8e 00 00 00       	jmp    80100bc6 <exec+0x146>
80100b38:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100b3f:	90                   	nop
    if(ph.type != ELF_PROG_LOAD)
80100b40:	83 bd 04 ff ff ff 01 	cmpl   $0x1,-0xfc(%ebp)
80100b47:	75 6c                	jne    80100bb5 <exec+0x135>
    if(ph.memsz < ph.filesz)
80100b49:	8b 85 18 ff ff ff    	mov    -0xe8(%ebp),%eax
80100b4f:	3b 85 14 ff ff ff    	cmp    -0xec(%ebp),%eax
80100b55:	0f 82 87 00 00 00    	jb     80100be2 <exec+0x162>
    if(ph.vaddr + ph.memsz < ph.vaddr)
80100b5b:	03 85 0c ff ff ff    	add    -0xf4(%ebp),%eax
80100b61:	72 7f                	jb     80100be2 <exec+0x162>
    if((sz = allocuvm(pgdir, sz, ph.vaddr + ph.memsz)) == 0)
80100b63:	83 ec 04             	sub    $0x4,%esp
80100b66:	50                   	push   %eax
80100b67:	ff b5 f0 fe ff ff    	pushl  -0x110(%ebp)
80100b6d:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
80100b73:	e8 18 63 00 00       	call   80106e90 <allocuvm>
80100b78:	83 c4 10             	add    $0x10,%esp
80100b7b:	89 85 f0 fe ff ff    	mov    %eax,-0x110(%ebp)
80100b81:	85 c0                	test   %eax,%eax
80100b83:	74 5d                	je     80100be2 <exec+0x162>
    if(ph.vaddr % PGSIZE != 0)
80100b85:	8b 85 0c ff ff ff    	mov    -0xf4(%ebp),%eax
80100b8b:	a9 ff 0f 00 00       	test   $0xfff,%eax
80100b90:	75 50                	jne    80100be2 <exec+0x162>
    if(loaduvm(pgdir, (char*)ph.vaddr, ip, ph.off, ph.filesz) < 0)
80100b92:	83 ec 0c             	sub    $0xc,%esp
80100b95:	ff b5 14 ff ff ff    	pushl  -0xec(%ebp)
80100b9b:	ff b5 08 ff ff ff    	pushl  -0xf8(%ebp)
80100ba1:	53                   	push   %ebx
80100ba2:	50                   	push   %eax
80100ba3:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
80100ba9:	e8 22 62 00 00       	call   80106dd0 <loaduvm>
80100bae:	83 c4 20             	add    $0x20,%esp
80100bb1:	85 c0                	test   %eax,%eax
80100bb3:	78 2d                	js     80100be2 <exec+0x162>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100bb5:	0f b7 85 50 ff ff ff 	movzwl -0xb0(%ebp),%eax
80100bbc:	83 c7 01             	add    $0x1,%edi
80100bbf:	83 c6 20             	add    $0x20,%esi
80100bc2:	39 f8                	cmp    %edi,%eax
80100bc4:	7e 3a                	jle    80100c00 <exec+0x180>
    if(readi(ip, (char*)&ph, off, sizeof(ph)) != sizeof(ph))
80100bc6:	8d 85 04 ff ff ff    	lea    -0xfc(%ebp),%eax
80100bcc:	6a 20                	push   $0x20
80100bce:	56                   	push   %esi
80100bcf:	50                   	push   %eax
80100bd0:	53                   	push   %ebx
80100bd1:	e8 5a 0e 00 00       	call   80101a30 <readi>
80100bd6:	83 c4 10             	add    $0x10,%esp
80100bd9:	83 f8 20             	cmp    $0x20,%eax
80100bdc:	0f 84 5e ff ff ff    	je     80100b40 <exec+0xc0>
    freevm(pgdir);
80100be2:	83 ec 0c             	sub    $0xc,%esp
80100be5:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
80100beb:	e8 00 64 00 00       	call   80106ff0 <freevm>
  if(ip){
80100bf0:	83 c4 10             	add    $0x10,%esp
80100bf3:	e9 de fe ff ff       	jmp    80100ad6 <exec+0x56>
80100bf8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100bff:	90                   	nop
80100c00:	8b bd f0 fe ff ff    	mov    -0x110(%ebp),%edi
80100c06:	81 c7 ff 0f 00 00    	add    $0xfff,%edi
80100c0c:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
80100c12:	8d b7 00 40 00 00    	lea    0x4000(%edi),%esi
  iunlockput(ip);
80100c18:	83 ec 0c             	sub    $0xc,%esp
80100c1b:	53                   	push   %ebx
80100c1c:	e8 bf 0d 00 00       	call   801019e0 <iunlockput>
  end_op();
80100c21:	e8 1a 21 00 00       	call   80102d40 <end_op>
  if((sz = allocuvm(pgdir, sz, sz + NPAG*PGSIZE)) == 0){
80100c26:	83 c4 0c             	add    $0xc,%esp
80100c29:	56                   	push   %esi
80100c2a:	57                   	push   %edi
80100c2b:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
80100c31:	e8 5a 62 00 00       	call   80106e90 <allocuvm>
80100c36:	83 c4 10             	add    $0x10,%esp
80100c39:	89 c6                	mov    %eax,%esi
80100c3b:	85 c0                	test   %eax,%eax
80100c3d:	0f 84 ab 00 00 00    	je     80100cee <exec+0x26e>
  clearpteu(pgdir, (char*)(sz - NPAG*PGSIZE));
80100c43:	83 ec 08             	sub    $0x8,%esp
80100c46:	8d 80 00 c0 ff ff    	lea    -0x4000(%eax),%eax
  for(argc = 0; argv[argc]; argc++) {
80100c4c:	89 f3                	mov    %esi,%ebx
80100c4e:	31 ff                	xor    %edi,%edi
  clearpteu(pgdir, (char*)(sz - NPAG*PGSIZE));
80100c50:	50                   	push   %eax
80100c51:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
80100c57:	e8 b4 64 00 00       	call   80107110 <clearpteu>
  for(argc = 0; argv[argc]; argc++) {
80100c5c:	8b 45 0c             	mov    0xc(%ebp),%eax
80100c5f:	83 c4 10             	add    $0x10,%esp
80100c62:	8d 95 58 ff ff ff    	lea    -0xa8(%ebp),%edx
80100c68:	8b 00                	mov    (%eax),%eax
80100c6a:	85 c0                	test   %eax,%eax
80100c6c:	0f 84 a8 00 00 00    	je     80100d1a <exec+0x29a>
80100c72:	89 b5 f0 fe ff ff    	mov    %esi,-0x110(%ebp)
80100c78:	8b b5 f4 fe ff ff    	mov    -0x10c(%ebp),%esi
80100c7e:	eb 1f                	jmp    80100c9f <exec+0x21f>
80100c80:	8b 45 0c             	mov    0xc(%ebp),%eax
    ustack[3+argc] = sp;
80100c83:	89 9c bd 64 ff ff ff 	mov    %ebx,-0x9c(%ebp,%edi,4)
  for(argc = 0; argv[argc]; argc++) {
80100c8a:	83 c7 01             	add    $0x1,%edi
    ustack[3+argc] = sp;
80100c8d:	8d 95 58 ff ff ff    	lea    -0xa8(%ebp),%edx
  for(argc = 0; argv[argc]; argc++) {
80100c93:	8b 04 b8             	mov    (%eax,%edi,4),%eax
80100c96:	85 c0                	test   %eax,%eax
80100c98:	74 7a                	je     80100d14 <exec+0x294>
    if(argc >= MAXARG)
80100c9a:	83 ff 20             	cmp    $0x20,%edi
80100c9d:	74 34                	je     80100cd3 <exec+0x253>
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100c9f:	83 ec 0c             	sub    $0xc,%esp
80100ca2:	50                   	push   %eax
80100ca3:	e8 b8 3b 00 00       	call   80104860 <strlen>
80100ca8:	f7 d0                	not    %eax
80100caa:	01 c3                	add    %eax,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100cac:	58                   	pop    %eax
80100cad:	8b 45 0c             	mov    0xc(%ebp),%eax
    sp = (sp - (strlen(argv[argc]) + 1)) & ~3;
80100cb0:	83 e3 fc             	and    $0xfffffffc,%ebx
    if(copyout(pgdir, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
80100cb3:	ff 34 b8             	pushl  (%eax,%edi,4)
80100cb6:	e8 a5 3b 00 00       	call   80104860 <strlen>
80100cbb:	83 c0 01             	add    $0x1,%eax
80100cbe:	50                   	push   %eax
80100cbf:	8b 45 0c             	mov    0xc(%ebp),%eax
80100cc2:	ff 34 b8             	pushl  (%eax,%edi,4)
80100cc5:	53                   	push   %ebx
80100cc6:	56                   	push   %esi
80100cc7:	e8 b4 65 00 00       	call   80107280 <copyout>
80100ccc:	83 c4 20             	add    $0x20,%esp
80100ccf:	85 c0                	test   %eax,%eax
80100cd1:	79 ad                	jns    80100c80 <exec+0x200>
    freevm(pgdir);
80100cd3:	83 ec 0c             	sub    $0xc,%esp
80100cd6:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
80100cdc:	e8 0f 63 00 00       	call   80106ff0 <freevm>
80100ce1:	83 c4 10             	add    $0x10,%esp
  return -1;
80100ce4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100ce9:	e9 fe fd ff ff       	jmp    80100aec <exec+0x6c>
    cprintf("::----::");
80100cee:	83 ec 0c             	sub    $0xc,%esp
80100cf1:	68 0d 79 10 80       	push   $0x8010790d
80100cf6:	e8 b5 f9 ff ff       	call   801006b0 <cprintf>
    freevm(pgdir);
80100cfb:	5a                   	pop    %edx
80100cfc:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
80100d02:	e8 e9 62 00 00       	call   80106ff0 <freevm>
80100d07:	83 c4 10             	add    $0x10,%esp
  return -1;
80100d0a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100d0f:	e9 d8 fd ff ff       	jmp    80100aec <exec+0x6c>
80100d14:	8b b5 f0 fe ff ff    	mov    -0x110(%ebp),%esi
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100d1a:	8d 04 bd 04 00 00 00 	lea    0x4(,%edi,4),%eax
80100d21:	89 d9                	mov    %ebx,%ecx
  ustack[3+argc] = 0;
80100d23:	c7 84 bd 64 ff ff ff 	movl   $0x0,-0x9c(%ebp,%edi,4)
80100d2a:	00 00 00 00 
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100d2e:	29 c1                	sub    %eax,%ecx
  sp -= (3+argc+1) * 4;
80100d30:	83 c0 0c             	add    $0xc,%eax
  ustack[1] = argc;
80100d33:	89 bd 5c ff ff ff    	mov    %edi,-0xa4(%ebp)
  sp -= (3+argc+1) * 4;
80100d39:	29 c3                	sub    %eax,%ebx
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100d3b:	50                   	push   %eax
80100d3c:	52                   	push   %edx
80100d3d:	53                   	push   %ebx
80100d3e:	ff b5 f4 fe ff ff    	pushl  -0x10c(%ebp)
  ustack[0] = 0xffffffff;  // fake return PC
80100d44:	c7 85 58 ff ff ff ff 	movl   $0xffffffff,-0xa8(%ebp)
80100d4b:	ff ff ff 
  ustack[2] = sp - (argc+1)*4;  // argv pointer
80100d4e:	89 8d 60 ff ff ff    	mov    %ecx,-0xa0(%ebp)
  if(copyout(pgdir, sp, ustack, (3+argc+1)*4) < 0)
80100d54:	e8 27 65 00 00       	call   80107280 <copyout>
80100d59:	83 c4 10             	add    $0x10,%esp
80100d5c:	85 c0                	test   %eax,%eax
80100d5e:	0f 88 6f ff ff ff    	js     80100cd3 <exec+0x253>
  for(last=s=path; *s; s++)
80100d64:	8b 45 08             	mov    0x8(%ebp),%eax
80100d67:	8b 55 08             	mov    0x8(%ebp),%edx
80100d6a:	0f b6 00             	movzbl (%eax),%eax
80100d6d:	84 c0                	test   %al,%al
80100d6f:	74 16                	je     80100d87 <exec+0x307>
80100d71:	89 d1                	mov    %edx,%ecx
80100d73:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100d77:	90                   	nop
    if(*s == '/')
80100d78:	83 c1 01             	add    $0x1,%ecx
80100d7b:	3c 2f                	cmp    $0x2f,%al
  for(last=s=path; *s; s++)
80100d7d:	0f b6 01             	movzbl (%ecx),%eax
    if(*s == '/')
80100d80:	0f 44 d1             	cmove  %ecx,%edx
  for(last=s=path; *s; s++)
80100d83:	84 c0                	test   %al,%al
80100d85:	75 f1                	jne    80100d78 <exec+0x2f8>
  safestrcpy(curproc->name, last, sizeof(curproc->name));
80100d87:	8b bd ec fe ff ff    	mov    -0x114(%ebp),%edi
80100d8d:	83 ec 04             	sub    $0x4,%esp
80100d90:	6a 10                	push   $0x10
80100d92:	89 f8                	mov    %edi,%eax
80100d94:	52                   	push   %edx
80100d95:	05 ac 00 00 00       	add    $0xac,%eax
80100d9a:	50                   	push   %eax
80100d9b:	e8 80 3a 00 00       	call   80104820 <safestrcpy>
  curproc->pgdir = pgdir;
80100da0:	8b 8d f4 fe ff ff    	mov    -0x10c(%ebp),%ecx
  oldpgdir = curproc->pgdir;
80100da6:	89 f8                	mov    %edi,%eax
80100da8:	8b 7f 04             	mov    0x4(%edi),%edi
  curproc->sz = sz;
80100dab:	89 30                	mov    %esi,(%eax)
  curproc->pgdir = pgdir;
80100dad:	89 48 04             	mov    %ecx,0x4(%eax)
  curproc->tf->eip = elf.entry;  // main
80100db0:	89 c1                	mov    %eax,%ecx
80100db2:	8b 95 3c ff ff ff    	mov    -0xc4(%ebp),%edx
80100db8:	8b 40 18             	mov    0x18(%eax),%eax
80100dbb:	89 50 38             	mov    %edx,0x38(%eax)
  curproc->tf->esp = sp;
80100dbe:	8b 41 18             	mov    0x18(%ecx),%eax
80100dc1:	89 58 44             	mov    %ebx,0x44(%eax)
  switchuvm(curproc);
80100dc4:	89 0c 24             	mov    %ecx,(%esp)
80100dc7:	e8 74 5e 00 00       	call   80106c40 <switchuvm>
  freevm(oldpgdir);
80100dcc:	89 3c 24             	mov    %edi,(%esp)
80100dcf:	e8 1c 62 00 00       	call   80106ff0 <freevm>
  return 0;
80100dd4:	83 c4 10             	add    $0x10,%esp
80100dd7:	31 c0                	xor    %eax,%eax
80100dd9:	e9 0e fd ff ff       	jmp    80100aec <exec+0x6c>
    end_op();
80100dde:	e8 5d 1f 00 00       	call   80102d40 <end_op>
    cprintf("exec: fail\n");
80100de3:	83 ec 0c             	sub    $0xc,%esp
80100de6:	68 01 79 10 80       	push   $0x80107901
80100deb:	e8 c0 f8 ff ff       	call   801006b0 <cprintf>
    return -1;
80100df0:	83 c4 10             	add    $0x10,%esp
80100df3:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80100df8:	e9 ef fc ff ff       	jmp    80100aec <exec+0x6c>
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
80100dfd:	31 ff                	xor    %edi,%edi
80100dff:	be 00 40 00 00       	mov    $0x4000,%esi
80100e04:	e9 0f fe ff ff       	jmp    80100c18 <exec+0x198>
80100e09:	66 90                	xchg   %ax,%ax
80100e0b:	66 90                	xchg   %ax,%ax
80100e0d:	66 90                	xchg   %ax,%ax
80100e0f:	90                   	nop

80100e10 <fileinit>:
  struct file file[NFILE];
} ftable;

void
fileinit(void)
{
80100e10:	55                   	push   %ebp
80100e11:	89 e5                	mov    %esp,%ebp
80100e13:	83 ec 10             	sub    $0x10,%esp
  initlock(&ftable.lock, "ftable");
80100e16:	68 16 79 10 80       	push   $0x80107916
80100e1b:	68 80 10 11 80       	push   $0x80111080
80100e20:	e8 bb 35 00 00       	call   801043e0 <initlock>
}
80100e25:	83 c4 10             	add    $0x10,%esp
80100e28:	c9                   	leave  
80100e29:	c3                   	ret    
80100e2a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80100e30 <filealloc>:

// Allocate a file structure.
struct file*
filealloc(void)
{
80100e30:	55                   	push   %ebp
80100e31:	89 e5                	mov    %esp,%ebp
80100e33:	53                   	push   %ebx
  struct file *f;

  acquire(&ftable.lock);
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80100e34:	bb b4 10 11 80       	mov    $0x801110b4,%ebx
{
80100e39:	83 ec 10             	sub    $0x10,%esp
  acquire(&ftable.lock);
80100e3c:	68 80 10 11 80       	push   $0x80111080
80100e41:	e8 fa 36 00 00       	call   80104540 <acquire>
80100e46:	83 c4 10             	add    $0x10,%esp
80100e49:	eb 10                	jmp    80100e5b <filealloc+0x2b>
80100e4b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100e4f:	90                   	nop
  for(f = ftable.file; f < ftable.file + NFILE; f++){
80100e50:	83 c3 18             	add    $0x18,%ebx
80100e53:	81 fb 14 1a 11 80    	cmp    $0x80111a14,%ebx
80100e59:	74 25                	je     80100e80 <filealloc+0x50>
    if(f->ref == 0){
80100e5b:	8b 43 04             	mov    0x4(%ebx),%eax
80100e5e:	85 c0                	test   %eax,%eax
80100e60:	75 ee                	jne    80100e50 <filealloc+0x20>
      f->ref = 1;
      release(&ftable.lock);
80100e62:	83 ec 0c             	sub    $0xc,%esp
      f->ref = 1;
80100e65:	c7 43 04 01 00 00 00 	movl   $0x1,0x4(%ebx)
      release(&ftable.lock);
80100e6c:	68 80 10 11 80       	push   $0x80111080
80100e71:	e8 8a 37 00 00       	call   80104600 <release>
      return f;
    }
  }
  release(&ftable.lock);
  return 0;
}
80100e76:	89 d8                	mov    %ebx,%eax
      return f;
80100e78:	83 c4 10             	add    $0x10,%esp
}
80100e7b:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100e7e:	c9                   	leave  
80100e7f:	c3                   	ret    
  release(&ftable.lock);
80100e80:	83 ec 0c             	sub    $0xc,%esp
  return 0;
80100e83:	31 db                	xor    %ebx,%ebx
  release(&ftable.lock);
80100e85:	68 80 10 11 80       	push   $0x80111080
80100e8a:	e8 71 37 00 00       	call   80104600 <release>
}
80100e8f:	89 d8                	mov    %ebx,%eax
  return 0;
80100e91:	83 c4 10             	add    $0x10,%esp
}
80100e94:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100e97:	c9                   	leave  
80100e98:	c3                   	ret    
80100e99:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80100ea0 <filedup>:

// Increment ref count for file f.
struct file*
filedup(struct file *f)
{
80100ea0:	55                   	push   %ebp
80100ea1:	89 e5                	mov    %esp,%ebp
80100ea3:	53                   	push   %ebx
80100ea4:	83 ec 10             	sub    $0x10,%esp
80100ea7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ftable.lock);
80100eaa:	68 80 10 11 80       	push   $0x80111080
80100eaf:	e8 8c 36 00 00       	call   80104540 <acquire>
  if(f->ref < 1)
80100eb4:	8b 43 04             	mov    0x4(%ebx),%eax
80100eb7:	83 c4 10             	add    $0x10,%esp
80100eba:	85 c0                	test   %eax,%eax
80100ebc:	7e 1a                	jle    80100ed8 <filedup+0x38>
    panic("filedup");
  f->ref++;
80100ebe:	83 c0 01             	add    $0x1,%eax
  release(&ftable.lock);
80100ec1:	83 ec 0c             	sub    $0xc,%esp
  f->ref++;
80100ec4:	89 43 04             	mov    %eax,0x4(%ebx)
  release(&ftable.lock);
80100ec7:	68 80 10 11 80       	push   $0x80111080
80100ecc:	e8 2f 37 00 00       	call   80104600 <release>
  return f;
}
80100ed1:	89 d8                	mov    %ebx,%eax
80100ed3:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80100ed6:	c9                   	leave  
80100ed7:	c3                   	ret    
    panic("filedup");
80100ed8:	83 ec 0c             	sub    $0xc,%esp
80100edb:	68 1d 79 10 80       	push   $0x8010791d
80100ee0:	e8 ab f4 ff ff       	call   80100390 <panic>
80100ee5:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100eec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80100ef0 <fileclose>:

// Close file f.  (Decrement ref count, close when reaches 0.)
void
fileclose(struct file *f)
{
80100ef0:	55                   	push   %ebp
80100ef1:	89 e5                	mov    %esp,%ebp
80100ef3:	57                   	push   %edi
80100ef4:	56                   	push   %esi
80100ef5:	53                   	push   %ebx
80100ef6:	83 ec 28             	sub    $0x28,%esp
80100ef9:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct file ff;

  acquire(&ftable.lock);
80100efc:	68 80 10 11 80       	push   $0x80111080
80100f01:	e8 3a 36 00 00       	call   80104540 <acquire>
  if(f->ref < 1)
80100f06:	8b 43 04             	mov    0x4(%ebx),%eax
80100f09:	83 c4 10             	add    $0x10,%esp
80100f0c:	85 c0                	test   %eax,%eax
80100f0e:	0f 8e a3 00 00 00    	jle    80100fb7 <fileclose+0xc7>
    panic("fileclose");
  if(--f->ref > 0){
80100f14:	83 e8 01             	sub    $0x1,%eax
80100f17:	89 43 04             	mov    %eax,0x4(%ebx)
80100f1a:	75 44                	jne    80100f60 <fileclose+0x70>
    release(&ftable.lock);
    return;
  }
  ff = *f;
80100f1c:	0f b6 43 09          	movzbl 0x9(%ebx),%eax
  f->ref = 0;
  f->type = FD_NONE;
  release(&ftable.lock);
80100f20:	83 ec 0c             	sub    $0xc,%esp
  ff = *f;
80100f23:	8b 3b                	mov    (%ebx),%edi
  f->type = FD_NONE;
80100f25:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  ff = *f;
80100f2b:	8b 73 0c             	mov    0xc(%ebx),%esi
80100f2e:	88 45 e7             	mov    %al,-0x19(%ebp)
80100f31:	8b 43 10             	mov    0x10(%ebx),%eax
  release(&ftable.lock);
80100f34:	68 80 10 11 80       	push   $0x80111080
  ff = *f;
80100f39:	89 45 e0             	mov    %eax,-0x20(%ebp)
  release(&ftable.lock);
80100f3c:	e8 bf 36 00 00       	call   80104600 <release>

  if(ff.type == FD_PIPE)
80100f41:	83 c4 10             	add    $0x10,%esp
80100f44:	83 ff 01             	cmp    $0x1,%edi
80100f47:	74 2f                	je     80100f78 <fileclose+0x88>
    pipeclose(ff.pipe, ff.writable);
  else if(ff.type == FD_INODE){
80100f49:	83 ff 02             	cmp    $0x2,%edi
80100f4c:	74 4a                	je     80100f98 <fileclose+0xa8>
    begin_op();
    iput(ff.ip);
    end_op();
  }
}
80100f4e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100f51:	5b                   	pop    %ebx
80100f52:	5e                   	pop    %esi
80100f53:	5f                   	pop    %edi
80100f54:	5d                   	pop    %ebp
80100f55:	c3                   	ret    
80100f56:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100f5d:	8d 76 00             	lea    0x0(%esi),%esi
    release(&ftable.lock);
80100f60:	c7 45 08 80 10 11 80 	movl   $0x80111080,0x8(%ebp)
}
80100f67:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100f6a:	5b                   	pop    %ebx
80100f6b:	5e                   	pop    %esi
80100f6c:	5f                   	pop    %edi
80100f6d:	5d                   	pop    %ebp
    release(&ftable.lock);
80100f6e:	e9 8d 36 00 00       	jmp    80104600 <release>
80100f73:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100f77:	90                   	nop
    pipeclose(ff.pipe, ff.writable);
80100f78:	0f be 5d e7          	movsbl -0x19(%ebp),%ebx
80100f7c:	83 ec 08             	sub    $0x8,%esp
80100f7f:	53                   	push   %ebx
80100f80:	56                   	push   %esi
80100f81:	e8 fa 24 00 00       	call   80103480 <pipeclose>
80100f86:	83 c4 10             	add    $0x10,%esp
}
80100f89:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100f8c:	5b                   	pop    %ebx
80100f8d:	5e                   	pop    %esi
80100f8e:	5f                   	pop    %edi
80100f8f:	5d                   	pop    %ebp
80100f90:	c3                   	ret    
80100f91:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    begin_op();
80100f98:	e8 33 1d 00 00       	call   80102cd0 <begin_op>
    iput(ff.ip);
80100f9d:	83 ec 0c             	sub    $0xc,%esp
80100fa0:	ff 75 e0             	pushl  -0x20(%ebp)
80100fa3:	e8 d8 08 00 00       	call   80101880 <iput>
    end_op();
80100fa8:	83 c4 10             	add    $0x10,%esp
}
80100fab:	8d 65 f4             	lea    -0xc(%ebp),%esp
80100fae:	5b                   	pop    %ebx
80100faf:	5e                   	pop    %esi
80100fb0:	5f                   	pop    %edi
80100fb1:	5d                   	pop    %ebp
    end_op();
80100fb2:	e9 89 1d 00 00       	jmp    80102d40 <end_op>
    panic("fileclose");
80100fb7:	83 ec 0c             	sub    $0xc,%esp
80100fba:	68 25 79 10 80       	push   $0x80107925
80100fbf:	e8 cc f3 ff ff       	call   80100390 <panic>
80100fc4:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80100fcb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80100fcf:	90                   	nop

80100fd0 <filestat>:

// Get metadata about file f.
int
filestat(struct file *f, struct stat *st)
{
80100fd0:	55                   	push   %ebp
80100fd1:	89 e5                	mov    %esp,%ebp
80100fd3:	53                   	push   %ebx
80100fd4:	83 ec 04             	sub    $0x4,%esp
80100fd7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(f->type == FD_INODE){
80100fda:	83 3b 02             	cmpl   $0x2,(%ebx)
80100fdd:	75 31                	jne    80101010 <filestat+0x40>
    ilock(f->ip);
80100fdf:	83 ec 0c             	sub    $0xc,%esp
80100fe2:	ff 73 10             	pushl  0x10(%ebx)
80100fe5:	e8 66 07 00 00       	call   80101750 <ilock>
    stati(f->ip, st);
80100fea:	58                   	pop    %eax
80100feb:	5a                   	pop    %edx
80100fec:	ff 75 0c             	pushl  0xc(%ebp)
80100fef:	ff 73 10             	pushl  0x10(%ebx)
80100ff2:	e8 09 0a 00 00       	call   80101a00 <stati>
    iunlock(f->ip);
80100ff7:	59                   	pop    %ecx
80100ff8:	ff 73 10             	pushl  0x10(%ebx)
80100ffb:	e8 30 08 00 00       	call   80101830 <iunlock>
    return 0;
80101000:	83 c4 10             	add    $0x10,%esp
80101003:	31 c0                	xor    %eax,%eax
  }
  return -1;
}
80101005:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101008:	c9                   	leave  
80101009:	c3                   	ret    
8010100a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  return -1;
80101010:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80101015:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101018:	c9                   	leave  
80101019:	c3                   	ret    
8010101a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101020 <fileread>:

// Read from file f.
int
fileread(struct file *f, char *addr, int n)
{
80101020:	55                   	push   %ebp
80101021:	89 e5                	mov    %esp,%ebp
80101023:	57                   	push   %edi
80101024:	56                   	push   %esi
80101025:	53                   	push   %ebx
80101026:	83 ec 0c             	sub    $0xc,%esp
80101029:	8b 5d 08             	mov    0x8(%ebp),%ebx
8010102c:	8b 75 0c             	mov    0xc(%ebp),%esi
8010102f:	8b 7d 10             	mov    0x10(%ebp),%edi
  int r;

  if(f->readable == 0)
80101032:	80 7b 08 00          	cmpb   $0x0,0x8(%ebx)
80101036:	74 60                	je     80101098 <fileread+0x78>
    return -1;
  if(f->type == FD_PIPE)
80101038:	8b 03                	mov    (%ebx),%eax
8010103a:	83 f8 01             	cmp    $0x1,%eax
8010103d:	74 41                	je     80101080 <fileread+0x60>
    return piperead(f->pipe, addr, n);
  if(f->type == FD_INODE){
8010103f:	83 f8 02             	cmp    $0x2,%eax
80101042:	75 5b                	jne    8010109f <fileread+0x7f>
    ilock(f->ip);
80101044:	83 ec 0c             	sub    $0xc,%esp
80101047:	ff 73 10             	pushl  0x10(%ebx)
8010104a:	e8 01 07 00 00       	call   80101750 <ilock>
    if((r = readi(f->ip, addr, f->off, n)) > 0)
8010104f:	57                   	push   %edi
80101050:	ff 73 14             	pushl  0x14(%ebx)
80101053:	56                   	push   %esi
80101054:	ff 73 10             	pushl  0x10(%ebx)
80101057:	e8 d4 09 00 00       	call   80101a30 <readi>
8010105c:	83 c4 20             	add    $0x20,%esp
8010105f:	89 c6                	mov    %eax,%esi
80101061:	85 c0                	test   %eax,%eax
80101063:	7e 03                	jle    80101068 <fileread+0x48>
      f->off += r;
80101065:	01 43 14             	add    %eax,0x14(%ebx)
    iunlock(f->ip);
80101068:	83 ec 0c             	sub    $0xc,%esp
8010106b:	ff 73 10             	pushl  0x10(%ebx)
8010106e:	e8 bd 07 00 00       	call   80101830 <iunlock>
    return r;
80101073:	83 c4 10             	add    $0x10,%esp
  }
  panic("fileread");
}
80101076:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101079:	89 f0                	mov    %esi,%eax
8010107b:	5b                   	pop    %ebx
8010107c:	5e                   	pop    %esi
8010107d:	5f                   	pop    %edi
8010107e:	5d                   	pop    %ebp
8010107f:	c3                   	ret    
    return piperead(f->pipe, addr, n);
80101080:	8b 43 0c             	mov    0xc(%ebx),%eax
80101083:	89 45 08             	mov    %eax,0x8(%ebp)
}
80101086:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101089:	5b                   	pop    %ebx
8010108a:	5e                   	pop    %esi
8010108b:	5f                   	pop    %edi
8010108c:	5d                   	pop    %ebp
    return piperead(f->pipe, addr, n);
8010108d:	e9 9e 25 00 00       	jmp    80103630 <piperead>
80101092:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    return -1;
80101098:	be ff ff ff ff       	mov    $0xffffffff,%esi
8010109d:	eb d7                	jmp    80101076 <fileread+0x56>
  panic("fileread");
8010109f:	83 ec 0c             	sub    $0xc,%esp
801010a2:	68 2f 79 10 80       	push   $0x8010792f
801010a7:	e8 e4 f2 ff ff       	call   80100390 <panic>
801010ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801010b0 <filewrite>:

//PAGEBREAK!
// Write to file f.
int
filewrite(struct file *f, char *addr, int n)
{
801010b0:	55                   	push   %ebp
801010b1:	89 e5                	mov    %esp,%ebp
801010b3:	57                   	push   %edi
801010b4:	56                   	push   %esi
801010b5:	53                   	push   %ebx
801010b6:	83 ec 1c             	sub    $0x1c,%esp
801010b9:	8b 45 0c             	mov    0xc(%ebp),%eax
801010bc:	8b 5d 08             	mov    0x8(%ebp),%ebx
801010bf:	89 45 dc             	mov    %eax,-0x24(%ebp)
801010c2:	8b 45 10             	mov    0x10(%ebp),%eax
  int r;

  if(f->writable == 0)
801010c5:	80 7b 09 00          	cmpb   $0x0,0x9(%ebx)
{
801010c9:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(f->writable == 0)
801010cc:	0f 84 bb 00 00 00    	je     8010118d <filewrite+0xdd>
    return -1;
  if(f->type == FD_PIPE)
801010d2:	8b 03                	mov    (%ebx),%eax
801010d4:	83 f8 01             	cmp    $0x1,%eax
801010d7:	0f 84 bf 00 00 00    	je     8010119c <filewrite+0xec>
    return pipewrite(f->pipe, addr, n);
  if(f->type == FD_INODE){
801010dd:	83 f8 02             	cmp    $0x2,%eax
801010e0:	0f 85 c8 00 00 00    	jne    801011ae <filewrite+0xfe>
    // and 2 blocks of slop for non-aligned writes.
    // this really belongs lower down, since writei()
    // might be writing a device like the console.
    int max = ((MAXOPBLOCKS-1-1-2) / 2) * 512;
    int i = 0;
    while(i < n){
801010e6:	8b 45 e4             	mov    -0x1c(%ebp),%eax
    int i = 0;
801010e9:	31 ff                	xor    %edi,%edi
    while(i < n){
801010eb:	85 c0                	test   %eax,%eax
801010ed:	7f 30                	jg     8010111f <filewrite+0x6f>
801010ef:	e9 94 00 00 00       	jmp    80101188 <filewrite+0xd8>
801010f4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
        n1 = max;

      begin_op();
      ilock(f->ip);
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
        f->off += r;
801010f8:	01 43 14             	add    %eax,0x14(%ebx)
      iunlock(f->ip);
801010fb:	83 ec 0c             	sub    $0xc,%esp
801010fe:	ff 73 10             	pushl  0x10(%ebx)
        f->off += r;
80101101:	89 45 e0             	mov    %eax,-0x20(%ebp)
      iunlock(f->ip);
80101104:	e8 27 07 00 00       	call   80101830 <iunlock>
      end_op();
80101109:	e8 32 1c 00 00       	call   80102d40 <end_op>

      if(r < 0)
        break;
      if(r != n1)
8010110e:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101111:	83 c4 10             	add    $0x10,%esp
80101114:	39 f0                	cmp    %esi,%eax
80101116:	75 60                	jne    80101178 <filewrite+0xc8>
        panic("short filewrite");
      i += r;
80101118:	01 c7                	add    %eax,%edi
    while(i < n){
8010111a:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
8010111d:	7e 69                	jle    80101188 <filewrite+0xd8>
      int n1 = n - i;
8010111f:	8b 75 e4             	mov    -0x1c(%ebp),%esi
80101122:	b8 00 06 00 00       	mov    $0x600,%eax
80101127:	29 fe                	sub    %edi,%esi
      if(n1 > max)
80101129:	81 fe 00 06 00 00    	cmp    $0x600,%esi
8010112f:	0f 4f f0             	cmovg  %eax,%esi
      begin_op();
80101132:	e8 99 1b 00 00       	call   80102cd0 <begin_op>
      ilock(f->ip);
80101137:	83 ec 0c             	sub    $0xc,%esp
8010113a:	ff 73 10             	pushl  0x10(%ebx)
8010113d:	e8 0e 06 00 00       	call   80101750 <ilock>
      if ((r = writei(f->ip, addr + i, f->off, n1)) > 0)
80101142:	8b 45 dc             	mov    -0x24(%ebp),%eax
80101145:	56                   	push   %esi
80101146:	ff 73 14             	pushl  0x14(%ebx)
80101149:	01 f8                	add    %edi,%eax
8010114b:	50                   	push   %eax
8010114c:	ff 73 10             	pushl  0x10(%ebx)
8010114f:	e8 dc 09 00 00       	call   80101b30 <writei>
80101154:	83 c4 20             	add    $0x20,%esp
80101157:	85 c0                	test   %eax,%eax
80101159:	7f 9d                	jg     801010f8 <filewrite+0x48>
      iunlock(f->ip);
8010115b:	83 ec 0c             	sub    $0xc,%esp
8010115e:	ff 73 10             	pushl  0x10(%ebx)
80101161:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80101164:	e8 c7 06 00 00       	call   80101830 <iunlock>
      end_op();
80101169:	e8 d2 1b 00 00       	call   80102d40 <end_op>
      if(r < 0)
8010116e:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101171:	83 c4 10             	add    $0x10,%esp
80101174:	85 c0                	test   %eax,%eax
80101176:	75 15                	jne    8010118d <filewrite+0xdd>
        panic("short filewrite");
80101178:	83 ec 0c             	sub    $0xc,%esp
8010117b:	68 38 79 10 80       	push   $0x80107938
80101180:	e8 0b f2 ff ff       	call   80100390 <panic>
80101185:	8d 76 00             	lea    0x0(%esi),%esi
    }
    return i == n ? n : -1;
80101188:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
8010118b:	74 05                	je     80101192 <filewrite+0xe2>
8010118d:	bf ff ff ff ff       	mov    $0xffffffff,%edi
  }
  panic("filewrite");
}
80101192:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101195:	89 f8                	mov    %edi,%eax
80101197:	5b                   	pop    %ebx
80101198:	5e                   	pop    %esi
80101199:	5f                   	pop    %edi
8010119a:	5d                   	pop    %ebp
8010119b:	c3                   	ret    
    return pipewrite(f->pipe, addr, n);
8010119c:	8b 43 0c             	mov    0xc(%ebx),%eax
8010119f:	89 45 08             	mov    %eax,0x8(%ebp)
}
801011a2:	8d 65 f4             	lea    -0xc(%ebp),%esp
801011a5:	5b                   	pop    %ebx
801011a6:	5e                   	pop    %esi
801011a7:	5f                   	pop    %edi
801011a8:	5d                   	pop    %ebp
    return pipewrite(f->pipe, addr, n);
801011a9:	e9 72 23 00 00       	jmp    80103520 <pipewrite>
  panic("filewrite");
801011ae:	83 ec 0c             	sub    $0xc,%esp
801011b1:	68 3e 79 10 80       	push   $0x8010793e
801011b6:	e8 d5 f1 ff ff       	call   80100390 <panic>
801011bb:	66 90                	xchg   %ax,%ax
801011bd:	66 90                	xchg   %ax,%ax
801011bf:	90                   	nop

801011c0 <balloc>:
// Blocks.

// Allocate a zeroed disk block.
static uint
balloc(uint dev)
{
801011c0:	55                   	push   %ebp
801011c1:	89 e5                	mov    %esp,%ebp
801011c3:	57                   	push   %edi
801011c4:	56                   	push   %esi
801011c5:	53                   	push   %ebx
801011c6:	83 ec 1c             	sub    $0x1c,%esp
  int b, bi, m;
  struct buf *bp;

  bp = 0;
  for(b = 0; b < sb.size; b += BPB){
801011c9:	8b 0d 80 1a 11 80    	mov    0x80111a80,%ecx
{
801011cf:	89 45 d8             	mov    %eax,-0x28(%ebp)
  for(b = 0; b < sb.size; b += BPB){
801011d2:	85 c9                	test   %ecx,%ecx
801011d4:	0f 84 87 00 00 00    	je     80101261 <balloc+0xa1>
801011da:	c7 45 dc 00 00 00 00 	movl   $0x0,-0x24(%ebp)
    bp = bread(dev, BBLOCK(b, sb));
801011e1:	8b 75 dc             	mov    -0x24(%ebp),%esi
801011e4:	83 ec 08             	sub    $0x8,%esp
801011e7:	89 f0                	mov    %esi,%eax
801011e9:	c1 f8 0c             	sar    $0xc,%eax
801011ec:	03 05 98 1a 11 80    	add    0x80111a98,%eax
801011f2:	50                   	push   %eax
801011f3:	ff 75 d8             	pushl  -0x28(%ebp)
801011f6:	e8 d5 ee ff ff       	call   801000d0 <bread>
801011fb:	83 c4 10             	add    $0x10,%esp
801011fe:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
80101201:	a1 80 1a 11 80       	mov    0x80111a80,%eax
80101206:	89 45 e0             	mov    %eax,-0x20(%ebp)
80101209:	31 c0                	xor    %eax,%eax
8010120b:	eb 2f                	jmp    8010123c <balloc+0x7c>
8010120d:	8d 76 00             	lea    0x0(%esi),%esi
      m = 1 << (bi % 8);
80101210:	89 c1                	mov    %eax,%ecx
80101212:	bb 01 00 00 00       	mov    $0x1,%ebx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
80101217:	8b 55 e4             	mov    -0x1c(%ebp),%edx
      m = 1 << (bi % 8);
8010121a:	83 e1 07             	and    $0x7,%ecx
8010121d:	d3 e3                	shl    %cl,%ebx
      if((bp->data[bi/8] & m) == 0){  // Is block free?
8010121f:	89 c1                	mov    %eax,%ecx
80101221:	c1 f9 03             	sar    $0x3,%ecx
80101224:	0f b6 7c 0a 5c       	movzbl 0x5c(%edx,%ecx,1),%edi
80101229:	89 fa                	mov    %edi,%edx
8010122b:	85 df                	test   %ebx,%edi
8010122d:	74 41                	je     80101270 <balloc+0xb0>
    for(bi = 0; bi < BPB && b + bi < sb.size; bi++){
8010122f:	83 c0 01             	add    $0x1,%eax
80101232:	83 c6 01             	add    $0x1,%esi
80101235:	3d 00 10 00 00       	cmp    $0x1000,%eax
8010123a:	74 05                	je     80101241 <balloc+0x81>
8010123c:	39 75 e0             	cmp    %esi,-0x20(%ebp)
8010123f:	77 cf                	ja     80101210 <balloc+0x50>
        brelse(bp);
        bzero(dev, b + bi);
        return b + bi;
      }
    }
    brelse(bp);
80101241:	83 ec 0c             	sub    $0xc,%esp
80101244:	ff 75 e4             	pushl  -0x1c(%ebp)
80101247:	e8 a4 ef ff ff       	call   801001f0 <brelse>
  for(b = 0; b < sb.size; b += BPB){
8010124c:	81 45 dc 00 10 00 00 	addl   $0x1000,-0x24(%ebp)
80101253:	83 c4 10             	add    $0x10,%esp
80101256:	8b 45 dc             	mov    -0x24(%ebp),%eax
80101259:	39 05 80 1a 11 80    	cmp    %eax,0x80111a80
8010125f:	77 80                	ja     801011e1 <balloc+0x21>
  }
  panic("balloc: out of blocks");
80101261:	83 ec 0c             	sub    $0xc,%esp
80101264:	68 48 79 10 80       	push   $0x80107948
80101269:	e8 22 f1 ff ff       	call   80100390 <panic>
8010126e:	66 90                	xchg   %ax,%ax
        bp->data[bi/8] |= m;  // Mark block in use.
80101270:	8b 7d e4             	mov    -0x1c(%ebp),%edi
        log_write(bp);
80101273:	83 ec 0c             	sub    $0xc,%esp
        bp->data[bi/8] |= m;  // Mark block in use.
80101276:	09 da                	or     %ebx,%edx
80101278:	88 54 0f 5c          	mov    %dl,0x5c(%edi,%ecx,1)
        log_write(bp);
8010127c:	57                   	push   %edi
8010127d:	e8 2e 1c 00 00       	call   80102eb0 <log_write>
        brelse(bp);
80101282:	89 3c 24             	mov    %edi,(%esp)
80101285:	e8 66 ef ff ff       	call   801001f0 <brelse>
  bp = bread(dev, bno);
8010128a:	58                   	pop    %eax
8010128b:	5a                   	pop    %edx
8010128c:	56                   	push   %esi
8010128d:	ff 75 d8             	pushl  -0x28(%ebp)
80101290:	e8 3b ee ff ff       	call   801000d0 <bread>
  memset(bp->data, 0, BSIZE);
80101295:	83 c4 0c             	add    $0xc,%esp
  bp = bread(dev, bno);
80101298:	89 c3                	mov    %eax,%ebx
  memset(bp->data, 0, BSIZE);
8010129a:	8d 40 5c             	lea    0x5c(%eax),%eax
8010129d:	68 00 02 00 00       	push   $0x200
801012a2:	6a 00                	push   $0x0
801012a4:	50                   	push   %eax
801012a5:	e8 a6 33 00 00       	call   80104650 <memset>
  log_write(bp);
801012aa:	89 1c 24             	mov    %ebx,(%esp)
801012ad:	e8 fe 1b 00 00       	call   80102eb0 <log_write>
  brelse(bp);
801012b2:	89 1c 24             	mov    %ebx,(%esp)
801012b5:	e8 36 ef ff ff       	call   801001f0 <brelse>
}
801012ba:	8d 65 f4             	lea    -0xc(%ebp),%esp
801012bd:	89 f0                	mov    %esi,%eax
801012bf:	5b                   	pop    %ebx
801012c0:	5e                   	pop    %esi
801012c1:	5f                   	pop    %edi
801012c2:	5d                   	pop    %ebp
801012c3:	c3                   	ret    
801012c4:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801012cb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801012cf:	90                   	nop

801012d0 <iget>:
// Find the inode with number inum on device dev
// and return the in-memory copy. Does not lock
// the inode and does not read it from disk.
static struct inode*
iget(uint dev, uint inum)
{
801012d0:	55                   	push   %ebp
801012d1:	89 e5                	mov    %esp,%ebp
801012d3:	57                   	push   %edi
801012d4:	89 c7                	mov    %eax,%edi
801012d6:	56                   	push   %esi
  struct inode *ip, *empty;

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
801012d7:	31 f6                	xor    %esi,%esi
{
801012d9:	53                   	push   %ebx
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
801012da:	bb d4 1a 11 80       	mov    $0x80111ad4,%ebx
{
801012df:	83 ec 28             	sub    $0x28,%esp
801012e2:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  acquire(&icache.lock);
801012e5:	68 a0 1a 11 80       	push   $0x80111aa0
801012ea:	e8 51 32 00 00       	call   80104540 <acquire>
801012ef:	83 c4 10             	add    $0x10,%esp
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
801012f2:	8b 55 e4             	mov    -0x1c(%ebp),%edx
801012f5:	eb 1b                	jmp    80101312 <iget+0x42>
801012f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801012fe:	66 90                	xchg   %ax,%ax
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101300:	39 3b                	cmp    %edi,(%ebx)
80101302:	74 6c                	je     80101370 <iget+0xa0>
80101304:	81 c3 90 00 00 00    	add    $0x90,%ebx
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010130a:	81 fb f4 36 11 80    	cmp    $0x801136f4,%ebx
80101310:	73 26                	jae    80101338 <iget+0x68>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101312:	8b 4b 08             	mov    0x8(%ebx),%ecx
80101315:	85 c9                	test   %ecx,%ecx
80101317:	7f e7                	jg     80101300 <iget+0x30>
      ip->ref++;
      release(&icache.lock);
      return ip;
    }
    if(empty == 0 && ip->ref == 0)    // Remember empty slot.
80101319:	85 f6                	test   %esi,%esi
8010131b:	75 e7                	jne    80101304 <iget+0x34>
8010131d:	8d 83 90 00 00 00    	lea    0x90(%ebx),%eax
80101323:	85 c9                	test   %ecx,%ecx
80101325:	75 70                	jne    80101397 <iget+0xc7>
80101327:	89 de                	mov    %ebx,%esi
80101329:	89 c3                	mov    %eax,%ebx
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
8010132b:	81 fb f4 36 11 80    	cmp    $0x801136f4,%ebx
80101331:	72 df                	jb     80101312 <iget+0x42>
80101333:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101337:	90                   	nop
      empty = ip;
  }

  // Recycle an inode cache entry.
  if(empty == 0)
80101338:	85 f6                	test   %esi,%esi
8010133a:	74 74                	je     801013b0 <iget+0xe0>
  ip = empty;
  ip->dev = dev;
  ip->inum = inum;
  ip->ref = 1;
  ip->valid = 0;
  release(&icache.lock);
8010133c:	83 ec 0c             	sub    $0xc,%esp
  ip->dev = dev;
8010133f:	89 3e                	mov    %edi,(%esi)
  ip->inum = inum;
80101341:	89 56 04             	mov    %edx,0x4(%esi)
  ip->ref = 1;
80101344:	c7 46 08 01 00 00 00 	movl   $0x1,0x8(%esi)
  ip->valid = 0;
8010134b:	c7 46 4c 00 00 00 00 	movl   $0x0,0x4c(%esi)
  release(&icache.lock);
80101352:	68 a0 1a 11 80       	push   $0x80111aa0
80101357:	e8 a4 32 00 00       	call   80104600 <release>

  return ip;
8010135c:	83 c4 10             	add    $0x10,%esp
}
8010135f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101362:	89 f0                	mov    %esi,%eax
80101364:	5b                   	pop    %ebx
80101365:	5e                   	pop    %esi
80101366:	5f                   	pop    %edi
80101367:	5d                   	pop    %ebp
80101368:	c3                   	ret    
80101369:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
80101370:	39 53 04             	cmp    %edx,0x4(%ebx)
80101373:	75 8f                	jne    80101304 <iget+0x34>
      release(&icache.lock);
80101375:	83 ec 0c             	sub    $0xc,%esp
      ip->ref++;
80101378:	83 c1 01             	add    $0x1,%ecx
      return ip;
8010137b:	89 de                	mov    %ebx,%esi
      ip->ref++;
8010137d:	89 4b 08             	mov    %ecx,0x8(%ebx)
      release(&icache.lock);
80101380:	68 a0 1a 11 80       	push   $0x80111aa0
80101385:	e8 76 32 00 00       	call   80104600 <release>
      return ip;
8010138a:	83 c4 10             	add    $0x10,%esp
}
8010138d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101390:	89 f0                	mov    %esi,%eax
80101392:	5b                   	pop    %ebx
80101393:	5e                   	pop    %esi
80101394:	5f                   	pop    %edi
80101395:	5d                   	pop    %ebp
80101396:	c3                   	ret    
  for(ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++){
80101397:	3d f4 36 11 80       	cmp    $0x801136f4,%eax
8010139c:	73 12                	jae    801013b0 <iget+0xe0>
    if(ip->ref > 0 && ip->dev == dev && ip->inum == inum){
8010139e:	8b 48 08             	mov    0x8(%eax),%ecx
801013a1:	89 c3                	mov    %eax,%ebx
801013a3:	85 c9                	test   %ecx,%ecx
801013a5:	0f 8f 55 ff ff ff    	jg     80101300 <iget+0x30>
801013ab:	e9 6d ff ff ff       	jmp    8010131d <iget+0x4d>
    panic("iget: no inodes");
801013b0:	83 ec 0c             	sub    $0xc,%esp
801013b3:	68 5e 79 10 80       	push   $0x8010795e
801013b8:	e8 d3 ef ff ff       	call   80100390 <panic>
801013bd:	8d 76 00             	lea    0x0(%esi),%esi

801013c0 <bmap>:

// Return the disk block address of the nth block in inode ip.
// If there is no such block, bmap allocates one.
static uint
bmap(struct inode *ip, uint bn)
{
801013c0:	55                   	push   %ebp
801013c1:	89 e5                	mov    %esp,%ebp
801013c3:	57                   	push   %edi
801013c4:	56                   	push   %esi
801013c5:	89 c6                	mov    %eax,%esi
801013c7:	53                   	push   %ebx
801013c8:	83 ec 1c             	sub    $0x1c,%esp
  uint addr, *a;
  struct buf *bp;

  if(bn < NDIRECT){
801013cb:	83 fa 0b             	cmp    $0xb,%edx
801013ce:	0f 86 84 00 00 00    	jbe    80101458 <bmap+0x98>
    if((addr = ip->addrs[bn]) == 0)
      ip->addrs[bn] = addr = balloc(ip->dev);
    return addr;
  }
  bn -= NDIRECT;
801013d4:	8d 5a f4             	lea    -0xc(%edx),%ebx

  if(bn < NINDIRECT){
801013d7:	83 fb 7f             	cmp    $0x7f,%ebx
801013da:	0f 87 98 00 00 00    	ja     80101478 <bmap+0xb8>
    // Load indirect block, allocating if necessary.
    if((addr = ip->addrs[NDIRECT]) == 0)
801013e0:	8b 90 8c 00 00 00    	mov    0x8c(%eax),%edx
801013e6:	8b 00                	mov    (%eax),%eax
801013e8:	85 d2                	test   %edx,%edx
801013ea:	74 54                	je     80101440 <bmap+0x80>
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
    bp = bread(ip->dev, addr);
801013ec:	83 ec 08             	sub    $0x8,%esp
801013ef:	52                   	push   %edx
801013f0:	50                   	push   %eax
801013f1:	e8 da ec ff ff       	call   801000d0 <bread>
    a = (uint*)bp->data;
    if((addr = a[bn]) == 0){
801013f6:	83 c4 10             	add    $0x10,%esp
801013f9:	8d 54 98 5c          	lea    0x5c(%eax,%ebx,4),%edx
    bp = bread(ip->dev, addr);
801013fd:	89 c7                	mov    %eax,%edi
    if((addr = a[bn]) == 0){
801013ff:	8b 1a                	mov    (%edx),%ebx
80101401:	85 db                	test   %ebx,%ebx
80101403:	74 1b                	je     80101420 <bmap+0x60>
      a[bn] = addr = balloc(ip->dev);
      log_write(bp);
    }
    brelse(bp);
80101405:	83 ec 0c             	sub    $0xc,%esp
80101408:	57                   	push   %edi
80101409:	e8 e2 ed ff ff       	call   801001f0 <brelse>
    return addr;
8010140e:	83 c4 10             	add    $0x10,%esp
  }

  panic("bmap: out of range");
}
80101411:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101414:	89 d8                	mov    %ebx,%eax
80101416:	5b                   	pop    %ebx
80101417:	5e                   	pop    %esi
80101418:	5f                   	pop    %edi
80101419:	5d                   	pop    %ebp
8010141a:	c3                   	ret    
8010141b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010141f:	90                   	nop
      a[bn] = addr = balloc(ip->dev);
80101420:	8b 06                	mov    (%esi),%eax
80101422:	89 55 e4             	mov    %edx,-0x1c(%ebp)
80101425:	e8 96 fd ff ff       	call   801011c0 <balloc>
8010142a:	8b 55 e4             	mov    -0x1c(%ebp),%edx
      log_write(bp);
8010142d:	83 ec 0c             	sub    $0xc,%esp
      a[bn] = addr = balloc(ip->dev);
80101430:	89 c3                	mov    %eax,%ebx
80101432:	89 02                	mov    %eax,(%edx)
      log_write(bp);
80101434:	57                   	push   %edi
80101435:	e8 76 1a 00 00       	call   80102eb0 <log_write>
8010143a:	83 c4 10             	add    $0x10,%esp
8010143d:	eb c6                	jmp    80101405 <bmap+0x45>
8010143f:	90                   	nop
      ip->addrs[NDIRECT] = addr = balloc(ip->dev);
80101440:	e8 7b fd ff ff       	call   801011c0 <balloc>
80101445:	89 c2                	mov    %eax,%edx
80101447:	89 86 8c 00 00 00    	mov    %eax,0x8c(%esi)
8010144d:	8b 06                	mov    (%esi),%eax
8010144f:	eb 9b                	jmp    801013ec <bmap+0x2c>
80101451:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if((addr = ip->addrs[bn]) == 0)
80101458:	8d 3c 90             	lea    (%eax,%edx,4),%edi
8010145b:	8b 5f 5c             	mov    0x5c(%edi),%ebx
8010145e:	85 db                	test   %ebx,%ebx
80101460:	75 af                	jne    80101411 <bmap+0x51>
      ip->addrs[bn] = addr = balloc(ip->dev);
80101462:	8b 00                	mov    (%eax),%eax
80101464:	e8 57 fd ff ff       	call   801011c0 <balloc>
80101469:	89 47 5c             	mov    %eax,0x5c(%edi)
8010146c:	89 c3                	mov    %eax,%ebx
}
8010146e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101471:	89 d8                	mov    %ebx,%eax
80101473:	5b                   	pop    %ebx
80101474:	5e                   	pop    %esi
80101475:	5f                   	pop    %edi
80101476:	5d                   	pop    %ebp
80101477:	c3                   	ret    
  panic("bmap: out of range");
80101478:	83 ec 0c             	sub    $0xc,%esp
8010147b:	68 6e 79 10 80       	push   $0x8010796e
80101480:	e8 0b ef ff ff       	call   80100390 <panic>
80101485:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010148c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101490 <readsb>:
{
80101490:	55                   	push   %ebp
80101491:	89 e5                	mov    %esp,%ebp
80101493:	56                   	push   %esi
80101494:	53                   	push   %ebx
80101495:	8b 75 0c             	mov    0xc(%ebp),%esi
  bp = bread(dev, 1);
80101498:	83 ec 08             	sub    $0x8,%esp
8010149b:	6a 01                	push   $0x1
8010149d:	ff 75 08             	pushl  0x8(%ebp)
801014a0:	e8 2b ec ff ff       	call   801000d0 <bread>
  memmove(sb, bp->data, sizeof(*sb));
801014a5:	83 c4 0c             	add    $0xc,%esp
  bp = bread(dev, 1);
801014a8:	89 c3                	mov    %eax,%ebx
  memmove(sb, bp->data, sizeof(*sb));
801014aa:	8d 40 5c             	lea    0x5c(%eax),%eax
801014ad:	6a 1c                	push   $0x1c
801014af:	50                   	push   %eax
801014b0:	56                   	push   %esi
801014b1:	e8 3a 32 00 00       	call   801046f0 <memmove>
  brelse(bp);
801014b6:	89 5d 08             	mov    %ebx,0x8(%ebp)
801014b9:	83 c4 10             	add    $0x10,%esp
}
801014bc:	8d 65 f8             	lea    -0x8(%ebp),%esp
801014bf:	5b                   	pop    %ebx
801014c0:	5e                   	pop    %esi
801014c1:	5d                   	pop    %ebp
  brelse(bp);
801014c2:	e9 29 ed ff ff       	jmp    801001f0 <brelse>
801014c7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801014ce:	66 90                	xchg   %ax,%ax

801014d0 <bfree>:
{
801014d0:	55                   	push   %ebp
801014d1:	89 e5                	mov    %esp,%ebp
801014d3:	56                   	push   %esi
801014d4:	89 c6                	mov    %eax,%esi
801014d6:	53                   	push   %ebx
801014d7:	89 d3                	mov    %edx,%ebx
  readsb(dev, &sb);
801014d9:	83 ec 08             	sub    $0x8,%esp
801014dc:	68 80 1a 11 80       	push   $0x80111a80
801014e1:	50                   	push   %eax
801014e2:	e8 a9 ff ff ff       	call   80101490 <readsb>
  bp = bread(dev, BBLOCK(b, sb));
801014e7:	58                   	pop    %eax
801014e8:	5a                   	pop    %edx
801014e9:	89 da                	mov    %ebx,%edx
801014eb:	c1 ea 0c             	shr    $0xc,%edx
801014ee:	03 15 98 1a 11 80    	add    0x80111a98,%edx
801014f4:	52                   	push   %edx
801014f5:	56                   	push   %esi
801014f6:	e8 d5 eb ff ff       	call   801000d0 <bread>
  m = 1 << (bi % 8);
801014fb:	89 d9                	mov    %ebx,%ecx
  if((bp->data[bi/8] & m) == 0)
801014fd:	c1 fb 03             	sar    $0x3,%ebx
  m = 1 << (bi % 8);
80101500:	ba 01 00 00 00       	mov    $0x1,%edx
80101505:	83 e1 07             	and    $0x7,%ecx
  if((bp->data[bi/8] & m) == 0)
80101508:	81 e3 ff 01 00 00    	and    $0x1ff,%ebx
8010150e:	83 c4 10             	add    $0x10,%esp
  m = 1 << (bi % 8);
80101511:	d3 e2                	shl    %cl,%edx
  if((bp->data[bi/8] & m) == 0)
80101513:	0f b6 4c 18 5c       	movzbl 0x5c(%eax,%ebx,1),%ecx
80101518:	85 d1                	test   %edx,%ecx
8010151a:	74 25                	je     80101541 <bfree+0x71>
  bp->data[bi/8] &= ~m;
8010151c:	f7 d2                	not    %edx
8010151e:	89 c6                	mov    %eax,%esi
  log_write(bp);
80101520:	83 ec 0c             	sub    $0xc,%esp
  bp->data[bi/8] &= ~m;
80101523:	21 ca                	and    %ecx,%edx
80101525:	88 54 1e 5c          	mov    %dl,0x5c(%esi,%ebx,1)
  log_write(bp);
80101529:	56                   	push   %esi
8010152a:	e8 81 19 00 00       	call   80102eb0 <log_write>
  brelse(bp);
8010152f:	89 34 24             	mov    %esi,(%esp)
80101532:	e8 b9 ec ff ff       	call   801001f0 <brelse>
}
80101537:	83 c4 10             	add    $0x10,%esp
8010153a:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010153d:	5b                   	pop    %ebx
8010153e:	5e                   	pop    %esi
8010153f:	5d                   	pop    %ebp
80101540:	c3                   	ret    
    panic("freeing free block");
80101541:	83 ec 0c             	sub    $0xc,%esp
80101544:	68 81 79 10 80       	push   $0x80107981
80101549:	e8 42 ee ff ff       	call   80100390 <panic>
8010154e:	66 90                	xchg   %ax,%ax

80101550 <iinit>:
{
80101550:	55                   	push   %ebp
80101551:	89 e5                	mov    %esp,%ebp
80101553:	53                   	push   %ebx
80101554:	bb e0 1a 11 80       	mov    $0x80111ae0,%ebx
80101559:	83 ec 0c             	sub    $0xc,%esp
  initlock(&icache.lock, "icache");
8010155c:	68 94 79 10 80       	push   $0x80107994
80101561:	68 a0 1a 11 80       	push   $0x80111aa0
80101566:	e8 75 2e 00 00       	call   801043e0 <initlock>
  for(i = 0; i < NINODE; i++) {
8010156b:	83 c4 10             	add    $0x10,%esp
8010156e:	66 90                	xchg   %ax,%ax
    initsleeplock(&icache.inode[i].lock, "inode");
80101570:	83 ec 08             	sub    $0x8,%esp
80101573:	68 9b 79 10 80       	push   $0x8010799b
80101578:	53                   	push   %ebx
80101579:	81 c3 90 00 00 00    	add    $0x90,%ebx
8010157f:	e8 2c 2d 00 00       	call   801042b0 <initsleeplock>
  for(i = 0; i < NINODE; i++) {
80101584:	83 c4 10             	add    $0x10,%esp
80101587:	81 fb 00 37 11 80    	cmp    $0x80113700,%ebx
8010158d:	75 e1                	jne    80101570 <iinit+0x20>
  readsb(dev, &sb);
8010158f:	83 ec 08             	sub    $0x8,%esp
80101592:	68 80 1a 11 80       	push   $0x80111a80
80101597:	ff 75 08             	pushl  0x8(%ebp)
8010159a:	e8 f1 fe ff ff       	call   80101490 <readsb>
  cprintf("sb: size %d nblocks %d ninodes %d nlog %d logstart %d\
8010159f:	ff 35 98 1a 11 80    	pushl  0x80111a98
801015a5:	ff 35 94 1a 11 80    	pushl  0x80111a94
801015ab:	ff 35 90 1a 11 80    	pushl  0x80111a90
801015b1:	ff 35 8c 1a 11 80    	pushl  0x80111a8c
801015b7:	ff 35 88 1a 11 80    	pushl  0x80111a88
801015bd:	ff 35 84 1a 11 80    	pushl  0x80111a84
801015c3:	ff 35 80 1a 11 80    	pushl  0x80111a80
801015c9:	68 00 7a 10 80       	push   $0x80107a00
801015ce:	e8 dd f0 ff ff       	call   801006b0 <cprintf>
}
801015d3:	83 c4 30             	add    $0x30,%esp
801015d6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801015d9:	c9                   	leave  
801015da:	c3                   	ret    
801015db:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801015df:	90                   	nop

801015e0 <ialloc>:
{
801015e0:	55                   	push   %ebp
801015e1:	89 e5                	mov    %esp,%ebp
801015e3:	57                   	push   %edi
801015e4:	56                   	push   %esi
801015e5:	53                   	push   %ebx
801015e6:	83 ec 1c             	sub    $0x1c,%esp
801015e9:	8b 45 0c             	mov    0xc(%ebp),%eax
  for(inum = 1; inum < sb.ninodes; inum++){
801015ec:	83 3d 88 1a 11 80 01 	cmpl   $0x1,0x80111a88
{
801015f3:	8b 75 08             	mov    0x8(%ebp),%esi
801015f6:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  for(inum = 1; inum < sb.ninodes; inum++){
801015f9:	0f 86 91 00 00 00    	jbe    80101690 <ialloc+0xb0>
801015ff:	bb 01 00 00 00       	mov    $0x1,%ebx
80101604:	eb 21                	jmp    80101627 <ialloc+0x47>
80101606:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010160d:	8d 76 00             	lea    0x0(%esi),%esi
    brelse(bp);
80101610:	83 ec 0c             	sub    $0xc,%esp
  for(inum = 1; inum < sb.ninodes; inum++){
80101613:	83 c3 01             	add    $0x1,%ebx
    brelse(bp);
80101616:	57                   	push   %edi
80101617:	e8 d4 eb ff ff       	call   801001f0 <brelse>
  for(inum = 1; inum < sb.ninodes; inum++){
8010161c:	83 c4 10             	add    $0x10,%esp
8010161f:	3b 1d 88 1a 11 80    	cmp    0x80111a88,%ebx
80101625:	73 69                	jae    80101690 <ialloc+0xb0>
    bp = bread(dev, IBLOCK(inum, sb));
80101627:	89 d8                	mov    %ebx,%eax
80101629:	83 ec 08             	sub    $0x8,%esp
8010162c:	c1 e8 03             	shr    $0x3,%eax
8010162f:	03 05 94 1a 11 80    	add    0x80111a94,%eax
80101635:	50                   	push   %eax
80101636:	56                   	push   %esi
80101637:	e8 94 ea ff ff       	call   801000d0 <bread>
    if(dip->type == 0){  // a free inode
8010163c:	83 c4 10             	add    $0x10,%esp
    bp = bread(dev, IBLOCK(inum, sb));
8010163f:	89 c7                	mov    %eax,%edi
    dip = (struct dinode*)bp->data + inum%IPB;
80101641:	89 d8                	mov    %ebx,%eax
80101643:	83 e0 07             	and    $0x7,%eax
80101646:	c1 e0 06             	shl    $0x6,%eax
80101649:	8d 4c 07 5c          	lea    0x5c(%edi,%eax,1),%ecx
    if(dip->type == 0){  // a free inode
8010164d:	66 83 39 00          	cmpw   $0x0,(%ecx)
80101651:	75 bd                	jne    80101610 <ialloc+0x30>
      memset(dip, 0, sizeof(*dip));
80101653:	83 ec 04             	sub    $0x4,%esp
80101656:	89 4d e0             	mov    %ecx,-0x20(%ebp)
80101659:	6a 40                	push   $0x40
8010165b:	6a 00                	push   $0x0
8010165d:	51                   	push   %ecx
8010165e:	e8 ed 2f 00 00       	call   80104650 <memset>
      dip->type = type;
80101663:	0f b7 45 e4          	movzwl -0x1c(%ebp),%eax
80101667:	8b 4d e0             	mov    -0x20(%ebp),%ecx
8010166a:	66 89 01             	mov    %ax,(%ecx)
      log_write(bp);   // mark it allocated on the disk
8010166d:	89 3c 24             	mov    %edi,(%esp)
80101670:	e8 3b 18 00 00       	call   80102eb0 <log_write>
      brelse(bp);
80101675:	89 3c 24             	mov    %edi,(%esp)
80101678:	e8 73 eb ff ff       	call   801001f0 <brelse>
      return iget(dev, inum);
8010167d:	83 c4 10             	add    $0x10,%esp
}
80101680:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return iget(dev, inum);
80101683:	89 da                	mov    %ebx,%edx
80101685:	89 f0                	mov    %esi,%eax
}
80101687:	5b                   	pop    %ebx
80101688:	5e                   	pop    %esi
80101689:	5f                   	pop    %edi
8010168a:	5d                   	pop    %ebp
      return iget(dev, inum);
8010168b:	e9 40 fc ff ff       	jmp    801012d0 <iget>
  panic("ialloc: no inodes");
80101690:	83 ec 0c             	sub    $0xc,%esp
80101693:	68 a1 79 10 80       	push   $0x801079a1
80101698:	e8 f3 ec ff ff       	call   80100390 <panic>
8010169d:	8d 76 00             	lea    0x0(%esi),%esi

801016a0 <iupdate>:
{
801016a0:	55                   	push   %ebp
801016a1:	89 e5                	mov    %esp,%ebp
801016a3:	56                   	push   %esi
801016a4:	53                   	push   %ebx
801016a5:	8b 5d 08             	mov    0x8(%ebp),%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801016a8:	8b 43 04             	mov    0x4(%ebx),%eax
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801016ab:	83 c3 5c             	add    $0x5c,%ebx
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801016ae:	83 ec 08             	sub    $0x8,%esp
801016b1:	c1 e8 03             	shr    $0x3,%eax
801016b4:	03 05 94 1a 11 80    	add    0x80111a94,%eax
801016ba:	50                   	push   %eax
801016bb:	ff 73 a4             	pushl  -0x5c(%ebx)
801016be:	e8 0d ea ff ff       	call   801000d0 <bread>
  dip->type = ip->type;
801016c3:	0f b7 53 f4          	movzwl -0xc(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801016c7:	83 c4 0c             	add    $0xc,%esp
  bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801016ca:	89 c6                	mov    %eax,%esi
  dip = (struct dinode*)bp->data + ip->inum%IPB;
801016cc:	8b 43 a8             	mov    -0x58(%ebx),%eax
801016cf:	83 e0 07             	and    $0x7,%eax
801016d2:	c1 e0 06             	shl    $0x6,%eax
801016d5:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
  dip->type = ip->type;
801016d9:	66 89 10             	mov    %dx,(%eax)
  dip->major = ip->major;
801016dc:	0f b7 53 f6          	movzwl -0xa(%ebx),%edx
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801016e0:	83 c0 0c             	add    $0xc,%eax
  dip->major = ip->major;
801016e3:	66 89 50 f6          	mov    %dx,-0xa(%eax)
  dip->minor = ip->minor;
801016e7:	0f b7 53 f8          	movzwl -0x8(%ebx),%edx
801016eb:	66 89 50 f8          	mov    %dx,-0x8(%eax)
  dip->nlink = ip->nlink;
801016ef:	0f b7 53 fa          	movzwl -0x6(%ebx),%edx
801016f3:	66 89 50 fa          	mov    %dx,-0x6(%eax)
  dip->size = ip->size;
801016f7:	8b 53 fc             	mov    -0x4(%ebx),%edx
801016fa:	89 50 fc             	mov    %edx,-0x4(%eax)
  memmove(dip->addrs, ip->addrs, sizeof(ip->addrs));
801016fd:	6a 34                	push   $0x34
801016ff:	53                   	push   %ebx
80101700:	50                   	push   %eax
80101701:	e8 ea 2f 00 00       	call   801046f0 <memmove>
  log_write(bp);
80101706:	89 34 24             	mov    %esi,(%esp)
80101709:	e8 a2 17 00 00       	call   80102eb0 <log_write>
  brelse(bp);
8010170e:	89 75 08             	mov    %esi,0x8(%ebp)
80101711:	83 c4 10             	add    $0x10,%esp
}
80101714:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101717:	5b                   	pop    %ebx
80101718:	5e                   	pop    %esi
80101719:	5d                   	pop    %ebp
  brelse(bp);
8010171a:	e9 d1 ea ff ff       	jmp    801001f0 <brelse>
8010171f:	90                   	nop

80101720 <idup>:
{
80101720:	55                   	push   %ebp
80101721:	89 e5                	mov    %esp,%ebp
80101723:	53                   	push   %ebx
80101724:	83 ec 10             	sub    $0x10,%esp
80101727:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&icache.lock);
8010172a:	68 a0 1a 11 80       	push   $0x80111aa0
8010172f:	e8 0c 2e 00 00       	call   80104540 <acquire>
  ip->ref++;
80101734:	83 43 08 01          	addl   $0x1,0x8(%ebx)
  release(&icache.lock);
80101738:	c7 04 24 a0 1a 11 80 	movl   $0x80111aa0,(%esp)
8010173f:	e8 bc 2e 00 00       	call   80104600 <release>
}
80101744:	89 d8                	mov    %ebx,%eax
80101746:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80101749:	c9                   	leave  
8010174a:	c3                   	ret    
8010174b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010174f:	90                   	nop

80101750 <ilock>:
{
80101750:	55                   	push   %ebp
80101751:	89 e5                	mov    %esp,%ebp
80101753:	56                   	push   %esi
80101754:	53                   	push   %ebx
80101755:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || ip->ref < 1)
80101758:	85 db                	test   %ebx,%ebx
8010175a:	0f 84 b7 00 00 00    	je     80101817 <ilock+0xc7>
80101760:	8b 53 08             	mov    0x8(%ebx),%edx
80101763:	85 d2                	test   %edx,%edx
80101765:	0f 8e ac 00 00 00    	jle    80101817 <ilock+0xc7>
  acquiresleep(&ip->lock);
8010176b:	83 ec 0c             	sub    $0xc,%esp
8010176e:	8d 43 0c             	lea    0xc(%ebx),%eax
80101771:	50                   	push   %eax
80101772:	e8 79 2b 00 00       	call   801042f0 <acquiresleep>
  if(ip->valid == 0){
80101777:	8b 43 4c             	mov    0x4c(%ebx),%eax
8010177a:	83 c4 10             	add    $0x10,%esp
8010177d:	85 c0                	test   %eax,%eax
8010177f:	74 0f                	je     80101790 <ilock+0x40>
}
80101781:	8d 65 f8             	lea    -0x8(%ebp),%esp
80101784:	5b                   	pop    %ebx
80101785:	5e                   	pop    %esi
80101786:	5d                   	pop    %ebp
80101787:	c3                   	ret    
80101788:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010178f:	90                   	nop
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
80101790:	8b 43 04             	mov    0x4(%ebx),%eax
80101793:	83 ec 08             	sub    $0x8,%esp
80101796:	c1 e8 03             	shr    $0x3,%eax
80101799:	03 05 94 1a 11 80    	add    0x80111a94,%eax
8010179f:	50                   	push   %eax
801017a0:	ff 33                	pushl  (%ebx)
801017a2:	e8 29 e9 ff ff       	call   801000d0 <bread>
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801017a7:	83 c4 0c             	add    $0xc,%esp
    bp = bread(ip->dev, IBLOCK(ip->inum, sb));
801017aa:	89 c6                	mov    %eax,%esi
    dip = (struct dinode*)bp->data + ip->inum%IPB;
801017ac:	8b 43 04             	mov    0x4(%ebx),%eax
801017af:	83 e0 07             	and    $0x7,%eax
801017b2:	c1 e0 06             	shl    $0x6,%eax
801017b5:	8d 44 06 5c          	lea    0x5c(%esi,%eax,1),%eax
    ip->type = dip->type;
801017b9:	0f b7 10             	movzwl (%eax),%edx
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801017bc:	83 c0 0c             	add    $0xc,%eax
    ip->type = dip->type;
801017bf:	66 89 53 50          	mov    %dx,0x50(%ebx)
    ip->major = dip->major;
801017c3:	0f b7 50 f6          	movzwl -0xa(%eax),%edx
801017c7:	66 89 53 52          	mov    %dx,0x52(%ebx)
    ip->minor = dip->minor;
801017cb:	0f b7 50 f8          	movzwl -0x8(%eax),%edx
801017cf:	66 89 53 54          	mov    %dx,0x54(%ebx)
    ip->nlink = dip->nlink;
801017d3:	0f b7 50 fa          	movzwl -0x6(%eax),%edx
801017d7:	66 89 53 56          	mov    %dx,0x56(%ebx)
    ip->size = dip->size;
801017db:	8b 50 fc             	mov    -0x4(%eax),%edx
801017de:	89 53 58             	mov    %edx,0x58(%ebx)
    memmove(ip->addrs, dip->addrs, sizeof(ip->addrs));
801017e1:	6a 34                	push   $0x34
801017e3:	50                   	push   %eax
801017e4:	8d 43 5c             	lea    0x5c(%ebx),%eax
801017e7:	50                   	push   %eax
801017e8:	e8 03 2f 00 00       	call   801046f0 <memmove>
    brelse(bp);
801017ed:	89 34 24             	mov    %esi,(%esp)
801017f0:	e8 fb e9 ff ff       	call   801001f0 <brelse>
    if(ip->type == 0)
801017f5:	83 c4 10             	add    $0x10,%esp
801017f8:	66 83 7b 50 00       	cmpw   $0x0,0x50(%ebx)
    ip->valid = 1;
801017fd:	c7 43 4c 01 00 00 00 	movl   $0x1,0x4c(%ebx)
    if(ip->type == 0)
80101804:	0f 85 77 ff ff ff    	jne    80101781 <ilock+0x31>
      panic("ilock: no type");
8010180a:	83 ec 0c             	sub    $0xc,%esp
8010180d:	68 b9 79 10 80       	push   $0x801079b9
80101812:	e8 79 eb ff ff       	call   80100390 <panic>
    panic("ilock");
80101817:	83 ec 0c             	sub    $0xc,%esp
8010181a:	68 b3 79 10 80       	push   $0x801079b3
8010181f:	e8 6c eb ff ff       	call   80100390 <panic>
80101824:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010182b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010182f:	90                   	nop

80101830 <iunlock>:
{
80101830:	55                   	push   %ebp
80101831:	89 e5                	mov    %esp,%ebp
80101833:	56                   	push   %esi
80101834:	53                   	push   %ebx
80101835:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(ip == 0 || !holdingsleep(&ip->lock) || ip->ref < 1)
80101838:	85 db                	test   %ebx,%ebx
8010183a:	74 28                	je     80101864 <iunlock+0x34>
8010183c:	83 ec 0c             	sub    $0xc,%esp
8010183f:	8d 73 0c             	lea    0xc(%ebx),%esi
80101842:	56                   	push   %esi
80101843:	e8 48 2b 00 00       	call   80104390 <holdingsleep>
80101848:	83 c4 10             	add    $0x10,%esp
8010184b:	85 c0                	test   %eax,%eax
8010184d:	74 15                	je     80101864 <iunlock+0x34>
8010184f:	8b 43 08             	mov    0x8(%ebx),%eax
80101852:	85 c0                	test   %eax,%eax
80101854:	7e 0e                	jle    80101864 <iunlock+0x34>
  releasesleep(&ip->lock);
80101856:	89 75 08             	mov    %esi,0x8(%ebp)
}
80101859:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010185c:	5b                   	pop    %ebx
8010185d:	5e                   	pop    %esi
8010185e:	5d                   	pop    %ebp
  releasesleep(&ip->lock);
8010185f:	e9 ec 2a 00 00       	jmp    80104350 <releasesleep>
    panic("iunlock");
80101864:	83 ec 0c             	sub    $0xc,%esp
80101867:	68 c8 79 10 80       	push   $0x801079c8
8010186c:	e8 1f eb ff ff       	call   80100390 <panic>
80101871:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101878:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010187f:	90                   	nop

80101880 <iput>:
{
80101880:	55                   	push   %ebp
80101881:	89 e5                	mov    %esp,%ebp
80101883:	57                   	push   %edi
80101884:	56                   	push   %esi
80101885:	53                   	push   %ebx
80101886:	83 ec 28             	sub    $0x28,%esp
80101889:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquiresleep(&ip->lock);
8010188c:	8d 7b 0c             	lea    0xc(%ebx),%edi
8010188f:	57                   	push   %edi
80101890:	e8 5b 2a 00 00       	call   801042f0 <acquiresleep>
  if(ip->valid && ip->nlink == 0){
80101895:	8b 53 4c             	mov    0x4c(%ebx),%edx
80101898:	83 c4 10             	add    $0x10,%esp
8010189b:	85 d2                	test   %edx,%edx
8010189d:	74 07                	je     801018a6 <iput+0x26>
8010189f:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
801018a4:	74 32                	je     801018d8 <iput+0x58>
  releasesleep(&ip->lock);
801018a6:	83 ec 0c             	sub    $0xc,%esp
801018a9:	57                   	push   %edi
801018aa:	e8 a1 2a 00 00       	call   80104350 <releasesleep>
  acquire(&icache.lock);
801018af:	c7 04 24 a0 1a 11 80 	movl   $0x80111aa0,(%esp)
801018b6:	e8 85 2c 00 00       	call   80104540 <acquire>
  ip->ref--;
801018bb:	83 6b 08 01          	subl   $0x1,0x8(%ebx)
  release(&icache.lock);
801018bf:	83 c4 10             	add    $0x10,%esp
801018c2:	c7 45 08 a0 1a 11 80 	movl   $0x80111aa0,0x8(%ebp)
}
801018c9:	8d 65 f4             	lea    -0xc(%ebp),%esp
801018cc:	5b                   	pop    %ebx
801018cd:	5e                   	pop    %esi
801018ce:	5f                   	pop    %edi
801018cf:	5d                   	pop    %ebp
  release(&icache.lock);
801018d0:	e9 2b 2d 00 00       	jmp    80104600 <release>
801018d5:	8d 76 00             	lea    0x0(%esi),%esi
    acquire(&icache.lock);
801018d8:	83 ec 0c             	sub    $0xc,%esp
801018db:	68 a0 1a 11 80       	push   $0x80111aa0
801018e0:	e8 5b 2c 00 00       	call   80104540 <acquire>
    int r = ip->ref;
801018e5:	8b 73 08             	mov    0x8(%ebx),%esi
    release(&icache.lock);
801018e8:	c7 04 24 a0 1a 11 80 	movl   $0x80111aa0,(%esp)
801018ef:	e8 0c 2d 00 00       	call   80104600 <release>
    if(r == 1){
801018f4:	83 c4 10             	add    $0x10,%esp
801018f7:	83 fe 01             	cmp    $0x1,%esi
801018fa:	75 aa                	jne    801018a6 <iput+0x26>
801018fc:	8d 8b 8c 00 00 00    	lea    0x8c(%ebx),%ecx
80101902:	89 7d e4             	mov    %edi,-0x1c(%ebp)
80101905:	8d 73 5c             	lea    0x5c(%ebx),%esi
80101908:	89 cf                	mov    %ecx,%edi
8010190a:	eb 0b                	jmp    80101917 <iput+0x97>
8010190c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101910:	83 c6 04             	add    $0x4,%esi
{
  int i, j;
  struct buf *bp;
  uint *a;

  for(i = 0; i < NDIRECT; i++){
80101913:	39 fe                	cmp    %edi,%esi
80101915:	74 19                	je     80101930 <iput+0xb0>
    if(ip->addrs[i]){
80101917:	8b 16                	mov    (%esi),%edx
80101919:	85 d2                	test   %edx,%edx
8010191b:	74 f3                	je     80101910 <iput+0x90>
      bfree(ip->dev, ip->addrs[i]);
8010191d:	8b 03                	mov    (%ebx),%eax
8010191f:	e8 ac fb ff ff       	call   801014d0 <bfree>
      ip->addrs[i] = 0;
80101924:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
8010192a:	eb e4                	jmp    80101910 <iput+0x90>
8010192c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    }
  }

  if(ip->addrs[NDIRECT]){
80101930:	8b 83 8c 00 00 00    	mov    0x8c(%ebx),%eax
80101936:	8b 7d e4             	mov    -0x1c(%ebp),%edi
80101939:	85 c0                	test   %eax,%eax
8010193b:	75 33                	jne    80101970 <iput+0xf0>
    bfree(ip->dev, ip->addrs[NDIRECT]);
    ip->addrs[NDIRECT] = 0;
  }

  ip->size = 0;
  iupdate(ip);
8010193d:	83 ec 0c             	sub    $0xc,%esp
  ip->size = 0;
80101940:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  iupdate(ip);
80101947:	53                   	push   %ebx
80101948:	e8 53 fd ff ff       	call   801016a0 <iupdate>
      ip->type = 0;
8010194d:	31 c0                	xor    %eax,%eax
8010194f:	66 89 43 50          	mov    %ax,0x50(%ebx)
      iupdate(ip);
80101953:	89 1c 24             	mov    %ebx,(%esp)
80101956:	e8 45 fd ff ff       	call   801016a0 <iupdate>
      ip->valid = 0;
8010195b:	c7 43 4c 00 00 00 00 	movl   $0x0,0x4c(%ebx)
80101962:	83 c4 10             	add    $0x10,%esp
80101965:	e9 3c ff ff ff       	jmp    801018a6 <iput+0x26>
8010196a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    bp = bread(ip->dev, ip->addrs[NDIRECT]);
80101970:	83 ec 08             	sub    $0x8,%esp
80101973:	50                   	push   %eax
80101974:	ff 33                	pushl  (%ebx)
80101976:	e8 55 e7 ff ff       	call   801000d0 <bread>
8010197b:	89 7d e0             	mov    %edi,-0x20(%ebp)
8010197e:	83 c4 10             	add    $0x10,%esp
80101981:	8d 88 5c 02 00 00    	lea    0x25c(%eax),%ecx
80101987:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    for(j = 0; j < NINDIRECT; j++){
8010198a:	8d 70 5c             	lea    0x5c(%eax),%esi
8010198d:	89 cf                	mov    %ecx,%edi
8010198f:	eb 0e                	jmp    8010199f <iput+0x11f>
80101991:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101998:	83 c6 04             	add    $0x4,%esi
8010199b:	39 f7                	cmp    %esi,%edi
8010199d:	74 11                	je     801019b0 <iput+0x130>
      if(a[j])
8010199f:	8b 16                	mov    (%esi),%edx
801019a1:	85 d2                	test   %edx,%edx
801019a3:	74 f3                	je     80101998 <iput+0x118>
        bfree(ip->dev, a[j]);
801019a5:	8b 03                	mov    (%ebx),%eax
801019a7:	e8 24 fb ff ff       	call   801014d0 <bfree>
801019ac:	eb ea                	jmp    80101998 <iput+0x118>
801019ae:	66 90                	xchg   %ax,%ax
    brelse(bp);
801019b0:	83 ec 0c             	sub    $0xc,%esp
801019b3:	ff 75 e4             	pushl  -0x1c(%ebp)
801019b6:	8b 7d e0             	mov    -0x20(%ebp),%edi
801019b9:	e8 32 e8 ff ff       	call   801001f0 <brelse>
    bfree(ip->dev, ip->addrs[NDIRECT]);
801019be:	8b 93 8c 00 00 00    	mov    0x8c(%ebx),%edx
801019c4:	8b 03                	mov    (%ebx),%eax
801019c6:	e8 05 fb ff ff       	call   801014d0 <bfree>
    ip->addrs[NDIRECT] = 0;
801019cb:	83 c4 10             	add    $0x10,%esp
801019ce:	c7 83 8c 00 00 00 00 	movl   $0x0,0x8c(%ebx)
801019d5:	00 00 00 
801019d8:	e9 60 ff ff ff       	jmp    8010193d <iput+0xbd>
801019dd:	8d 76 00             	lea    0x0(%esi),%esi

801019e0 <iunlockput>:
{
801019e0:	55                   	push   %ebp
801019e1:	89 e5                	mov    %esp,%ebp
801019e3:	53                   	push   %ebx
801019e4:	83 ec 10             	sub    $0x10,%esp
801019e7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  iunlock(ip);
801019ea:	53                   	push   %ebx
801019eb:	e8 40 fe ff ff       	call   80101830 <iunlock>
  iput(ip);
801019f0:	89 5d 08             	mov    %ebx,0x8(%ebp)
801019f3:	83 c4 10             	add    $0x10,%esp
}
801019f6:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801019f9:	c9                   	leave  
  iput(ip);
801019fa:	e9 81 fe ff ff       	jmp    80101880 <iput>
801019ff:	90                   	nop

80101a00 <stati>:

// Copy stat information from inode.
// Caller must hold ip->lock.
void
stati(struct inode *ip, struct stat *st)
{
80101a00:	55                   	push   %ebp
80101a01:	89 e5                	mov    %esp,%ebp
80101a03:	8b 55 08             	mov    0x8(%ebp),%edx
80101a06:	8b 45 0c             	mov    0xc(%ebp),%eax
  st->dev = ip->dev;
80101a09:	8b 0a                	mov    (%edx),%ecx
80101a0b:	89 48 04             	mov    %ecx,0x4(%eax)
  st->ino = ip->inum;
80101a0e:	8b 4a 04             	mov    0x4(%edx),%ecx
80101a11:	89 48 08             	mov    %ecx,0x8(%eax)
  st->type = ip->type;
80101a14:	0f b7 4a 50          	movzwl 0x50(%edx),%ecx
80101a18:	66 89 08             	mov    %cx,(%eax)
  st->nlink = ip->nlink;
80101a1b:	0f b7 4a 56          	movzwl 0x56(%edx),%ecx
80101a1f:	66 89 48 0c          	mov    %cx,0xc(%eax)
  st->size = ip->size;
80101a23:	8b 52 58             	mov    0x58(%edx),%edx
80101a26:	89 50 10             	mov    %edx,0x10(%eax)
}
80101a29:	5d                   	pop    %ebp
80101a2a:	c3                   	ret    
80101a2b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101a2f:	90                   	nop

80101a30 <readi>:
//PAGEBREAK!
// Read data from inode.
// Caller must hold ip->lock.
int
readi(struct inode *ip, char *dst, uint off, uint n)
{
80101a30:	55                   	push   %ebp
80101a31:	89 e5                	mov    %esp,%ebp
80101a33:	57                   	push   %edi
80101a34:	56                   	push   %esi
80101a35:	53                   	push   %ebx
80101a36:	83 ec 1c             	sub    $0x1c,%esp
80101a39:	8b 7d 0c             	mov    0xc(%ebp),%edi
80101a3c:	8b 45 08             	mov    0x8(%ebp),%eax
80101a3f:	8b 75 10             	mov    0x10(%ebp),%esi
80101a42:	89 7d e0             	mov    %edi,-0x20(%ebp)
80101a45:	8b 7d 14             	mov    0x14(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101a48:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
{
80101a4d:	89 45 d8             	mov    %eax,-0x28(%ebp)
80101a50:	89 7d e4             	mov    %edi,-0x1c(%ebp)
  if(ip->type == T_DEV){
80101a53:	0f 84 a7 00 00 00    	je     80101b00 <readi+0xd0>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
      return -1;
    return devsw[ip->major].read(ip, dst, n);
  }

  if(off > ip->size || off + n < off)
80101a59:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101a5c:	8b 40 58             	mov    0x58(%eax),%eax
80101a5f:	39 c6                	cmp    %eax,%esi
80101a61:	0f 87 ba 00 00 00    	ja     80101b21 <readi+0xf1>
80101a67:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80101a6a:	31 c9                	xor    %ecx,%ecx
80101a6c:	89 da                	mov    %ebx,%edx
80101a6e:	01 f2                	add    %esi,%edx
80101a70:	0f 92 c1             	setb   %cl
80101a73:	89 cf                	mov    %ecx,%edi
80101a75:	0f 82 a6 00 00 00    	jb     80101b21 <readi+0xf1>
    return -1;
  if(off + n > ip->size)
    n = ip->size - off;
80101a7b:	89 c1                	mov    %eax,%ecx
80101a7d:	29 f1                	sub    %esi,%ecx
80101a7f:	39 d0                	cmp    %edx,%eax
80101a81:	0f 43 cb             	cmovae %ebx,%ecx
80101a84:	89 4d e4             	mov    %ecx,-0x1c(%ebp)

  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101a87:	85 c9                	test   %ecx,%ecx
80101a89:	74 67                	je     80101af2 <readi+0xc2>
80101a8b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101a8f:	90                   	nop
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101a90:	8b 5d d8             	mov    -0x28(%ebp),%ebx
80101a93:	89 f2                	mov    %esi,%edx
80101a95:	c1 ea 09             	shr    $0x9,%edx
80101a98:	89 d8                	mov    %ebx,%eax
80101a9a:	e8 21 f9 ff ff       	call   801013c0 <bmap>
80101a9f:	83 ec 08             	sub    $0x8,%esp
80101aa2:	50                   	push   %eax
80101aa3:	ff 33                	pushl  (%ebx)
80101aa5:	e8 26 e6 ff ff       	call   801000d0 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
80101aaa:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80101aad:	b9 00 02 00 00       	mov    $0x200,%ecx
80101ab2:	83 c4 0c             	add    $0xc,%esp
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101ab5:	89 c2                	mov    %eax,%edx
    m = min(n - tot, BSIZE - off%BSIZE);
80101ab7:	89 f0                	mov    %esi,%eax
80101ab9:	25 ff 01 00 00       	and    $0x1ff,%eax
80101abe:	29 fb                	sub    %edi,%ebx
    memmove(dst, bp->data + off%BSIZE, m);
80101ac0:	89 55 dc             	mov    %edx,-0x24(%ebp)
    m = min(n - tot, BSIZE - off%BSIZE);
80101ac3:	29 c1                	sub    %eax,%ecx
    memmove(dst, bp->data + off%BSIZE, m);
80101ac5:	8d 44 02 5c          	lea    0x5c(%edx,%eax,1),%eax
    m = min(n - tot, BSIZE - off%BSIZE);
80101ac9:	39 d9                	cmp    %ebx,%ecx
80101acb:	0f 46 d9             	cmovbe %ecx,%ebx
    memmove(dst, bp->data + off%BSIZE, m);
80101ace:	53                   	push   %ebx
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101acf:	01 df                	add    %ebx,%edi
80101ad1:	01 de                	add    %ebx,%esi
    memmove(dst, bp->data + off%BSIZE, m);
80101ad3:	50                   	push   %eax
80101ad4:	ff 75 e0             	pushl  -0x20(%ebp)
80101ad7:	e8 14 2c 00 00       	call   801046f0 <memmove>
    brelse(bp);
80101adc:	8b 55 dc             	mov    -0x24(%ebp),%edx
80101adf:	89 14 24             	mov    %edx,(%esp)
80101ae2:	e8 09 e7 ff ff       	call   801001f0 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, dst+=m){
80101ae7:	01 5d e0             	add    %ebx,-0x20(%ebp)
80101aea:	83 c4 10             	add    $0x10,%esp
80101aed:	39 7d e4             	cmp    %edi,-0x1c(%ebp)
80101af0:	77 9e                	ja     80101a90 <readi+0x60>
  }
  return n;
80101af2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
}
80101af5:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101af8:	5b                   	pop    %ebx
80101af9:	5e                   	pop    %esi
80101afa:	5f                   	pop    %edi
80101afb:	5d                   	pop    %ebp
80101afc:	c3                   	ret    
80101afd:	8d 76 00             	lea    0x0(%esi),%esi
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].read)
80101b00:	0f bf 40 52          	movswl 0x52(%eax),%eax
80101b04:	66 83 f8 09          	cmp    $0x9,%ax
80101b08:	77 17                	ja     80101b21 <readi+0xf1>
80101b0a:	8b 04 c5 20 1a 11 80 	mov    -0x7feee5e0(,%eax,8),%eax
80101b11:	85 c0                	test   %eax,%eax
80101b13:	74 0c                	je     80101b21 <readi+0xf1>
    return devsw[ip->major].read(ip, dst, n);
80101b15:	89 7d 10             	mov    %edi,0x10(%ebp)
}
80101b18:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101b1b:	5b                   	pop    %ebx
80101b1c:	5e                   	pop    %esi
80101b1d:	5f                   	pop    %edi
80101b1e:	5d                   	pop    %ebp
    return devsw[ip->major].read(ip, dst, n);
80101b1f:	ff e0                	jmp    *%eax
      return -1;
80101b21:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101b26:	eb cd                	jmp    80101af5 <readi+0xc5>
80101b28:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101b2f:	90                   	nop

80101b30 <writei>:
// PAGEBREAK!
// Write data to inode.
// Caller must hold ip->lock.
int
writei(struct inode *ip, char *src, uint off, uint n)
{
80101b30:	55                   	push   %ebp
80101b31:	89 e5                	mov    %esp,%ebp
80101b33:	57                   	push   %edi
80101b34:	56                   	push   %esi
80101b35:	53                   	push   %ebx
80101b36:	83 ec 1c             	sub    $0x1c,%esp
80101b39:	8b 45 08             	mov    0x8(%ebp),%eax
80101b3c:	8b 75 0c             	mov    0xc(%ebp),%esi
80101b3f:	8b 7d 14             	mov    0x14(%ebp),%edi
  uint tot, m;
  struct buf *bp;

  if(ip->type == T_DEV){
80101b42:	66 83 78 50 03       	cmpw   $0x3,0x50(%eax)
{
80101b47:	89 75 dc             	mov    %esi,-0x24(%ebp)
80101b4a:	89 45 d8             	mov    %eax,-0x28(%ebp)
80101b4d:	8b 75 10             	mov    0x10(%ebp),%esi
80101b50:	89 7d e0             	mov    %edi,-0x20(%ebp)
  if(ip->type == T_DEV){
80101b53:	0f 84 b7 00 00 00    	je     80101c10 <writei+0xe0>
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
      return -1;
    return devsw[ip->major].write(ip, src, n);
  }

  if(off > ip->size || off + n < off)
80101b59:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101b5c:	39 70 58             	cmp    %esi,0x58(%eax)
80101b5f:	0f 82 e7 00 00 00    	jb     80101c4c <writei+0x11c>
    return -1;
  if(off + n > MAXFILE*BSIZE)
80101b65:	8b 7d e0             	mov    -0x20(%ebp),%edi
80101b68:	89 f8                	mov    %edi,%eax
80101b6a:	01 f0                	add    %esi,%eax
80101b6c:	0f 82 da 00 00 00    	jb     80101c4c <writei+0x11c>
80101b72:	3d 00 18 01 00       	cmp    $0x11800,%eax
80101b77:	0f 87 cf 00 00 00    	ja     80101c4c <writei+0x11c>
    return -1;

  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101b7d:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
80101b84:	85 ff                	test   %edi,%edi
80101b86:	74 79                	je     80101c01 <writei+0xd1>
80101b88:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101b8f:	90                   	nop
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101b90:	8b 7d d8             	mov    -0x28(%ebp),%edi
80101b93:	89 f2                	mov    %esi,%edx
80101b95:	c1 ea 09             	shr    $0x9,%edx
80101b98:	89 f8                	mov    %edi,%eax
80101b9a:	e8 21 f8 ff ff       	call   801013c0 <bmap>
80101b9f:	83 ec 08             	sub    $0x8,%esp
80101ba2:	50                   	push   %eax
80101ba3:	ff 37                	pushl  (%edi)
80101ba5:	e8 26 e5 ff ff       	call   801000d0 <bread>
    m = min(n - tot, BSIZE - off%BSIZE);
80101baa:	b9 00 02 00 00       	mov    $0x200,%ecx
80101baf:	8b 5d e0             	mov    -0x20(%ebp),%ebx
80101bb2:	2b 5d e4             	sub    -0x1c(%ebp),%ebx
    bp = bread(ip->dev, bmap(ip, off/BSIZE));
80101bb5:	89 c7                	mov    %eax,%edi
    m = min(n - tot, BSIZE - off%BSIZE);
80101bb7:	89 f0                	mov    %esi,%eax
80101bb9:	83 c4 0c             	add    $0xc,%esp
80101bbc:	25 ff 01 00 00       	and    $0x1ff,%eax
80101bc1:	29 c1                	sub    %eax,%ecx
    memmove(bp->data + off%BSIZE, src, m);
80101bc3:	8d 44 07 5c          	lea    0x5c(%edi,%eax,1),%eax
    m = min(n - tot, BSIZE - off%BSIZE);
80101bc7:	39 d9                	cmp    %ebx,%ecx
80101bc9:	0f 46 d9             	cmovbe %ecx,%ebx
    memmove(bp->data + off%BSIZE, src, m);
80101bcc:	53                   	push   %ebx
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101bcd:	01 de                	add    %ebx,%esi
    memmove(bp->data + off%BSIZE, src, m);
80101bcf:	ff 75 dc             	pushl  -0x24(%ebp)
80101bd2:	50                   	push   %eax
80101bd3:	e8 18 2b 00 00       	call   801046f0 <memmove>
    log_write(bp);
80101bd8:	89 3c 24             	mov    %edi,(%esp)
80101bdb:	e8 d0 12 00 00       	call   80102eb0 <log_write>
    brelse(bp);
80101be0:	89 3c 24             	mov    %edi,(%esp)
80101be3:	e8 08 e6 ff ff       	call   801001f0 <brelse>
  for(tot=0; tot<n; tot+=m, off+=m, src+=m){
80101be8:	01 5d e4             	add    %ebx,-0x1c(%ebp)
80101beb:	83 c4 10             	add    $0x10,%esp
80101bee:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101bf1:	01 5d dc             	add    %ebx,-0x24(%ebp)
80101bf4:	39 45 e0             	cmp    %eax,-0x20(%ebp)
80101bf7:	77 97                	ja     80101b90 <writei+0x60>
  }

  if(n > 0 && off > ip->size){
80101bf9:	8b 45 d8             	mov    -0x28(%ebp),%eax
80101bfc:	3b 70 58             	cmp    0x58(%eax),%esi
80101bff:	77 37                	ja     80101c38 <writei+0x108>
    ip->size = off;
    iupdate(ip);
  }
  return n;
80101c01:	8b 45 e0             	mov    -0x20(%ebp),%eax
}
80101c04:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101c07:	5b                   	pop    %ebx
80101c08:	5e                   	pop    %esi
80101c09:	5f                   	pop    %edi
80101c0a:	5d                   	pop    %ebp
80101c0b:	c3                   	ret    
80101c0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(ip->major < 0 || ip->major >= NDEV || !devsw[ip->major].write)
80101c10:	0f bf 40 52          	movswl 0x52(%eax),%eax
80101c14:	66 83 f8 09          	cmp    $0x9,%ax
80101c18:	77 32                	ja     80101c4c <writei+0x11c>
80101c1a:	8b 04 c5 24 1a 11 80 	mov    -0x7feee5dc(,%eax,8),%eax
80101c21:	85 c0                	test   %eax,%eax
80101c23:	74 27                	je     80101c4c <writei+0x11c>
    return devsw[ip->major].write(ip, src, n);
80101c25:	89 7d 10             	mov    %edi,0x10(%ebp)
}
80101c28:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101c2b:	5b                   	pop    %ebx
80101c2c:	5e                   	pop    %esi
80101c2d:	5f                   	pop    %edi
80101c2e:	5d                   	pop    %ebp
    return devsw[ip->major].write(ip, src, n);
80101c2f:	ff e0                	jmp    *%eax
80101c31:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    ip->size = off;
80101c38:	8b 45 d8             	mov    -0x28(%ebp),%eax
    iupdate(ip);
80101c3b:	83 ec 0c             	sub    $0xc,%esp
    ip->size = off;
80101c3e:	89 70 58             	mov    %esi,0x58(%eax)
    iupdate(ip);
80101c41:	50                   	push   %eax
80101c42:	e8 59 fa ff ff       	call   801016a0 <iupdate>
80101c47:	83 c4 10             	add    $0x10,%esp
80101c4a:	eb b5                	jmp    80101c01 <writei+0xd1>
      return -1;
80101c4c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101c51:	eb b1                	jmp    80101c04 <writei+0xd4>
80101c53:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101c5a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101c60 <namecmp>:
//PAGEBREAK!
// Directories

int
namecmp(const char *s, const char *t)
{
80101c60:	55                   	push   %ebp
80101c61:	89 e5                	mov    %esp,%ebp
80101c63:	83 ec 0c             	sub    $0xc,%esp
  return strncmp(s, t, DIRSIZ);
80101c66:	6a 0e                	push   $0xe
80101c68:	ff 75 0c             	pushl  0xc(%ebp)
80101c6b:	ff 75 08             	pushl  0x8(%ebp)
80101c6e:	e8 ed 2a 00 00       	call   80104760 <strncmp>
}
80101c73:	c9                   	leave  
80101c74:	c3                   	ret    
80101c75:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101c7c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80101c80 <dirlookup>:

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode*
dirlookup(struct inode *dp, char *name, uint *poff)
{
80101c80:	55                   	push   %ebp
80101c81:	89 e5                	mov    %esp,%ebp
80101c83:	57                   	push   %edi
80101c84:	56                   	push   %esi
80101c85:	53                   	push   %ebx
80101c86:	83 ec 1c             	sub    $0x1c,%esp
80101c89:	8b 5d 08             	mov    0x8(%ebp),%ebx
  uint off, inum;
  struct dirent de;

  if(dp->type != T_DIR)
80101c8c:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80101c91:	0f 85 85 00 00 00    	jne    80101d1c <dirlookup+0x9c>
    panic("dirlookup not DIR");

  for(off = 0; off < dp->size; off += sizeof(de)){
80101c97:	8b 53 58             	mov    0x58(%ebx),%edx
80101c9a:	31 ff                	xor    %edi,%edi
80101c9c:	8d 75 d8             	lea    -0x28(%ebp),%esi
80101c9f:	85 d2                	test   %edx,%edx
80101ca1:	74 3e                	je     80101ce1 <dirlookup+0x61>
80101ca3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101ca7:	90                   	nop
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101ca8:	6a 10                	push   $0x10
80101caa:	57                   	push   %edi
80101cab:	56                   	push   %esi
80101cac:	53                   	push   %ebx
80101cad:	e8 7e fd ff ff       	call   80101a30 <readi>
80101cb2:	83 c4 10             	add    $0x10,%esp
80101cb5:	83 f8 10             	cmp    $0x10,%eax
80101cb8:	75 55                	jne    80101d0f <dirlookup+0x8f>
      panic("dirlookup read");
    if(de.inum == 0)
80101cba:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80101cbf:	74 18                	je     80101cd9 <dirlookup+0x59>
  return strncmp(s, t, DIRSIZ);
80101cc1:	83 ec 04             	sub    $0x4,%esp
80101cc4:	8d 45 da             	lea    -0x26(%ebp),%eax
80101cc7:	6a 0e                	push   $0xe
80101cc9:	50                   	push   %eax
80101cca:	ff 75 0c             	pushl  0xc(%ebp)
80101ccd:	e8 8e 2a 00 00       	call   80104760 <strncmp>
      continue;
    if(namecmp(name, de.name) == 0){
80101cd2:	83 c4 10             	add    $0x10,%esp
80101cd5:	85 c0                	test   %eax,%eax
80101cd7:	74 17                	je     80101cf0 <dirlookup+0x70>
  for(off = 0; off < dp->size; off += sizeof(de)){
80101cd9:	83 c7 10             	add    $0x10,%edi
80101cdc:	3b 7b 58             	cmp    0x58(%ebx),%edi
80101cdf:	72 c7                	jb     80101ca8 <dirlookup+0x28>
      return iget(dp->dev, inum);
    }
  }

  return 0;
}
80101ce1:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80101ce4:	31 c0                	xor    %eax,%eax
}
80101ce6:	5b                   	pop    %ebx
80101ce7:	5e                   	pop    %esi
80101ce8:	5f                   	pop    %edi
80101ce9:	5d                   	pop    %ebp
80101cea:	c3                   	ret    
80101ceb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101cef:	90                   	nop
      if(poff)
80101cf0:	8b 45 10             	mov    0x10(%ebp),%eax
80101cf3:	85 c0                	test   %eax,%eax
80101cf5:	74 05                	je     80101cfc <dirlookup+0x7c>
        *poff = off;
80101cf7:	8b 45 10             	mov    0x10(%ebp),%eax
80101cfa:	89 38                	mov    %edi,(%eax)
      inum = de.inum;
80101cfc:	0f b7 55 d8          	movzwl -0x28(%ebp),%edx
      return iget(dp->dev, inum);
80101d00:	8b 03                	mov    (%ebx),%eax
80101d02:	e8 c9 f5 ff ff       	call   801012d0 <iget>
}
80101d07:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101d0a:	5b                   	pop    %ebx
80101d0b:	5e                   	pop    %esi
80101d0c:	5f                   	pop    %edi
80101d0d:	5d                   	pop    %ebp
80101d0e:	c3                   	ret    
      panic("dirlookup read");
80101d0f:	83 ec 0c             	sub    $0xc,%esp
80101d12:	68 e2 79 10 80       	push   $0x801079e2
80101d17:	e8 74 e6 ff ff       	call   80100390 <panic>
    panic("dirlookup not DIR");
80101d1c:	83 ec 0c             	sub    $0xc,%esp
80101d1f:	68 d0 79 10 80       	push   $0x801079d0
80101d24:	e8 67 e6 ff ff       	call   80100390 <panic>
80101d29:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101d30 <namex>:
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode*
namex(char *path, int nameiparent, char *name)
{
80101d30:	55                   	push   %ebp
80101d31:	89 e5                	mov    %esp,%ebp
80101d33:	57                   	push   %edi
80101d34:	56                   	push   %esi
80101d35:	53                   	push   %ebx
80101d36:	89 c3                	mov    %eax,%ebx
80101d38:	83 ec 1c             	sub    $0x1c,%esp
  struct inode *ip, *next;

  if(*path == '/')
80101d3b:	80 38 2f             	cmpb   $0x2f,(%eax)
{
80101d3e:	89 55 e0             	mov    %edx,-0x20(%ebp)
80101d41:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
  if(*path == '/')
80101d44:	0f 84 86 01 00 00    	je     80101ed0 <namex+0x1a0>
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(myproc()->cwd);
80101d4a:	e8 d1 1b 00 00       	call   80103920 <myproc>
  acquire(&icache.lock);
80101d4f:	83 ec 0c             	sub    $0xc,%esp
80101d52:	89 df                	mov    %ebx,%edi
    ip = idup(myproc()->cwd);
80101d54:	8b b0 a4 00 00 00    	mov    0xa4(%eax),%esi
  acquire(&icache.lock);
80101d5a:	68 a0 1a 11 80       	push   $0x80111aa0
80101d5f:	e8 dc 27 00 00       	call   80104540 <acquire>
  ip->ref++;
80101d64:	83 46 08 01          	addl   $0x1,0x8(%esi)
  release(&icache.lock);
80101d68:	c7 04 24 a0 1a 11 80 	movl   $0x80111aa0,(%esp)
80101d6f:	e8 8c 28 00 00       	call   80104600 <release>
80101d74:	83 c4 10             	add    $0x10,%esp
80101d77:	eb 0a                	jmp    80101d83 <namex+0x53>
80101d79:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    path++;
80101d80:	83 c7 01             	add    $0x1,%edi
  while(*path == '/')
80101d83:	0f b6 07             	movzbl (%edi),%eax
80101d86:	3c 2f                	cmp    $0x2f,%al
80101d88:	74 f6                	je     80101d80 <namex+0x50>
  if(*path == 0)
80101d8a:	84 c0                	test   %al,%al
80101d8c:	0f 84 ee 00 00 00    	je     80101e80 <namex+0x150>
  while(*path != '/' && *path != 0)
80101d92:	0f b6 07             	movzbl (%edi),%eax
80101d95:	84 c0                	test   %al,%al
80101d97:	0f 84 fb 00 00 00    	je     80101e98 <namex+0x168>
80101d9d:	89 fb                	mov    %edi,%ebx
80101d9f:	3c 2f                	cmp    $0x2f,%al
80101da1:	0f 84 f1 00 00 00    	je     80101e98 <namex+0x168>
80101da7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101dae:	66 90                	xchg   %ax,%ax
    path++;
80101db0:	83 c3 01             	add    $0x1,%ebx
  while(*path != '/' && *path != 0)
80101db3:	0f b6 03             	movzbl (%ebx),%eax
80101db6:	3c 2f                	cmp    $0x2f,%al
80101db8:	74 04                	je     80101dbe <namex+0x8e>
80101dba:	84 c0                	test   %al,%al
80101dbc:	75 f2                	jne    80101db0 <namex+0x80>
  len = path - s;
80101dbe:	89 d8                	mov    %ebx,%eax
80101dc0:	29 f8                	sub    %edi,%eax
  if(len >= DIRSIZ)
80101dc2:	83 f8 0d             	cmp    $0xd,%eax
80101dc5:	0f 8e 85 00 00 00    	jle    80101e50 <namex+0x120>
    memmove(name, s, DIRSIZ);
80101dcb:	83 ec 04             	sub    $0x4,%esp
80101dce:	6a 0e                	push   $0xe
80101dd0:	57                   	push   %edi
    path++;
80101dd1:	89 df                	mov    %ebx,%edi
    memmove(name, s, DIRSIZ);
80101dd3:	ff 75 e4             	pushl  -0x1c(%ebp)
80101dd6:	e8 15 29 00 00       	call   801046f0 <memmove>
80101ddb:	83 c4 10             	add    $0x10,%esp
  while(*path == '/')
80101dde:	80 3b 2f             	cmpb   $0x2f,(%ebx)
80101de1:	75 0d                	jne    80101df0 <namex+0xc0>
80101de3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80101de7:	90                   	nop
    path++;
80101de8:	83 c7 01             	add    $0x1,%edi
  while(*path == '/')
80101deb:	80 3f 2f             	cmpb   $0x2f,(%edi)
80101dee:	74 f8                	je     80101de8 <namex+0xb8>

  while((path = skipelem(path, name)) != 0){
    ilock(ip);
80101df0:	83 ec 0c             	sub    $0xc,%esp
80101df3:	56                   	push   %esi
80101df4:	e8 57 f9 ff ff       	call   80101750 <ilock>
    if(ip->type != T_DIR){
80101df9:	83 c4 10             	add    $0x10,%esp
80101dfc:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
80101e01:	0f 85 a1 00 00 00    	jne    80101ea8 <namex+0x178>
      iunlockput(ip);
      return 0;
    }
    if(nameiparent && *path == '\0'){
80101e07:	8b 55 e0             	mov    -0x20(%ebp),%edx
80101e0a:	85 d2                	test   %edx,%edx
80101e0c:	74 09                	je     80101e17 <namex+0xe7>
80101e0e:	80 3f 00             	cmpb   $0x0,(%edi)
80101e11:	0f 84 d9 00 00 00    	je     80101ef0 <namex+0x1c0>
      // Stop one level early.
      iunlock(ip);
      return ip;
    }
    if((next = dirlookup(ip, name, 0)) == 0){
80101e17:	83 ec 04             	sub    $0x4,%esp
80101e1a:	6a 00                	push   $0x0
80101e1c:	ff 75 e4             	pushl  -0x1c(%ebp)
80101e1f:	56                   	push   %esi
80101e20:	e8 5b fe ff ff       	call   80101c80 <dirlookup>
80101e25:	83 c4 10             	add    $0x10,%esp
80101e28:	89 c3                	mov    %eax,%ebx
80101e2a:	85 c0                	test   %eax,%eax
80101e2c:	74 7a                	je     80101ea8 <namex+0x178>
  iunlock(ip);
80101e2e:	83 ec 0c             	sub    $0xc,%esp
80101e31:	56                   	push   %esi
80101e32:	e8 f9 f9 ff ff       	call   80101830 <iunlock>
  iput(ip);
80101e37:	89 34 24             	mov    %esi,(%esp)
80101e3a:	89 de                	mov    %ebx,%esi
80101e3c:	e8 3f fa ff ff       	call   80101880 <iput>
  while(*path == '/')
80101e41:	83 c4 10             	add    $0x10,%esp
80101e44:	e9 3a ff ff ff       	jmp    80101d83 <namex+0x53>
80101e49:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101e50:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80101e53:	8d 0c 02             	lea    (%edx,%eax,1),%ecx
80101e56:	89 4d dc             	mov    %ecx,-0x24(%ebp)
    memmove(name, s, len);
80101e59:	83 ec 04             	sub    $0x4,%esp
80101e5c:	50                   	push   %eax
80101e5d:	57                   	push   %edi
    name[len] = 0;
80101e5e:	89 df                	mov    %ebx,%edi
    memmove(name, s, len);
80101e60:	ff 75 e4             	pushl  -0x1c(%ebp)
80101e63:	e8 88 28 00 00       	call   801046f0 <memmove>
    name[len] = 0;
80101e68:	8b 45 dc             	mov    -0x24(%ebp),%eax
80101e6b:	83 c4 10             	add    $0x10,%esp
80101e6e:	c6 00 00             	movb   $0x0,(%eax)
80101e71:	e9 68 ff ff ff       	jmp    80101dde <namex+0xae>
80101e76:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101e7d:	8d 76 00             	lea    0x0(%esi),%esi
      return 0;
    }
    iunlockput(ip);
    ip = next;
  }
  if(nameiparent){
80101e80:	8b 45 e0             	mov    -0x20(%ebp),%eax
80101e83:	85 c0                	test   %eax,%eax
80101e85:	0f 85 85 00 00 00    	jne    80101f10 <namex+0x1e0>
    iput(ip);
    return 0;
  }
  return ip;
}
80101e8b:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101e8e:	89 f0                	mov    %esi,%eax
80101e90:	5b                   	pop    %ebx
80101e91:	5e                   	pop    %esi
80101e92:	5f                   	pop    %edi
80101e93:	5d                   	pop    %ebp
80101e94:	c3                   	ret    
80101e95:	8d 76 00             	lea    0x0(%esi),%esi
  while(*path != '/' && *path != 0)
80101e98:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80101e9b:	89 fb                	mov    %edi,%ebx
80101e9d:	89 45 dc             	mov    %eax,-0x24(%ebp)
80101ea0:	31 c0                	xor    %eax,%eax
80101ea2:	eb b5                	jmp    80101e59 <namex+0x129>
80101ea4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  iunlock(ip);
80101ea8:	83 ec 0c             	sub    $0xc,%esp
80101eab:	56                   	push   %esi
80101eac:	e8 7f f9 ff ff       	call   80101830 <iunlock>
  iput(ip);
80101eb1:	89 34 24             	mov    %esi,(%esp)
      return 0;
80101eb4:	31 f6                	xor    %esi,%esi
  iput(ip);
80101eb6:	e8 c5 f9 ff ff       	call   80101880 <iput>
      return 0;
80101ebb:	83 c4 10             	add    $0x10,%esp
}
80101ebe:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101ec1:	89 f0                	mov    %esi,%eax
80101ec3:	5b                   	pop    %ebx
80101ec4:	5e                   	pop    %esi
80101ec5:	5f                   	pop    %edi
80101ec6:	5d                   	pop    %ebp
80101ec7:	c3                   	ret    
80101ec8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101ecf:	90                   	nop
    ip = iget(ROOTDEV, ROOTINO);
80101ed0:	ba 01 00 00 00       	mov    $0x1,%edx
80101ed5:	b8 01 00 00 00       	mov    $0x1,%eax
80101eda:	89 df                	mov    %ebx,%edi
80101edc:	e8 ef f3 ff ff       	call   801012d0 <iget>
80101ee1:	89 c6                	mov    %eax,%esi
80101ee3:	e9 9b fe ff ff       	jmp    80101d83 <namex+0x53>
80101ee8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101eef:	90                   	nop
      iunlock(ip);
80101ef0:	83 ec 0c             	sub    $0xc,%esp
80101ef3:	56                   	push   %esi
80101ef4:	e8 37 f9 ff ff       	call   80101830 <iunlock>
      return ip;
80101ef9:	83 c4 10             	add    $0x10,%esp
}
80101efc:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101eff:	89 f0                	mov    %esi,%eax
80101f01:	5b                   	pop    %ebx
80101f02:	5e                   	pop    %esi
80101f03:	5f                   	pop    %edi
80101f04:	5d                   	pop    %ebp
80101f05:	c3                   	ret    
80101f06:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101f0d:	8d 76 00             	lea    0x0(%esi),%esi
    iput(ip);
80101f10:	83 ec 0c             	sub    $0xc,%esp
80101f13:	56                   	push   %esi
    return 0;
80101f14:	31 f6                	xor    %esi,%esi
    iput(ip);
80101f16:	e8 65 f9 ff ff       	call   80101880 <iput>
    return 0;
80101f1b:	83 c4 10             	add    $0x10,%esp
80101f1e:	e9 68 ff ff ff       	jmp    80101e8b <namex+0x15b>
80101f23:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101f2a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80101f30 <dirlink>:
{
80101f30:	55                   	push   %ebp
80101f31:	89 e5                	mov    %esp,%ebp
80101f33:	57                   	push   %edi
80101f34:	56                   	push   %esi
80101f35:	53                   	push   %ebx
80101f36:	83 ec 20             	sub    $0x20,%esp
80101f39:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if((ip = dirlookup(dp, name, 0)) != 0){
80101f3c:	6a 00                	push   $0x0
80101f3e:	ff 75 0c             	pushl  0xc(%ebp)
80101f41:	53                   	push   %ebx
80101f42:	e8 39 fd ff ff       	call   80101c80 <dirlookup>
80101f47:	83 c4 10             	add    $0x10,%esp
80101f4a:	85 c0                	test   %eax,%eax
80101f4c:	75 67                	jne    80101fb5 <dirlink+0x85>
  for(off = 0; off < dp->size; off += sizeof(de)){
80101f4e:	8b 7b 58             	mov    0x58(%ebx),%edi
80101f51:	8d 75 d8             	lea    -0x28(%ebp),%esi
80101f54:	85 ff                	test   %edi,%edi
80101f56:	74 29                	je     80101f81 <dirlink+0x51>
80101f58:	31 ff                	xor    %edi,%edi
80101f5a:	8d 75 d8             	lea    -0x28(%ebp),%esi
80101f5d:	eb 09                	jmp    80101f68 <dirlink+0x38>
80101f5f:	90                   	nop
80101f60:	83 c7 10             	add    $0x10,%edi
80101f63:	3b 7b 58             	cmp    0x58(%ebx),%edi
80101f66:	73 19                	jae    80101f81 <dirlink+0x51>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101f68:	6a 10                	push   $0x10
80101f6a:	57                   	push   %edi
80101f6b:	56                   	push   %esi
80101f6c:	53                   	push   %ebx
80101f6d:	e8 be fa ff ff       	call   80101a30 <readi>
80101f72:	83 c4 10             	add    $0x10,%esp
80101f75:	83 f8 10             	cmp    $0x10,%eax
80101f78:	75 4e                	jne    80101fc8 <dirlink+0x98>
    if(de.inum == 0)
80101f7a:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
80101f7f:	75 df                	jne    80101f60 <dirlink+0x30>
  strncpy(de.name, name, DIRSIZ);
80101f81:	83 ec 04             	sub    $0x4,%esp
80101f84:	8d 45 da             	lea    -0x26(%ebp),%eax
80101f87:	6a 0e                	push   $0xe
80101f89:	ff 75 0c             	pushl  0xc(%ebp)
80101f8c:	50                   	push   %eax
80101f8d:	e8 2e 28 00 00       	call   801047c0 <strncpy>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101f92:	6a 10                	push   $0x10
  de.inum = inum;
80101f94:	8b 45 10             	mov    0x10(%ebp),%eax
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101f97:	57                   	push   %edi
80101f98:	56                   	push   %esi
80101f99:	53                   	push   %ebx
  de.inum = inum;
80101f9a:	66 89 45 d8          	mov    %ax,-0x28(%ebp)
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80101f9e:	e8 8d fb ff ff       	call   80101b30 <writei>
80101fa3:	83 c4 20             	add    $0x20,%esp
80101fa6:	83 f8 10             	cmp    $0x10,%eax
80101fa9:	75 2a                	jne    80101fd5 <dirlink+0xa5>
  return 0;
80101fab:	31 c0                	xor    %eax,%eax
}
80101fad:	8d 65 f4             	lea    -0xc(%ebp),%esp
80101fb0:	5b                   	pop    %ebx
80101fb1:	5e                   	pop    %esi
80101fb2:	5f                   	pop    %edi
80101fb3:	5d                   	pop    %ebp
80101fb4:	c3                   	ret    
    iput(ip);
80101fb5:	83 ec 0c             	sub    $0xc,%esp
80101fb8:	50                   	push   %eax
80101fb9:	e8 c2 f8 ff ff       	call   80101880 <iput>
    return -1;
80101fbe:	83 c4 10             	add    $0x10,%esp
80101fc1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80101fc6:	eb e5                	jmp    80101fad <dirlink+0x7d>
      panic("dirlink read");
80101fc8:	83 ec 0c             	sub    $0xc,%esp
80101fcb:	68 f1 79 10 80       	push   $0x801079f1
80101fd0:	e8 bb e3 ff ff       	call   80100390 <panic>
    panic("dirlink");
80101fd5:	83 ec 0c             	sub    $0xc,%esp
80101fd8:	68 f2 7f 10 80       	push   $0x80107ff2
80101fdd:	e8 ae e3 ff ff       	call   80100390 <panic>
80101fe2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80101fe9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80101ff0 <namei>:

struct inode*
namei(char *path)
{
80101ff0:	55                   	push   %ebp
  char name[DIRSIZ];
  return namex(path, 0, name);
80101ff1:	31 d2                	xor    %edx,%edx
{
80101ff3:	89 e5                	mov    %esp,%ebp
80101ff5:	83 ec 18             	sub    $0x18,%esp
  return namex(path, 0, name);
80101ff8:	8b 45 08             	mov    0x8(%ebp),%eax
80101ffb:	8d 4d ea             	lea    -0x16(%ebp),%ecx
80101ffe:	e8 2d fd ff ff       	call   80101d30 <namex>
}
80102003:	c9                   	leave  
80102004:	c3                   	ret    
80102005:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010200c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80102010 <nameiparent>:

struct inode*
nameiparent(char *path, char *name)
{
80102010:	55                   	push   %ebp
  return namex(path, 1, name);
80102011:	ba 01 00 00 00       	mov    $0x1,%edx
{
80102016:	89 e5                	mov    %esp,%ebp
  return namex(path, 1, name);
80102018:	8b 4d 0c             	mov    0xc(%ebp),%ecx
8010201b:	8b 45 08             	mov    0x8(%ebp),%eax
}
8010201e:	5d                   	pop    %ebp
  return namex(path, 1, name);
8010201f:	e9 0c fd ff ff       	jmp    80101d30 <namex>
80102024:	66 90                	xchg   %ax,%ax
80102026:	66 90                	xchg   %ax,%ax
80102028:	66 90                	xchg   %ax,%ax
8010202a:	66 90                	xchg   %ax,%ax
8010202c:	66 90                	xchg   %ax,%ax
8010202e:	66 90                	xchg   %ax,%ax

80102030 <idestart>:
}

// Start the request for b.  Caller must hold idelock.
static void
idestart(struct buf *b)
{
80102030:	55                   	push   %ebp
80102031:	89 e5                	mov    %esp,%ebp
80102033:	57                   	push   %edi
80102034:	56                   	push   %esi
80102035:	53                   	push   %ebx
80102036:	83 ec 0c             	sub    $0xc,%esp
  if(b == 0)
80102039:	85 c0                	test   %eax,%eax
8010203b:	0f 84 b4 00 00 00    	je     801020f5 <idestart+0xc5>
    panic("idestart");
  if(b->blockno >= FSSIZE)
80102041:	8b 70 08             	mov    0x8(%eax),%esi
80102044:	89 c3                	mov    %eax,%ebx
80102046:	81 fe e7 03 00 00    	cmp    $0x3e7,%esi
8010204c:	0f 87 96 00 00 00    	ja     801020e8 <idestart+0xb8>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102052:	b9 f7 01 00 00       	mov    $0x1f7,%ecx
80102057:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010205e:	66 90                	xchg   %ax,%ax
80102060:	89 ca                	mov    %ecx,%edx
80102062:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
80102063:	83 e0 c0             	and    $0xffffffc0,%eax
80102066:	3c 40                	cmp    $0x40,%al
80102068:	75 f6                	jne    80102060 <idestart+0x30>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010206a:	31 ff                	xor    %edi,%edi
8010206c:	ba f6 03 00 00       	mov    $0x3f6,%edx
80102071:	89 f8                	mov    %edi,%eax
80102073:	ee                   	out    %al,(%dx)
80102074:	b8 01 00 00 00       	mov    $0x1,%eax
80102079:	ba f2 01 00 00       	mov    $0x1f2,%edx
8010207e:	ee                   	out    %al,(%dx)
8010207f:	ba f3 01 00 00       	mov    $0x1f3,%edx
80102084:	89 f0                	mov    %esi,%eax
80102086:	ee                   	out    %al,(%dx)

  idewait(0);
  outb(0x3f6, 0);  // generate interrupt
  outb(0x1f2, sector_per_block);  // number of sectors
  outb(0x1f3, sector & 0xff);
  outb(0x1f4, (sector >> 8) & 0xff);
80102087:	89 f0                	mov    %esi,%eax
80102089:	ba f4 01 00 00       	mov    $0x1f4,%edx
8010208e:	c1 f8 08             	sar    $0x8,%eax
80102091:	ee                   	out    %al,(%dx)
80102092:	ba f5 01 00 00       	mov    $0x1f5,%edx
80102097:	89 f8                	mov    %edi,%eax
80102099:	ee                   	out    %al,(%dx)
  outb(0x1f5, (sector >> 16) & 0xff);
  outb(0x1f6, 0xe0 | ((b->dev&1)<<4) | ((sector>>24)&0x0f));
8010209a:	0f b6 43 04          	movzbl 0x4(%ebx),%eax
8010209e:	ba f6 01 00 00       	mov    $0x1f6,%edx
801020a3:	c1 e0 04             	shl    $0x4,%eax
801020a6:	83 e0 10             	and    $0x10,%eax
801020a9:	83 c8 e0             	or     $0xffffffe0,%eax
801020ac:	ee                   	out    %al,(%dx)
  if(b->flags & B_DIRTY){
801020ad:	f6 03 04             	testb  $0x4,(%ebx)
801020b0:	75 16                	jne    801020c8 <idestart+0x98>
801020b2:	b8 20 00 00 00       	mov    $0x20,%eax
801020b7:	89 ca                	mov    %ecx,%edx
801020b9:	ee                   	out    %al,(%dx)
    outb(0x1f7, write_cmd);
    outsl(0x1f0, b->data, BSIZE/4);
  } else {
    outb(0x1f7, read_cmd);
  }
}
801020ba:	8d 65 f4             	lea    -0xc(%ebp),%esp
801020bd:	5b                   	pop    %ebx
801020be:	5e                   	pop    %esi
801020bf:	5f                   	pop    %edi
801020c0:	5d                   	pop    %ebp
801020c1:	c3                   	ret    
801020c2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
801020c8:	b8 30 00 00 00       	mov    $0x30,%eax
801020cd:	89 ca                	mov    %ecx,%edx
801020cf:	ee                   	out    %al,(%dx)
  asm volatile("cld; rep outsl" :
801020d0:	b9 80 00 00 00       	mov    $0x80,%ecx
    outsl(0x1f0, b->data, BSIZE/4);
801020d5:	8d 73 5c             	lea    0x5c(%ebx),%esi
801020d8:	ba f0 01 00 00       	mov    $0x1f0,%edx
801020dd:	fc                   	cld    
801020de:	f3 6f                	rep outsl %ds:(%esi),(%dx)
}
801020e0:	8d 65 f4             	lea    -0xc(%ebp),%esp
801020e3:	5b                   	pop    %ebx
801020e4:	5e                   	pop    %esi
801020e5:	5f                   	pop    %edi
801020e6:	5d                   	pop    %ebp
801020e7:	c3                   	ret    
    panic("incorrect blockno");
801020e8:	83 ec 0c             	sub    $0xc,%esp
801020eb:	68 5c 7a 10 80       	push   $0x80107a5c
801020f0:	e8 9b e2 ff ff       	call   80100390 <panic>
    panic("idestart");
801020f5:	83 ec 0c             	sub    $0xc,%esp
801020f8:	68 53 7a 10 80       	push   $0x80107a53
801020fd:	e8 8e e2 ff ff       	call   80100390 <panic>
80102102:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102109:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80102110 <ideinit>:
{
80102110:	55                   	push   %ebp
80102111:	89 e5                	mov    %esp,%ebp
80102113:	83 ec 10             	sub    $0x10,%esp
  initlock(&idelock, "ide");
80102116:	68 6e 7a 10 80       	push   $0x80107a6e
8010211b:	68 80 b5 10 80       	push   $0x8010b580
80102120:	e8 bb 22 00 00       	call   801043e0 <initlock>
  ioapicenable(IRQ_IDE, ncpu - 1);
80102125:	58                   	pop    %eax
80102126:	a1 c0 3d 11 80       	mov    0x80113dc0,%eax
8010212b:	5a                   	pop    %edx
8010212c:	83 e8 01             	sub    $0x1,%eax
8010212f:	50                   	push   %eax
80102130:	6a 0e                	push   $0xe
80102132:	e8 a9 02 00 00       	call   801023e0 <ioapicenable>
80102137:	83 c4 10             	add    $0x10,%esp
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010213a:	ba f7 01 00 00       	mov    $0x1f7,%edx
8010213f:	90                   	nop
80102140:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
80102141:	83 e0 c0             	and    $0xffffffc0,%eax
80102144:	3c 40                	cmp    $0x40,%al
80102146:	75 f8                	jne    80102140 <ideinit+0x30>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102148:	b8 f0 ff ff ff       	mov    $0xfffffff0,%eax
8010214d:	ba f6 01 00 00       	mov    $0x1f6,%edx
80102152:	ee                   	out    %al,(%dx)
80102153:	b9 e8 03 00 00       	mov    $0x3e8,%ecx
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102158:	ba f7 01 00 00       	mov    $0x1f7,%edx
8010215d:	eb 06                	jmp    80102165 <ideinit+0x55>
8010215f:	90                   	nop
  for(i=0; i<1000; i++){
80102160:	83 e9 01             	sub    $0x1,%ecx
80102163:	74 0f                	je     80102174 <ideinit+0x64>
80102165:	ec                   	in     (%dx),%al
    if(inb(0x1f7) != 0){
80102166:	84 c0                	test   %al,%al
80102168:	74 f6                	je     80102160 <ideinit+0x50>
      havedisk1 = 1;
8010216a:	c7 05 60 b5 10 80 01 	movl   $0x1,0x8010b560
80102171:	00 00 00 
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102174:	b8 e0 ff ff ff       	mov    $0xffffffe0,%eax
80102179:	ba f6 01 00 00       	mov    $0x1f6,%edx
8010217e:	ee                   	out    %al,(%dx)
}
8010217f:	c9                   	leave  
80102180:	c3                   	ret    
80102181:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102188:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010218f:	90                   	nop

80102190 <ideintr>:

// Interrupt handler.
void
ideintr(void)
{
80102190:	55                   	push   %ebp
80102191:	89 e5                	mov    %esp,%ebp
80102193:	57                   	push   %edi
80102194:	56                   	push   %esi
80102195:	53                   	push   %ebx
80102196:	83 ec 18             	sub    $0x18,%esp
  struct buf *b;

  // First queued buffer is the active request.
  acquire(&idelock);
80102199:	68 80 b5 10 80       	push   $0x8010b580
8010219e:	e8 9d 23 00 00       	call   80104540 <acquire>

  if((b = idequeue) == 0){
801021a3:	8b 1d 64 b5 10 80    	mov    0x8010b564,%ebx
801021a9:	83 c4 10             	add    $0x10,%esp
801021ac:	85 db                	test   %ebx,%ebx
801021ae:	74 63                	je     80102213 <ideintr+0x83>
    release(&idelock);
    return;
  }
  idequeue = b->qnext;
801021b0:	8b 43 58             	mov    0x58(%ebx),%eax
801021b3:	a3 64 b5 10 80       	mov    %eax,0x8010b564

  // Read data if needed.
  if(!(b->flags & B_DIRTY) && idewait(1) >= 0)
801021b8:	8b 33                	mov    (%ebx),%esi
801021ba:	f7 c6 04 00 00 00    	test   $0x4,%esi
801021c0:	75 2f                	jne    801021f1 <ideintr+0x61>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801021c2:	ba f7 01 00 00       	mov    $0x1f7,%edx
801021c7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801021ce:	66 90                	xchg   %ax,%ax
801021d0:	ec                   	in     (%dx),%al
  while(((r = inb(0x1f7)) & (IDE_BSY|IDE_DRDY)) != IDE_DRDY)
801021d1:	89 c1                	mov    %eax,%ecx
801021d3:	83 e1 c0             	and    $0xffffffc0,%ecx
801021d6:	80 f9 40             	cmp    $0x40,%cl
801021d9:	75 f5                	jne    801021d0 <ideintr+0x40>
  if(checkerr && (r & (IDE_DF|IDE_ERR)) != 0)
801021db:	a8 21                	test   $0x21,%al
801021dd:	75 12                	jne    801021f1 <ideintr+0x61>
    insl(0x1f0, b->data, BSIZE/4);
801021df:	8d 7b 5c             	lea    0x5c(%ebx),%edi
  asm volatile("cld; rep insl" :
801021e2:	b9 80 00 00 00       	mov    $0x80,%ecx
801021e7:	ba f0 01 00 00       	mov    $0x1f0,%edx
801021ec:	fc                   	cld    
801021ed:	f3 6d                	rep insl (%dx),%es:(%edi)
801021ef:	8b 33                	mov    (%ebx),%esi

  // Wake process waiting for this buf.
  b->flags |= B_VALID;
  b->flags &= ~B_DIRTY;
801021f1:	83 e6 fb             	and    $0xfffffffb,%esi
  wakeup(b);
801021f4:	83 ec 0c             	sub    $0xc,%esp
  b->flags &= ~B_DIRTY;
801021f7:	83 ce 02             	or     $0x2,%esi
801021fa:	89 33                	mov    %esi,(%ebx)
  wakeup(b);
801021fc:	53                   	push   %ebx
801021fd:	e8 ee 1e 00 00       	call   801040f0 <wakeup>

  // Start disk on next buf in queue.
  if(idequeue != 0)
80102202:	a1 64 b5 10 80       	mov    0x8010b564,%eax
80102207:	83 c4 10             	add    $0x10,%esp
8010220a:	85 c0                	test   %eax,%eax
8010220c:	74 05                	je     80102213 <ideintr+0x83>
    idestart(idequeue);
8010220e:	e8 1d fe ff ff       	call   80102030 <idestart>
    release(&idelock);
80102213:	83 ec 0c             	sub    $0xc,%esp
80102216:	68 80 b5 10 80       	push   $0x8010b580
8010221b:	e8 e0 23 00 00       	call   80104600 <release>

  release(&idelock);
}
80102220:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102223:	5b                   	pop    %ebx
80102224:	5e                   	pop    %esi
80102225:	5f                   	pop    %edi
80102226:	5d                   	pop    %ebp
80102227:	c3                   	ret    
80102228:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010222f:	90                   	nop

80102230 <iderw>:
// Sync buf with disk.
// If B_DIRTY is set, write buf to disk, clear B_DIRTY, set B_VALID.
// Else if B_VALID is not set, read buf from disk, set B_VALID.
void
iderw(struct buf *b)
{
80102230:	55                   	push   %ebp
80102231:	89 e5                	mov    %esp,%ebp
80102233:	53                   	push   %ebx
80102234:	83 ec 10             	sub    $0x10,%esp
80102237:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct buf **pp;

  if(!holdingsleep(&b->lock))
8010223a:	8d 43 0c             	lea    0xc(%ebx),%eax
8010223d:	50                   	push   %eax
8010223e:	e8 4d 21 00 00       	call   80104390 <holdingsleep>
80102243:	83 c4 10             	add    $0x10,%esp
80102246:	85 c0                	test   %eax,%eax
80102248:	0f 84 d3 00 00 00    	je     80102321 <iderw+0xf1>
    panic("iderw: buf not locked");
  if((b->flags & (B_VALID|B_DIRTY)) == B_VALID)
8010224e:	8b 03                	mov    (%ebx),%eax
80102250:	83 e0 06             	and    $0x6,%eax
80102253:	83 f8 02             	cmp    $0x2,%eax
80102256:	0f 84 b8 00 00 00    	je     80102314 <iderw+0xe4>
    panic("iderw: nothing to do");
  if(b->dev != 0 && !havedisk1)
8010225c:	8b 53 04             	mov    0x4(%ebx),%edx
8010225f:	85 d2                	test   %edx,%edx
80102261:	74 0d                	je     80102270 <iderw+0x40>
80102263:	a1 60 b5 10 80       	mov    0x8010b560,%eax
80102268:	85 c0                	test   %eax,%eax
8010226a:	0f 84 97 00 00 00    	je     80102307 <iderw+0xd7>
    panic("iderw: ide disk 1 not present");

  acquire(&idelock);  //DOC:acquire-lock
80102270:	83 ec 0c             	sub    $0xc,%esp
80102273:	68 80 b5 10 80       	push   $0x8010b580
80102278:	e8 c3 22 00 00       	call   80104540 <acquire>

  // Append b to idequeue.
  b->qnext = 0;
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
8010227d:	8b 15 64 b5 10 80    	mov    0x8010b564,%edx
  b->qnext = 0;
80102283:	c7 43 58 00 00 00 00 	movl   $0x0,0x58(%ebx)
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
8010228a:	83 c4 10             	add    $0x10,%esp
8010228d:	85 d2                	test   %edx,%edx
8010228f:	75 09                	jne    8010229a <iderw+0x6a>
80102291:	eb 6d                	jmp    80102300 <iderw+0xd0>
80102293:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102297:	90                   	nop
80102298:	89 c2                	mov    %eax,%edx
8010229a:	8b 42 58             	mov    0x58(%edx),%eax
8010229d:	85 c0                	test   %eax,%eax
8010229f:	75 f7                	jne    80102298 <iderw+0x68>
801022a1:	83 c2 58             	add    $0x58,%edx
    ;
  *pp = b;
801022a4:	89 1a                	mov    %ebx,(%edx)

  // Start disk if necessary.
  if(idequeue == b)
801022a6:	39 1d 64 b5 10 80    	cmp    %ebx,0x8010b564
801022ac:	74 42                	je     801022f0 <iderw+0xc0>
    idestart(b);

  // Wait for request to finish.
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
801022ae:	8b 03                	mov    (%ebx),%eax
801022b0:	83 e0 06             	and    $0x6,%eax
801022b3:	83 f8 02             	cmp    $0x2,%eax
801022b6:	74 23                	je     801022db <iderw+0xab>
801022b8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801022bf:	90                   	nop
    sleep(b, &idelock);
801022c0:	83 ec 08             	sub    $0x8,%esp
801022c3:	68 80 b5 10 80       	push   $0x8010b580
801022c8:	53                   	push   %ebx
801022c9:	e8 62 1c 00 00       	call   80103f30 <sleep>
  while((b->flags & (B_VALID|B_DIRTY)) != B_VALID){
801022ce:	8b 03                	mov    (%ebx),%eax
801022d0:	83 c4 10             	add    $0x10,%esp
801022d3:	83 e0 06             	and    $0x6,%eax
801022d6:	83 f8 02             	cmp    $0x2,%eax
801022d9:	75 e5                	jne    801022c0 <iderw+0x90>
  }


  release(&idelock);
801022db:	c7 45 08 80 b5 10 80 	movl   $0x8010b580,0x8(%ebp)
}
801022e2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801022e5:	c9                   	leave  
  release(&idelock);
801022e6:	e9 15 23 00 00       	jmp    80104600 <release>
801022eb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801022ef:	90                   	nop
    idestart(b);
801022f0:	89 d8                	mov    %ebx,%eax
801022f2:	e8 39 fd ff ff       	call   80102030 <idestart>
801022f7:	eb b5                	jmp    801022ae <iderw+0x7e>
801022f9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  for(pp=&idequeue; *pp; pp=&(*pp)->qnext)  //DOC:insert-queue
80102300:	ba 64 b5 10 80       	mov    $0x8010b564,%edx
80102305:	eb 9d                	jmp    801022a4 <iderw+0x74>
    panic("iderw: ide disk 1 not present");
80102307:	83 ec 0c             	sub    $0xc,%esp
8010230a:	68 9d 7a 10 80       	push   $0x80107a9d
8010230f:	e8 7c e0 ff ff       	call   80100390 <panic>
    panic("iderw: nothing to do");
80102314:	83 ec 0c             	sub    $0xc,%esp
80102317:	68 88 7a 10 80       	push   $0x80107a88
8010231c:	e8 6f e0 ff ff       	call   80100390 <panic>
    panic("iderw: buf not locked");
80102321:	83 ec 0c             	sub    $0xc,%esp
80102324:	68 72 7a 10 80       	push   $0x80107a72
80102329:	e8 62 e0 ff ff       	call   80100390 <panic>
8010232e:	66 90                	xchg   %ax,%ax

80102330 <ioapicinit>:
  ioapic->data = data;
}

void
ioapicinit(void)
{
80102330:	55                   	push   %ebp
  int i, id, maxintr;

  ioapic = (volatile struct ioapic*)IOAPIC;
80102331:	c7 05 f4 36 11 80 00 	movl   $0xfec00000,0x801136f4
80102338:	00 c0 fe 
{
8010233b:	89 e5                	mov    %esp,%ebp
8010233d:	56                   	push   %esi
8010233e:	53                   	push   %ebx
  ioapic->reg = reg;
8010233f:	c7 05 00 00 c0 fe 01 	movl   $0x1,0xfec00000
80102346:	00 00 00 
  return ioapic->data;
80102349:	8b 15 f4 36 11 80    	mov    0x801136f4,%edx
8010234f:	8b 72 10             	mov    0x10(%edx),%esi
  ioapic->reg = reg;
80102352:	c7 02 00 00 00 00    	movl   $0x0,(%edx)
  return ioapic->data;
80102358:	8b 0d f4 36 11 80    	mov    0x801136f4,%ecx
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
  id = ioapicread(REG_ID) >> 24;
  if(id != ioapicid)
8010235e:	0f b6 15 20 38 11 80 	movzbl 0x80113820,%edx
  maxintr = (ioapicread(REG_VER) >> 16) & 0xFF;
80102365:	c1 ee 10             	shr    $0x10,%esi
80102368:	89 f0                	mov    %esi,%eax
8010236a:	0f b6 f0             	movzbl %al,%esi
  return ioapic->data;
8010236d:	8b 41 10             	mov    0x10(%ecx),%eax
  id = ioapicread(REG_ID) >> 24;
80102370:	c1 e8 18             	shr    $0x18,%eax
  if(id != ioapicid)
80102373:	39 c2                	cmp    %eax,%edx
80102375:	74 16                	je     8010238d <ioapicinit+0x5d>
    cprintf("ioapicinit: id isn't equal to ioapicid; not a MP\n");
80102377:	83 ec 0c             	sub    $0xc,%esp
8010237a:	68 bc 7a 10 80       	push   $0x80107abc
8010237f:	e8 2c e3 ff ff       	call   801006b0 <cprintf>
80102384:	8b 0d f4 36 11 80    	mov    0x801136f4,%ecx
8010238a:	83 c4 10             	add    $0x10,%esp
8010238d:	83 c6 21             	add    $0x21,%esi
{
80102390:	ba 10 00 00 00       	mov    $0x10,%edx
80102395:	b8 20 00 00 00       	mov    $0x20,%eax
8010239a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  ioapic->reg = reg;
801023a0:	89 11                	mov    %edx,(%ecx)

  // Mark all interrupts edge-triggered, active high, disabled,
  // and not routed to any CPUs.
  for(i = 0; i <= maxintr; i++){
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
801023a2:	89 c3                	mov    %eax,%ebx
  ioapic->data = data;
801023a4:	8b 0d f4 36 11 80    	mov    0x801136f4,%ecx
801023aa:	83 c0 01             	add    $0x1,%eax
    ioapicwrite(REG_TABLE+2*i, INT_DISABLED | (T_IRQ0 + i));
801023ad:	81 cb 00 00 01 00    	or     $0x10000,%ebx
  ioapic->data = data;
801023b3:	89 59 10             	mov    %ebx,0x10(%ecx)
  ioapic->reg = reg;
801023b6:	8d 5a 01             	lea    0x1(%edx),%ebx
801023b9:	83 c2 02             	add    $0x2,%edx
801023bc:	89 19                	mov    %ebx,(%ecx)
  ioapic->data = data;
801023be:	8b 0d f4 36 11 80    	mov    0x801136f4,%ecx
801023c4:	c7 41 10 00 00 00 00 	movl   $0x0,0x10(%ecx)
  for(i = 0; i <= maxintr; i++){
801023cb:	39 f0                	cmp    %esi,%eax
801023cd:	75 d1                	jne    801023a0 <ioapicinit+0x70>
    ioapicwrite(REG_TABLE+2*i+1, 0);
  }
}
801023cf:	8d 65 f8             	lea    -0x8(%ebp),%esp
801023d2:	5b                   	pop    %ebx
801023d3:	5e                   	pop    %esi
801023d4:	5d                   	pop    %ebp
801023d5:	c3                   	ret    
801023d6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801023dd:	8d 76 00             	lea    0x0(%esi),%esi

801023e0 <ioapicenable>:

void
ioapicenable(int irq, int cpunum)
{
801023e0:	55                   	push   %ebp
  ioapic->reg = reg;
801023e1:	8b 0d f4 36 11 80    	mov    0x801136f4,%ecx
{
801023e7:	89 e5                	mov    %esp,%ebp
801023e9:	8b 45 08             	mov    0x8(%ebp),%eax
  // Mark interrupt edge-triggered, active high,
  // enabled, and routed to the given cpunum,
  // which happens to be that cpu's APIC ID.
  ioapicwrite(REG_TABLE+2*irq, T_IRQ0 + irq);
801023ec:	8d 50 20             	lea    0x20(%eax),%edx
801023ef:	8d 44 00 10          	lea    0x10(%eax,%eax,1),%eax
  ioapic->reg = reg;
801023f3:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
801023f5:	8b 0d f4 36 11 80    	mov    0x801136f4,%ecx
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
801023fb:	83 c0 01             	add    $0x1,%eax
  ioapic->data = data;
801023fe:	89 51 10             	mov    %edx,0x10(%ecx)
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
80102401:	8b 55 0c             	mov    0xc(%ebp),%edx
  ioapic->reg = reg;
80102404:	89 01                	mov    %eax,(%ecx)
  ioapic->data = data;
80102406:	a1 f4 36 11 80       	mov    0x801136f4,%eax
  ioapicwrite(REG_TABLE+2*irq+1, cpunum << 24);
8010240b:	c1 e2 18             	shl    $0x18,%edx
  ioapic->data = data;
8010240e:	89 50 10             	mov    %edx,0x10(%eax)
}
80102411:	5d                   	pop    %ebp
80102412:	c3                   	ret    
80102413:	66 90                	xchg   %ax,%ax
80102415:	66 90                	xchg   %ax,%ax
80102417:	66 90                	xchg   %ax,%ax
80102419:	66 90                	xchg   %ax,%ax
8010241b:	66 90                	xchg   %ax,%ax
8010241d:	66 90                	xchg   %ax,%ax
8010241f:	90                   	nop

80102420 <kfree>:
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(char *v)
{
80102420:	55                   	push   %ebp
80102421:	89 e5                	mov    %esp,%ebp
80102423:	53                   	push   %ebx
80102424:	83 ec 04             	sub    $0x4,%esp
80102427:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct run *r;

  if((uint)v % PGSIZE || v < end || V2P(v) >= PHYSTOP)
8010242a:	f7 c3 ff 0f 00 00    	test   $0xfff,%ebx
80102430:	75 76                	jne    801024a8 <kfree+0x88>
80102432:	81 fb a4 76 11 80    	cmp    $0x801176a4,%ebx
80102438:	72 6e                	jb     801024a8 <kfree+0x88>
8010243a:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80102440:	3d ff ff ff 0d       	cmp    $0xdffffff,%eax
80102445:	77 61                	ja     801024a8 <kfree+0x88>
    panic("kfree");

  // Fill with junk to catch dangling refs.
  memset(v, 1, PGSIZE);
80102447:	83 ec 04             	sub    $0x4,%esp
8010244a:	68 00 10 00 00       	push   $0x1000
8010244f:	6a 01                	push   $0x1
80102451:	53                   	push   %ebx
80102452:	e8 f9 21 00 00       	call   80104650 <memset>

  if(kmem.use_lock)
80102457:	8b 15 34 37 11 80    	mov    0x80113734,%edx
8010245d:	83 c4 10             	add    $0x10,%esp
80102460:	85 d2                	test   %edx,%edx
80102462:	75 1c                	jne    80102480 <kfree+0x60>
    acquire(&kmem.lock);
  r = (struct run*)v;
  r->next = kmem.freelist;
80102464:	a1 38 37 11 80       	mov    0x80113738,%eax
80102469:	89 03                	mov    %eax,(%ebx)
  kmem.freelist = r;
  if(kmem.use_lock)
8010246b:	a1 34 37 11 80       	mov    0x80113734,%eax
  kmem.freelist = r;
80102470:	89 1d 38 37 11 80    	mov    %ebx,0x80113738
  if(kmem.use_lock)
80102476:	85 c0                	test   %eax,%eax
80102478:	75 1e                	jne    80102498 <kfree+0x78>
    release(&kmem.lock);
}
8010247a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010247d:	c9                   	leave  
8010247e:	c3                   	ret    
8010247f:	90                   	nop
    acquire(&kmem.lock);
80102480:	83 ec 0c             	sub    $0xc,%esp
80102483:	68 00 37 11 80       	push   $0x80113700
80102488:	e8 b3 20 00 00       	call   80104540 <acquire>
8010248d:	83 c4 10             	add    $0x10,%esp
80102490:	eb d2                	jmp    80102464 <kfree+0x44>
80102492:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    release(&kmem.lock);
80102498:	c7 45 08 00 37 11 80 	movl   $0x80113700,0x8(%ebp)
}
8010249f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801024a2:	c9                   	leave  
    release(&kmem.lock);
801024a3:	e9 58 21 00 00       	jmp    80104600 <release>
    panic("kfree");
801024a8:	83 ec 0c             	sub    $0xc,%esp
801024ab:	68 ee 7a 10 80       	push   $0x80107aee
801024b0:	e8 db de ff ff       	call   80100390 <panic>
801024b5:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801024bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801024c0 <freerange>:
{
801024c0:	55                   	push   %ebp
801024c1:	89 e5                	mov    %esp,%ebp
801024c3:	56                   	push   %esi
  p = (char*)PGROUNDUP((uint)vstart);
801024c4:	8b 45 08             	mov    0x8(%ebp),%eax
{
801024c7:	8b 75 0c             	mov    0xc(%ebp),%esi
801024ca:	53                   	push   %ebx
  p = (char*)PGROUNDUP((uint)vstart);
801024cb:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
801024d1:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801024d7:	81 c3 00 10 00 00    	add    $0x1000,%ebx
801024dd:	39 de                	cmp    %ebx,%esi
801024df:	72 23                	jb     80102504 <freerange+0x44>
801024e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    kfree(p);
801024e8:	83 ec 0c             	sub    $0xc,%esp
801024eb:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801024f1:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
801024f7:	50                   	push   %eax
801024f8:	e8 23 ff ff ff       	call   80102420 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801024fd:	83 c4 10             	add    $0x10,%esp
80102500:	39 f3                	cmp    %esi,%ebx
80102502:	76 e4                	jbe    801024e8 <freerange+0x28>
}
80102504:	8d 65 f8             	lea    -0x8(%ebp),%esp
80102507:	5b                   	pop    %ebx
80102508:	5e                   	pop    %esi
80102509:	5d                   	pop    %ebp
8010250a:	c3                   	ret    
8010250b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010250f:	90                   	nop

80102510 <kinit1>:
{
80102510:	55                   	push   %ebp
80102511:	89 e5                	mov    %esp,%ebp
80102513:	56                   	push   %esi
80102514:	53                   	push   %ebx
80102515:	8b 75 0c             	mov    0xc(%ebp),%esi
  initlock(&kmem.lock, "kmem");
80102518:	83 ec 08             	sub    $0x8,%esp
8010251b:	68 f4 7a 10 80       	push   $0x80107af4
80102520:	68 00 37 11 80       	push   $0x80113700
80102525:	e8 b6 1e 00 00       	call   801043e0 <initlock>
  p = (char*)PGROUNDUP((uint)vstart);
8010252a:	8b 45 08             	mov    0x8(%ebp),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
8010252d:	83 c4 10             	add    $0x10,%esp
  kmem.use_lock = 0;
80102530:	c7 05 34 37 11 80 00 	movl   $0x0,0x80113734
80102537:	00 00 00 
  p = (char*)PGROUNDUP((uint)vstart);
8010253a:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102540:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102546:	81 c3 00 10 00 00    	add    $0x1000,%ebx
8010254c:	39 de                	cmp    %ebx,%esi
8010254e:	72 1c                	jb     8010256c <kinit1+0x5c>
    kfree(p);
80102550:	83 ec 0c             	sub    $0xc,%esp
80102553:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102559:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
8010255f:	50                   	push   %eax
80102560:	e8 bb fe ff ff       	call   80102420 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102565:	83 c4 10             	add    $0x10,%esp
80102568:	39 de                	cmp    %ebx,%esi
8010256a:	73 e4                	jae    80102550 <kinit1+0x40>
}
8010256c:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010256f:	5b                   	pop    %ebx
80102570:	5e                   	pop    %esi
80102571:	5d                   	pop    %ebp
80102572:	c3                   	ret    
80102573:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010257a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80102580 <kinit2>:
{
80102580:	55                   	push   %ebp
80102581:	89 e5                	mov    %esp,%ebp
80102583:	56                   	push   %esi
  p = (char*)PGROUNDUP((uint)vstart);
80102584:	8b 45 08             	mov    0x8(%ebp),%eax
{
80102587:	8b 75 0c             	mov    0xc(%ebp),%esi
8010258a:	53                   	push   %ebx
  p = (char*)PGROUNDUP((uint)vstart);
8010258b:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80102591:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
80102597:	81 c3 00 10 00 00    	add    $0x1000,%ebx
8010259d:	39 de                	cmp    %ebx,%esi
8010259f:	72 23                	jb     801025c4 <kinit2+0x44>
801025a1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    kfree(p);
801025a8:	83 ec 0c             	sub    $0xc,%esp
801025ab:	8d 83 00 f0 ff ff    	lea    -0x1000(%ebx),%eax
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801025b1:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    kfree(p);
801025b7:	50                   	push   %eax
801025b8:	e8 63 fe ff ff       	call   80102420 <kfree>
  for(; p + PGSIZE <= (char*)vend; p += PGSIZE)
801025bd:	83 c4 10             	add    $0x10,%esp
801025c0:	39 de                	cmp    %ebx,%esi
801025c2:	73 e4                	jae    801025a8 <kinit2+0x28>
  kmem.use_lock = 1;
801025c4:	c7 05 34 37 11 80 01 	movl   $0x1,0x80113734
801025cb:	00 00 00 
}
801025ce:	8d 65 f8             	lea    -0x8(%ebp),%esp
801025d1:	5b                   	pop    %ebx
801025d2:	5e                   	pop    %esi
801025d3:	5d                   	pop    %ebp
801025d4:	c3                   	ret    
801025d5:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801025dc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801025e0 <kalloc>:
// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
char*
kalloc(void)
{
801025e0:	55                   	push   %ebp
801025e1:	89 e5                	mov    %esp,%ebp
801025e3:	53                   	push   %ebx
801025e4:	83 ec 04             	sub    $0x4,%esp
  struct run *r;

  if(kmem.use_lock)
801025e7:	a1 34 37 11 80       	mov    0x80113734,%eax
801025ec:	85 c0                	test   %eax,%eax
801025ee:	75 20                	jne    80102610 <kalloc+0x30>
    acquire(&kmem.lock);
  r = kmem.freelist;
801025f0:	8b 1d 38 37 11 80    	mov    0x80113738,%ebx
  if(r)
801025f6:	85 db                	test   %ebx,%ebx
801025f8:	74 07                	je     80102601 <kalloc+0x21>
    kmem.freelist = r->next;
801025fa:	8b 03                	mov    (%ebx),%eax
801025fc:	a3 38 37 11 80       	mov    %eax,0x80113738
  if(kmem.use_lock)
    release(&kmem.lock);
  return (char*)r;
}
80102601:	89 d8                	mov    %ebx,%eax
80102603:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102606:	c9                   	leave  
80102607:	c3                   	ret    
80102608:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010260f:	90                   	nop
    acquire(&kmem.lock);
80102610:	83 ec 0c             	sub    $0xc,%esp
80102613:	68 00 37 11 80       	push   $0x80113700
80102618:	e8 23 1f 00 00       	call   80104540 <acquire>
  r = kmem.freelist;
8010261d:	8b 1d 38 37 11 80    	mov    0x80113738,%ebx
  if(r)
80102623:	83 c4 10             	add    $0x10,%esp
80102626:	a1 34 37 11 80       	mov    0x80113734,%eax
8010262b:	85 db                	test   %ebx,%ebx
8010262d:	74 08                	je     80102637 <kalloc+0x57>
    kmem.freelist = r->next;
8010262f:	8b 13                	mov    (%ebx),%edx
80102631:	89 15 38 37 11 80    	mov    %edx,0x80113738
  if(kmem.use_lock)
80102637:	85 c0                	test   %eax,%eax
80102639:	74 c6                	je     80102601 <kalloc+0x21>
    release(&kmem.lock);
8010263b:	83 ec 0c             	sub    $0xc,%esp
8010263e:	68 00 37 11 80       	push   $0x80113700
80102643:	e8 b8 1f 00 00       	call   80104600 <release>
}
80102648:	89 d8                	mov    %ebx,%eax
    release(&kmem.lock);
8010264a:	83 c4 10             	add    $0x10,%esp
}
8010264d:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102650:	c9                   	leave  
80102651:	c3                   	ret    
80102652:	66 90                	xchg   %ax,%ax
80102654:	66 90                	xchg   %ax,%ax
80102656:	66 90                	xchg   %ax,%ax
80102658:	66 90                	xchg   %ax,%ax
8010265a:	66 90                	xchg   %ax,%ax
8010265c:	66 90                	xchg   %ax,%ax
8010265e:	66 90                	xchg   %ax,%ax

80102660 <kbdgetc>:
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102660:	ba 64 00 00 00       	mov    $0x64,%edx
80102665:	ec                   	in     (%dx),%al
    normalmap, shiftmap, ctlmap, ctlmap
  };
  uint st, data, c;

  st = inb(KBSTATP);
  if((st & KBS_DIB) == 0)
80102666:	a8 01                	test   $0x1,%al
80102668:	0f 84 c2 00 00 00    	je     80102730 <kbdgetc+0xd0>
{
8010266e:	55                   	push   %ebp
8010266f:	ba 60 00 00 00       	mov    $0x60,%edx
80102674:	89 e5                	mov    %esp,%ebp
80102676:	53                   	push   %ebx
80102677:	ec                   	in     (%dx),%al
    return -1;
  data = inb(KBDATAP);
80102678:	0f b6 d0             	movzbl %al,%edx

  if(data == 0xE0){
8010267b:	8b 1d b4 b5 10 80    	mov    0x8010b5b4,%ebx
80102681:	81 fa e0 00 00 00    	cmp    $0xe0,%edx
80102687:	74 57                	je     801026e0 <kbdgetc+0x80>
    shift |= E0ESC;
    return 0;
  } else if(data & 0x80){
80102689:	89 d9                	mov    %ebx,%ecx
8010268b:	83 e1 40             	and    $0x40,%ecx
8010268e:	84 c0                	test   %al,%al
80102690:	78 5e                	js     801026f0 <kbdgetc+0x90>
    // Key released
    data = (shift & E0ESC ? data : data & 0x7F);
    shift &= ~(shiftcode[data] | E0ESC);
    return 0;
  } else if(shift & E0ESC){
80102692:	85 c9                	test   %ecx,%ecx
80102694:	74 09                	je     8010269f <kbdgetc+0x3f>
    // Last character was an E0 escape; or with 0x80
    data |= 0x80;
80102696:	83 c8 80             	or     $0xffffff80,%eax
    shift &= ~E0ESC;
80102699:	83 e3 bf             	and    $0xffffffbf,%ebx
    data |= 0x80;
8010269c:	0f b6 d0             	movzbl %al,%edx
  }

  shift |= shiftcode[data];
8010269f:	0f b6 8a 20 7c 10 80 	movzbl -0x7fef83e0(%edx),%ecx
  shift ^= togglecode[data];
801026a6:	0f b6 82 20 7b 10 80 	movzbl -0x7fef84e0(%edx),%eax
  shift |= shiftcode[data];
801026ad:	09 d9                	or     %ebx,%ecx
  shift ^= togglecode[data];
801026af:	31 c1                	xor    %eax,%ecx
  c = charcode[shift & (CTL | SHIFT)][data];
801026b1:	89 c8                	mov    %ecx,%eax
  shift ^= togglecode[data];
801026b3:	89 0d b4 b5 10 80    	mov    %ecx,0x8010b5b4
  c = charcode[shift & (CTL | SHIFT)][data];
801026b9:	83 e0 03             	and    $0x3,%eax
  if(shift & CAPSLOCK){
801026bc:	83 e1 08             	and    $0x8,%ecx
  c = charcode[shift & (CTL | SHIFT)][data];
801026bf:	8b 04 85 00 7b 10 80 	mov    -0x7fef8500(,%eax,4),%eax
801026c6:	0f b6 04 10          	movzbl (%eax,%edx,1),%eax
  if(shift & CAPSLOCK){
801026ca:	74 0b                	je     801026d7 <kbdgetc+0x77>
    if('a' <= c && c <= 'z')
801026cc:	8d 50 9f             	lea    -0x61(%eax),%edx
801026cf:	83 fa 19             	cmp    $0x19,%edx
801026d2:	77 44                	ja     80102718 <kbdgetc+0xb8>
      c += 'A' - 'a';
801026d4:	83 e8 20             	sub    $0x20,%eax
    else if('A' <= c && c <= 'Z')
      c += 'a' - 'A';
  }
  return c;
}
801026d7:	5b                   	pop    %ebx
801026d8:	5d                   	pop    %ebp
801026d9:	c3                   	ret    
801026da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    shift |= E0ESC;
801026e0:	83 cb 40             	or     $0x40,%ebx
    return 0;
801026e3:	31 c0                	xor    %eax,%eax
    shift |= E0ESC;
801026e5:	89 1d b4 b5 10 80    	mov    %ebx,0x8010b5b4
}
801026eb:	5b                   	pop    %ebx
801026ec:	5d                   	pop    %ebp
801026ed:	c3                   	ret    
801026ee:	66 90                	xchg   %ax,%ax
    data = (shift & E0ESC ? data : data & 0x7F);
801026f0:	83 e0 7f             	and    $0x7f,%eax
801026f3:	85 c9                	test   %ecx,%ecx
801026f5:	0f 44 d0             	cmove  %eax,%edx
    return 0;
801026f8:	31 c0                	xor    %eax,%eax
    shift &= ~(shiftcode[data] | E0ESC);
801026fa:	0f b6 8a 20 7c 10 80 	movzbl -0x7fef83e0(%edx),%ecx
80102701:	83 c9 40             	or     $0x40,%ecx
80102704:	0f b6 c9             	movzbl %cl,%ecx
80102707:	f7 d1                	not    %ecx
80102709:	21 d9                	and    %ebx,%ecx
}
8010270b:	5b                   	pop    %ebx
8010270c:	5d                   	pop    %ebp
    shift &= ~(shiftcode[data] | E0ESC);
8010270d:	89 0d b4 b5 10 80    	mov    %ecx,0x8010b5b4
}
80102713:	c3                   	ret    
80102714:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    else if('A' <= c && c <= 'Z')
80102718:	8d 48 bf             	lea    -0x41(%eax),%ecx
      c += 'a' - 'A';
8010271b:	8d 50 20             	lea    0x20(%eax),%edx
}
8010271e:	5b                   	pop    %ebx
8010271f:	5d                   	pop    %ebp
      c += 'a' - 'A';
80102720:	83 f9 1a             	cmp    $0x1a,%ecx
80102723:	0f 42 c2             	cmovb  %edx,%eax
}
80102726:	c3                   	ret    
80102727:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010272e:	66 90                	xchg   %ax,%ax
    return -1;
80102730:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80102735:	c3                   	ret    
80102736:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010273d:	8d 76 00             	lea    0x0(%esi),%esi

80102740 <kbdintr>:

void
kbdintr(void)
{
80102740:	55                   	push   %ebp
80102741:	89 e5                	mov    %esp,%ebp
80102743:	83 ec 14             	sub    $0x14,%esp
  consoleintr(kbdgetc);
80102746:	68 60 26 10 80       	push   $0x80102660
8010274b:	e8 10 e1 ff ff       	call   80100860 <consoleintr>
}
80102750:	83 c4 10             	add    $0x10,%esp
80102753:	c9                   	leave  
80102754:	c3                   	ret    
80102755:	66 90                	xchg   %ax,%ax
80102757:	66 90                	xchg   %ax,%ax
80102759:	66 90                	xchg   %ax,%ax
8010275b:	66 90                	xchg   %ax,%ax
8010275d:	66 90                	xchg   %ax,%ax
8010275f:	90                   	nop

80102760 <lapicinit>:
}

void
lapicinit(void)
{
  if(!lapic)
80102760:	a1 3c 37 11 80       	mov    0x8011373c,%eax
80102765:	85 c0                	test   %eax,%eax
80102767:	0f 84 cb 00 00 00    	je     80102838 <lapicinit+0xd8>
  lapic[index] = value;
8010276d:	c7 80 f0 00 00 00 3f 	movl   $0x13f,0xf0(%eax)
80102774:	01 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102777:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
8010277a:	c7 80 e0 03 00 00 0b 	movl   $0xb,0x3e0(%eax)
80102781:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102784:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102787:	c7 80 20 03 00 00 20 	movl   $0x20020,0x320(%eax)
8010278e:	00 02 00 
  lapic[ID];  // wait for write to finish, by reading
80102791:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102794:	c7 80 80 03 00 00 80 	movl   $0x989680,0x380(%eax)
8010279b:	96 98 00 
  lapic[ID];  // wait for write to finish, by reading
8010279e:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801027a1:	c7 80 50 03 00 00 00 	movl   $0x10000,0x350(%eax)
801027a8:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
801027ab:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801027ae:	c7 80 60 03 00 00 00 	movl   $0x10000,0x360(%eax)
801027b5:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
801027b8:	8b 50 20             	mov    0x20(%eax),%edx
  lapicw(LINT0, MASKED);
  lapicw(LINT1, MASKED);

  // Disable performance counter overflow interrupts
  // on machines that provide that interrupt entry.
  if(((lapic[VER]>>16) & 0xFF) >= 4)
801027bb:	8b 50 30             	mov    0x30(%eax),%edx
801027be:	c1 ea 10             	shr    $0x10,%edx
801027c1:	81 e2 fc 00 00 00    	and    $0xfc,%edx
801027c7:	75 77                	jne    80102840 <lapicinit+0xe0>
  lapic[index] = value;
801027c9:	c7 80 70 03 00 00 33 	movl   $0x33,0x370(%eax)
801027d0:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
801027d3:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801027d6:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
801027dd:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
801027e0:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801027e3:	c7 80 80 02 00 00 00 	movl   $0x0,0x280(%eax)
801027ea:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
801027ed:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801027f0:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
801027f7:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
801027fa:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
801027fd:	c7 80 10 03 00 00 00 	movl   $0x0,0x310(%eax)
80102804:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102807:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
8010280a:	c7 80 00 03 00 00 00 	movl   $0x88500,0x300(%eax)
80102811:	85 08 00 
  lapic[ID];  // wait for write to finish, by reading
80102814:	8b 50 20             	mov    0x20(%eax),%edx
80102817:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010281e:	66 90                	xchg   %ax,%ax
  lapicw(EOI, 0);

  // Send an Init Level De-Assert to synchronise arbitration ID's.
  lapicw(ICRHI, 0);
  lapicw(ICRLO, BCAST | INIT | LEVEL);
  while(lapic[ICRLO] & DELIVS)
80102820:	8b 90 00 03 00 00    	mov    0x300(%eax),%edx
80102826:	80 e6 10             	and    $0x10,%dh
80102829:	75 f5                	jne    80102820 <lapicinit+0xc0>
  lapic[index] = value;
8010282b:	c7 80 80 00 00 00 00 	movl   $0x0,0x80(%eax)
80102832:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102835:	8b 40 20             	mov    0x20(%eax),%eax
    ;

  // Enable interrupts on the APIC (but not on the processor).
  lapicw(TPR, 0);
}
80102838:	c3                   	ret    
80102839:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  lapic[index] = value;
80102840:	c7 80 40 03 00 00 00 	movl   $0x10000,0x340(%eax)
80102847:	00 01 00 
  lapic[ID];  // wait for write to finish, by reading
8010284a:	8b 50 20             	mov    0x20(%eax),%edx
8010284d:	e9 77 ff ff ff       	jmp    801027c9 <lapicinit+0x69>
80102852:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102859:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80102860 <lapicid>:

int
lapicid(void)
{
  if (!lapic)
80102860:	a1 3c 37 11 80       	mov    0x8011373c,%eax
80102865:	85 c0                	test   %eax,%eax
80102867:	74 07                	je     80102870 <lapicid+0x10>
    return 0;
  return lapic[ID] >> 24;
80102869:	8b 40 20             	mov    0x20(%eax),%eax
8010286c:	c1 e8 18             	shr    $0x18,%eax
8010286f:	c3                   	ret    
    return 0;
80102870:	31 c0                	xor    %eax,%eax
}
80102872:	c3                   	ret    
80102873:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010287a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80102880 <lapiceoi>:

// Acknowledge interrupt.
void
lapiceoi(void)
{
  if(lapic)
80102880:	a1 3c 37 11 80       	mov    0x8011373c,%eax
80102885:	85 c0                	test   %eax,%eax
80102887:	74 0d                	je     80102896 <lapiceoi+0x16>
  lapic[index] = value;
80102889:	c7 80 b0 00 00 00 00 	movl   $0x0,0xb0(%eax)
80102890:	00 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102893:	8b 40 20             	mov    0x20(%eax),%eax
    lapicw(EOI, 0);
}
80102896:	c3                   	ret    
80102897:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010289e:	66 90                	xchg   %ax,%ax

801028a0 <microdelay>:
// Spin for a given number of microseconds.
// On real hardware would want to tune this dynamically.
void
microdelay(int us)
{
}
801028a0:	c3                   	ret    
801028a1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801028a8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801028af:	90                   	nop

801028b0 <lapicstartap>:

// Start additional processor running entry code at addr.
// See Appendix B of MultiProcessor Specification.
void
lapicstartap(uchar apicid, uint addr)
{
801028b0:	55                   	push   %ebp
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801028b1:	b8 0f 00 00 00       	mov    $0xf,%eax
801028b6:	ba 70 00 00 00       	mov    $0x70,%edx
801028bb:	89 e5                	mov    %esp,%ebp
801028bd:	53                   	push   %ebx
801028be:	8b 4d 0c             	mov    0xc(%ebp),%ecx
801028c1:	8b 5d 08             	mov    0x8(%ebp),%ebx
801028c4:	ee                   	out    %al,(%dx)
801028c5:	b8 0a 00 00 00       	mov    $0xa,%eax
801028ca:	ba 71 00 00 00       	mov    $0x71,%edx
801028cf:	ee                   	out    %al,(%dx)
  // and the warm reset vector (DWORD based at 40:67) to point at
  // the AP startup code prior to the [universal startup algorithm]."
  outb(CMOS_PORT, 0xF);  // offset 0xF is shutdown code
  outb(CMOS_PORT+1, 0x0A);
  wrv = (ushort*)P2V((0x40<<4 | 0x67));  // Warm reset vector
  wrv[0] = 0;
801028d0:	31 c0                	xor    %eax,%eax
  wrv[1] = addr >> 4;

  // "Universal startup algorithm."
  // Send INIT (level-triggered) interrupt to reset other CPU.
  lapicw(ICRHI, apicid<<24);
801028d2:	c1 e3 18             	shl    $0x18,%ebx
  wrv[0] = 0;
801028d5:	66 a3 67 04 00 80    	mov    %ax,0x80000467
  wrv[1] = addr >> 4;
801028db:	89 c8                	mov    %ecx,%eax
  // when it is in the halted state due to an INIT.  So the second
  // should be ignored, but it is part of the official Intel algorithm.
  // Bochs complains about the second one.  Too bad for Bochs.
  for(i = 0; i < 2; i++){
    lapicw(ICRHI, apicid<<24);
    lapicw(ICRLO, STARTUP | (addr>>12));
801028dd:	c1 e9 0c             	shr    $0xc,%ecx
  lapicw(ICRHI, apicid<<24);
801028e0:	89 da                	mov    %ebx,%edx
  wrv[1] = addr >> 4;
801028e2:	c1 e8 04             	shr    $0x4,%eax
    lapicw(ICRLO, STARTUP | (addr>>12));
801028e5:	80 cd 06             	or     $0x6,%ch
  wrv[1] = addr >> 4;
801028e8:	66 a3 69 04 00 80    	mov    %ax,0x80000469
  lapic[index] = value;
801028ee:	a1 3c 37 11 80       	mov    0x8011373c,%eax
801028f3:	89 98 10 03 00 00    	mov    %ebx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
801028f9:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
801028fc:	c7 80 00 03 00 00 00 	movl   $0xc500,0x300(%eax)
80102903:	c5 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102906:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102909:	c7 80 00 03 00 00 00 	movl   $0x8500,0x300(%eax)
80102910:	85 00 00 
  lapic[ID];  // wait for write to finish, by reading
80102913:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102916:	89 90 10 03 00 00    	mov    %edx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
8010291c:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
8010291f:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
  lapic[ID];  // wait for write to finish, by reading
80102925:	8b 58 20             	mov    0x20(%eax),%ebx
  lapic[index] = value;
80102928:	89 90 10 03 00 00    	mov    %edx,0x310(%eax)
  lapic[ID];  // wait for write to finish, by reading
8010292e:	8b 50 20             	mov    0x20(%eax),%edx
  lapic[index] = value;
80102931:	89 88 00 03 00 00    	mov    %ecx,0x300(%eax)
    microdelay(200);
  }
}
80102937:	5b                   	pop    %ebx
  lapic[ID];  // wait for write to finish, by reading
80102938:	8b 40 20             	mov    0x20(%eax),%eax
}
8010293b:	5d                   	pop    %ebp
8010293c:	c3                   	ret    
8010293d:	8d 76 00             	lea    0x0(%esi),%esi

80102940 <cmostime>:
}

// qemu seems to use 24-hour GWT and the values are BCD encoded
void
cmostime(struct rtcdate *r)
{
80102940:	55                   	push   %ebp
80102941:	b8 0b 00 00 00       	mov    $0xb,%eax
80102946:	ba 70 00 00 00       	mov    $0x70,%edx
8010294b:	89 e5                	mov    %esp,%ebp
8010294d:	57                   	push   %edi
8010294e:	56                   	push   %esi
8010294f:	53                   	push   %ebx
80102950:	83 ec 4c             	sub    $0x4c,%esp
80102953:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102954:	ba 71 00 00 00       	mov    $0x71,%edx
80102959:	ec                   	in     (%dx),%al
  struct rtcdate t1, t2;
  int sb, bcd;

  sb = cmos_read(CMOS_STATB);

  bcd = (sb & (1 << 2)) == 0;
8010295a:	83 e0 04             	and    $0x4,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
8010295d:	bb 70 00 00 00       	mov    $0x70,%ebx
80102962:	88 45 b3             	mov    %al,-0x4d(%ebp)
80102965:	8d 76 00             	lea    0x0(%esi),%esi
80102968:	31 c0                	xor    %eax,%eax
8010296a:	89 da                	mov    %ebx,%edx
8010296c:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010296d:	b9 71 00 00 00       	mov    $0x71,%ecx
80102972:	89 ca                	mov    %ecx,%edx
80102974:	ec                   	in     (%dx),%al
80102975:	88 45 b7             	mov    %al,-0x49(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102978:	89 da                	mov    %ebx,%edx
8010297a:	b8 02 00 00 00       	mov    $0x2,%eax
8010297f:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102980:	89 ca                	mov    %ecx,%edx
80102982:	ec                   	in     (%dx),%al
80102983:	88 45 b6             	mov    %al,-0x4a(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102986:	89 da                	mov    %ebx,%edx
80102988:	b8 04 00 00 00       	mov    $0x4,%eax
8010298d:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010298e:	89 ca                	mov    %ecx,%edx
80102990:	ec                   	in     (%dx),%al
80102991:	88 45 b5             	mov    %al,-0x4b(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102994:	89 da                	mov    %ebx,%edx
80102996:	b8 07 00 00 00       	mov    $0x7,%eax
8010299b:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
8010299c:	89 ca                	mov    %ecx,%edx
8010299e:	ec                   	in     (%dx),%al
8010299f:	88 45 b4             	mov    %al,-0x4c(%ebp)
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801029a2:	89 da                	mov    %ebx,%edx
801029a4:	b8 08 00 00 00       	mov    $0x8,%eax
801029a9:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801029aa:	89 ca                	mov    %ecx,%edx
801029ac:	ec                   	in     (%dx),%al
801029ad:	89 c7                	mov    %eax,%edi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801029af:	89 da                	mov    %ebx,%edx
801029b1:	b8 09 00 00 00       	mov    $0x9,%eax
801029b6:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801029b7:	89 ca                	mov    %ecx,%edx
801029b9:	ec                   	in     (%dx),%al
801029ba:	89 c6                	mov    %eax,%esi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801029bc:	89 da                	mov    %ebx,%edx
801029be:	b8 0a 00 00 00       	mov    $0xa,%eax
801029c3:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801029c4:	89 ca                	mov    %ecx,%edx
801029c6:	ec                   	in     (%dx),%al

  // make sure CMOS doesn't modify time while we read it
  for(;;) {
    fill_rtcdate(&t1);
    if(cmos_read(CMOS_STATA) & CMOS_UIP)
801029c7:	84 c0                	test   %al,%al
801029c9:	78 9d                	js     80102968 <cmostime+0x28>
  return inb(CMOS_RETURN);
801029cb:	0f b6 45 b7          	movzbl -0x49(%ebp),%eax
801029cf:	89 fa                	mov    %edi,%edx
801029d1:	0f b6 fa             	movzbl %dl,%edi
801029d4:	89 f2                	mov    %esi,%edx
801029d6:	89 45 b8             	mov    %eax,-0x48(%ebp)
801029d9:	0f b6 45 b6          	movzbl -0x4a(%ebp),%eax
801029dd:	0f b6 f2             	movzbl %dl,%esi
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801029e0:	89 da                	mov    %ebx,%edx
801029e2:	89 7d c8             	mov    %edi,-0x38(%ebp)
801029e5:	89 45 bc             	mov    %eax,-0x44(%ebp)
801029e8:	0f b6 45 b5          	movzbl -0x4b(%ebp),%eax
801029ec:	89 75 cc             	mov    %esi,-0x34(%ebp)
801029ef:	89 45 c0             	mov    %eax,-0x40(%ebp)
801029f2:	0f b6 45 b4          	movzbl -0x4c(%ebp),%eax
801029f6:	89 45 c4             	mov    %eax,-0x3c(%ebp)
801029f9:	31 c0                	xor    %eax,%eax
801029fb:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801029fc:	89 ca                	mov    %ecx,%edx
801029fe:	ec                   	in     (%dx),%al
801029ff:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a02:	89 da                	mov    %ebx,%edx
80102a04:	89 45 d0             	mov    %eax,-0x30(%ebp)
80102a07:	b8 02 00 00 00       	mov    $0x2,%eax
80102a0c:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a0d:	89 ca                	mov    %ecx,%edx
80102a0f:	ec                   	in     (%dx),%al
80102a10:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a13:	89 da                	mov    %ebx,%edx
80102a15:	89 45 d4             	mov    %eax,-0x2c(%ebp)
80102a18:	b8 04 00 00 00       	mov    $0x4,%eax
80102a1d:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a1e:	89 ca                	mov    %ecx,%edx
80102a20:	ec                   	in     (%dx),%al
80102a21:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a24:	89 da                	mov    %ebx,%edx
80102a26:	89 45 d8             	mov    %eax,-0x28(%ebp)
80102a29:	b8 07 00 00 00       	mov    $0x7,%eax
80102a2e:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a2f:	89 ca                	mov    %ecx,%edx
80102a31:	ec                   	in     (%dx),%al
80102a32:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a35:	89 da                	mov    %ebx,%edx
80102a37:	89 45 dc             	mov    %eax,-0x24(%ebp)
80102a3a:	b8 08 00 00 00       	mov    $0x8,%eax
80102a3f:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a40:	89 ca                	mov    %ecx,%edx
80102a42:	ec                   	in     (%dx),%al
80102a43:	0f b6 c0             	movzbl %al,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80102a46:	89 da                	mov    %ebx,%edx
80102a48:	89 45 e0             	mov    %eax,-0x20(%ebp)
80102a4b:	b8 09 00 00 00       	mov    $0x9,%eax
80102a50:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80102a51:	89 ca                	mov    %ecx,%edx
80102a53:	ec                   	in     (%dx),%al
80102a54:	0f b6 c0             	movzbl %al,%eax
        continue;
    fill_rtcdate(&t2);
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
80102a57:	83 ec 04             	sub    $0x4,%esp
  return inb(CMOS_RETURN);
80102a5a:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(memcmp(&t1, &t2, sizeof(t1)) == 0)
80102a5d:	8d 45 d0             	lea    -0x30(%ebp),%eax
80102a60:	6a 18                	push   $0x18
80102a62:	50                   	push   %eax
80102a63:	8d 45 b8             	lea    -0x48(%ebp),%eax
80102a66:	50                   	push   %eax
80102a67:	e8 34 1c 00 00       	call   801046a0 <memcmp>
80102a6c:	83 c4 10             	add    $0x10,%esp
80102a6f:	85 c0                	test   %eax,%eax
80102a71:	0f 85 f1 fe ff ff    	jne    80102968 <cmostime+0x28>
      break;
  }

  // convert
  if(bcd) {
80102a77:	80 7d b3 00          	cmpb   $0x0,-0x4d(%ebp)
80102a7b:	75 78                	jne    80102af5 <cmostime+0x1b5>
#define    CONV(x)     (t1.x = ((t1.x >> 4) * 10) + (t1.x & 0xf))
    CONV(second);
80102a7d:	8b 45 b8             	mov    -0x48(%ebp),%eax
80102a80:	89 c2                	mov    %eax,%edx
80102a82:	83 e0 0f             	and    $0xf,%eax
80102a85:	c1 ea 04             	shr    $0x4,%edx
80102a88:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102a8b:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102a8e:	89 45 b8             	mov    %eax,-0x48(%ebp)
    CONV(minute);
80102a91:	8b 45 bc             	mov    -0x44(%ebp),%eax
80102a94:	89 c2                	mov    %eax,%edx
80102a96:	83 e0 0f             	and    $0xf,%eax
80102a99:	c1 ea 04             	shr    $0x4,%edx
80102a9c:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102a9f:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102aa2:	89 45 bc             	mov    %eax,-0x44(%ebp)
    CONV(hour  );
80102aa5:	8b 45 c0             	mov    -0x40(%ebp),%eax
80102aa8:	89 c2                	mov    %eax,%edx
80102aaa:	83 e0 0f             	and    $0xf,%eax
80102aad:	c1 ea 04             	shr    $0x4,%edx
80102ab0:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102ab3:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102ab6:	89 45 c0             	mov    %eax,-0x40(%ebp)
    CONV(day   );
80102ab9:	8b 45 c4             	mov    -0x3c(%ebp),%eax
80102abc:	89 c2                	mov    %eax,%edx
80102abe:	83 e0 0f             	and    $0xf,%eax
80102ac1:	c1 ea 04             	shr    $0x4,%edx
80102ac4:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102ac7:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102aca:	89 45 c4             	mov    %eax,-0x3c(%ebp)
    CONV(month );
80102acd:	8b 45 c8             	mov    -0x38(%ebp),%eax
80102ad0:	89 c2                	mov    %eax,%edx
80102ad2:	83 e0 0f             	and    $0xf,%eax
80102ad5:	c1 ea 04             	shr    $0x4,%edx
80102ad8:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102adb:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102ade:	89 45 c8             	mov    %eax,-0x38(%ebp)
    CONV(year  );
80102ae1:	8b 45 cc             	mov    -0x34(%ebp),%eax
80102ae4:	89 c2                	mov    %eax,%edx
80102ae6:	83 e0 0f             	and    $0xf,%eax
80102ae9:	c1 ea 04             	shr    $0x4,%edx
80102aec:	8d 14 92             	lea    (%edx,%edx,4),%edx
80102aef:	8d 04 50             	lea    (%eax,%edx,2),%eax
80102af2:	89 45 cc             	mov    %eax,-0x34(%ebp)
#undef     CONV
  }

  *r = t1;
80102af5:	8b 75 08             	mov    0x8(%ebp),%esi
80102af8:	8b 45 b8             	mov    -0x48(%ebp),%eax
80102afb:	89 06                	mov    %eax,(%esi)
80102afd:	8b 45 bc             	mov    -0x44(%ebp),%eax
80102b00:	89 46 04             	mov    %eax,0x4(%esi)
80102b03:	8b 45 c0             	mov    -0x40(%ebp),%eax
80102b06:	89 46 08             	mov    %eax,0x8(%esi)
80102b09:	8b 45 c4             	mov    -0x3c(%ebp),%eax
80102b0c:	89 46 0c             	mov    %eax,0xc(%esi)
80102b0f:	8b 45 c8             	mov    -0x38(%ebp),%eax
80102b12:	89 46 10             	mov    %eax,0x10(%esi)
80102b15:	8b 45 cc             	mov    -0x34(%ebp),%eax
80102b18:	89 46 14             	mov    %eax,0x14(%esi)
  r->year += 2000;
80102b1b:	81 46 14 d0 07 00 00 	addl   $0x7d0,0x14(%esi)
}
80102b22:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102b25:	5b                   	pop    %ebx
80102b26:	5e                   	pop    %esi
80102b27:	5f                   	pop    %edi
80102b28:	5d                   	pop    %ebp
80102b29:	c3                   	ret    
80102b2a:	66 90                	xchg   %ax,%ax
80102b2c:	66 90                	xchg   %ax,%ax
80102b2e:	66 90                	xchg   %ax,%ax

80102b30 <install_trans>:
static void
install_trans(void)
{
  int tail;

  for (tail = 0; tail < log.lh.n; tail++) {
80102b30:	8b 0d 88 37 11 80    	mov    0x80113788,%ecx
80102b36:	85 c9                	test   %ecx,%ecx
80102b38:	0f 8e 8a 00 00 00    	jle    80102bc8 <install_trans+0x98>
{
80102b3e:	55                   	push   %ebp
80102b3f:	89 e5                	mov    %esp,%ebp
80102b41:	57                   	push   %edi
80102b42:	56                   	push   %esi
80102b43:	53                   	push   %ebx
  for (tail = 0; tail < log.lh.n; tail++) {
80102b44:	31 db                	xor    %ebx,%ebx
{
80102b46:	83 ec 0c             	sub    $0xc,%esp
80102b49:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    struct buf *lbuf = bread(log.dev, log.start+tail+1); // read log block
80102b50:	a1 74 37 11 80       	mov    0x80113774,%eax
80102b55:	83 ec 08             	sub    $0x8,%esp
80102b58:	01 d8                	add    %ebx,%eax
80102b5a:	83 c0 01             	add    $0x1,%eax
80102b5d:	50                   	push   %eax
80102b5e:	ff 35 84 37 11 80    	pushl  0x80113784
80102b64:	e8 67 d5 ff ff       	call   801000d0 <bread>
80102b69:	89 c7                	mov    %eax,%edi
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80102b6b:	58                   	pop    %eax
80102b6c:	5a                   	pop    %edx
80102b6d:	ff 34 9d 8c 37 11 80 	pushl  -0x7feec874(,%ebx,4)
80102b74:	ff 35 84 37 11 80    	pushl  0x80113784
  for (tail = 0; tail < log.lh.n; tail++) {
80102b7a:	83 c3 01             	add    $0x1,%ebx
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80102b7d:	e8 4e d5 ff ff       	call   801000d0 <bread>
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
80102b82:	83 c4 0c             	add    $0xc,%esp
    struct buf *dbuf = bread(log.dev, log.lh.block[tail]); // read dst
80102b85:	89 c6                	mov    %eax,%esi
    memmove(dbuf->data, lbuf->data, BSIZE);  // copy block to dst
80102b87:	8d 47 5c             	lea    0x5c(%edi),%eax
80102b8a:	68 00 02 00 00       	push   $0x200
80102b8f:	50                   	push   %eax
80102b90:	8d 46 5c             	lea    0x5c(%esi),%eax
80102b93:	50                   	push   %eax
80102b94:	e8 57 1b 00 00       	call   801046f0 <memmove>
    bwrite(dbuf);  // write dst to disk
80102b99:	89 34 24             	mov    %esi,(%esp)
80102b9c:	e8 0f d6 ff ff       	call   801001b0 <bwrite>
    brelse(lbuf);
80102ba1:	89 3c 24             	mov    %edi,(%esp)
80102ba4:	e8 47 d6 ff ff       	call   801001f0 <brelse>
    brelse(dbuf);
80102ba9:	89 34 24             	mov    %esi,(%esp)
80102bac:	e8 3f d6 ff ff       	call   801001f0 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80102bb1:	83 c4 10             	add    $0x10,%esp
80102bb4:	39 1d 88 37 11 80    	cmp    %ebx,0x80113788
80102bba:	7f 94                	jg     80102b50 <install_trans+0x20>
  }
}
80102bbc:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102bbf:	5b                   	pop    %ebx
80102bc0:	5e                   	pop    %esi
80102bc1:	5f                   	pop    %edi
80102bc2:	5d                   	pop    %ebp
80102bc3:	c3                   	ret    
80102bc4:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102bc8:	c3                   	ret    
80102bc9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80102bd0 <write_head>:
// Write in-memory log header to disk.
// This is the true point at which the
// current transaction commits.
static void
write_head(void)
{
80102bd0:	55                   	push   %ebp
80102bd1:	89 e5                	mov    %esp,%ebp
80102bd3:	53                   	push   %ebx
80102bd4:	83 ec 0c             	sub    $0xc,%esp
  struct buf *buf = bread(log.dev, log.start);
80102bd7:	ff 35 74 37 11 80    	pushl  0x80113774
80102bdd:	ff 35 84 37 11 80    	pushl  0x80113784
80102be3:	e8 e8 d4 ff ff       	call   801000d0 <bread>
  struct logheader *hb = (struct logheader *) (buf->data);
  int i;
  hb->n = log.lh.n;
  for (i = 0; i < log.lh.n; i++) {
80102be8:	83 c4 10             	add    $0x10,%esp
  struct buf *buf = bread(log.dev, log.start);
80102beb:	89 c3                	mov    %eax,%ebx
  hb->n = log.lh.n;
80102bed:	a1 88 37 11 80       	mov    0x80113788,%eax
80102bf2:	89 43 5c             	mov    %eax,0x5c(%ebx)
  for (i = 0; i < log.lh.n; i++) {
80102bf5:	85 c0                	test   %eax,%eax
80102bf7:	7e 19                	jle    80102c12 <write_head+0x42>
80102bf9:	31 d2                	xor    %edx,%edx
80102bfb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102bff:	90                   	nop
    hb->block[i] = log.lh.block[i];
80102c00:	8b 0c 95 8c 37 11 80 	mov    -0x7feec874(,%edx,4),%ecx
80102c07:	89 4c 93 60          	mov    %ecx,0x60(%ebx,%edx,4)
  for (i = 0; i < log.lh.n; i++) {
80102c0b:	83 c2 01             	add    $0x1,%edx
80102c0e:	39 d0                	cmp    %edx,%eax
80102c10:	75 ee                	jne    80102c00 <write_head+0x30>
  }
  bwrite(buf);
80102c12:	83 ec 0c             	sub    $0xc,%esp
80102c15:	53                   	push   %ebx
80102c16:	e8 95 d5 ff ff       	call   801001b0 <bwrite>
  brelse(buf);
80102c1b:	89 1c 24             	mov    %ebx,(%esp)
80102c1e:	e8 cd d5 ff ff       	call   801001f0 <brelse>
}
80102c23:	83 c4 10             	add    $0x10,%esp
80102c26:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102c29:	c9                   	leave  
80102c2a:	c3                   	ret    
80102c2b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102c2f:	90                   	nop

80102c30 <initlog>:
{
80102c30:	55                   	push   %ebp
80102c31:	89 e5                	mov    %esp,%ebp
80102c33:	53                   	push   %ebx
80102c34:	83 ec 2c             	sub    $0x2c,%esp
80102c37:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&log.lock, "log");
80102c3a:	68 20 7d 10 80       	push   $0x80107d20
80102c3f:	68 40 37 11 80       	push   $0x80113740
80102c44:	e8 97 17 00 00       	call   801043e0 <initlock>
  readsb(dev, &sb);
80102c49:	58                   	pop    %eax
80102c4a:	8d 45 dc             	lea    -0x24(%ebp),%eax
80102c4d:	5a                   	pop    %edx
80102c4e:	50                   	push   %eax
80102c4f:	53                   	push   %ebx
80102c50:	e8 3b e8 ff ff       	call   80101490 <readsb>
  log.start = sb.logstart;
80102c55:	8b 45 ec             	mov    -0x14(%ebp),%eax
  struct buf *buf = bread(log.dev, log.start);
80102c58:	59                   	pop    %ecx
  log.dev = dev;
80102c59:	89 1d 84 37 11 80    	mov    %ebx,0x80113784
  log.size = sb.nlog;
80102c5f:	8b 55 e8             	mov    -0x18(%ebp),%edx
  log.start = sb.logstart;
80102c62:	a3 74 37 11 80       	mov    %eax,0x80113774
  log.size = sb.nlog;
80102c67:	89 15 78 37 11 80    	mov    %edx,0x80113778
  struct buf *buf = bread(log.dev, log.start);
80102c6d:	5a                   	pop    %edx
80102c6e:	50                   	push   %eax
80102c6f:	53                   	push   %ebx
80102c70:	e8 5b d4 ff ff       	call   801000d0 <bread>
  for (i = 0; i < log.lh.n; i++) {
80102c75:	83 c4 10             	add    $0x10,%esp
  log.lh.n = lh->n;
80102c78:	8b 48 5c             	mov    0x5c(%eax),%ecx
80102c7b:	89 0d 88 37 11 80    	mov    %ecx,0x80113788
  for (i = 0; i < log.lh.n; i++) {
80102c81:	85 c9                	test   %ecx,%ecx
80102c83:	7e 1d                	jle    80102ca2 <initlog+0x72>
80102c85:	31 d2                	xor    %edx,%edx
80102c87:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102c8e:	66 90                	xchg   %ax,%ax
    log.lh.block[i] = lh->block[i];
80102c90:	8b 5c 90 60          	mov    0x60(%eax,%edx,4),%ebx
80102c94:	89 1c 95 8c 37 11 80 	mov    %ebx,-0x7feec874(,%edx,4)
  for (i = 0; i < log.lh.n; i++) {
80102c9b:	83 c2 01             	add    $0x1,%edx
80102c9e:	39 d1                	cmp    %edx,%ecx
80102ca0:	75 ee                	jne    80102c90 <initlog+0x60>
  brelse(buf);
80102ca2:	83 ec 0c             	sub    $0xc,%esp
80102ca5:	50                   	push   %eax
80102ca6:	e8 45 d5 ff ff       	call   801001f0 <brelse>

static void
recover_from_log(void)
{
  read_head();
  install_trans(); // if committed, copy from log to disk
80102cab:	e8 80 fe ff ff       	call   80102b30 <install_trans>
  log.lh.n = 0;
80102cb0:	c7 05 88 37 11 80 00 	movl   $0x0,0x80113788
80102cb7:	00 00 00 
  write_head(); // clear the log
80102cba:	e8 11 ff ff ff       	call   80102bd0 <write_head>
}
80102cbf:	83 c4 10             	add    $0x10,%esp
80102cc2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80102cc5:	c9                   	leave  
80102cc6:	c3                   	ret    
80102cc7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102cce:	66 90                	xchg   %ax,%ax

80102cd0 <begin_op>:
}

// called at the start of each FS system call.
void
begin_op(void)
{
80102cd0:	55                   	push   %ebp
80102cd1:	89 e5                	mov    %esp,%ebp
80102cd3:	83 ec 14             	sub    $0x14,%esp
  acquire(&log.lock);
80102cd6:	68 40 37 11 80       	push   $0x80113740
80102cdb:	e8 60 18 00 00       	call   80104540 <acquire>
80102ce0:	83 c4 10             	add    $0x10,%esp
80102ce3:	eb 18                	jmp    80102cfd <begin_op+0x2d>
80102ce5:	8d 76 00             	lea    0x0(%esi),%esi
  while(1){
    if(log.committing){
      sleep(&log, &log.lock);
80102ce8:	83 ec 08             	sub    $0x8,%esp
80102ceb:	68 40 37 11 80       	push   $0x80113740
80102cf0:	68 40 37 11 80       	push   $0x80113740
80102cf5:	e8 36 12 00 00       	call   80103f30 <sleep>
80102cfa:	83 c4 10             	add    $0x10,%esp
    if(log.committing){
80102cfd:	a1 80 37 11 80       	mov    0x80113780,%eax
80102d02:	85 c0                	test   %eax,%eax
80102d04:	75 e2                	jne    80102ce8 <begin_op+0x18>
    } else if(log.lh.n + (log.outstanding+1)*MAXOPBLOCKS > LOGSIZE){
80102d06:	a1 7c 37 11 80       	mov    0x8011377c,%eax
80102d0b:	8b 15 88 37 11 80    	mov    0x80113788,%edx
80102d11:	83 c0 01             	add    $0x1,%eax
80102d14:	8d 0c 80             	lea    (%eax,%eax,4),%ecx
80102d17:	8d 14 4a             	lea    (%edx,%ecx,2),%edx
80102d1a:	83 fa 1e             	cmp    $0x1e,%edx
80102d1d:	7f c9                	jg     80102ce8 <begin_op+0x18>
      // this op might exhaust log space; wait for commit.
      sleep(&log, &log.lock);
    } else {
      log.outstanding += 1;
      release(&log.lock);
80102d1f:	83 ec 0c             	sub    $0xc,%esp
      log.outstanding += 1;
80102d22:	a3 7c 37 11 80       	mov    %eax,0x8011377c
      release(&log.lock);
80102d27:	68 40 37 11 80       	push   $0x80113740
80102d2c:	e8 cf 18 00 00       	call   80104600 <release>
      break;
    }
  }
}
80102d31:	83 c4 10             	add    $0x10,%esp
80102d34:	c9                   	leave  
80102d35:	c3                   	ret    
80102d36:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102d3d:	8d 76 00             	lea    0x0(%esi),%esi

80102d40 <end_op>:

// called at the end of each FS system call.
// commits if this was the last outstanding operation.
void
end_op(void)
{
80102d40:	55                   	push   %ebp
80102d41:	89 e5                	mov    %esp,%ebp
80102d43:	57                   	push   %edi
80102d44:	56                   	push   %esi
80102d45:	53                   	push   %ebx
80102d46:	83 ec 18             	sub    $0x18,%esp
  int do_commit = 0;

  acquire(&log.lock);
80102d49:	68 40 37 11 80       	push   $0x80113740
80102d4e:	e8 ed 17 00 00       	call   80104540 <acquire>
  log.outstanding -= 1;
80102d53:	a1 7c 37 11 80       	mov    0x8011377c,%eax
  if(log.committing)
80102d58:	8b 35 80 37 11 80    	mov    0x80113780,%esi
80102d5e:	83 c4 10             	add    $0x10,%esp
  log.outstanding -= 1;
80102d61:	8d 58 ff             	lea    -0x1(%eax),%ebx
80102d64:	89 1d 7c 37 11 80    	mov    %ebx,0x8011377c
  if(log.committing)
80102d6a:	85 f6                	test   %esi,%esi
80102d6c:	0f 85 22 01 00 00    	jne    80102e94 <end_op+0x154>
    panic("log.committing");
  if(log.outstanding == 0){
80102d72:	85 db                	test   %ebx,%ebx
80102d74:	0f 85 f6 00 00 00    	jne    80102e70 <end_op+0x130>
    do_commit = 1;
    log.committing = 1;
80102d7a:	c7 05 80 37 11 80 01 	movl   $0x1,0x80113780
80102d81:	00 00 00 
    // begin_op() may be waiting for log space,
    // and decrementing log.outstanding has decreased
    // the amount of reserved space.
    wakeup(&log);
  }
  release(&log.lock);
80102d84:	83 ec 0c             	sub    $0xc,%esp
80102d87:	68 40 37 11 80       	push   $0x80113740
80102d8c:	e8 6f 18 00 00       	call   80104600 <release>
}

static void
commit()
{
  if (log.lh.n > 0) {
80102d91:	8b 0d 88 37 11 80    	mov    0x80113788,%ecx
80102d97:	83 c4 10             	add    $0x10,%esp
80102d9a:	85 c9                	test   %ecx,%ecx
80102d9c:	7f 42                	jg     80102de0 <end_op+0xa0>
    acquire(&log.lock);
80102d9e:	83 ec 0c             	sub    $0xc,%esp
80102da1:	68 40 37 11 80       	push   $0x80113740
80102da6:	e8 95 17 00 00       	call   80104540 <acquire>
    wakeup(&log);
80102dab:	c7 04 24 40 37 11 80 	movl   $0x80113740,(%esp)
    log.committing = 0;
80102db2:	c7 05 80 37 11 80 00 	movl   $0x0,0x80113780
80102db9:	00 00 00 
    wakeup(&log);
80102dbc:	e8 2f 13 00 00       	call   801040f0 <wakeup>
    release(&log.lock);
80102dc1:	c7 04 24 40 37 11 80 	movl   $0x80113740,(%esp)
80102dc8:	e8 33 18 00 00       	call   80104600 <release>
80102dcd:	83 c4 10             	add    $0x10,%esp
}
80102dd0:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102dd3:	5b                   	pop    %ebx
80102dd4:	5e                   	pop    %esi
80102dd5:	5f                   	pop    %edi
80102dd6:	5d                   	pop    %ebp
80102dd7:	c3                   	ret    
80102dd8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102ddf:	90                   	nop
    struct buf *to = bread(log.dev, log.start+tail+1); // log block
80102de0:	a1 74 37 11 80       	mov    0x80113774,%eax
80102de5:	83 ec 08             	sub    $0x8,%esp
80102de8:	01 d8                	add    %ebx,%eax
80102dea:	83 c0 01             	add    $0x1,%eax
80102ded:	50                   	push   %eax
80102dee:	ff 35 84 37 11 80    	pushl  0x80113784
80102df4:	e8 d7 d2 ff ff       	call   801000d0 <bread>
80102df9:	89 c6                	mov    %eax,%esi
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80102dfb:	58                   	pop    %eax
80102dfc:	5a                   	pop    %edx
80102dfd:	ff 34 9d 8c 37 11 80 	pushl  -0x7feec874(,%ebx,4)
80102e04:	ff 35 84 37 11 80    	pushl  0x80113784
  for (tail = 0; tail < log.lh.n; tail++) {
80102e0a:	83 c3 01             	add    $0x1,%ebx
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80102e0d:	e8 be d2 ff ff       	call   801000d0 <bread>
    memmove(to->data, from->data, BSIZE);
80102e12:	83 c4 0c             	add    $0xc,%esp
    struct buf *from = bread(log.dev, log.lh.block[tail]); // cache block
80102e15:	89 c7                	mov    %eax,%edi
    memmove(to->data, from->data, BSIZE);
80102e17:	8d 40 5c             	lea    0x5c(%eax),%eax
80102e1a:	68 00 02 00 00       	push   $0x200
80102e1f:	50                   	push   %eax
80102e20:	8d 46 5c             	lea    0x5c(%esi),%eax
80102e23:	50                   	push   %eax
80102e24:	e8 c7 18 00 00       	call   801046f0 <memmove>
    bwrite(to);  // write the log
80102e29:	89 34 24             	mov    %esi,(%esp)
80102e2c:	e8 7f d3 ff ff       	call   801001b0 <bwrite>
    brelse(from);
80102e31:	89 3c 24             	mov    %edi,(%esp)
80102e34:	e8 b7 d3 ff ff       	call   801001f0 <brelse>
    brelse(to);
80102e39:	89 34 24             	mov    %esi,(%esp)
80102e3c:	e8 af d3 ff ff       	call   801001f0 <brelse>
  for (tail = 0; tail < log.lh.n; tail++) {
80102e41:	83 c4 10             	add    $0x10,%esp
80102e44:	3b 1d 88 37 11 80    	cmp    0x80113788,%ebx
80102e4a:	7c 94                	jl     80102de0 <end_op+0xa0>
    write_log();     // Write modified blocks from cache to log
    write_head();    // Write header to disk -- the real commit
80102e4c:	e8 7f fd ff ff       	call   80102bd0 <write_head>
    install_trans(); // Now install writes to home locations
80102e51:	e8 da fc ff ff       	call   80102b30 <install_trans>
    log.lh.n = 0;
80102e56:	c7 05 88 37 11 80 00 	movl   $0x0,0x80113788
80102e5d:	00 00 00 
    write_head();    // Erase the transaction from the log
80102e60:	e8 6b fd ff ff       	call   80102bd0 <write_head>
80102e65:	e9 34 ff ff ff       	jmp    80102d9e <end_op+0x5e>
80102e6a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    wakeup(&log);
80102e70:	83 ec 0c             	sub    $0xc,%esp
80102e73:	68 40 37 11 80       	push   $0x80113740
80102e78:	e8 73 12 00 00       	call   801040f0 <wakeup>
  release(&log.lock);
80102e7d:	c7 04 24 40 37 11 80 	movl   $0x80113740,(%esp)
80102e84:	e8 77 17 00 00       	call   80104600 <release>
80102e89:	83 c4 10             	add    $0x10,%esp
}
80102e8c:	8d 65 f4             	lea    -0xc(%ebp),%esp
80102e8f:	5b                   	pop    %ebx
80102e90:	5e                   	pop    %esi
80102e91:	5f                   	pop    %edi
80102e92:	5d                   	pop    %ebp
80102e93:	c3                   	ret    
    panic("log.committing");
80102e94:	83 ec 0c             	sub    $0xc,%esp
80102e97:	68 24 7d 10 80       	push   $0x80107d24
80102e9c:	e8 ef d4 ff ff       	call   80100390 <panic>
80102ea1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102ea8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80102eaf:	90                   	nop

80102eb0 <log_write>:
//   modify bp->data[]
//   log_write(bp)
//   brelse(bp)
void
log_write(struct buf *b)
{
80102eb0:	55                   	push   %ebp
80102eb1:	89 e5                	mov    %esp,%ebp
80102eb3:	53                   	push   %ebx
80102eb4:	83 ec 04             	sub    $0x4,%esp
  int i;

  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80102eb7:	8b 15 88 37 11 80    	mov    0x80113788,%edx
{
80102ebd:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if (log.lh.n >= LOGSIZE || log.lh.n >= log.size - 1)
80102ec0:	83 fa 1d             	cmp    $0x1d,%edx
80102ec3:	0f 8f 94 00 00 00    	jg     80102f5d <log_write+0xad>
80102ec9:	a1 78 37 11 80       	mov    0x80113778,%eax
80102ece:	83 e8 01             	sub    $0x1,%eax
80102ed1:	39 c2                	cmp    %eax,%edx
80102ed3:	0f 8d 84 00 00 00    	jge    80102f5d <log_write+0xad>
    panic("too big a transaction");
  if (log.outstanding < 1)
80102ed9:	a1 7c 37 11 80       	mov    0x8011377c,%eax
80102ede:	85 c0                	test   %eax,%eax
80102ee0:	0f 8e 84 00 00 00    	jle    80102f6a <log_write+0xba>
    panic("log_write outside of trans");

  acquire(&log.lock);
80102ee6:	83 ec 0c             	sub    $0xc,%esp
80102ee9:	68 40 37 11 80       	push   $0x80113740
80102eee:	e8 4d 16 00 00       	call   80104540 <acquire>
  for (i = 0; i < log.lh.n; i++) {
80102ef3:	8b 15 88 37 11 80    	mov    0x80113788,%edx
80102ef9:	83 c4 10             	add    $0x10,%esp
80102efc:	85 d2                	test   %edx,%edx
80102efe:	7e 51                	jle    80102f51 <log_write+0xa1>
    if (log.lh.block[i] == b->blockno)   // log absorbtion
80102f00:	8b 4b 08             	mov    0x8(%ebx),%ecx
  for (i = 0; i < log.lh.n; i++) {
80102f03:	31 c0                	xor    %eax,%eax
    if (log.lh.block[i] == b->blockno)   // log absorbtion
80102f05:	3b 0d 8c 37 11 80    	cmp    0x8011378c,%ecx
80102f0b:	75 0c                	jne    80102f19 <log_write+0x69>
80102f0d:	eb 39                	jmp    80102f48 <log_write+0x98>
80102f0f:	90                   	nop
80102f10:	39 0c 85 8c 37 11 80 	cmp    %ecx,-0x7feec874(,%eax,4)
80102f17:	74 2f                	je     80102f48 <log_write+0x98>
  for (i = 0; i < log.lh.n; i++) {
80102f19:	83 c0 01             	add    $0x1,%eax
80102f1c:	39 c2                	cmp    %eax,%edx
80102f1e:	75 f0                	jne    80102f10 <log_write+0x60>
      break;
  }
  log.lh.block[i] = b->blockno;
80102f20:	89 0c 95 8c 37 11 80 	mov    %ecx,-0x7feec874(,%edx,4)
  if (i == log.lh.n)
    log.lh.n++;
80102f27:	83 c2 01             	add    $0x1,%edx
80102f2a:	89 15 88 37 11 80    	mov    %edx,0x80113788
  b->flags |= B_DIRTY; // prevent eviction
80102f30:	83 0b 04             	orl    $0x4,(%ebx)
  release(&log.lock);
}
80102f33:	8b 5d fc             	mov    -0x4(%ebp),%ebx
  release(&log.lock);
80102f36:	c7 45 08 40 37 11 80 	movl   $0x80113740,0x8(%ebp)
}
80102f3d:	c9                   	leave  
  release(&log.lock);
80102f3e:	e9 bd 16 00 00       	jmp    80104600 <release>
80102f43:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80102f47:	90                   	nop
  log.lh.block[i] = b->blockno;
80102f48:	89 0c 85 8c 37 11 80 	mov    %ecx,-0x7feec874(,%eax,4)
  if (i == log.lh.n)
80102f4f:	eb df                	jmp    80102f30 <log_write+0x80>
  log.lh.block[i] = b->blockno;
80102f51:	8b 43 08             	mov    0x8(%ebx),%eax
80102f54:	a3 8c 37 11 80       	mov    %eax,0x8011378c
  if (i == log.lh.n)
80102f59:	75 d5                	jne    80102f30 <log_write+0x80>
80102f5b:	eb ca                	jmp    80102f27 <log_write+0x77>
    panic("too big a transaction");
80102f5d:	83 ec 0c             	sub    $0xc,%esp
80102f60:	68 33 7d 10 80       	push   $0x80107d33
80102f65:	e8 26 d4 ff ff       	call   80100390 <panic>
    panic("log_write outside of trans");
80102f6a:	83 ec 0c             	sub    $0xc,%esp
80102f6d:	68 49 7d 10 80       	push   $0x80107d49
80102f72:	e8 19 d4 ff ff       	call   80100390 <panic>
80102f77:	66 90                	xchg   %ax,%ax
80102f79:	66 90                	xchg   %ax,%ax
80102f7b:	66 90                	xchg   %ax,%ax
80102f7d:	66 90                	xchg   %ax,%ax
80102f7f:	90                   	nop

80102f80 <mpmain>:
}

// Common CPU setup code.
static void
mpmain(void)
{
80102f80:	55                   	push   %ebp
80102f81:	89 e5                	mov    %esp,%ebp
80102f83:	53                   	push   %ebx
80102f84:	83 ec 04             	sub    $0x4,%esp
  cprintf("cpu%d: starting %d\n", cpuid(), cpuid());
80102f87:	e8 74 09 00 00       	call   80103900 <cpuid>
80102f8c:	89 c3                	mov    %eax,%ebx
80102f8e:	e8 6d 09 00 00       	call   80103900 <cpuid>
80102f93:	83 ec 04             	sub    $0x4,%esp
80102f96:	53                   	push   %ebx
80102f97:	50                   	push   %eax
80102f98:	68 64 7d 10 80       	push   $0x80107d64
80102f9d:	e8 0e d7 ff ff       	call   801006b0 <cprintf>
  idtinit();       // load idt register
80102fa2:	e8 29 2a 00 00       	call   801059d0 <idtinit>
  xchg(&(mycpu()->started), 1); // tell startothers() we're up
80102fa7:	e8 d4 08 00 00       	call   80103880 <mycpu>
80102fac:	89 c2                	mov    %eax,%edx
xchg(volatile uint *addr, uint newval)
{
  uint result;

  // The + in "+m" denotes a read-modify-write operand.
  asm volatile("lock; xchgl %0, %1" :
80102fae:	b8 01 00 00 00       	mov    $0x1,%eax
80102fb3:	f0 87 82 a0 00 00 00 	lock xchg %eax,0xa0(%edx)
  scheduler();     // start running processes
80102fba:	e8 31 0c 00 00       	call   80103bf0 <scheduler>
80102fbf:	90                   	nop

80102fc0 <mpenter>:
{
80102fc0:	55                   	push   %ebp
80102fc1:	89 e5                	mov    %esp,%ebp
80102fc3:	83 ec 08             	sub    $0x8,%esp
  switchkvm();
80102fc6:	e8 65 3c 00 00       	call   80106c30 <switchkvm>
  seginit();
80102fcb:	e8 d0 3a 00 00       	call   80106aa0 <seginit>
  lapicinit();
80102fd0:	e8 8b f7 ff ff       	call   80102760 <lapicinit>
  mpmain();
80102fd5:	e8 a6 ff ff ff       	call   80102f80 <mpmain>
80102fda:	66 90                	xchg   %ax,%ax
80102fdc:	66 90                	xchg   %ax,%ax
80102fde:	66 90                	xchg   %ax,%ax

80102fe0 <main>:
{
80102fe0:	8d 4c 24 04          	lea    0x4(%esp),%ecx
80102fe4:	83 e4 f0             	and    $0xfffffff0,%esp
80102fe7:	ff 71 fc             	pushl  -0x4(%ecx)
80102fea:	55                   	push   %ebp
80102feb:	89 e5                	mov    %esp,%ebp
80102fed:	53                   	push   %ebx
80102fee:	51                   	push   %ecx
  kinit1(end, P2V(4*1024*1024)); // phys page allocator
80102fef:	83 ec 08             	sub    $0x8,%esp
80102ff2:	68 00 00 40 80       	push   $0x80400000
80102ff7:	68 a4 76 11 80       	push   $0x801176a4
80102ffc:	e8 0f f5 ff ff       	call   80102510 <kinit1>
  kvmalloc();      // kernel page table
80103001:	e8 ea 40 00 00       	call   801070f0 <kvmalloc>
  mpinit();        // detect other processors
80103006:	e8 85 01 00 00       	call   80103190 <mpinit>
  lapicinit();     // interrupt controller
8010300b:	e8 50 f7 ff ff       	call   80102760 <lapicinit>
  seginit();       // segment descriptors
80103010:	e8 8b 3a 00 00       	call   80106aa0 <seginit>
  picinit();       // disable pic
80103015:	e8 46 03 00 00       	call   80103360 <picinit>
  ioapicinit();    // another interrupt controller
8010301a:	e8 11 f3 ff ff       	call   80102330 <ioapicinit>
  consoleinit();   // console hardware
8010301f:	e8 0c da ff ff       	call   80100a30 <consoleinit>
  uartinit();      // serial port
80103024:	e8 c7 2d 00 00       	call   80105df0 <uartinit>
  pinit();         // process table
80103029:	e8 32 08 00 00       	call   80103860 <pinit>
  tvinit();        // trap vectors
8010302e:	e8 1d 29 00 00       	call   80105950 <tvinit>
  binit();         // buffer cache
80103033:	e8 08 d0 ff ff       	call   80100040 <binit>
  fileinit();      // file table
80103038:	e8 d3 dd ff ff       	call   80100e10 <fileinit>
  ideinit();       // disk 
8010303d:	e8 ce f0 ff ff       	call   80102110 <ideinit>
  shminit();	   // shared memory
80103042:	e8 99 45 00 00       	call   801075e0 <shminit>

  // Write entry code to unused memory at 0x7000.
  // The linker has placed the image of entryother.S in
  // _binary_entryother_start.
  code = P2V(0x7000);
  memmove(code, _binary_entryother_start, (uint)_binary_entryother_size);
80103047:	83 c4 0c             	add    $0xc,%esp
8010304a:	68 8a 00 00 00       	push   $0x8a
8010304f:	68 8c b4 10 80       	push   $0x8010b48c
80103054:	68 00 70 00 80       	push   $0x80007000
80103059:	e8 92 16 00 00       	call   801046f0 <memmove>

  for(c = cpus; c < cpus+ncpu; c++){
8010305e:	83 c4 10             	add    $0x10,%esp
80103061:	69 05 c0 3d 11 80 b0 	imul   $0xb0,0x80113dc0,%eax
80103068:	00 00 00 
8010306b:	05 40 38 11 80       	add    $0x80113840,%eax
80103070:	3d 40 38 11 80       	cmp    $0x80113840,%eax
80103075:	76 79                	jbe    801030f0 <main+0x110>
80103077:	bb 40 38 11 80       	mov    $0x80113840,%ebx
8010307c:	eb 1b                	jmp    80103099 <main+0xb9>
8010307e:	66 90                	xchg   %ax,%ax
80103080:	69 05 c0 3d 11 80 b0 	imul   $0xb0,0x80113dc0,%eax
80103087:	00 00 00 
8010308a:	81 c3 b0 00 00 00    	add    $0xb0,%ebx
80103090:	05 40 38 11 80       	add    $0x80113840,%eax
80103095:	39 c3                	cmp    %eax,%ebx
80103097:	73 57                	jae    801030f0 <main+0x110>
    if(c == mycpu())  // We've started already.
80103099:	e8 e2 07 00 00       	call   80103880 <mycpu>
8010309e:	39 d8                	cmp    %ebx,%eax
801030a0:	74 de                	je     80103080 <main+0xa0>
      continue;

    // Tell entryother.S what stack to use, where to enter, and what
    // pgdir to use. We cannot use kpgdir yet, because the AP processor
    // is running in low  memory, so we use entrypgdir for the APs too.
    stack = kalloc();
801030a2:	e8 39 f5 ff ff       	call   801025e0 <kalloc>
    *(void**)(code-4) = stack + KSTACKSIZE;
    *(void(**)(void))(code-8) = mpenter;
    *(int**)(code-12) = (void *) V2P(entrypgdir);

    lapicstartap(c->apicid, V2P(code));
801030a7:	83 ec 08             	sub    $0x8,%esp
    *(void(**)(void))(code-8) = mpenter;
801030aa:	c7 05 f8 6f 00 80 c0 	movl   $0x80102fc0,0x80006ff8
801030b1:	2f 10 80 
    *(int**)(code-12) = (void *) V2P(entrypgdir);
801030b4:	c7 05 f4 6f 00 80 00 	movl   $0x10a000,0x80006ff4
801030bb:	a0 10 00 
    *(void**)(code-4) = stack + KSTACKSIZE;
801030be:	05 00 10 00 00       	add    $0x1000,%eax
801030c3:	a3 fc 6f 00 80       	mov    %eax,0x80006ffc
    lapicstartap(c->apicid, V2P(code));
801030c8:	0f b6 03             	movzbl (%ebx),%eax
801030cb:	68 00 70 00 00       	push   $0x7000
801030d0:	50                   	push   %eax
801030d1:	e8 da f7 ff ff       	call   801028b0 <lapicstartap>
801030d6:	83 c4 10             	add    $0x10,%esp
801030d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

    // wait for cpu to finish mpmain()
    while(c->started == 0)
801030e0:	8b 83 a0 00 00 00    	mov    0xa0(%ebx),%eax
801030e6:	85 c0                	test   %eax,%eax
801030e8:	74 f6                	je     801030e0 <main+0x100>
801030ea:	eb 94                	jmp    80103080 <main+0xa0>
801030ec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  kinit2(P2V(4*1024*1024), P2V(PHYSTOP)); // must come after startothers()
801030f0:	83 ec 08             	sub    $0x8,%esp
801030f3:	68 00 00 00 8e       	push   $0x8e000000
801030f8:	68 00 00 40 80       	push   $0x80400000
801030fd:	e8 7e f4 ff ff       	call   80102580 <kinit2>
  userinit();      // first user process
80103102:	e8 49 08 00 00       	call   80103950 <userinit>
  mpmain();        // finish this processor's setup
80103107:	e8 74 fe ff ff       	call   80102f80 <mpmain>
8010310c:	66 90                	xchg   %ax,%ax
8010310e:	66 90                	xchg   %ax,%ax

80103110 <mpsearch1>:
}

// Look for an MP structure in the len bytes at addr.
static struct mp*
mpsearch1(uint a, int len)
{
80103110:	55                   	push   %ebp
80103111:	89 e5                	mov    %esp,%ebp
80103113:	57                   	push   %edi
80103114:	56                   	push   %esi
  uchar *e, *p, *addr;

  addr = P2V(a);
80103115:	8d b0 00 00 00 80    	lea    -0x80000000(%eax),%esi
{
8010311b:	53                   	push   %ebx
  e = addr+len;
8010311c:	8d 1c 16             	lea    (%esi,%edx,1),%ebx
{
8010311f:	83 ec 0c             	sub    $0xc,%esp
  for(p = addr; p < e; p += sizeof(struct mp))
80103122:	39 de                	cmp    %ebx,%esi
80103124:	72 10                	jb     80103136 <mpsearch1+0x26>
80103126:	eb 50                	jmp    80103178 <mpsearch1+0x68>
80103128:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010312f:	90                   	nop
80103130:	89 fe                	mov    %edi,%esi
80103132:	39 fb                	cmp    %edi,%ebx
80103134:	76 42                	jbe    80103178 <mpsearch1+0x68>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80103136:	83 ec 04             	sub    $0x4,%esp
80103139:	8d 7e 10             	lea    0x10(%esi),%edi
8010313c:	6a 04                	push   $0x4
8010313e:	68 78 7d 10 80       	push   $0x80107d78
80103143:	56                   	push   %esi
80103144:	e8 57 15 00 00       	call   801046a0 <memcmp>
80103149:	83 c4 10             	add    $0x10,%esp
8010314c:	85 c0                	test   %eax,%eax
8010314e:	75 e0                	jne    80103130 <mpsearch1+0x20>
80103150:	89 f1                	mov    %esi,%ecx
80103152:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    sum += addr[i];
80103158:	0f b6 11             	movzbl (%ecx),%edx
8010315b:	83 c1 01             	add    $0x1,%ecx
8010315e:	01 d0                	add    %edx,%eax
  for(i=0; i<len; i++)
80103160:	39 f9                	cmp    %edi,%ecx
80103162:	75 f4                	jne    80103158 <mpsearch1+0x48>
    if(memcmp(p, "_MP_", 4) == 0 && sum(p, sizeof(struct mp)) == 0)
80103164:	84 c0                	test   %al,%al
80103166:	75 c8                	jne    80103130 <mpsearch1+0x20>
      return (struct mp*)p;
  return 0;
}
80103168:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010316b:	89 f0                	mov    %esi,%eax
8010316d:	5b                   	pop    %ebx
8010316e:	5e                   	pop    %esi
8010316f:	5f                   	pop    %edi
80103170:	5d                   	pop    %ebp
80103171:	c3                   	ret    
80103172:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80103178:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
8010317b:	31 f6                	xor    %esi,%esi
}
8010317d:	5b                   	pop    %ebx
8010317e:	89 f0                	mov    %esi,%eax
80103180:	5e                   	pop    %esi
80103181:	5f                   	pop    %edi
80103182:	5d                   	pop    %ebp
80103183:	c3                   	ret    
80103184:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010318b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010318f:	90                   	nop

80103190 <mpinit>:
  return conf;
}

void
mpinit(void)
{
80103190:	55                   	push   %ebp
80103191:	89 e5                	mov    %esp,%ebp
80103193:	57                   	push   %edi
80103194:	56                   	push   %esi
80103195:	53                   	push   %ebx
80103196:	83 ec 1c             	sub    $0x1c,%esp
  if((p = ((bda[0x0F]<<8)| bda[0x0E]) << 4)){
80103199:	0f b6 05 0f 04 00 80 	movzbl 0x8000040f,%eax
801031a0:	0f b6 15 0e 04 00 80 	movzbl 0x8000040e,%edx
801031a7:	c1 e0 08             	shl    $0x8,%eax
801031aa:	09 d0                	or     %edx,%eax
801031ac:	c1 e0 04             	shl    $0x4,%eax
801031af:	75 1b                	jne    801031cc <mpinit+0x3c>
    p = ((bda[0x14]<<8)|bda[0x13])*1024;
801031b1:	0f b6 05 14 04 00 80 	movzbl 0x80000414,%eax
801031b8:	0f b6 15 13 04 00 80 	movzbl 0x80000413,%edx
801031bf:	c1 e0 08             	shl    $0x8,%eax
801031c2:	09 d0                	or     %edx,%eax
801031c4:	c1 e0 0a             	shl    $0xa,%eax
    if((mp = mpsearch1(p-1024, 1024)))
801031c7:	2d 00 04 00 00       	sub    $0x400,%eax
    if((mp = mpsearch1(p, 1024)))
801031cc:	ba 00 04 00 00       	mov    $0x400,%edx
801031d1:	e8 3a ff ff ff       	call   80103110 <mpsearch1>
801031d6:	89 c7                	mov    %eax,%edi
801031d8:	85 c0                	test   %eax,%eax
801031da:	0f 84 c0 00 00 00    	je     801032a0 <mpinit+0x110>
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
801031e0:	8b 5f 04             	mov    0x4(%edi),%ebx
801031e3:	85 db                	test   %ebx,%ebx
801031e5:	0f 84 d5 00 00 00    	je     801032c0 <mpinit+0x130>
  if(memcmp(conf, "PCMP", 4) != 0)
801031eb:	83 ec 04             	sub    $0x4,%esp
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
801031ee:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
  if(memcmp(conf, "PCMP", 4) != 0)
801031f4:	6a 04                	push   $0x4
801031f6:	68 95 7d 10 80       	push   $0x80107d95
801031fb:	50                   	push   %eax
  conf = (struct mpconf*) P2V((uint) mp->physaddr);
801031fc:	89 45 e4             	mov    %eax,-0x1c(%ebp)
  if(memcmp(conf, "PCMP", 4) != 0)
801031ff:	e8 9c 14 00 00       	call   801046a0 <memcmp>
80103204:	83 c4 10             	add    $0x10,%esp
80103207:	85 c0                	test   %eax,%eax
80103209:	0f 85 b1 00 00 00    	jne    801032c0 <mpinit+0x130>
  if(conf->version != 1 && conf->version != 4)
8010320f:	0f b6 83 06 00 00 80 	movzbl -0x7ffffffa(%ebx),%eax
80103216:	3c 01                	cmp    $0x1,%al
80103218:	0f 95 c2             	setne  %dl
8010321b:	3c 04                	cmp    $0x4,%al
8010321d:	0f 95 c0             	setne  %al
80103220:	20 c2                	and    %al,%dl
80103222:	0f 85 98 00 00 00    	jne    801032c0 <mpinit+0x130>
  if(sum((uchar*)conf, conf->length) != 0)
80103228:	0f b7 8b 04 00 00 80 	movzwl -0x7ffffffc(%ebx),%ecx
  for(i=0; i<len; i++)
8010322f:	66 85 c9             	test   %cx,%cx
80103232:	74 21                	je     80103255 <mpinit+0xc5>
80103234:	89 d8                	mov    %ebx,%eax
80103236:	8d 34 19             	lea    (%ecx,%ebx,1),%esi
  sum = 0;
80103239:	31 d2                	xor    %edx,%edx
8010323b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010323f:	90                   	nop
    sum += addr[i];
80103240:	0f b6 88 00 00 00 80 	movzbl -0x80000000(%eax),%ecx
80103247:	83 c0 01             	add    $0x1,%eax
8010324a:	01 ca                	add    %ecx,%edx
  for(i=0; i<len; i++)
8010324c:	39 c6                	cmp    %eax,%esi
8010324e:	75 f0                	jne    80103240 <mpinit+0xb0>
80103250:	84 d2                	test   %dl,%dl
80103252:	0f 95 c2             	setne  %dl
  struct mp *mp;
  struct mpconf *conf;
  struct mpproc *proc;
  struct mpioapic *ioapic;

  if((conf = mpconfig(&mp)) == 0)
80103255:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80103258:	85 c9                	test   %ecx,%ecx
8010325a:	74 64                	je     801032c0 <mpinit+0x130>
8010325c:	84 d2                	test   %dl,%dl
8010325e:	75 60                	jne    801032c0 <mpinit+0x130>
    panic("Expect to run on an SMP");
  ismp = 1;
  lapic = (uint*)conf->lapicaddr;
80103260:	8b 83 24 00 00 80    	mov    -0x7fffffdc(%ebx),%eax
80103266:	a3 3c 37 11 80       	mov    %eax,0x8011373c
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
8010326b:	0f b7 93 04 00 00 80 	movzwl -0x7ffffffc(%ebx),%edx
80103272:	8d 83 2c 00 00 80    	lea    -0x7fffffd4(%ebx),%eax
  ismp = 1;
80103278:	bb 01 00 00 00       	mov    $0x1,%ebx
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
8010327d:	01 d1                	add    %edx,%ecx
8010327f:	89 ce                	mov    %ecx,%esi
80103281:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103288:	39 c6                	cmp    %eax,%esi
8010328a:	76 4b                	jbe    801032d7 <mpinit+0x147>
    switch(*p){
8010328c:	0f b6 10             	movzbl (%eax),%edx
8010328f:	80 fa 04             	cmp    $0x4,%dl
80103292:	0f 87 bf 00 00 00    	ja     80103357 <mpinit+0x1c7>
80103298:	ff 24 95 bc 7d 10 80 	jmp    *-0x7fef8244(,%edx,4)
8010329f:	90                   	nop
  return mpsearch1(0xF0000, 0x10000);
801032a0:	ba 00 00 01 00       	mov    $0x10000,%edx
801032a5:	b8 00 00 0f 00       	mov    $0xf0000,%eax
801032aa:	e8 61 fe ff ff       	call   80103110 <mpsearch1>
801032af:	89 c7                	mov    %eax,%edi
  if((mp = mpsearch()) == 0 || mp->physaddr == 0)
801032b1:	85 c0                	test   %eax,%eax
801032b3:	0f 85 27 ff ff ff    	jne    801031e0 <mpinit+0x50>
801032b9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    panic("Expect to run on an SMP");
801032c0:	83 ec 0c             	sub    $0xc,%esp
801032c3:	68 7d 7d 10 80       	push   $0x80107d7d
801032c8:	e8 c3 d0 ff ff       	call   80100390 <panic>
801032cd:	8d 76 00             	lea    0x0(%esi),%esi
      p += sizeof(struct mpioapic);
      continue;
    case MPBUS:
    case MPIOINTR:
    case MPLINTR:
      p += 8;
801032d0:	83 c0 08             	add    $0x8,%eax
  for(p=(uchar*)(conf+1), e=(uchar*)conf+conf->length; p<e; ){
801032d3:	39 c6                	cmp    %eax,%esi
801032d5:	77 b5                	ja     8010328c <mpinit+0xfc>
    default:
      ismp = 0;
      break;
    }
  }
  if(!ismp)
801032d7:	85 db                	test   %ebx,%ebx
801032d9:	74 6f                	je     8010334a <mpinit+0x1ba>
    panic("Didn't find a suitable machine");

  if(mp->imcrp){
801032db:	80 7f 0c 00          	cmpb   $0x0,0xc(%edi)
801032df:	74 15                	je     801032f6 <mpinit+0x166>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801032e1:	b8 70 00 00 00       	mov    $0x70,%eax
801032e6:	ba 22 00 00 00       	mov    $0x22,%edx
801032eb:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
801032ec:	ba 23 00 00 00       	mov    $0x23,%edx
801032f1:	ec                   	in     (%dx),%al
    // Bochs doesn't support IMCR, so this doesn't run on Bochs.
    // But it would on real hardware.
    outb(0x22, 0x70);   // Select IMCR
    outb(0x23, inb(0x23) | 1);  // Mask external interrupts.
801032f2:	83 c8 01             	or     $0x1,%eax
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
801032f5:	ee                   	out    %al,(%dx)
  }
}
801032f6:	8d 65 f4             	lea    -0xc(%ebp),%esp
801032f9:	5b                   	pop    %ebx
801032fa:	5e                   	pop    %esi
801032fb:	5f                   	pop    %edi
801032fc:	5d                   	pop    %ebp
801032fd:	c3                   	ret    
801032fe:	66 90                	xchg   %ax,%ax
      if(ncpu < NCPU) {
80103300:	8b 15 c0 3d 11 80    	mov    0x80113dc0,%edx
80103306:	83 fa 07             	cmp    $0x7,%edx
80103309:	7f 1f                	jg     8010332a <mpinit+0x19a>
        cpus[ncpu].apicid = proc->apicid;  // apicid may differ from ncpu
8010330b:	69 ca b0 00 00 00    	imul   $0xb0,%edx,%ecx
80103311:	89 55 e4             	mov    %edx,-0x1c(%ebp)
80103314:	0f b6 50 01          	movzbl 0x1(%eax),%edx
80103318:	88 91 40 38 11 80    	mov    %dl,-0x7feec7c0(%ecx)
        ncpu++;
8010331e:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80103321:	83 c2 01             	add    $0x1,%edx
80103324:	89 15 c0 3d 11 80    	mov    %edx,0x80113dc0
      p += sizeof(struct mpproc);
8010332a:	83 c0 14             	add    $0x14,%eax
      continue;
8010332d:	e9 56 ff ff ff       	jmp    80103288 <mpinit+0xf8>
80103332:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      ioapicid = ioapic->apicno;
80103338:	0f b6 50 01          	movzbl 0x1(%eax),%edx
      p += sizeof(struct mpioapic);
8010333c:	83 c0 08             	add    $0x8,%eax
      ioapicid = ioapic->apicno;
8010333f:	88 15 20 38 11 80    	mov    %dl,0x80113820
      continue;
80103345:	e9 3e ff ff ff       	jmp    80103288 <mpinit+0xf8>
    panic("Didn't find a suitable machine");
8010334a:	83 ec 0c             	sub    $0xc,%esp
8010334d:	68 9c 7d 10 80       	push   $0x80107d9c
80103352:	e8 39 d0 ff ff       	call   80100390 <panic>
      ismp = 0;
80103357:	31 db                	xor    %ebx,%ebx
80103359:	e9 31 ff ff ff       	jmp    8010328f <mpinit+0xff>
8010335e:	66 90                	xchg   %ax,%ax

80103360 <picinit>:
80103360:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103365:	ba 21 00 00 00       	mov    $0x21,%edx
8010336a:	ee                   	out    %al,(%dx)
8010336b:	ba a1 00 00 00       	mov    $0xa1,%edx
80103370:	ee                   	out    %al,(%dx)
picinit(void)
{
  // mask all interrupts
  outb(IO_PIC1+1, 0xFF);
  outb(IO_PIC2+1, 0xFF);
}
80103371:	c3                   	ret    
80103372:	66 90                	xchg   %ax,%ax
80103374:	66 90                	xchg   %ax,%ax
80103376:	66 90                	xchg   %ax,%ax
80103378:	66 90                	xchg   %ax,%ax
8010337a:	66 90                	xchg   %ax,%ax
8010337c:	66 90                	xchg   %ax,%ax
8010337e:	66 90                	xchg   %ax,%ax

80103380 <pipealloc>:
  int writeopen;  // write fd is still open
};

int
pipealloc(struct file **f0, struct file **f1)
{
80103380:	55                   	push   %ebp
80103381:	89 e5                	mov    %esp,%ebp
80103383:	57                   	push   %edi
80103384:	56                   	push   %esi
80103385:	53                   	push   %ebx
80103386:	83 ec 0c             	sub    $0xc,%esp
80103389:	8b 5d 08             	mov    0x8(%ebp),%ebx
8010338c:	8b 75 0c             	mov    0xc(%ebp),%esi
  struct pipe *p;

  p = 0;
  *f0 = *f1 = 0;
8010338f:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
80103395:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  if((*f0 = filealloc()) == 0 || (*f1 = filealloc()) == 0)
8010339b:	e8 90 da ff ff       	call   80100e30 <filealloc>
801033a0:	89 03                	mov    %eax,(%ebx)
801033a2:	85 c0                	test   %eax,%eax
801033a4:	0f 84 a8 00 00 00    	je     80103452 <pipealloc+0xd2>
801033aa:	e8 81 da ff ff       	call   80100e30 <filealloc>
801033af:	89 06                	mov    %eax,(%esi)
801033b1:	85 c0                	test   %eax,%eax
801033b3:	0f 84 87 00 00 00    	je     80103440 <pipealloc+0xc0>
    goto bad;
  if((p = (struct pipe*)kalloc()) == 0)
801033b9:	e8 22 f2 ff ff       	call   801025e0 <kalloc>
801033be:	89 c7                	mov    %eax,%edi
801033c0:	85 c0                	test   %eax,%eax
801033c2:	0f 84 b0 00 00 00    	je     80103478 <pipealloc+0xf8>
    goto bad;
  p->readopen = 1;
801033c8:	c7 80 3c 02 00 00 01 	movl   $0x1,0x23c(%eax)
801033cf:	00 00 00 
  p->writeopen = 1;
  p->nwrite = 0;
  p->nread = 0;
  initlock(&p->lock, "pipe");
801033d2:	83 ec 08             	sub    $0x8,%esp
  p->writeopen = 1;
801033d5:	c7 80 40 02 00 00 01 	movl   $0x1,0x240(%eax)
801033dc:	00 00 00 
  p->nwrite = 0;
801033df:	c7 80 38 02 00 00 00 	movl   $0x0,0x238(%eax)
801033e6:	00 00 00 
  p->nread = 0;
801033e9:	c7 80 34 02 00 00 00 	movl   $0x0,0x234(%eax)
801033f0:	00 00 00 
  initlock(&p->lock, "pipe");
801033f3:	68 d0 7d 10 80       	push   $0x80107dd0
801033f8:	50                   	push   %eax
801033f9:	e8 e2 0f 00 00       	call   801043e0 <initlock>
  (*f0)->type = FD_PIPE;
801033fe:	8b 03                	mov    (%ebx),%eax
  (*f0)->pipe = p;
  (*f1)->type = FD_PIPE;
  (*f1)->readable = 0;
  (*f1)->writable = 1;
  (*f1)->pipe = p;
  return 0;
80103400:	83 c4 10             	add    $0x10,%esp
  (*f0)->type = FD_PIPE;
80103403:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f0)->readable = 1;
80103409:	8b 03                	mov    (%ebx),%eax
8010340b:	c6 40 08 01          	movb   $0x1,0x8(%eax)
  (*f0)->writable = 0;
8010340f:	8b 03                	mov    (%ebx),%eax
80103411:	c6 40 09 00          	movb   $0x0,0x9(%eax)
  (*f0)->pipe = p;
80103415:	8b 03                	mov    (%ebx),%eax
80103417:	89 78 0c             	mov    %edi,0xc(%eax)
  (*f1)->type = FD_PIPE;
8010341a:	8b 06                	mov    (%esi),%eax
8010341c:	c7 00 01 00 00 00    	movl   $0x1,(%eax)
  (*f1)->readable = 0;
80103422:	8b 06                	mov    (%esi),%eax
80103424:	c6 40 08 00          	movb   $0x0,0x8(%eax)
  (*f1)->writable = 1;
80103428:	8b 06                	mov    (%esi),%eax
8010342a:	c6 40 09 01          	movb   $0x1,0x9(%eax)
  (*f1)->pipe = p;
8010342e:	8b 06                	mov    (%esi),%eax
80103430:	89 78 0c             	mov    %edi,0xc(%eax)
  if(*f0)
    fileclose(*f0);
  if(*f1)
    fileclose(*f1);
  return -1;
}
80103433:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80103436:	31 c0                	xor    %eax,%eax
}
80103438:	5b                   	pop    %ebx
80103439:	5e                   	pop    %esi
8010343a:	5f                   	pop    %edi
8010343b:	5d                   	pop    %ebp
8010343c:	c3                   	ret    
8010343d:	8d 76 00             	lea    0x0(%esi),%esi
  if(*f0)
80103440:	8b 03                	mov    (%ebx),%eax
80103442:	85 c0                	test   %eax,%eax
80103444:	74 1e                	je     80103464 <pipealloc+0xe4>
    fileclose(*f0);
80103446:	83 ec 0c             	sub    $0xc,%esp
80103449:	50                   	push   %eax
8010344a:	e8 a1 da ff ff       	call   80100ef0 <fileclose>
8010344f:	83 c4 10             	add    $0x10,%esp
  if(*f1)
80103452:	8b 06                	mov    (%esi),%eax
80103454:	85 c0                	test   %eax,%eax
80103456:	74 0c                	je     80103464 <pipealloc+0xe4>
    fileclose(*f1);
80103458:	83 ec 0c             	sub    $0xc,%esp
8010345b:	50                   	push   %eax
8010345c:	e8 8f da ff ff       	call   80100ef0 <fileclose>
80103461:	83 c4 10             	add    $0x10,%esp
}
80103464:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return -1;
80103467:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010346c:	5b                   	pop    %ebx
8010346d:	5e                   	pop    %esi
8010346e:	5f                   	pop    %edi
8010346f:	5d                   	pop    %ebp
80103470:	c3                   	ret    
80103471:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  if(*f0)
80103478:	8b 03                	mov    (%ebx),%eax
8010347a:	85 c0                	test   %eax,%eax
8010347c:	75 c8                	jne    80103446 <pipealloc+0xc6>
8010347e:	eb d2                	jmp    80103452 <pipealloc+0xd2>

80103480 <pipeclose>:

void
pipeclose(struct pipe *p, int writable)
{
80103480:	55                   	push   %ebp
80103481:	89 e5                	mov    %esp,%ebp
80103483:	56                   	push   %esi
80103484:	53                   	push   %ebx
80103485:	8b 5d 08             	mov    0x8(%ebp),%ebx
80103488:	8b 75 0c             	mov    0xc(%ebp),%esi
  acquire(&p->lock);
8010348b:	83 ec 0c             	sub    $0xc,%esp
8010348e:	53                   	push   %ebx
8010348f:	e8 ac 10 00 00       	call   80104540 <acquire>
  if(writable){
80103494:	83 c4 10             	add    $0x10,%esp
80103497:	85 f6                	test   %esi,%esi
80103499:	74 65                	je     80103500 <pipeclose+0x80>
    p->writeopen = 0;
    wakeup(&p->nread);
8010349b:	83 ec 0c             	sub    $0xc,%esp
8010349e:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
    p->writeopen = 0;
801034a4:	c7 83 40 02 00 00 00 	movl   $0x0,0x240(%ebx)
801034ab:	00 00 00 
    wakeup(&p->nread);
801034ae:	50                   	push   %eax
801034af:	e8 3c 0c 00 00       	call   801040f0 <wakeup>
801034b4:	83 c4 10             	add    $0x10,%esp
  } else {
    p->readopen = 0;
    wakeup(&p->nwrite);
  }
  if(p->readopen == 0 && p->writeopen == 0){
801034b7:	8b 93 3c 02 00 00    	mov    0x23c(%ebx),%edx
801034bd:	85 d2                	test   %edx,%edx
801034bf:	75 0a                	jne    801034cb <pipeclose+0x4b>
801034c1:	8b 83 40 02 00 00    	mov    0x240(%ebx),%eax
801034c7:	85 c0                	test   %eax,%eax
801034c9:	74 15                	je     801034e0 <pipeclose+0x60>
    release(&p->lock);
    kfree((char*)p);
  } else
    release(&p->lock);
801034cb:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
801034ce:	8d 65 f8             	lea    -0x8(%ebp),%esp
801034d1:	5b                   	pop    %ebx
801034d2:	5e                   	pop    %esi
801034d3:	5d                   	pop    %ebp
    release(&p->lock);
801034d4:	e9 27 11 00 00       	jmp    80104600 <release>
801034d9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    release(&p->lock);
801034e0:	83 ec 0c             	sub    $0xc,%esp
801034e3:	53                   	push   %ebx
801034e4:	e8 17 11 00 00       	call   80104600 <release>
    kfree((char*)p);
801034e9:	89 5d 08             	mov    %ebx,0x8(%ebp)
801034ec:	83 c4 10             	add    $0x10,%esp
}
801034ef:	8d 65 f8             	lea    -0x8(%ebp),%esp
801034f2:	5b                   	pop    %ebx
801034f3:	5e                   	pop    %esi
801034f4:	5d                   	pop    %ebp
    kfree((char*)p);
801034f5:	e9 26 ef ff ff       	jmp    80102420 <kfree>
801034fa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    wakeup(&p->nwrite);
80103500:	83 ec 0c             	sub    $0xc,%esp
80103503:	8d 83 38 02 00 00    	lea    0x238(%ebx),%eax
    p->readopen = 0;
80103509:	c7 83 3c 02 00 00 00 	movl   $0x0,0x23c(%ebx)
80103510:	00 00 00 
    wakeup(&p->nwrite);
80103513:	50                   	push   %eax
80103514:	e8 d7 0b 00 00       	call   801040f0 <wakeup>
80103519:	83 c4 10             	add    $0x10,%esp
8010351c:	eb 99                	jmp    801034b7 <pipeclose+0x37>
8010351e:	66 90                	xchg   %ax,%ax

80103520 <pipewrite>:

//PAGEBREAK: 40
int
pipewrite(struct pipe *p, char *addr, int n)
{
80103520:	55                   	push   %ebp
80103521:	89 e5                	mov    %esp,%ebp
80103523:	57                   	push   %edi
80103524:	56                   	push   %esi
80103525:	53                   	push   %ebx
80103526:	83 ec 28             	sub    $0x28,%esp
80103529:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int i;

  acquire(&p->lock);
8010352c:	53                   	push   %ebx
8010352d:	e8 0e 10 00 00       	call   80104540 <acquire>
  for(i = 0; i < n; i++){
80103532:	8b 45 10             	mov    0x10(%ebp),%eax
80103535:	83 c4 10             	add    $0x10,%esp
80103538:	85 c0                	test   %eax,%eax
8010353a:	0f 8e c8 00 00 00    	jle    80103608 <pipewrite+0xe8>
80103540:	8b 4d 0c             	mov    0xc(%ebp),%ecx
80103543:	8b 83 38 02 00 00    	mov    0x238(%ebx),%eax
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
      if(p->readopen == 0 || myproc()->killed){
        release(&p->lock);
        return -1;
      }
      wakeup(&p->nread);
80103549:	8d bb 34 02 00 00    	lea    0x234(%ebx),%edi
8010354f:	89 4d e4             	mov    %ecx,-0x1c(%ebp)
80103552:	03 4d 10             	add    0x10(%ebp),%ecx
80103555:	89 4d e0             	mov    %ecx,-0x20(%ebp)
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103558:	8b 8b 34 02 00 00    	mov    0x234(%ebx),%ecx
8010355e:	8d 91 00 02 00 00    	lea    0x200(%ecx),%edx
80103564:	39 d0                	cmp    %edx,%eax
80103566:	75 71                	jne    801035d9 <pipewrite+0xb9>
      if(p->readopen == 0 || myproc()->killed){
80103568:	8b 83 3c 02 00 00    	mov    0x23c(%ebx),%eax
8010356e:	85 c0                	test   %eax,%eax
80103570:	74 4e                	je     801035c0 <pipewrite+0xa0>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80103572:	8d b3 38 02 00 00    	lea    0x238(%ebx),%esi
80103578:	eb 3a                	jmp    801035b4 <pipewrite+0x94>
8010357a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      wakeup(&p->nread);
80103580:	83 ec 0c             	sub    $0xc,%esp
80103583:	57                   	push   %edi
80103584:	e8 67 0b 00 00       	call   801040f0 <wakeup>
      sleep(&p->nwrite, &p->lock);  //DOC: pipewrite-sleep
80103589:	5a                   	pop    %edx
8010358a:	59                   	pop    %ecx
8010358b:	53                   	push   %ebx
8010358c:	56                   	push   %esi
8010358d:	e8 9e 09 00 00       	call   80103f30 <sleep>
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
80103592:	8b 83 34 02 00 00    	mov    0x234(%ebx),%eax
80103598:	8b 93 38 02 00 00    	mov    0x238(%ebx),%edx
8010359e:	83 c4 10             	add    $0x10,%esp
801035a1:	05 00 02 00 00       	add    $0x200,%eax
801035a6:	39 c2                	cmp    %eax,%edx
801035a8:	75 36                	jne    801035e0 <pipewrite+0xc0>
      if(p->readopen == 0 || myproc()->killed){
801035aa:	8b 83 3c 02 00 00    	mov    0x23c(%ebx),%eax
801035b0:	85 c0                	test   %eax,%eax
801035b2:	74 0c                	je     801035c0 <pipewrite+0xa0>
801035b4:	e8 67 03 00 00       	call   80103920 <myproc>
801035b9:	8b 40 24             	mov    0x24(%eax),%eax
801035bc:	85 c0                	test   %eax,%eax
801035be:	74 c0                	je     80103580 <pipewrite+0x60>
        release(&p->lock);
801035c0:	83 ec 0c             	sub    $0xc,%esp
801035c3:	53                   	push   %ebx
801035c4:	e8 37 10 00 00       	call   80104600 <release>
        return -1;
801035c9:	83 c4 10             	add    $0x10,%esp
801035cc:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
  }
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
  release(&p->lock);
  return n;
}
801035d1:	8d 65 f4             	lea    -0xc(%ebp),%esp
801035d4:	5b                   	pop    %ebx
801035d5:	5e                   	pop    %esi
801035d6:	5f                   	pop    %edi
801035d7:	5d                   	pop    %ebp
801035d8:	c3                   	ret    
    while(p->nwrite == p->nread + PIPESIZE){  //DOC: pipewrite-full
801035d9:	89 c2                	mov    %eax,%edx
801035db:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801035df:	90                   	nop
    p->data[p->nwrite++ % PIPESIZE] = addr[i];
801035e0:	8b 75 e4             	mov    -0x1c(%ebp),%esi
801035e3:	8d 42 01             	lea    0x1(%edx),%eax
801035e6:	81 e2 ff 01 00 00    	and    $0x1ff,%edx
801035ec:	89 83 38 02 00 00    	mov    %eax,0x238(%ebx)
801035f2:	0f b6 0e             	movzbl (%esi),%ecx
801035f5:	83 c6 01             	add    $0x1,%esi
801035f8:	89 75 e4             	mov    %esi,-0x1c(%ebp)
801035fb:	88 4c 13 34          	mov    %cl,0x34(%ebx,%edx,1)
  for(i = 0; i < n; i++){
801035ff:	3b 75 e0             	cmp    -0x20(%ebp),%esi
80103602:	0f 85 50 ff ff ff    	jne    80103558 <pipewrite+0x38>
  wakeup(&p->nread);  //DOC: pipewrite-wakeup1
80103608:	83 ec 0c             	sub    $0xc,%esp
8010360b:	8d 83 34 02 00 00    	lea    0x234(%ebx),%eax
80103611:	50                   	push   %eax
80103612:	e8 d9 0a 00 00       	call   801040f0 <wakeup>
  release(&p->lock);
80103617:	89 1c 24             	mov    %ebx,(%esp)
8010361a:	e8 e1 0f 00 00       	call   80104600 <release>
  return n;
8010361f:	83 c4 10             	add    $0x10,%esp
80103622:	8b 45 10             	mov    0x10(%ebp),%eax
80103625:	eb aa                	jmp    801035d1 <pipewrite+0xb1>
80103627:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010362e:	66 90                	xchg   %ax,%ax

80103630 <piperead>:

int
piperead(struct pipe *p, char *addr, int n)
{
80103630:	55                   	push   %ebp
80103631:	89 e5                	mov    %esp,%ebp
80103633:	57                   	push   %edi
80103634:	56                   	push   %esi
80103635:	53                   	push   %ebx
80103636:	83 ec 18             	sub    $0x18,%esp
80103639:	8b 75 08             	mov    0x8(%ebp),%esi
8010363c:	8b 7d 0c             	mov    0xc(%ebp),%edi
  int i;

  acquire(&p->lock);
8010363f:	56                   	push   %esi
80103640:	e8 fb 0e 00 00       	call   80104540 <acquire>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
80103645:	83 c4 10             	add    $0x10,%esp
80103648:	8b 8e 34 02 00 00    	mov    0x234(%esi),%ecx
8010364e:	3b 8e 38 02 00 00    	cmp    0x238(%esi),%ecx
80103654:	75 6a                	jne    801036c0 <piperead+0x90>
80103656:	8b 9e 40 02 00 00    	mov    0x240(%esi),%ebx
8010365c:	85 db                	test   %ebx,%ebx
8010365e:	0f 84 c4 00 00 00    	je     80103728 <piperead+0xf8>
    if(myproc()->killed){
      release(&p->lock);
      return -1;
    }
    sleep(&p->nread, &p->lock); //DOC: piperead-sleep
80103664:	8d 9e 34 02 00 00    	lea    0x234(%esi),%ebx
8010366a:	eb 2d                	jmp    80103699 <piperead+0x69>
8010366c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103670:	83 ec 08             	sub    $0x8,%esp
80103673:	56                   	push   %esi
80103674:	53                   	push   %ebx
80103675:	e8 b6 08 00 00       	call   80103f30 <sleep>
  while(p->nread == p->nwrite && p->writeopen){  //DOC: pipe-empty
8010367a:	83 c4 10             	add    $0x10,%esp
8010367d:	8b 8e 34 02 00 00    	mov    0x234(%esi),%ecx
80103683:	3b 8e 38 02 00 00    	cmp    0x238(%esi),%ecx
80103689:	75 35                	jne    801036c0 <piperead+0x90>
8010368b:	8b 96 40 02 00 00    	mov    0x240(%esi),%edx
80103691:	85 d2                	test   %edx,%edx
80103693:	0f 84 8f 00 00 00    	je     80103728 <piperead+0xf8>
    if(myproc()->killed){
80103699:	e8 82 02 00 00       	call   80103920 <myproc>
8010369e:	8b 48 24             	mov    0x24(%eax),%ecx
801036a1:	85 c9                	test   %ecx,%ecx
801036a3:	74 cb                	je     80103670 <piperead+0x40>
      release(&p->lock);
801036a5:	83 ec 0c             	sub    $0xc,%esp
      return -1;
801036a8:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
      release(&p->lock);
801036ad:	56                   	push   %esi
801036ae:	e8 4d 0f 00 00       	call   80104600 <release>
      return -1;
801036b3:	83 c4 10             	add    $0x10,%esp
    addr[i] = p->data[p->nread++ % PIPESIZE];
  }
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
  release(&p->lock);
  return i;
}
801036b6:	8d 65 f4             	lea    -0xc(%ebp),%esp
801036b9:	89 d8                	mov    %ebx,%eax
801036bb:	5b                   	pop    %ebx
801036bc:	5e                   	pop    %esi
801036bd:	5f                   	pop    %edi
801036be:	5d                   	pop    %ebp
801036bf:	c3                   	ret    
  for(i = 0; i < n; i++){  //DOC: piperead-copy
801036c0:	8b 45 10             	mov    0x10(%ebp),%eax
801036c3:	85 c0                	test   %eax,%eax
801036c5:	7e 61                	jle    80103728 <piperead+0xf8>
    if(p->nread == p->nwrite)
801036c7:	31 db                	xor    %ebx,%ebx
801036c9:	eb 13                	jmp    801036de <piperead+0xae>
801036cb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801036cf:	90                   	nop
801036d0:	8b 8e 34 02 00 00    	mov    0x234(%esi),%ecx
801036d6:	3b 8e 38 02 00 00    	cmp    0x238(%esi),%ecx
801036dc:	74 1f                	je     801036fd <piperead+0xcd>
    addr[i] = p->data[p->nread++ % PIPESIZE];
801036de:	8d 41 01             	lea    0x1(%ecx),%eax
801036e1:	81 e1 ff 01 00 00    	and    $0x1ff,%ecx
801036e7:	89 86 34 02 00 00    	mov    %eax,0x234(%esi)
801036ed:	0f b6 44 0e 34       	movzbl 0x34(%esi,%ecx,1),%eax
801036f2:	88 04 1f             	mov    %al,(%edi,%ebx,1)
  for(i = 0; i < n; i++){  //DOC: piperead-copy
801036f5:	83 c3 01             	add    $0x1,%ebx
801036f8:	39 5d 10             	cmp    %ebx,0x10(%ebp)
801036fb:	75 d3                	jne    801036d0 <piperead+0xa0>
  wakeup(&p->nwrite);  //DOC: piperead-wakeup
801036fd:	83 ec 0c             	sub    $0xc,%esp
80103700:	8d 86 38 02 00 00    	lea    0x238(%esi),%eax
80103706:	50                   	push   %eax
80103707:	e8 e4 09 00 00       	call   801040f0 <wakeup>
  release(&p->lock);
8010370c:	89 34 24             	mov    %esi,(%esp)
8010370f:	e8 ec 0e 00 00       	call   80104600 <release>
  return i;
80103714:	83 c4 10             	add    $0x10,%esp
}
80103717:	8d 65 f4             	lea    -0xc(%ebp),%esp
8010371a:	89 d8                	mov    %ebx,%eax
8010371c:	5b                   	pop    %ebx
8010371d:	5e                   	pop    %esi
8010371e:	5f                   	pop    %edi
8010371f:	5d                   	pop    %ebp
80103720:	c3                   	ret    
80103721:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(p->nread == p->nwrite)
80103728:	31 db                	xor    %ebx,%ebx
8010372a:	eb d1                	jmp    801036fd <piperead+0xcd>
8010372c:	66 90                	xchg   %ax,%ax
8010372e:	66 90                	xchg   %ax,%ax

80103730 <allocproc>:
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc*
allocproc(void)
{
80103730:	55                   	push   %ebp
80103731:	89 e5                	mov    %esp,%ebp
80103733:	53                   	push   %ebx
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103734:	bb 14 3e 11 80       	mov    $0x80113e14,%ebx
{
80103739:	83 ec 10             	sub    $0x10,%esp
  acquire(&ptable.lock);
8010373c:	68 e0 3d 11 80       	push   $0x80113de0
80103741:	e8 fa 0d 00 00       	call   80104540 <acquire>
80103746:	83 c4 10             	add    $0x10,%esp
80103749:	eb 13                	jmp    8010375e <allocproc+0x2e>
8010374b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010374f:	90                   	nop
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103750:	81 c3 bc 00 00 00    	add    $0xbc,%ebx
80103756:	81 fb 14 6d 11 80    	cmp    $0x80116d14,%ebx
8010375c:	74 7a                	je     801037d8 <allocproc+0xa8>
    if(p->state == UNUSED)
8010375e:	8b 43 0c             	mov    0xc(%ebx),%eax
80103761:	85 c0                	test   %eax,%eax
80103763:	75 eb                	jne    80103750 <allocproc+0x20>
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  p->pid = nextpid++;
80103765:	a1 04 b0 10 80       	mov    0x8010b004,%eax

  release(&ptable.lock);
8010376a:	83 ec 0c             	sub    $0xc,%esp
  p->state = EMBRYO;
8010376d:	c7 43 0c 01 00 00 00 	movl   $0x1,0xc(%ebx)
  p->pid = nextpid++;
80103774:	89 43 10             	mov    %eax,0x10(%ebx)
80103777:	8d 50 01             	lea    0x1(%eax),%edx
  release(&ptable.lock);
8010377a:	68 e0 3d 11 80       	push   $0x80113de0
  p->pid = nextpid++;
8010377f:	89 15 04 b0 10 80    	mov    %edx,0x8010b004
  release(&ptable.lock);
80103785:	e8 76 0e 00 00       	call   80104600 <release>

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
8010378a:	e8 51 ee ff ff       	call   801025e0 <kalloc>
8010378f:	83 c4 10             	add    $0x10,%esp
80103792:	89 43 08             	mov    %eax,0x8(%ebx)
80103795:	85 c0                	test   %eax,%eax
80103797:	74 58                	je     801037f1 <allocproc+0xc1>
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
80103799:	8d 90 b4 0f 00 00    	lea    0xfb4(%eax),%edx
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
8010379f:	83 ec 04             	sub    $0x4,%esp
  sp -= sizeof *p->context;
801037a2:	05 9c 0f 00 00       	add    $0xf9c,%eax
  sp -= sizeof *p->tf;
801037a7:	89 53 18             	mov    %edx,0x18(%ebx)
  *(uint*)sp = (uint)trapret;
801037aa:	c7 40 14 3f 59 10 80 	movl   $0x8010593f,0x14(%eax)
  p->context = (struct context*)sp;
801037b1:	89 43 1c             	mov    %eax,0x1c(%ebx)
  memset(p->context, 0, sizeof *p->context);
801037b4:	6a 14                	push   $0x14
801037b6:	6a 00                	push   $0x0
801037b8:	50                   	push   %eax
801037b9:	e8 92 0e 00 00       	call   80104650 <memset>
  p->context->eip = (uint)forkret;
801037be:	8b 43 1c             	mov    0x1c(%ebx),%eax

  return p;
801037c1:	83 c4 10             	add    $0x10,%esp
  p->context->eip = (uint)forkret;
801037c4:	c7 40 10 10 38 10 80 	movl   $0x80103810,0x10(%eax)
}
801037cb:	89 d8                	mov    %ebx,%eax
801037cd:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801037d0:	c9                   	leave  
801037d1:	c3                   	ret    
801037d2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  release(&ptable.lock);
801037d8:	83 ec 0c             	sub    $0xc,%esp
  return 0;
801037db:	31 db                	xor    %ebx,%ebx
  release(&ptable.lock);
801037dd:	68 e0 3d 11 80       	push   $0x80113de0
801037e2:	e8 19 0e 00 00       	call   80104600 <release>
}
801037e7:	89 d8                	mov    %ebx,%eax
  return 0;
801037e9:	83 c4 10             	add    $0x10,%esp
}
801037ec:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801037ef:	c9                   	leave  
801037f0:	c3                   	ret    
    p->state = UNUSED;
801037f1:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return 0;
801037f8:	31 db                	xor    %ebx,%ebx
}
801037fa:	89 d8                	mov    %ebx,%eax
801037fc:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801037ff:	c9                   	leave  
80103800:	c3                   	ret    
80103801:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103808:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010380f:	90                   	nop

80103810 <forkret>:

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
80103810:	55                   	push   %ebp
80103811:	89 e5                	mov    %esp,%ebp
80103813:	83 ec 14             	sub    $0x14,%esp
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);
80103816:	68 e0 3d 11 80       	push   $0x80113de0
8010381b:	e8 e0 0d 00 00       	call   80104600 <release>

  if (first) {
80103820:	a1 00 b0 10 80       	mov    0x8010b000,%eax
80103825:	83 c4 10             	add    $0x10,%esp
80103828:	85 c0                	test   %eax,%eax
8010382a:	75 04                	jne    80103830 <forkret+0x20>
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }

  // Return to "caller", actually trapret (see allocproc).
}
8010382c:	c9                   	leave  
8010382d:	c3                   	ret    
8010382e:	66 90                	xchg   %ax,%ax
    first = 0;
80103830:	c7 05 00 b0 10 80 00 	movl   $0x0,0x8010b000
80103837:	00 00 00 
    iinit(ROOTDEV);
8010383a:	83 ec 0c             	sub    $0xc,%esp
8010383d:	6a 01                	push   $0x1
8010383f:	e8 0c dd ff ff       	call   80101550 <iinit>
    initlog(ROOTDEV);
80103844:	c7 04 24 01 00 00 00 	movl   $0x1,(%esp)
8010384b:	e8 e0 f3 ff ff       	call   80102c30 <initlog>
80103850:	83 c4 10             	add    $0x10,%esp
}
80103853:	c9                   	leave  
80103854:	c3                   	ret    
80103855:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010385c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80103860 <pinit>:
{
80103860:	55                   	push   %ebp
80103861:	89 e5                	mov    %esp,%ebp
80103863:	83 ec 10             	sub    $0x10,%esp
  initlock(&ptable.lock, "ptable");
80103866:	68 d5 7d 10 80       	push   $0x80107dd5
8010386b:	68 e0 3d 11 80       	push   $0x80113de0
80103870:	e8 6b 0b 00 00       	call   801043e0 <initlock>
  seminit();
80103875:	83 c4 10             	add    $0x10,%esp
}
80103878:	c9                   	leave  
  seminit();
80103879:	e9 92 3a 00 00       	jmp    80107310 <seminit>
8010387e:	66 90                	xchg   %ax,%ax

80103880 <mycpu>:
{
80103880:	55                   	push   %ebp
80103881:	89 e5                	mov    %esp,%ebp
80103883:	56                   	push   %esi
80103884:	53                   	push   %ebx
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103885:	9c                   	pushf  
80103886:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80103887:	f6 c4 02             	test   $0x2,%ah
8010388a:	75 5d                	jne    801038e9 <mycpu+0x69>
  apicid = lapicid();
8010388c:	e8 cf ef ff ff       	call   80102860 <lapicid>
  for (i = 0; i < ncpu; ++i) {
80103891:	8b 35 c0 3d 11 80    	mov    0x80113dc0,%esi
80103897:	85 f6                	test   %esi,%esi
80103899:	7e 41                	jle    801038dc <mycpu+0x5c>
    if (cpus[i].apicid == apicid)
8010389b:	0f b6 15 40 38 11 80 	movzbl 0x80113840,%edx
801038a2:	39 d0                	cmp    %edx,%eax
801038a4:	74 2f                	je     801038d5 <mycpu+0x55>
  for (i = 0; i < ncpu; ++i) {
801038a6:	31 d2                	xor    %edx,%edx
801038a8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801038af:	90                   	nop
801038b0:	83 c2 01             	add    $0x1,%edx
801038b3:	39 f2                	cmp    %esi,%edx
801038b5:	74 25                	je     801038dc <mycpu+0x5c>
    if (cpus[i].apicid == apicid)
801038b7:	69 ca b0 00 00 00    	imul   $0xb0,%edx,%ecx
801038bd:	0f b6 99 40 38 11 80 	movzbl -0x7feec7c0(%ecx),%ebx
801038c4:	39 c3                	cmp    %eax,%ebx
801038c6:	75 e8                	jne    801038b0 <mycpu+0x30>
801038c8:	8d 81 40 38 11 80    	lea    -0x7feec7c0(%ecx),%eax
}
801038ce:	8d 65 f8             	lea    -0x8(%ebp),%esp
801038d1:	5b                   	pop    %ebx
801038d2:	5e                   	pop    %esi
801038d3:	5d                   	pop    %ebp
801038d4:	c3                   	ret    
    if (cpus[i].apicid == apicid)
801038d5:	b8 40 38 11 80       	mov    $0x80113840,%eax
      return &cpus[i];
801038da:	eb f2                	jmp    801038ce <mycpu+0x4e>
  panic("unknown apicid\n");
801038dc:	83 ec 0c             	sub    $0xc,%esp
801038df:	68 dc 7d 10 80       	push   $0x80107ddc
801038e4:	e8 a7 ca ff ff       	call   80100390 <panic>
    panic("mycpu called with interrupts enabled\n");
801038e9:	83 ec 0c             	sub    $0xc,%esp
801038ec:	68 b8 7e 10 80       	push   $0x80107eb8
801038f1:	e8 9a ca ff ff       	call   80100390 <panic>
801038f6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801038fd:	8d 76 00             	lea    0x0(%esi),%esi

80103900 <cpuid>:
cpuid() {
80103900:	55                   	push   %ebp
80103901:	89 e5                	mov    %esp,%ebp
80103903:	83 ec 08             	sub    $0x8,%esp
  return mycpu()-cpus;
80103906:	e8 75 ff ff ff       	call   80103880 <mycpu>
}
8010390b:	c9                   	leave  
  return mycpu()-cpus;
8010390c:	2d 40 38 11 80       	sub    $0x80113840,%eax
80103911:	c1 f8 04             	sar    $0x4,%eax
80103914:	69 c0 a3 8b 2e ba    	imul   $0xba2e8ba3,%eax,%eax
}
8010391a:	c3                   	ret    
8010391b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010391f:	90                   	nop

80103920 <myproc>:
myproc(void) {
80103920:	55                   	push   %ebp
80103921:	89 e5                	mov    %esp,%ebp
80103923:	53                   	push   %ebx
80103924:	83 ec 04             	sub    $0x4,%esp
  pushcli();
80103927:	e8 24 0b 00 00       	call   80104450 <pushcli>
  c = mycpu();
8010392c:	e8 4f ff ff ff       	call   80103880 <mycpu>
  p = c->proc;
80103931:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103937:	e8 64 0b 00 00       	call   801044a0 <popcli>
}
8010393c:	83 c4 04             	add    $0x4,%esp
8010393f:	89 d8                	mov    %ebx,%eax
80103941:	5b                   	pop    %ebx
80103942:	5d                   	pop    %ebp
80103943:	c3                   	ret    
80103944:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010394b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010394f:	90                   	nop

80103950 <userinit>:
{
80103950:	55                   	push   %ebp
80103951:	89 e5                	mov    %esp,%ebp
80103953:	53                   	push   %ebx
80103954:	83 ec 04             	sub    $0x4,%esp
  p = allocproc();
80103957:	e8 d4 fd ff ff       	call   80103730 <allocproc>
8010395c:	89 c3                	mov    %eax,%ebx
  initproc = p;
8010395e:	a3 b8 b5 10 80       	mov    %eax,0x8010b5b8
  if((p->pgdir = setupkvm()) == 0)
80103963:	e8 08 37 00 00       	call   80107070 <setupkvm>
80103968:	89 43 04             	mov    %eax,0x4(%ebx)
8010396b:	85 c0                	test   %eax,%eax
8010396d:	0f 84 c3 00 00 00    	je     80103a36 <userinit+0xe6>
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
80103973:	83 ec 04             	sub    $0x4,%esp
80103976:	68 2c 00 00 00       	push   $0x2c
8010397b:	68 60 b4 10 80       	push   $0x8010b460
80103980:	50                   	push   %eax
80103981:	e8 ca 33 00 00       	call   80106d50 <inituvm>
  memset(p->tf, 0, sizeof(*p->tf));
80103986:	83 c4 0c             	add    $0xc,%esp
  p->sz = PGSIZE;
80103989:	c7 03 00 10 00 00    	movl   $0x1000,(%ebx)
  memset(p->tf, 0, sizeof(*p->tf));
8010398f:	6a 4c                	push   $0x4c
80103991:	6a 00                	push   $0x0
80103993:	ff 73 18             	pushl  0x18(%ebx)
80103996:	e8 b5 0c 00 00       	call   80104650 <memset>
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
8010399b:	8b 43 18             	mov    0x18(%ebx),%eax
8010399e:	ba 1b 00 00 00       	mov    $0x1b,%edx
  safestrcpy(p->name, "initcode", sizeof(p->name));
801039a3:	83 c4 0c             	add    $0xc,%esp
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
801039a6:	b9 23 00 00 00       	mov    $0x23,%ecx
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
801039ab:	66 89 50 3c          	mov    %dx,0x3c(%eax)
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
801039af:	8b 43 18             	mov    0x18(%ebx),%eax
801039b2:	66 89 48 2c          	mov    %cx,0x2c(%eax)
  p->tf->es = p->tf->ds;
801039b6:	8b 43 18             	mov    0x18(%ebx),%eax
801039b9:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
801039bd:	66 89 50 28          	mov    %dx,0x28(%eax)
  p->tf->ss = p->tf->ds;
801039c1:	8b 43 18             	mov    0x18(%ebx),%eax
801039c4:	0f b7 50 2c          	movzwl 0x2c(%eax),%edx
801039c8:	66 89 50 48          	mov    %dx,0x48(%eax)
  p->tf->eflags = FL_IF;
801039cc:	8b 43 18             	mov    0x18(%ebx),%eax
801039cf:	c7 40 40 00 02 00 00 	movl   $0x200,0x40(%eax)
  p->tf->esp = PGSIZE;
801039d6:	8b 43 18             	mov    0x18(%ebx),%eax
801039d9:	c7 40 44 00 10 00 00 	movl   $0x1000,0x44(%eax)
  p->tf->eip = 0;  // beginning of initcode.S
801039e0:	8b 43 18             	mov    0x18(%ebx),%eax
801039e3:	c7 40 38 00 00 00 00 	movl   $0x0,0x38(%eax)
  safestrcpy(p->name, "initcode", sizeof(p->name));
801039ea:	8d 83 ac 00 00 00    	lea    0xac(%ebx),%eax
801039f0:	6a 10                	push   $0x10
801039f2:	68 05 7e 10 80       	push   $0x80107e05
801039f7:	50                   	push   %eax
801039f8:	e8 23 0e 00 00       	call   80104820 <safestrcpy>
  p->cwd = namei("/");
801039fd:	c7 04 24 0e 7e 10 80 	movl   $0x80107e0e,(%esp)
80103a04:	e8 e7 e5 ff ff       	call   80101ff0 <namei>
80103a09:	89 83 a4 00 00 00    	mov    %eax,0xa4(%ebx)
  acquire(&ptable.lock);
80103a0f:	c7 04 24 e0 3d 11 80 	movl   $0x80113de0,(%esp)
80103a16:	e8 25 0b 00 00       	call   80104540 <acquire>
  p->state = RUNNABLE;
80103a1b:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  release(&ptable.lock);
80103a22:	c7 04 24 e0 3d 11 80 	movl   $0x80113de0,(%esp)
80103a29:	e8 d2 0b 00 00       	call   80104600 <release>
}
80103a2e:	83 c4 10             	add    $0x10,%esp
80103a31:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103a34:	c9                   	leave  
80103a35:	c3                   	ret    
    panic("userinit: out of memory?");
80103a36:	83 ec 0c             	sub    $0xc,%esp
80103a39:	68 ec 7d 10 80       	push   $0x80107dec
80103a3e:	e8 4d c9 ff ff       	call   80100390 <panic>
80103a43:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103a4a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80103a50 <growproc>:
{
80103a50:	55                   	push   %ebp
80103a51:	89 e5                	mov    %esp,%ebp
80103a53:	56                   	push   %esi
80103a54:	53                   	push   %ebx
80103a55:	8b 75 08             	mov    0x8(%ebp),%esi
  pushcli();
80103a58:	e8 f3 09 00 00       	call   80104450 <pushcli>
  c = mycpu();
80103a5d:	e8 1e fe ff ff       	call   80103880 <mycpu>
  p = c->proc;
80103a62:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103a68:	e8 33 0a 00 00       	call   801044a0 <popcli>
  sz = curproc->sz;
80103a6d:	8b 03                	mov    (%ebx),%eax
  if(n > 0){
80103a6f:	85 f6                	test   %esi,%esi
80103a71:	7f 1d                	jg     80103a90 <growproc+0x40>
  } else if(n < 0){
80103a73:	75 3b                	jne    80103ab0 <growproc+0x60>
  switchuvm(curproc);
80103a75:	83 ec 0c             	sub    $0xc,%esp
  curproc->sz = sz;
80103a78:	89 03                	mov    %eax,(%ebx)
  switchuvm(curproc);
80103a7a:	53                   	push   %ebx
80103a7b:	e8 c0 31 00 00       	call   80106c40 <switchuvm>
  return 0;
80103a80:	83 c4 10             	add    $0x10,%esp
80103a83:	31 c0                	xor    %eax,%eax
}
80103a85:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103a88:	5b                   	pop    %ebx
80103a89:	5e                   	pop    %esi
80103a8a:	5d                   	pop    %ebp
80103a8b:	c3                   	ret    
80103a8c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if((sz = allocuvm(curproc->pgdir, sz, sz + n)) == 0)
80103a90:	83 ec 04             	sub    $0x4,%esp
80103a93:	01 c6                	add    %eax,%esi
80103a95:	56                   	push   %esi
80103a96:	50                   	push   %eax
80103a97:	ff 73 04             	pushl  0x4(%ebx)
80103a9a:	e8 f1 33 00 00       	call   80106e90 <allocuvm>
80103a9f:	83 c4 10             	add    $0x10,%esp
80103aa2:	85 c0                	test   %eax,%eax
80103aa4:	75 cf                	jne    80103a75 <growproc+0x25>
      return -1;
80103aa6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80103aab:	eb d8                	jmp    80103a85 <growproc+0x35>
80103aad:	8d 76 00             	lea    0x0(%esi),%esi
    if((sz = deallocuvm(curproc->pgdir, sz, sz + n)) == 0)
80103ab0:	83 ec 04             	sub    $0x4,%esp
80103ab3:	01 c6                	add    %eax,%esi
80103ab5:	56                   	push   %esi
80103ab6:	50                   	push   %eax
80103ab7:	ff 73 04             	pushl  0x4(%ebx)
80103aba:	e8 01 35 00 00       	call   80106fc0 <deallocuvm>
80103abf:	83 c4 10             	add    $0x10,%esp
80103ac2:	85 c0                	test   %eax,%eax
80103ac4:	75 af                	jne    80103a75 <growproc+0x25>
80103ac6:	eb de                	jmp    80103aa6 <growproc+0x56>
80103ac8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103acf:	90                   	nop

80103ad0 <fork>:
{
80103ad0:	55                   	push   %ebp
80103ad1:	89 e5                	mov    %esp,%ebp
80103ad3:	57                   	push   %edi
80103ad4:	56                   	push   %esi
80103ad5:	53                   	push   %ebx
80103ad6:	83 ec 1c             	sub    $0x1c,%esp
  pushcli();
80103ad9:	e8 72 09 00 00       	call   80104450 <pushcli>
  c = mycpu();
80103ade:	e8 9d fd ff ff       	call   80103880 <mycpu>
  p = c->proc;
80103ae3:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103ae9:	e8 b2 09 00 00       	call   801044a0 <popcli>
  if((np = allocproc()) == 0){
80103aee:	e8 3d fc ff ff       	call   80103730 <allocproc>
80103af3:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80103af6:	85 c0                	test   %eax,%eax
80103af8:	0f 84 c3 00 00 00    	je     80103bc1 <fork+0xf1>
  if((np->pgdir = copyuvm(curproc->pgdir, curproc->sz)) == 0){
80103afe:	83 ec 08             	sub    $0x8,%esp
80103b01:	ff 33                	pushl  (%ebx)
80103b03:	89 c7                	mov    %eax,%edi
80103b05:	ff 73 04             	pushl  0x4(%ebx)
80103b08:	e8 33 36 00 00       	call   80107140 <copyuvm>
80103b0d:	83 c4 10             	add    $0x10,%esp
80103b10:	89 47 04             	mov    %eax,0x4(%edi)
80103b13:	85 c0                	test   %eax,%eax
80103b15:	0f 84 ad 00 00 00    	je     80103bc8 <fork+0xf8>
  np->sz = curproc->sz;
80103b1b:	8b 03                	mov    (%ebx),%eax
80103b1d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80103b20:	89 01                	mov    %eax,(%ecx)
  *np->tf = *curproc->tf;
80103b22:	8b 79 18             	mov    0x18(%ecx),%edi
  np->parent = curproc;
80103b25:	89 c8                	mov    %ecx,%eax
80103b27:	89 59 14             	mov    %ebx,0x14(%ecx)
  *np->tf = *curproc->tf;
80103b2a:	b9 13 00 00 00       	mov    $0x13,%ecx
80103b2f:	8b 73 18             	mov    0x18(%ebx),%esi
80103b32:	f3 a5                	rep movsl %ds:(%esi),%es:(%edi)
  for(i = 0; i < NOFILE; i++)
80103b34:	31 f6                	xor    %esi,%esi
  np->tf->eax = 0;
80103b36:	8b 40 18             	mov    0x18(%eax),%eax
80103b39:	c7 40 1c 00 00 00 00 	movl   $0x0,0x1c(%eax)
    if(curproc->ofile[i])
80103b40:	8b 44 b3 28          	mov    0x28(%ebx,%esi,4),%eax
80103b44:	85 c0                	test   %eax,%eax
80103b46:	74 13                	je     80103b5b <fork+0x8b>
      np->ofile[i] = filedup(curproc->ofile[i]);
80103b48:	83 ec 0c             	sub    $0xc,%esp
80103b4b:	50                   	push   %eax
80103b4c:	e8 4f d3 ff ff       	call   80100ea0 <filedup>
80103b51:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80103b54:	83 c4 10             	add    $0x10,%esp
80103b57:	89 44 b2 28          	mov    %eax,0x28(%edx,%esi,4)
  for(i = 0; i < NOFILE; i++)
80103b5b:	83 c6 01             	add    $0x1,%esi
80103b5e:	83 fe 10             	cmp    $0x10,%esi
80103b61:	75 dd                	jne    80103b40 <fork+0x70>
  np->cwd = idup(curproc->cwd);
80103b63:	83 ec 0c             	sub    $0xc,%esp
80103b66:	ff b3 a4 00 00 00    	pushl  0xa4(%ebx)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103b6c:	81 c3 ac 00 00 00    	add    $0xac,%ebx
  np->cwd = idup(curproc->cwd);
80103b72:	e8 a9 db ff ff       	call   80101720 <idup>
80103b77:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103b7a:	83 c4 0c             	add    $0xc,%esp
  np->cwd = idup(curproc->cwd);
80103b7d:	89 87 a4 00 00 00    	mov    %eax,0xa4(%edi)
  safestrcpy(np->name, curproc->name, sizeof(curproc->name));
80103b83:	8d 87 ac 00 00 00    	lea    0xac(%edi),%eax
80103b89:	6a 10                	push   $0x10
80103b8b:	53                   	push   %ebx
80103b8c:	50                   	push   %eax
80103b8d:	e8 8e 0c 00 00       	call   80104820 <safestrcpy>
  pid = np->pid;
80103b92:	8b 5f 10             	mov    0x10(%edi),%ebx
  acquire(&ptable.lock);
80103b95:	c7 04 24 e0 3d 11 80 	movl   $0x80113de0,(%esp)
80103b9c:	e8 9f 09 00 00       	call   80104540 <acquire>
  np->state = RUNNABLE;
80103ba1:	c7 47 0c 03 00 00 00 	movl   $0x3,0xc(%edi)
  release(&ptable.lock);
80103ba8:	c7 04 24 e0 3d 11 80 	movl   $0x80113de0,(%esp)
80103baf:	e8 4c 0a 00 00       	call   80104600 <release>
  return pid;
80103bb4:	83 c4 10             	add    $0x10,%esp
}
80103bb7:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103bba:	89 d8                	mov    %ebx,%eax
80103bbc:	5b                   	pop    %ebx
80103bbd:	5e                   	pop    %esi
80103bbe:	5f                   	pop    %edi
80103bbf:	5d                   	pop    %ebp
80103bc0:	c3                   	ret    
    return -1;
80103bc1:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80103bc6:	eb ef                	jmp    80103bb7 <fork+0xe7>
    kfree(np->kstack);
80103bc8:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
80103bcb:	83 ec 0c             	sub    $0xc,%esp
80103bce:	ff 73 08             	pushl  0x8(%ebx)
80103bd1:	e8 4a e8 ff ff       	call   80102420 <kfree>
    np->kstack = 0;
80103bd6:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
    return -1;
80103bdd:	83 c4 10             	add    $0x10,%esp
    np->state = UNUSED;
80103be0:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
    return -1;
80103be7:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80103bec:	eb c9                	jmp    80103bb7 <fork+0xe7>
80103bee:	66 90                	xchg   %ax,%ax

80103bf0 <scheduler>:
{
80103bf0:	55                   	push   %ebp
80103bf1:	89 e5                	mov    %esp,%ebp
80103bf3:	57                   	push   %edi
80103bf4:	56                   	push   %esi
80103bf5:	53                   	push   %ebx
80103bf6:	83 ec 0c             	sub    $0xc,%esp
  struct cpu *c = mycpu();
80103bf9:	e8 82 fc ff ff       	call   80103880 <mycpu>
  c->proc = 0;
80103bfe:	c7 80 ac 00 00 00 00 	movl   $0x0,0xac(%eax)
80103c05:	00 00 00 
  struct cpu *c = mycpu();
80103c08:	89 c6                	mov    %eax,%esi
  c->proc = 0;
80103c0a:	8d 78 04             	lea    0x4(%eax),%edi
80103c0d:	8d 76 00             	lea    0x0(%esi),%esi
  asm volatile("sti");
80103c10:	fb                   	sti    
    acquire(&ptable.lock);
80103c11:	83 ec 0c             	sub    $0xc,%esp
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103c14:	bb 14 3e 11 80       	mov    $0x80113e14,%ebx
    acquire(&ptable.lock);
80103c19:	68 e0 3d 11 80       	push   $0x80113de0
80103c1e:	e8 1d 09 00 00       	call   80104540 <acquire>
80103c23:	83 c4 10             	add    $0x10,%esp
80103c26:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103c2d:	8d 76 00             	lea    0x0(%esi),%esi
      if(p->state != RUNNABLE)
80103c30:	83 7b 0c 03          	cmpl   $0x3,0xc(%ebx)
80103c34:	75 3d                	jne    80103c73 <scheduler+0x83>
      switchuvm(p);
80103c36:	83 ec 0c             	sub    $0xc,%esp
      c->proc = p;
80103c39:	89 9e ac 00 00 00    	mov    %ebx,0xac(%esi)
      switchuvm(p);
80103c3f:	53                   	push   %ebx
80103c40:	e8 fb 2f 00 00       	call   80106c40 <switchuvm>
      swtch(&(c->scheduler), p->context);
80103c45:	58                   	pop    %eax
80103c46:	5a                   	pop    %edx
80103c47:	ff 73 1c             	pushl  0x1c(%ebx)
80103c4a:	57                   	push   %edi
      p->state = RUNNING;
80103c4b:	c7 43 0c 04 00 00 00 	movl   $0x4,0xc(%ebx)
	  p->ticks = 0;
80103c52:	c7 83 a8 00 00 00 00 	movl   $0x0,0xa8(%ebx)
80103c59:	00 00 00 
      swtch(&(c->scheduler), p->context);
80103c5c:	e8 1a 0c 00 00       	call   8010487b <swtch>
      switchkvm();
80103c61:	e8 ca 2f 00 00       	call   80106c30 <switchkvm>
      c->proc = 0;
80103c66:	83 c4 10             	add    $0x10,%esp
80103c69:	c7 86 ac 00 00 00 00 	movl   $0x0,0xac(%esi)
80103c70:	00 00 00 
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103c73:	81 c3 bc 00 00 00    	add    $0xbc,%ebx
80103c79:	81 fb 14 6d 11 80    	cmp    $0x80116d14,%ebx
80103c7f:	75 af                	jne    80103c30 <scheduler+0x40>
    release(&ptable.lock);
80103c81:	83 ec 0c             	sub    $0xc,%esp
80103c84:	68 e0 3d 11 80       	push   $0x80113de0
80103c89:	e8 72 09 00 00       	call   80104600 <release>
    sti();
80103c8e:	83 c4 10             	add    $0x10,%esp
80103c91:	e9 7a ff ff ff       	jmp    80103c10 <scheduler+0x20>
80103c96:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103c9d:	8d 76 00             	lea    0x0(%esi),%esi

80103ca0 <sched>:
{
80103ca0:	55                   	push   %ebp
80103ca1:	89 e5                	mov    %esp,%ebp
80103ca3:	56                   	push   %esi
80103ca4:	53                   	push   %ebx
  pushcli();
80103ca5:	e8 a6 07 00 00       	call   80104450 <pushcli>
  c = mycpu();
80103caa:	e8 d1 fb ff ff       	call   80103880 <mycpu>
  p = c->proc;
80103caf:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103cb5:	e8 e6 07 00 00       	call   801044a0 <popcli>
  if(!holding(&ptable.lock))
80103cba:	83 ec 0c             	sub    $0xc,%esp
80103cbd:	68 e0 3d 11 80       	push   $0x80113de0
80103cc2:	e8 39 08 00 00       	call   80104500 <holding>
80103cc7:	83 c4 10             	add    $0x10,%esp
80103cca:	85 c0                	test   %eax,%eax
80103ccc:	74 4f                	je     80103d1d <sched+0x7d>
  if(mycpu()->ncli != 1)
80103cce:	e8 ad fb ff ff       	call   80103880 <mycpu>
80103cd3:	83 b8 a4 00 00 00 01 	cmpl   $0x1,0xa4(%eax)
80103cda:	75 68                	jne    80103d44 <sched+0xa4>
  if(p->state == RUNNING)
80103cdc:	83 7b 0c 04          	cmpl   $0x4,0xc(%ebx)
80103ce0:	74 55                	je     80103d37 <sched+0x97>
  asm volatile("pushfl; popl %0" : "=r" (eflags));
80103ce2:	9c                   	pushf  
80103ce3:	58                   	pop    %eax
  if(readeflags()&FL_IF)
80103ce4:	f6 c4 02             	test   $0x2,%ah
80103ce7:	75 41                	jne    80103d2a <sched+0x8a>
  intena = mycpu()->intena;
80103ce9:	e8 92 fb ff ff       	call   80103880 <mycpu>
  swtch(&p->context, mycpu()->scheduler);
80103cee:	83 c3 1c             	add    $0x1c,%ebx
  intena = mycpu()->intena;
80103cf1:	8b b0 a8 00 00 00    	mov    0xa8(%eax),%esi
  swtch(&p->context, mycpu()->scheduler);
80103cf7:	e8 84 fb ff ff       	call   80103880 <mycpu>
80103cfc:	83 ec 08             	sub    $0x8,%esp
80103cff:	ff 70 04             	pushl  0x4(%eax)
80103d02:	53                   	push   %ebx
80103d03:	e8 73 0b 00 00       	call   8010487b <swtch>
  mycpu()->intena = intena;
80103d08:	e8 73 fb ff ff       	call   80103880 <mycpu>
}
80103d0d:	83 c4 10             	add    $0x10,%esp
  mycpu()->intena = intena;
80103d10:	89 b0 a8 00 00 00    	mov    %esi,0xa8(%eax)
}
80103d16:	8d 65 f8             	lea    -0x8(%ebp),%esp
80103d19:	5b                   	pop    %ebx
80103d1a:	5e                   	pop    %esi
80103d1b:	5d                   	pop    %ebp
80103d1c:	c3                   	ret    
    panic("sched ptable.lock");
80103d1d:	83 ec 0c             	sub    $0xc,%esp
80103d20:	68 10 7e 10 80       	push   $0x80107e10
80103d25:	e8 66 c6 ff ff       	call   80100390 <panic>
    panic("sched interruptible");
80103d2a:	83 ec 0c             	sub    $0xc,%esp
80103d2d:	68 3c 7e 10 80       	push   $0x80107e3c
80103d32:	e8 59 c6 ff ff       	call   80100390 <panic>
    panic("sched running");
80103d37:	83 ec 0c             	sub    $0xc,%esp
80103d3a:	68 2e 7e 10 80       	push   $0x80107e2e
80103d3f:	e8 4c c6 ff ff       	call   80100390 <panic>
    panic("sched locks");
80103d44:	83 ec 0c             	sub    $0xc,%esp
80103d47:	68 22 7e 10 80       	push   $0x80107e22
80103d4c:	e8 3f c6 ff ff       	call   80100390 <panic>
80103d51:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103d58:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103d5f:	90                   	nop

80103d60 <exit>:
{
80103d60:	55                   	push   %ebp
80103d61:	89 e5                	mov    %esp,%ebp
80103d63:	57                   	push   %edi
80103d64:	56                   	push   %esi
80103d65:	53                   	push   %ebx
80103d66:	83 ec 0c             	sub    $0xc,%esp
  pushcli();
80103d69:	e8 e2 06 00 00       	call   80104450 <pushcli>
  c = mycpu();
80103d6e:	e8 0d fb ff ff       	call   80103880 <mycpu>
  p = c->proc;
80103d73:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103d79:	e8 22 07 00 00       	call   801044a0 <popcli>
  if(curproc == initproc)
80103d7e:	8d 73 28             	lea    0x28(%ebx),%esi
80103d81:	8d 7b 68             	lea    0x68(%ebx),%edi
80103d84:	39 1d b8 b5 10 80    	cmp    %ebx,0x8010b5b8
80103d8a:	0f 84 41 01 00 00    	je     80103ed1 <exit+0x171>
    if(curproc->ofile[fd]){
80103d90:	8b 06                	mov    (%esi),%eax
80103d92:	85 c0                	test   %eax,%eax
80103d94:	74 12                	je     80103da8 <exit+0x48>
      fileclose(curproc->ofile[fd]);
80103d96:	83 ec 0c             	sub    $0xc,%esp
80103d99:	50                   	push   %eax
80103d9a:	e8 51 d1 ff ff       	call   80100ef0 <fileclose>
      curproc->ofile[fd] = 0;
80103d9f:	c7 06 00 00 00 00    	movl   $0x0,(%esi)
80103da5:	83 c4 10             	add    $0x10,%esp
80103da8:	83 c6 04             	add    $0x4,%esi
  for(fd = 0; fd < NOFILE; fd++){
80103dab:	39 fe                	cmp    %edi,%esi
80103dad:	75 e1                	jne    80103d90 <exit+0x30>
  for(fd = 0; fd < NSEM; fd++){
80103daf:	31 f6                	xor    %esi,%esi
    if(curproc->psem[fd]){
80103db1:	8b 54 b3 68          	mov    0x68(%ebx,%esi,4),%edx
80103db5:	85 d2                	test   %edx,%edx
80103db7:	74 14                	je     80103dcd <exit+0x6d>
      semclose(fd);
80103db9:	83 ec 0c             	sub    $0xc,%esp
80103dbc:	56                   	push   %esi
80103dbd:	e8 ae 37 00 00       	call   80107570 <semclose>
      curproc->psem[fd] = 0;
80103dc2:	c7 44 b3 68 00 00 00 	movl   $0x0,0x68(%ebx,%esi,4)
80103dc9:	00 
80103dca:	83 c4 10             	add    $0x10,%esp
  for(fd = 0; fd < NSEM; fd++){
80103dcd:	83 c6 01             	add    $0x1,%esi
80103dd0:	83 fe 05             	cmp    $0x5,%esi
80103dd3:	75 dc                	jne    80103db1 <exit+0x51>
  for(sm = 0; sm < NOSHM; sm++) {
80103dd5:	31 f6                	xor    %esi,%esi
    if (curproc->shm[sm].block) {
80103dd7:	8b 44 f3 7c          	mov    0x7c(%ebx,%esi,8),%eax
80103ddb:	85 c0                	test   %eax,%eax
80103ddd:	74 0d                	je     80103dec <exit+0x8c>
	  shmfree(curproc, sm);      
80103ddf:	83 ec 08             	sub    $0x8,%esp
80103de2:	56                   	push   %esi
80103de3:	53                   	push   %ebx
80103de4:	e8 07 3a 00 00       	call   801077f0 <shmfree>
80103de9:	83 c4 10             	add    $0x10,%esp
  for(sm = 0; sm < NOSHM; sm++) {
80103dec:	83 c6 01             	add    $0x1,%esi
80103def:	83 fe 05             	cmp    $0x5,%esi
80103df2:	75 e3                	jne    80103dd7 <exit+0x77>
  begin_op();
80103df4:	e8 d7 ee ff ff       	call   80102cd0 <begin_op>
  iput(curproc->cwd);
80103df9:	83 ec 0c             	sub    $0xc,%esp
80103dfc:	ff b3 a4 00 00 00    	pushl  0xa4(%ebx)
80103e02:	e8 79 da ff ff       	call   80101880 <iput>
  end_op();
80103e07:	e8 34 ef ff ff       	call   80102d40 <end_op>
  curproc->cwd = 0;
80103e0c:	c7 83 a4 00 00 00 00 	movl   $0x0,0xa4(%ebx)
80103e13:	00 00 00 
  acquire(&ptable.lock);
80103e16:	c7 04 24 e0 3d 11 80 	movl   $0x80113de0,(%esp)
80103e1d:	e8 1e 07 00 00       	call   80104540 <acquire>
  wakeup1(curproc->parent);
80103e22:	8b 53 14             	mov    0x14(%ebx),%edx
80103e25:	83 c4 10             	add    $0x10,%esp
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103e28:	b8 14 3e 11 80       	mov    $0x80113e14,%eax
80103e2d:	eb 0d                	jmp    80103e3c <exit+0xdc>
80103e2f:	90                   	nop
80103e30:	05 bc 00 00 00       	add    $0xbc,%eax
80103e35:	3d 14 6d 11 80       	cmp    $0x80116d14,%eax
80103e3a:	74 1e                	je     80103e5a <exit+0xfa>
    if(p->state == SLEEPING && p->chan == chan)
80103e3c:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
80103e40:	75 ee                	jne    80103e30 <exit+0xd0>
80103e42:	3b 50 20             	cmp    0x20(%eax),%edx
80103e45:	75 e9                	jne    80103e30 <exit+0xd0>
      p->state = RUNNABLE;
80103e47:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103e4e:	05 bc 00 00 00       	add    $0xbc,%eax
80103e53:	3d 14 6d 11 80       	cmp    $0x80116d14,%eax
80103e58:	75 e2                	jne    80103e3c <exit+0xdc>
      p->parent = initproc;
80103e5a:	8b 0d b8 b5 10 80    	mov    0x8010b5b8,%ecx
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80103e60:	ba 14 3e 11 80       	mov    $0x80113e14,%edx
80103e65:	eb 17                	jmp    80103e7e <exit+0x11e>
80103e67:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103e6e:	66 90                	xchg   %ax,%ax
80103e70:	81 c2 bc 00 00 00    	add    $0xbc,%edx
80103e76:	81 fa 14 6d 11 80    	cmp    $0x80116d14,%edx
80103e7c:	74 3a                	je     80103eb8 <exit+0x158>
    if(p->parent == curproc){
80103e7e:	39 5a 14             	cmp    %ebx,0x14(%edx)
80103e81:	75 ed                	jne    80103e70 <exit+0x110>
      if(p->state == ZOMBIE)
80103e83:	83 7a 0c 05          	cmpl   $0x5,0xc(%edx)
      p->parent = initproc;
80103e87:	89 4a 14             	mov    %ecx,0x14(%edx)
      if(p->state == ZOMBIE)
80103e8a:	75 e4                	jne    80103e70 <exit+0x110>
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80103e8c:	b8 14 3e 11 80       	mov    $0x80113e14,%eax
80103e91:	eb 11                	jmp    80103ea4 <exit+0x144>
80103e93:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80103e97:	90                   	nop
80103e98:	05 bc 00 00 00       	add    $0xbc,%eax
80103e9d:	3d 14 6d 11 80       	cmp    $0x80116d14,%eax
80103ea2:	74 cc                	je     80103e70 <exit+0x110>
    if(p->state == SLEEPING && p->chan == chan)
80103ea4:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
80103ea8:	75 ee                	jne    80103e98 <exit+0x138>
80103eaa:	3b 48 20             	cmp    0x20(%eax),%ecx
80103ead:	75 e9                	jne    80103e98 <exit+0x138>
      p->state = RUNNABLE;
80103eaf:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
80103eb6:	eb e0                	jmp    80103e98 <exit+0x138>
  curproc->state = ZOMBIE;
80103eb8:	c7 43 0c 05 00 00 00 	movl   $0x5,0xc(%ebx)
  sched();
80103ebf:	e8 dc fd ff ff       	call   80103ca0 <sched>
  panic("zombie exit");
80103ec4:	83 ec 0c             	sub    $0xc,%esp
80103ec7:	68 5d 7e 10 80       	push   $0x80107e5d
80103ecc:	e8 bf c4 ff ff       	call   80100390 <panic>
    panic("init exiting");
80103ed1:	83 ec 0c             	sub    $0xc,%esp
80103ed4:	68 50 7e 10 80       	push   $0x80107e50
80103ed9:	e8 b2 c4 ff ff       	call   80100390 <panic>
80103ede:	66 90                	xchg   %ax,%ax

80103ee0 <yield>:
{
80103ee0:	55                   	push   %ebp
80103ee1:	89 e5                	mov    %esp,%ebp
80103ee3:	53                   	push   %ebx
80103ee4:	83 ec 10             	sub    $0x10,%esp
  acquire(&ptable.lock);  //DOC: yieldlock
80103ee7:	68 e0 3d 11 80       	push   $0x80113de0
80103eec:	e8 4f 06 00 00       	call   80104540 <acquire>
  pushcli();
80103ef1:	e8 5a 05 00 00       	call   80104450 <pushcli>
  c = mycpu();
80103ef6:	e8 85 f9 ff ff       	call   80103880 <mycpu>
  p = c->proc;
80103efb:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103f01:	e8 9a 05 00 00       	call   801044a0 <popcli>
  myproc()->state = RUNNABLE;
80103f06:	c7 43 0c 03 00 00 00 	movl   $0x3,0xc(%ebx)
  sched();
80103f0d:	e8 8e fd ff ff       	call   80103ca0 <sched>
  release(&ptable.lock);
80103f12:	c7 04 24 e0 3d 11 80 	movl   $0x80113de0,(%esp)
80103f19:	e8 e2 06 00 00       	call   80104600 <release>
}
80103f1e:	83 c4 10             	add    $0x10,%esp
80103f21:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80103f24:	c9                   	leave  
80103f25:	c3                   	ret    
80103f26:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80103f2d:	8d 76 00             	lea    0x0(%esi),%esi

80103f30 <sleep>:
{
80103f30:	55                   	push   %ebp
80103f31:	89 e5                	mov    %esp,%ebp
80103f33:	57                   	push   %edi
80103f34:	56                   	push   %esi
80103f35:	53                   	push   %ebx
80103f36:	83 ec 0c             	sub    $0xc,%esp
80103f39:	8b 7d 08             	mov    0x8(%ebp),%edi
80103f3c:	8b 75 0c             	mov    0xc(%ebp),%esi
  pushcli();
80103f3f:	e8 0c 05 00 00       	call   80104450 <pushcli>
  c = mycpu();
80103f44:	e8 37 f9 ff ff       	call   80103880 <mycpu>
  p = c->proc;
80103f49:	8b 98 ac 00 00 00    	mov    0xac(%eax),%ebx
  popcli();
80103f4f:	e8 4c 05 00 00       	call   801044a0 <popcli>
  if(p == 0)
80103f54:	85 db                	test   %ebx,%ebx
80103f56:	0f 84 87 00 00 00    	je     80103fe3 <sleep+0xb3>
  if(lk == 0)
80103f5c:	85 f6                	test   %esi,%esi
80103f5e:	74 76                	je     80103fd6 <sleep+0xa6>
  if(lk != &ptable.lock){  //DOC: sleeplock0
80103f60:	81 fe e0 3d 11 80    	cmp    $0x80113de0,%esi
80103f66:	74 50                	je     80103fb8 <sleep+0x88>
    acquire(&ptable.lock);  //DOC: sleeplock1
80103f68:	83 ec 0c             	sub    $0xc,%esp
80103f6b:	68 e0 3d 11 80       	push   $0x80113de0
80103f70:	e8 cb 05 00 00       	call   80104540 <acquire>
    release(lk);
80103f75:	89 34 24             	mov    %esi,(%esp)
80103f78:	e8 83 06 00 00       	call   80104600 <release>
  p->chan = chan;
80103f7d:	89 7b 20             	mov    %edi,0x20(%ebx)
  p->state = SLEEPING;
80103f80:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
  sched();
80103f87:	e8 14 fd ff ff       	call   80103ca0 <sched>
  p->chan = 0;
80103f8c:	c7 43 20 00 00 00 00 	movl   $0x0,0x20(%ebx)
    release(&ptable.lock);
80103f93:	c7 04 24 e0 3d 11 80 	movl   $0x80113de0,(%esp)
80103f9a:	e8 61 06 00 00       	call   80104600 <release>
    acquire(lk);
80103f9f:	89 75 08             	mov    %esi,0x8(%ebp)
80103fa2:	83 c4 10             	add    $0x10,%esp
}
80103fa5:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103fa8:	5b                   	pop    %ebx
80103fa9:	5e                   	pop    %esi
80103faa:	5f                   	pop    %edi
80103fab:	5d                   	pop    %ebp
    acquire(lk);
80103fac:	e9 8f 05 00 00       	jmp    80104540 <acquire>
80103fb1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  p->chan = chan;
80103fb8:	89 7b 20             	mov    %edi,0x20(%ebx)
  p->state = SLEEPING;
80103fbb:	c7 43 0c 02 00 00 00 	movl   $0x2,0xc(%ebx)
  sched();
80103fc2:	e8 d9 fc ff ff       	call   80103ca0 <sched>
  p->chan = 0;
80103fc7:	c7 43 20 00 00 00 00 	movl   $0x0,0x20(%ebx)
}
80103fce:	8d 65 f4             	lea    -0xc(%ebp),%esp
80103fd1:	5b                   	pop    %ebx
80103fd2:	5e                   	pop    %esi
80103fd3:	5f                   	pop    %edi
80103fd4:	5d                   	pop    %ebp
80103fd5:	c3                   	ret    
    panic("sleep without lk");
80103fd6:	83 ec 0c             	sub    $0xc,%esp
80103fd9:	68 6f 7e 10 80       	push   $0x80107e6f
80103fde:	e8 ad c3 ff ff       	call   80100390 <panic>
    panic("sleep");
80103fe3:	83 ec 0c             	sub    $0xc,%esp
80103fe6:	68 69 7e 10 80       	push   $0x80107e69
80103feb:	e8 a0 c3 ff ff       	call   80100390 <panic>

80103ff0 <wait>:
{
80103ff0:	55                   	push   %ebp
80103ff1:	89 e5                	mov    %esp,%ebp
80103ff3:	56                   	push   %esi
80103ff4:	53                   	push   %ebx
  pushcli();
80103ff5:	e8 56 04 00 00       	call   80104450 <pushcli>
  c = mycpu();
80103ffa:	e8 81 f8 ff ff       	call   80103880 <mycpu>
  p = c->proc;
80103fff:	8b b0 ac 00 00 00    	mov    0xac(%eax),%esi
  popcli();
80104005:	e8 96 04 00 00       	call   801044a0 <popcli>
  acquire(&ptable.lock);
8010400a:	83 ec 0c             	sub    $0xc,%esp
8010400d:	68 e0 3d 11 80       	push   $0x80113de0
80104012:	e8 29 05 00 00       	call   80104540 <acquire>
80104017:	83 c4 10             	add    $0x10,%esp
    havekids = 0;
8010401a:	31 c0                	xor    %eax,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010401c:	bb 14 3e 11 80       	mov    $0x80113e14,%ebx
80104021:	eb 13                	jmp    80104036 <wait+0x46>
80104023:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104027:	90                   	nop
80104028:	81 c3 bc 00 00 00    	add    $0xbc,%ebx
8010402e:	81 fb 14 6d 11 80    	cmp    $0x80116d14,%ebx
80104034:	74 1e                	je     80104054 <wait+0x64>
      if(p->parent != curproc)
80104036:	39 73 14             	cmp    %esi,0x14(%ebx)
80104039:	75 ed                	jne    80104028 <wait+0x38>
      if(p->state == ZOMBIE){
8010403b:	83 7b 0c 05          	cmpl   $0x5,0xc(%ebx)
8010403f:	74 37                	je     80104078 <wait+0x88>
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104041:	81 c3 bc 00 00 00    	add    $0xbc,%ebx
      havekids = 1;
80104047:	b8 01 00 00 00       	mov    $0x1,%eax
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
8010404c:	81 fb 14 6d 11 80    	cmp    $0x80116d14,%ebx
80104052:	75 e2                	jne    80104036 <wait+0x46>
    if(!havekids || curproc->killed){
80104054:	85 c0                	test   %eax,%eax
80104056:	74 79                	je     801040d1 <wait+0xe1>
80104058:	8b 46 24             	mov    0x24(%esi),%eax
8010405b:	85 c0                	test   %eax,%eax
8010405d:	75 72                	jne    801040d1 <wait+0xe1>
    sleep(curproc, &ptable.lock);  //DOC: wait-sleep
8010405f:	83 ec 08             	sub    $0x8,%esp
80104062:	68 e0 3d 11 80       	push   $0x80113de0
80104067:	56                   	push   %esi
80104068:	e8 c3 fe ff ff       	call   80103f30 <sleep>
    havekids = 0;
8010406d:	83 c4 10             	add    $0x10,%esp
80104070:	eb a8                	jmp    8010401a <wait+0x2a>
80104072:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
        kfree(p->kstack);
80104078:	83 ec 0c             	sub    $0xc,%esp
8010407b:	ff 73 08             	pushl  0x8(%ebx)
        pid = p->pid;
8010407e:	8b 73 10             	mov    0x10(%ebx),%esi
        kfree(p->kstack);
80104081:	e8 9a e3 ff ff       	call   80102420 <kfree>
        freevm(p->pgdir);
80104086:	5a                   	pop    %edx
80104087:	ff 73 04             	pushl  0x4(%ebx)
        p->kstack = 0;	
8010408a:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
        freevm(p->pgdir);
80104091:	e8 5a 2f 00 00       	call   80106ff0 <freevm>
        release(&ptable.lock);
80104096:	c7 04 24 e0 3d 11 80 	movl   $0x80113de0,(%esp)
        p->pid = 0;
8010409d:	c7 43 10 00 00 00 00 	movl   $0x0,0x10(%ebx)
        p->parent = 0;
801040a4:	c7 43 14 00 00 00 00 	movl   $0x0,0x14(%ebx)
        p->name[0] = 0;
801040ab:	c6 83 ac 00 00 00 00 	movb   $0x0,0xac(%ebx)
        p->killed = 0;
801040b2:	c7 43 24 00 00 00 00 	movl   $0x0,0x24(%ebx)
        p->state = UNUSED;
801040b9:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
        release(&ptable.lock);
801040c0:	e8 3b 05 00 00       	call   80104600 <release>
        return pid;
801040c5:	83 c4 10             	add    $0x10,%esp
}
801040c8:	8d 65 f8             	lea    -0x8(%ebp),%esp
801040cb:	89 f0                	mov    %esi,%eax
801040cd:	5b                   	pop    %ebx
801040ce:	5e                   	pop    %esi
801040cf:	5d                   	pop    %ebp
801040d0:	c3                   	ret    
      release(&ptable.lock);
801040d1:	83 ec 0c             	sub    $0xc,%esp
      return -1;
801040d4:	be ff ff ff ff       	mov    $0xffffffff,%esi
      release(&ptable.lock);
801040d9:	68 e0 3d 11 80       	push   $0x80113de0
801040de:	e8 1d 05 00 00       	call   80104600 <release>
      return -1;
801040e3:	83 c4 10             	add    $0x10,%esp
801040e6:	eb e0                	jmp    801040c8 <wait+0xd8>
801040e8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801040ef:	90                   	nop

801040f0 <wakeup>:
}

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
801040f0:	55                   	push   %ebp
801040f1:	89 e5                	mov    %esp,%ebp
801040f3:	53                   	push   %ebx
801040f4:	83 ec 10             	sub    $0x10,%esp
801040f7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&ptable.lock);
801040fa:	68 e0 3d 11 80       	push   $0x80113de0
801040ff:	e8 3c 04 00 00       	call   80104540 <acquire>
80104104:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
80104107:	b8 14 3e 11 80       	mov    $0x80113e14,%eax
8010410c:	eb 0e                	jmp    8010411c <wakeup+0x2c>
8010410e:	66 90                	xchg   %ax,%ax
80104110:	05 bc 00 00 00       	add    $0xbc,%eax
80104115:	3d 14 6d 11 80       	cmp    $0x80116d14,%eax
8010411a:	74 1e                	je     8010413a <wakeup+0x4a>
    if(p->state == SLEEPING && p->chan == chan)
8010411c:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
80104120:	75 ee                	jne    80104110 <wakeup+0x20>
80104122:	3b 58 20             	cmp    0x20(%eax),%ebx
80104125:	75 e9                	jne    80104110 <wakeup+0x20>
      p->state = RUNNABLE;
80104127:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
8010412e:	05 bc 00 00 00       	add    $0xbc,%eax
80104133:	3d 14 6d 11 80       	cmp    $0x80116d14,%eax
80104138:	75 e2                	jne    8010411c <wakeup+0x2c>
  wakeup1(chan);
  release(&ptable.lock);
8010413a:	c7 45 08 e0 3d 11 80 	movl   $0x80113de0,0x8(%ebp)
}
80104141:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104144:	c9                   	leave  
  release(&ptable.lock);
80104145:	e9 b6 04 00 00       	jmp    80104600 <release>
8010414a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104150 <kill>:
// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int
kill(int pid)
{
80104150:	55                   	push   %ebp
80104151:	89 e5                	mov    %esp,%ebp
80104153:	53                   	push   %ebx
80104154:	83 ec 10             	sub    $0x10,%esp
80104157:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *p;

  acquire(&ptable.lock);
8010415a:	68 e0 3d 11 80       	push   $0x80113de0
8010415f:	e8 dc 03 00 00       	call   80104540 <acquire>
80104164:	83 c4 10             	add    $0x10,%esp
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
80104167:	b8 14 3e 11 80       	mov    $0x80113e14,%eax
8010416c:	eb 0e                	jmp    8010417c <kill+0x2c>
8010416e:	66 90                	xchg   %ax,%ax
80104170:	05 bc 00 00 00       	add    $0xbc,%eax
80104175:	3d 14 6d 11 80       	cmp    $0x80116d14,%eax
8010417a:	74 34                	je     801041b0 <kill+0x60>
    if(p->pid == pid){
8010417c:	39 58 10             	cmp    %ebx,0x10(%eax)
8010417f:	75 ef                	jne    80104170 <kill+0x20>
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
80104181:	83 78 0c 02          	cmpl   $0x2,0xc(%eax)
      p->killed = 1;
80104185:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
      if(p->state == SLEEPING)
8010418c:	75 07                	jne    80104195 <kill+0x45>
        p->state = RUNNABLE;
8010418e:	c7 40 0c 03 00 00 00 	movl   $0x3,0xc(%eax)
      release(&ptable.lock);
80104195:	83 ec 0c             	sub    $0xc,%esp
80104198:	68 e0 3d 11 80       	push   $0x80113de0
8010419d:	e8 5e 04 00 00       	call   80104600 <release>
      return 0;
801041a2:	83 c4 10             	add    $0x10,%esp
801041a5:	31 c0                	xor    %eax,%eax
    }
  }
  release(&ptable.lock);
  return -1;
}
801041a7:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801041aa:	c9                   	leave  
801041ab:	c3                   	ret    
801041ac:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  release(&ptable.lock);
801041b0:	83 ec 0c             	sub    $0xc,%esp
801041b3:	68 e0 3d 11 80       	push   $0x80113de0
801041b8:	e8 43 04 00 00       	call   80104600 <release>
  return -1;
801041bd:	83 c4 10             	add    $0x10,%esp
801041c0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801041c5:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801041c8:	c9                   	leave  
801041c9:	c3                   	ret    
801041ca:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801041d0 <procdump>:
// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
801041d0:	55                   	push   %ebp
801041d1:	89 e5                	mov    %esp,%ebp
801041d3:	57                   	push   %edi
801041d4:	56                   	push   %esi
801041d5:	8d 75 e8             	lea    -0x18(%ebp),%esi
801041d8:	53                   	push   %ebx
801041d9:	bb c0 3e 11 80       	mov    $0x80113ec0,%ebx
801041de:	83 ec 3c             	sub    $0x3c,%esp
801041e1:	eb 27                	jmp    8010420a <procdump+0x3a>
801041e3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801041e7:	90                   	nop
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
801041e8:	83 ec 0c             	sub    $0xc,%esp
801041eb:	68 6d 82 10 80       	push   $0x8010826d
801041f0:	e8 bb c4 ff ff       	call   801006b0 <cprintf>
801041f5:	83 c4 10             	add    $0x10,%esp
801041f8:	81 c3 bc 00 00 00    	add    $0xbc,%ebx
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
801041fe:	81 fb c0 6d 11 80    	cmp    $0x80116dc0,%ebx
80104204:	0f 84 96 00 00 00    	je     801042a0 <procdump+0xd0>
    if(p->state == UNUSED)
8010420a:	8b 83 60 ff ff ff    	mov    -0xa0(%ebx),%eax
80104210:	85 c0                	test   %eax,%eax
80104212:	74 e4                	je     801041f8 <procdump+0x28>
      state = "???";
80104214:	ba 80 7e 10 80       	mov    $0x80107e80,%edx
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
80104219:	83 f8 05             	cmp    $0x5,%eax
8010421c:	77 11                	ja     8010422f <procdump+0x5f>
8010421e:	8b 14 85 e0 7e 10 80 	mov    -0x7fef8120(,%eax,4),%edx
      state = "???";
80104225:	b8 80 7e 10 80       	mov    $0x80107e80,%eax
8010422a:	85 d2                	test   %edx,%edx
8010422c:	0f 44 d0             	cmove  %eax,%edx
    cprintf("%d %s %s", p->pid, state, p->name);
8010422f:	53                   	push   %ebx
80104230:	52                   	push   %edx
80104231:	ff b3 64 ff ff ff    	pushl  -0x9c(%ebx)
80104237:	68 84 7e 10 80       	push   $0x80107e84
8010423c:	e8 6f c4 ff ff       	call   801006b0 <cprintf>
    if(p->state == SLEEPING){
80104241:	83 c4 10             	add    $0x10,%esp
80104244:	83 bb 60 ff ff ff 02 	cmpl   $0x2,-0xa0(%ebx)
8010424b:	75 9b                	jne    801041e8 <procdump+0x18>
      getcallerpcs((uint*)p->context->ebp+2, pc);
8010424d:	83 ec 08             	sub    $0x8,%esp
80104250:	8d 45 c0             	lea    -0x40(%ebp),%eax
80104253:	8d 7d c0             	lea    -0x40(%ebp),%edi
80104256:	50                   	push   %eax
80104257:	8b 83 70 ff ff ff    	mov    -0x90(%ebx),%eax
8010425d:	8b 40 0c             	mov    0xc(%eax),%eax
80104260:	83 c0 08             	add    $0x8,%eax
80104263:	50                   	push   %eax
80104264:	e8 97 01 00 00       	call   80104400 <getcallerpcs>
      for(i=0; i<10 && pc[i] != 0; i++)
80104269:	83 c4 10             	add    $0x10,%esp
8010426c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104270:	8b 17                	mov    (%edi),%edx
80104272:	85 d2                	test   %edx,%edx
80104274:	0f 84 6e ff ff ff    	je     801041e8 <procdump+0x18>
        cprintf(" %p", pc[i]);
8010427a:	83 ec 08             	sub    $0x8,%esp
8010427d:	83 c7 04             	add    $0x4,%edi
80104280:	52                   	push   %edx
80104281:	68 c1 78 10 80       	push   $0x801078c1
80104286:	e8 25 c4 ff ff       	call   801006b0 <cprintf>
      for(i=0; i<10 && pc[i] != 0; i++)
8010428b:	83 c4 10             	add    $0x10,%esp
8010428e:	39 fe                	cmp    %edi,%esi
80104290:	75 de                	jne    80104270 <procdump+0xa0>
80104292:	e9 51 ff ff ff       	jmp    801041e8 <procdump+0x18>
80104297:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010429e:	66 90                	xchg   %ax,%ax
  }
}
801042a0:	8d 65 f4             	lea    -0xc(%ebp),%esp
801042a3:	5b                   	pop    %ebx
801042a4:	5e                   	pop    %esi
801042a5:	5f                   	pop    %edi
801042a6:	5d                   	pop    %ebp
801042a7:	c3                   	ret    
801042a8:	66 90                	xchg   %ax,%ax
801042aa:	66 90                	xchg   %ax,%ax
801042ac:	66 90                	xchg   %ax,%ax
801042ae:	66 90                	xchg   %ax,%ax

801042b0 <initsleeplock>:
#include "spinlock.h"
#include "sleeplock.h"

void
initsleeplock(struct sleeplock *lk, char *name)
{
801042b0:	55                   	push   %ebp
801042b1:	89 e5                	mov    %esp,%ebp
801042b3:	53                   	push   %ebx
801042b4:	83 ec 0c             	sub    $0xc,%esp
801042b7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  initlock(&lk->lk, "sleep lock");
801042ba:	68 f8 7e 10 80       	push   $0x80107ef8
801042bf:	8d 43 04             	lea    0x4(%ebx),%eax
801042c2:	50                   	push   %eax
801042c3:	e8 18 01 00 00       	call   801043e0 <initlock>
  lk->name = name;
801042c8:	8b 45 0c             	mov    0xc(%ebp),%eax
  lk->locked = 0;
801042cb:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
}
801042d1:	83 c4 10             	add    $0x10,%esp
  lk->pid = 0;
801042d4:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  lk->name = name;
801042db:	89 43 38             	mov    %eax,0x38(%ebx)
}
801042de:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801042e1:	c9                   	leave  
801042e2:	c3                   	ret    
801042e3:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801042ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801042f0 <acquiresleep>:

void
acquiresleep(struct sleeplock *lk)
{
801042f0:	55                   	push   %ebp
801042f1:	89 e5                	mov    %esp,%ebp
801042f3:	56                   	push   %esi
801042f4:	53                   	push   %ebx
801042f5:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
801042f8:	8d 73 04             	lea    0x4(%ebx),%esi
801042fb:	83 ec 0c             	sub    $0xc,%esp
801042fe:	56                   	push   %esi
801042ff:	e8 3c 02 00 00       	call   80104540 <acquire>
  while (lk->locked) {
80104304:	8b 13                	mov    (%ebx),%edx
80104306:	83 c4 10             	add    $0x10,%esp
80104309:	85 d2                	test   %edx,%edx
8010430b:	74 16                	je     80104323 <acquiresleep+0x33>
8010430d:	8d 76 00             	lea    0x0(%esi),%esi
    sleep(lk, &lk->lk);
80104310:	83 ec 08             	sub    $0x8,%esp
80104313:	56                   	push   %esi
80104314:	53                   	push   %ebx
80104315:	e8 16 fc ff ff       	call   80103f30 <sleep>
  while (lk->locked) {
8010431a:	8b 03                	mov    (%ebx),%eax
8010431c:	83 c4 10             	add    $0x10,%esp
8010431f:	85 c0                	test   %eax,%eax
80104321:	75 ed                	jne    80104310 <acquiresleep+0x20>
  }
  lk->locked = 1;
80104323:	c7 03 01 00 00 00    	movl   $0x1,(%ebx)
  lk->pid = myproc()->pid;
80104329:	e8 f2 f5 ff ff       	call   80103920 <myproc>
8010432e:	8b 40 10             	mov    0x10(%eax),%eax
80104331:	89 43 3c             	mov    %eax,0x3c(%ebx)
  release(&lk->lk);
80104334:	89 75 08             	mov    %esi,0x8(%ebp)
}
80104337:	8d 65 f8             	lea    -0x8(%ebp),%esp
8010433a:	5b                   	pop    %ebx
8010433b:	5e                   	pop    %esi
8010433c:	5d                   	pop    %ebp
  release(&lk->lk);
8010433d:	e9 be 02 00 00       	jmp    80104600 <release>
80104342:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104349:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80104350 <releasesleep>:

void
releasesleep(struct sleeplock *lk)
{
80104350:	55                   	push   %ebp
80104351:	89 e5                	mov    %esp,%ebp
80104353:	56                   	push   %esi
80104354:	53                   	push   %ebx
80104355:	8b 5d 08             	mov    0x8(%ebp),%ebx
  acquire(&lk->lk);
80104358:	8d 73 04             	lea    0x4(%ebx),%esi
8010435b:	83 ec 0c             	sub    $0xc,%esp
8010435e:	56                   	push   %esi
8010435f:	e8 dc 01 00 00       	call   80104540 <acquire>
  lk->locked = 0;
80104364:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
  lk->pid = 0;
8010436a:	c7 43 3c 00 00 00 00 	movl   $0x0,0x3c(%ebx)
  wakeup(lk);
80104371:	89 1c 24             	mov    %ebx,(%esp)
80104374:	e8 77 fd ff ff       	call   801040f0 <wakeup>
  release(&lk->lk);
80104379:	89 75 08             	mov    %esi,0x8(%ebp)
8010437c:	83 c4 10             	add    $0x10,%esp
}
8010437f:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104382:	5b                   	pop    %ebx
80104383:	5e                   	pop    %esi
80104384:	5d                   	pop    %ebp
  release(&lk->lk);
80104385:	e9 76 02 00 00       	jmp    80104600 <release>
8010438a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80104390 <holdingsleep>:

int
holdingsleep(struct sleeplock *lk)
{
80104390:	55                   	push   %ebp
80104391:	89 e5                	mov    %esp,%ebp
80104393:	57                   	push   %edi
80104394:	31 ff                	xor    %edi,%edi
80104396:	56                   	push   %esi
80104397:	53                   	push   %ebx
80104398:	83 ec 18             	sub    $0x18,%esp
8010439b:	8b 5d 08             	mov    0x8(%ebp),%ebx
  int r;
  
  acquire(&lk->lk);
8010439e:	8d 73 04             	lea    0x4(%ebx),%esi
801043a1:	56                   	push   %esi
801043a2:	e8 99 01 00 00       	call   80104540 <acquire>
  r = lk->locked && (lk->pid == myproc()->pid);
801043a7:	8b 03                	mov    (%ebx),%eax
801043a9:	83 c4 10             	add    $0x10,%esp
801043ac:	85 c0                	test   %eax,%eax
801043ae:	75 18                	jne    801043c8 <holdingsleep+0x38>
  release(&lk->lk);
801043b0:	83 ec 0c             	sub    $0xc,%esp
801043b3:	56                   	push   %esi
801043b4:	e8 47 02 00 00       	call   80104600 <release>
  return r;
}
801043b9:	8d 65 f4             	lea    -0xc(%ebp),%esp
801043bc:	89 f8                	mov    %edi,%eax
801043be:	5b                   	pop    %ebx
801043bf:	5e                   	pop    %esi
801043c0:	5f                   	pop    %edi
801043c1:	5d                   	pop    %ebp
801043c2:	c3                   	ret    
801043c3:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801043c7:	90                   	nop
  r = lk->locked && (lk->pid == myproc()->pid);
801043c8:	8b 5b 3c             	mov    0x3c(%ebx),%ebx
801043cb:	e8 50 f5 ff ff       	call   80103920 <myproc>
801043d0:	39 58 10             	cmp    %ebx,0x10(%eax)
801043d3:	0f 94 c0             	sete   %al
801043d6:	0f b6 c0             	movzbl %al,%eax
801043d9:	89 c7                	mov    %eax,%edi
801043db:	eb d3                	jmp    801043b0 <holdingsleep+0x20>
801043dd:	66 90                	xchg   %ax,%ax
801043df:	90                   	nop

801043e0 <initlock>:
#include "proc.h"
#include "spinlock.h"

void
initlock(struct spinlock *lk, char *name)
{
801043e0:	55                   	push   %ebp
801043e1:	89 e5                	mov    %esp,%ebp
801043e3:	8b 45 08             	mov    0x8(%ebp),%eax
  lk->name = name;
801043e6:	8b 55 0c             	mov    0xc(%ebp),%edx
  lk->locked = 0;
801043e9:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  lk->name = name;
801043ef:	89 50 04             	mov    %edx,0x4(%eax)
  lk->cpu = 0;
801043f2:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
}
801043f9:	5d                   	pop    %ebp
801043fa:	c3                   	ret    
801043fb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801043ff:	90                   	nop

80104400 <getcallerpcs>:
}

// Record the current call stack in pcs[] by following the %ebp chain.
void
getcallerpcs(void *v, uint pcs[])
{
80104400:	55                   	push   %ebp
  uint *ebp;
  int i;

  ebp = (uint*)v - 2;
  for(i = 0; i < 10; i++){
80104401:	31 d2                	xor    %edx,%edx
{
80104403:	89 e5                	mov    %esp,%ebp
80104405:	53                   	push   %ebx
  ebp = (uint*)v - 2;
80104406:	8b 45 08             	mov    0x8(%ebp),%eax
{
80104409:	8b 4d 0c             	mov    0xc(%ebp),%ecx
  ebp = (uint*)v - 2;
8010440c:	83 e8 08             	sub    $0x8,%eax
  for(i = 0; i < 10; i++){
8010440f:	90                   	nop
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80104410:	8d 98 00 00 00 80    	lea    -0x80000000(%eax),%ebx
80104416:	81 fb fe ff ff 7f    	cmp    $0x7ffffffe,%ebx
8010441c:	77 1a                	ja     80104438 <getcallerpcs+0x38>
      break;
    pcs[i] = ebp[1];     // saved %eip
8010441e:	8b 58 04             	mov    0x4(%eax),%ebx
80104421:	89 1c 91             	mov    %ebx,(%ecx,%edx,4)
  for(i = 0; i < 10; i++){
80104424:	83 c2 01             	add    $0x1,%edx
    ebp = (uint*)ebp[0]; // saved %ebp
80104427:	8b 00                	mov    (%eax),%eax
  for(i = 0; i < 10; i++){
80104429:	83 fa 0a             	cmp    $0xa,%edx
8010442c:	75 e2                	jne    80104410 <getcallerpcs+0x10>
  }
  for(; i < 10; i++)
    pcs[i] = 0;
}
8010442e:	5b                   	pop    %ebx
8010442f:	5d                   	pop    %ebp
80104430:	c3                   	ret    
80104431:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104438:	8d 04 91             	lea    (%ecx,%edx,4),%eax
8010443b:	8d 51 28             	lea    0x28(%ecx),%edx
8010443e:	66 90                	xchg   %ax,%ax
    pcs[i] = 0;
80104440:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
80104446:	83 c0 04             	add    $0x4,%eax
  for(; i < 10; i++)
80104449:	39 d0                	cmp    %edx,%eax
8010444b:	75 f3                	jne    80104440 <getcallerpcs+0x40>
}
8010444d:	5b                   	pop    %ebx
8010444e:	5d                   	pop    %ebp
8010444f:	c3                   	ret    

80104450 <pushcli>:
// it takes two popcli to undo two pushcli.  Also, if interrupts
// are off, then pushcli, popcli leaves them off.

void
pushcli(void)
{
80104450:	55                   	push   %ebp
80104451:	89 e5                	mov    %esp,%ebp
80104453:	53                   	push   %ebx
80104454:	83 ec 04             	sub    $0x4,%esp
80104457:	9c                   	pushf  
80104458:	5b                   	pop    %ebx
  asm volatile("cli");
80104459:	fa                   	cli    
  int eflags;

  eflags = readeflags();
  cli();
  if(mycpu()->ncli == 0)
8010445a:	e8 21 f4 ff ff       	call   80103880 <mycpu>
8010445f:	8b 80 a4 00 00 00    	mov    0xa4(%eax),%eax
80104465:	85 c0                	test   %eax,%eax
80104467:	74 17                	je     80104480 <pushcli+0x30>
    mycpu()->intena = eflags & FL_IF;
  mycpu()->ncli += 1;
80104469:	e8 12 f4 ff ff       	call   80103880 <mycpu>
8010446e:	83 80 a4 00 00 00 01 	addl   $0x1,0xa4(%eax)
}
80104475:	83 c4 04             	add    $0x4,%esp
80104478:	5b                   	pop    %ebx
80104479:	5d                   	pop    %ebp
8010447a:	c3                   	ret    
8010447b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010447f:	90                   	nop
    mycpu()->intena = eflags & FL_IF;
80104480:	e8 fb f3 ff ff       	call   80103880 <mycpu>
80104485:	81 e3 00 02 00 00    	and    $0x200,%ebx
8010448b:	89 98 a8 00 00 00    	mov    %ebx,0xa8(%eax)
80104491:	eb d6                	jmp    80104469 <pushcli+0x19>
80104493:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010449a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801044a0 <popcli>:

void
popcli(void)
{
801044a0:	55                   	push   %ebp
801044a1:	89 e5                	mov    %esp,%ebp
801044a3:	83 ec 08             	sub    $0x8,%esp
  asm volatile("pushfl; popl %0" : "=r" (eflags));
801044a6:	9c                   	pushf  
801044a7:	58                   	pop    %eax
  if(readeflags()&FL_IF)
801044a8:	f6 c4 02             	test   $0x2,%ah
801044ab:	75 35                	jne    801044e2 <popcli+0x42>
    panic("popcli - interruptible");
  if(--mycpu()->ncli < 0)
801044ad:	e8 ce f3 ff ff       	call   80103880 <mycpu>
801044b2:	83 a8 a4 00 00 00 01 	subl   $0x1,0xa4(%eax)
801044b9:	78 34                	js     801044ef <popcli+0x4f>
    panic("popcli");
  if(mycpu()->ncli == 0 && mycpu()->intena)
801044bb:	e8 c0 f3 ff ff       	call   80103880 <mycpu>
801044c0:	8b 90 a4 00 00 00    	mov    0xa4(%eax),%edx
801044c6:	85 d2                	test   %edx,%edx
801044c8:	74 06                	je     801044d0 <popcli+0x30>
    sti();
}
801044ca:	c9                   	leave  
801044cb:	c3                   	ret    
801044cc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
  if(mycpu()->ncli == 0 && mycpu()->intena)
801044d0:	e8 ab f3 ff ff       	call   80103880 <mycpu>
801044d5:	8b 80 a8 00 00 00    	mov    0xa8(%eax),%eax
801044db:	85 c0                	test   %eax,%eax
801044dd:	74 eb                	je     801044ca <popcli+0x2a>
  asm volatile("sti");
801044df:	fb                   	sti    
}
801044e0:	c9                   	leave  
801044e1:	c3                   	ret    
    panic("popcli - interruptible");
801044e2:	83 ec 0c             	sub    $0xc,%esp
801044e5:	68 03 7f 10 80       	push   $0x80107f03
801044ea:	e8 a1 be ff ff       	call   80100390 <panic>
    panic("popcli");
801044ef:	83 ec 0c             	sub    $0xc,%esp
801044f2:	68 1a 7f 10 80       	push   $0x80107f1a
801044f7:	e8 94 be ff ff       	call   80100390 <panic>
801044fc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104500 <holding>:
{
80104500:	55                   	push   %ebp
80104501:	89 e5                	mov    %esp,%ebp
80104503:	56                   	push   %esi
80104504:	53                   	push   %ebx
80104505:	8b 75 08             	mov    0x8(%ebp),%esi
80104508:	31 db                	xor    %ebx,%ebx
  pushcli();
8010450a:	e8 41 ff ff ff       	call   80104450 <pushcli>
  r = lock->locked && lock->cpu == mycpu();
8010450f:	8b 06                	mov    (%esi),%eax
80104511:	85 c0                	test   %eax,%eax
80104513:	75 0b                	jne    80104520 <holding+0x20>
  popcli();
80104515:	e8 86 ff ff ff       	call   801044a0 <popcli>
}
8010451a:	89 d8                	mov    %ebx,%eax
8010451c:	5b                   	pop    %ebx
8010451d:	5e                   	pop    %esi
8010451e:	5d                   	pop    %ebp
8010451f:	c3                   	ret    
  r = lock->locked && lock->cpu == mycpu();
80104520:	8b 5e 08             	mov    0x8(%esi),%ebx
80104523:	e8 58 f3 ff ff       	call   80103880 <mycpu>
80104528:	39 c3                	cmp    %eax,%ebx
8010452a:	0f 94 c3             	sete   %bl
  popcli();
8010452d:	e8 6e ff ff ff       	call   801044a0 <popcli>
  r = lock->locked && lock->cpu == mycpu();
80104532:	0f b6 db             	movzbl %bl,%ebx
}
80104535:	89 d8                	mov    %ebx,%eax
80104537:	5b                   	pop    %ebx
80104538:	5e                   	pop    %esi
80104539:	5d                   	pop    %ebp
8010453a:	c3                   	ret    
8010453b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010453f:	90                   	nop

80104540 <acquire>:
{
80104540:	55                   	push   %ebp
80104541:	89 e5                	mov    %esp,%ebp
80104543:	56                   	push   %esi
80104544:	53                   	push   %ebx
  pushcli(); // disable interrupts to avoid deadlock.
80104545:	e8 06 ff ff ff       	call   80104450 <pushcli>
  if(holding(lk))
8010454a:	8b 5d 08             	mov    0x8(%ebp),%ebx
8010454d:	83 ec 0c             	sub    $0xc,%esp
80104550:	53                   	push   %ebx
80104551:	e8 aa ff ff ff       	call   80104500 <holding>
80104556:	83 c4 10             	add    $0x10,%esp
80104559:	85 c0                	test   %eax,%eax
8010455b:	0f 85 83 00 00 00    	jne    801045e4 <acquire+0xa4>
80104561:	89 c6                	mov    %eax,%esi
  asm volatile("lock; xchgl %0, %1" :
80104563:	ba 01 00 00 00       	mov    $0x1,%edx
80104568:	eb 09                	jmp    80104573 <acquire+0x33>
8010456a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80104570:	8b 5d 08             	mov    0x8(%ebp),%ebx
80104573:	89 d0                	mov    %edx,%eax
80104575:	f0 87 03             	lock xchg %eax,(%ebx)
  while(xchg(&lk->locked, 1) != 0)
80104578:	85 c0                	test   %eax,%eax
8010457a:	75 f4                	jne    80104570 <acquire+0x30>
  __sync_synchronize();
8010457c:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  lk->cpu = mycpu();
80104581:	8b 5d 08             	mov    0x8(%ebp),%ebx
80104584:	e8 f7 f2 ff ff       	call   80103880 <mycpu>
80104589:	89 43 08             	mov    %eax,0x8(%ebx)
  ebp = (uint*)v - 2;
8010458c:	89 e8                	mov    %ebp,%eax
8010458e:	66 90                	xchg   %ax,%ax
    if(ebp == 0 || ebp < (uint*)KERNBASE || ebp == (uint*)0xffffffff)
80104590:	8d 90 00 00 00 80    	lea    -0x80000000(%eax),%edx
80104596:	81 fa fe ff ff 7f    	cmp    $0x7ffffffe,%edx
8010459c:	77 22                	ja     801045c0 <acquire+0x80>
    pcs[i] = ebp[1];     // saved %eip
8010459e:	8b 50 04             	mov    0x4(%eax),%edx
801045a1:	89 54 b3 0c          	mov    %edx,0xc(%ebx,%esi,4)
  for(i = 0; i < 10; i++){
801045a5:	83 c6 01             	add    $0x1,%esi
    ebp = (uint*)ebp[0]; // saved %ebp
801045a8:	8b 00                	mov    (%eax),%eax
  for(i = 0; i < 10; i++){
801045aa:	83 fe 0a             	cmp    $0xa,%esi
801045ad:	75 e1                	jne    80104590 <acquire+0x50>
}
801045af:	8d 65 f8             	lea    -0x8(%ebp),%esp
801045b2:	5b                   	pop    %ebx
801045b3:	5e                   	pop    %esi
801045b4:	5d                   	pop    %ebp
801045b5:	c3                   	ret    
801045b6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801045bd:	8d 76 00             	lea    0x0(%esi),%esi
801045c0:	8d 44 b3 0c          	lea    0xc(%ebx,%esi,4),%eax
801045c4:	83 c3 34             	add    $0x34,%ebx
801045c7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801045ce:	66 90                	xchg   %ax,%ax
    pcs[i] = 0;
801045d0:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
801045d6:	83 c0 04             	add    $0x4,%eax
  for(; i < 10; i++)
801045d9:	39 d8                	cmp    %ebx,%eax
801045db:	75 f3                	jne    801045d0 <acquire+0x90>
}
801045dd:	8d 65 f8             	lea    -0x8(%ebp),%esp
801045e0:	5b                   	pop    %ebx
801045e1:	5e                   	pop    %esi
801045e2:	5d                   	pop    %ebp
801045e3:	c3                   	ret    
    panic("acquire");
801045e4:	83 ec 0c             	sub    $0xc,%esp
801045e7:	68 21 7f 10 80       	push   $0x80107f21
801045ec:	e8 9f bd ff ff       	call   80100390 <panic>
801045f1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801045f8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801045ff:	90                   	nop

80104600 <release>:
{
80104600:	55                   	push   %ebp
80104601:	89 e5                	mov    %esp,%ebp
80104603:	53                   	push   %ebx
80104604:	83 ec 10             	sub    $0x10,%esp
80104607:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(!holding(lk))
8010460a:	53                   	push   %ebx
8010460b:	e8 f0 fe ff ff       	call   80104500 <holding>
80104610:	83 c4 10             	add    $0x10,%esp
80104613:	85 c0                	test   %eax,%eax
80104615:	74 22                	je     80104639 <release+0x39>
  lk->pcs[0] = 0;
80104617:	c7 43 0c 00 00 00 00 	movl   $0x0,0xc(%ebx)
  lk->cpu = 0;
8010461e:	c7 43 08 00 00 00 00 	movl   $0x0,0x8(%ebx)
  __sync_synchronize();
80104625:	f0 83 0c 24 00       	lock orl $0x0,(%esp)
  asm volatile("movl $0, %0" : "+m" (lk->locked) : );
8010462a:	c7 03 00 00 00 00    	movl   $0x0,(%ebx)
}
80104630:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104633:	c9                   	leave  
  popcli();
80104634:	e9 67 fe ff ff       	jmp    801044a0 <popcli>
    panic("release");
80104639:	83 ec 0c             	sub    $0xc,%esp
8010463c:	68 29 7f 10 80       	push   $0x80107f29
80104641:	e8 4a bd ff ff       	call   80100390 <panic>
80104646:	66 90                	xchg   %ax,%ax
80104648:	66 90                	xchg   %ax,%ax
8010464a:	66 90                	xchg   %ax,%ax
8010464c:	66 90                	xchg   %ax,%ax
8010464e:	66 90                	xchg   %ax,%ax

80104650 <memset>:
#include "types.h"
#include "x86.h"

void*
memset(void *dst, int c, uint n)
{
80104650:	55                   	push   %ebp
80104651:	89 e5                	mov    %esp,%ebp
80104653:	57                   	push   %edi
80104654:	8b 55 08             	mov    0x8(%ebp),%edx
80104657:	8b 4d 10             	mov    0x10(%ebp),%ecx
8010465a:	53                   	push   %ebx
  if ((int)dst%4 == 0 && n%4 == 0){
8010465b:	89 d0                	mov    %edx,%eax
8010465d:	09 c8                	or     %ecx,%eax
8010465f:	a8 03                	test   $0x3,%al
80104661:	75 2d                	jne    80104690 <memset+0x40>
    c &= 0xFF;
80104663:	0f b6 7d 0c          	movzbl 0xc(%ebp),%edi
    stosl(dst, (c<<24)|(c<<16)|(c<<8)|c, n/4);
80104667:	c1 e9 02             	shr    $0x2,%ecx
8010466a:	89 f8                	mov    %edi,%eax
8010466c:	89 fb                	mov    %edi,%ebx
8010466e:	c1 e0 18             	shl    $0x18,%eax
80104671:	c1 e3 10             	shl    $0x10,%ebx
80104674:	09 d8                	or     %ebx,%eax
80104676:	09 f8                	or     %edi,%eax
80104678:	c1 e7 08             	shl    $0x8,%edi
8010467b:	09 f8                	or     %edi,%eax
  asm volatile("cld; rep stosl" :
8010467d:	89 d7                	mov    %edx,%edi
8010467f:	fc                   	cld    
80104680:	f3 ab                	rep stos %eax,%es:(%edi)
  } else
    stosb(dst, c, n);
  return dst;
}
80104682:	5b                   	pop    %ebx
80104683:	89 d0                	mov    %edx,%eax
80104685:	5f                   	pop    %edi
80104686:	5d                   	pop    %ebp
80104687:	c3                   	ret    
80104688:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010468f:	90                   	nop
  asm volatile("cld; rep stosb" :
80104690:	89 d7                	mov    %edx,%edi
80104692:	8b 45 0c             	mov    0xc(%ebp),%eax
80104695:	fc                   	cld    
80104696:	f3 aa                	rep stos %al,%es:(%edi)
80104698:	5b                   	pop    %ebx
80104699:	89 d0                	mov    %edx,%eax
8010469b:	5f                   	pop    %edi
8010469c:	5d                   	pop    %ebp
8010469d:	c3                   	ret    
8010469e:	66 90                	xchg   %ax,%ax

801046a0 <memcmp>:

int
memcmp(const void *v1, const void *v2, uint n)
{
801046a0:	55                   	push   %ebp
801046a1:	89 e5                	mov    %esp,%ebp
801046a3:	56                   	push   %esi
801046a4:	8b 75 10             	mov    0x10(%ebp),%esi
801046a7:	8b 45 08             	mov    0x8(%ebp),%eax
801046aa:	53                   	push   %ebx
801046ab:	8b 55 0c             	mov    0xc(%ebp),%edx
  const uchar *s1, *s2;

  s1 = v1;
  s2 = v2;
  while(n-- > 0){
801046ae:	85 f6                	test   %esi,%esi
801046b0:	74 22                	je     801046d4 <memcmp+0x34>
    if(*s1 != *s2)
801046b2:	0f b6 08             	movzbl (%eax),%ecx
801046b5:	0f b6 1a             	movzbl (%edx),%ebx
801046b8:	01 c6                	add    %eax,%esi
801046ba:	38 cb                	cmp    %cl,%bl
801046bc:	74 0c                	je     801046ca <memcmp+0x2a>
801046be:	eb 20                	jmp    801046e0 <memcmp+0x40>
801046c0:	0f b6 08             	movzbl (%eax),%ecx
801046c3:	0f b6 1a             	movzbl (%edx),%ebx
801046c6:	38 d9                	cmp    %bl,%cl
801046c8:	75 16                	jne    801046e0 <memcmp+0x40>
      return *s1 - *s2;
    s1++, s2++;
801046ca:	83 c0 01             	add    $0x1,%eax
801046cd:	83 c2 01             	add    $0x1,%edx
  while(n-- > 0){
801046d0:	39 c6                	cmp    %eax,%esi
801046d2:	75 ec                	jne    801046c0 <memcmp+0x20>
  }

  return 0;
}
801046d4:	5b                   	pop    %ebx
  return 0;
801046d5:	31 c0                	xor    %eax,%eax
}
801046d7:	5e                   	pop    %esi
801046d8:	5d                   	pop    %ebp
801046d9:	c3                   	ret    
801046da:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      return *s1 - *s2;
801046e0:	0f b6 c1             	movzbl %cl,%eax
801046e3:	29 d8                	sub    %ebx,%eax
}
801046e5:	5b                   	pop    %ebx
801046e6:	5e                   	pop    %esi
801046e7:	5d                   	pop    %ebp
801046e8:	c3                   	ret    
801046e9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

801046f0 <memmove>:

void*
memmove(void *dst, const void *src, uint n)
{
801046f0:	55                   	push   %ebp
801046f1:	89 e5                	mov    %esp,%ebp
801046f3:	57                   	push   %edi
801046f4:	8b 45 08             	mov    0x8(%ebp),%eax
801046f7:	8b 4d 10             	mov    0x10(%ebp),%ecx
801046fa:	56                   	push   %esi
801046fb:	8b 75 0c             	mov    0xc(%ebp),%esi
  const char *s;
  char *d;

  s = src;
  d = dst;
  if(s < d && s + n > d){
801046fe:	39 c6                	cmp    %eax,%esi
80104700:	73 26                	jae    80104728 <memmove+0x38>
80104702:	8d 3c 0e             	lea    (%esi,%ecx,1),%edi
80104705:	39 f8                	cmp    %edi,%eax
80104707:	73 1f                	jae    80104728 <memmove+0x38>
80104709:	8d 51 ff             	lea    -0x1(%ecx),%edx
    s += n;
    d += n;
    while(n-- > 0)
8010470c:	85 c9                	test   %ecx,%ecx
8010470e:	74 0f                	je     8010471f <memmove+0x2f>
      *--d = *--s;
80104710:	0f b6 0c 16          	movzbl (%esi,%edx,1),%ecx
80104714:	88 0c 10             	mov    %cl,(%eax,%edx,1)
    while(n-- > 0)
80104717:	83 ea 01             	sub    $0x1,%edx
8010471a:	83 fa ff             	cmp    $0xffffffff,%edx
8010471d:	75 f1                	jne    80104710 <memmove+0x20>
  } else
    while(n-- > 0)
      *d++ = *s++;

  return dst;
}
8010471f:	5e                   	pop    %esi
80104720:	5f                   	pop    %edi
80104721:	5d                   	pop    %ebp
80104722:	c3                   	ret    
80104723:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104727:	90                   	nop
80104728:	8d 14 0e             	lea    (%esi,%ecx,1),%edx
    while(n-- > 0)
8010472b:	89 c7                	mov    %eax,%edi
8010472d:	85 c9                	test   %ecx,%ecx
8010472f:	74 ee                	je     8010471f <memmove+0x2f>
80104731:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
      *d++ = *s++;
80104738:	a4                   	movsb  %ds:(%esi),%es:(%edi)
    while(n-- > 0)
80104739:	39 d6                	cmp    %edx,%esi
8010473b:	75 fb                	jne    80104738 <memmove+0x48>
}
8010473d:	5e                   	pop    %esi
8010473e:	5f                   	pop    %edi
8010473f:	5d                   	pop    %ebp
80104740:	c3                   	ret    
80104741:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104748:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010474f:	90                   	nop

80104750 <memcpy>:

// memcpy exists to placate GCC.  Use memmove.
void*
memcpy(void *dst, const void *src, uint n)
{
  return memmove(dst, src, n);
80104750:	eb 9e                	jmp    801046f0 <memmove>
80104752:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104759:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80104760 <strncmp>:
}

int
strncmp(const char *p, const char *q, uint n)
{
80104760:	55                   	push   %ebp
80104761:	89 e5                	mov    %esp,%ebp
80104763:	57                   	push   %edi
80104764:	8b 7d 10             	mov    0x10(%ebp),%edi
80104767:	8b 4d 08             	mov    0x8(%ebp),%ecx
8010476a:	56                   	push   %esi
8010476b:	8b 75 0c             	mov    0xc(%ebp),%esi
8010476e:	53                   	push   %ebx
  while(n > 0 && *p && *p == *q)
8010476f:	85 ff                	test   %edi,%edi
80104771:	74 2f                	je     801047a2 <strncmp+0x42>
80104773:	0f b6 11             	movzbl (%ecx),%edx
80104776:	0f b6 1e             	movzbl (%esi),%ebx
80104779:	84 d2                	test   %dl,%dl
8010477b:	74 37                	je     801047b4 <strncmp+0x54>
8010477d:	38 da                	cmp    %bl,%dl
8010477f:	75 33                	jne    801047b4 <strncmp+0x54>
80104781:	01 f7                	add    %esi,%edi
80104783:	eb 13                	jmp    80104798 <strncmp+0x38>
80104785:	8d 76 00             	lea    0x0(%esi),%esi
80104788:	0f b6 11             	movzbl (%ecx),%edx
8010478b:	84 d2                	test   %dl,%dl
8010478d:	74 21                	je     801047b0 <strncmp+0x50>
8010478f:	0f b6 18             	movzbl (%eax),%ebx
80104792:	89 c6                	mov    %eax,%esi
80104794:	38 da                	cmp    %bl,%dl
80104796:	75 1c                	jne    801047b4 <strncmp+0x54>
    n--, p++, q++;
80104798:	8d 46 01             	lea    0x1(%esi),%eax
8010479b:	83 c1 01             	add    $0x1,%ecx
  while(n > 0 && *p && *p == *q)
8010479e:	39 f8                	cmp    %edi,%eax
801047a0:	75 e6                	jne    80104788 <strncmp+0x28>
  if(n == 0)
    return 0;
  return (uchar)*p - (uchar)*q;
}
801047a2:	5b                   	pop    %ebx
    return 0;
801047a3:	31 c0                	xor    %eax,%eax
}
801047a5:	5e                   	pop    %esi
801047a6:	5f                   	pop    %edi
801047a7:	5d                   	pop    %ebp
801047a8:	c3                   	ret    
801047a9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801047b0:	0f b6 5e 01          	movzbl 0x1(%esi),%ebx
  return (uchar)*p - (uchar)*q;
801047b4:	0f b6 c2             	movzbl %dl,%eax
801047b7:	29 d8                	sub    %ebx,%eax
}
801047b9:	5b                   	pop    %ebx
801047ba:	5e                   	pop    %esi
801047bb:	5f                   	pop    %edi
801047bc:	5d                   	pop    %ebp
801047bd:	c3                   	ret    
801047be:	66 90                	xchg   %ax,%ax

801047c0 <strncpy>:

char*
strncpy(char *s, const char *t, int n)
{
801047c0:	55                   	push   %ebp
801047c1:	89 e5                	mov    %esp,%ebp
801047c3:	57                   	push   %edi
801047c4:	8b 55 0c             	mov    0xc(%ebp),%edx
  char *os;

  os = s;
  while(n-- > 0 && (*s++ = *t++) != 0)
801047c7:	8b 4d 08             	mov    0x8(%ebp),%ecx
{
801047ca:	56                   	push   %esi
801047cb:	53                   	push   %ebx
801047cc:	8b 5d 10             	mov    0x10(%ebp),%ebx
  while(n-- > 0 && (*s++ = *t++) != 0)
801047cf:	eb 1a                	jmp    801047eb <strncpy+0x2b>
801047d1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801047d8:	83 c2 01             	add    $0x1,%edx
801047db:	0f b6 42 ff          	movzbl -0x1(%edx),%eax
801047df:	83 c1 01             	add    $0x1,%ecx
801047e2:	88 41 ff             	mov    %al,-0x1(%ecx)
801047e5:	84 c0                	test   %al,%al
801047e7:	74 09                	je     801047f2 <strncpy+0x32>
801047e9:	89 fb                	mov    %edi,%ebx
801047eb:	8d 7b ff             	lea    -0x1(%ebx),%edi
801047ee:	85 db                	test   %ebx,%ebx
801047f0:	7f e6                	jg     801047d8 <strncpy+0x18>
    ;
  while(n-- > 0)
801047f2:	89 ce                	mov    %ecx,%esi
801047f4:	85 ff                	test   %edi,%edi
801047f6:	7e 1b                	jle    80104813 <strncpy+0x53>
801047f8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801047ff:	90                   	nop
    *s++ = 0;
80104800:	83 c6 01             	add    $0x1,%esi
80104803:	c6 46 ff 00          	movb   $0x0,-0x1(%esi)
80104807:	89 f2                	mov    %esi,%edx
80104809:	f7 d2                	not    %edx
8010480b:	01 ca                	add    %ecx,%edx
8010480d:	01 da                	add    %ebx,%edx
  while(n-- > 0)
8010480f:	85 d2                	test   %edx,%edx
80104811:	7f ed                	jg     80104800 <strncpy+0x40>
  return os;
}
80104813:	5b                   	pop    %ebx
80104814:	8b 45 08             	mov    0x8(%ebp),%eax
80104817:	5e                   	pop    %esi
80104818:	5f                   	pop    %edi
80104819:	5d                   	pop    %ebp
8010481a:	c3                   	ret    
8010481b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010481f:	90                   	nop

80104820 <safestrcpy>:

// Like strncpy but guaranteed to NUL-terminate.
char*
safestrcpy(char *s, const char *t, int n)
{
80104820:	55                   	push   %ebp
80104821:	89 e5                	mov    %esp,%ebp
80104823:	56                   	push   %esi
80104824:	8b 4d 10             	mov    0x10(%ebp),%ecx
80104827:	8b 45 08             	mov    0x8(%ebp),%eax
8010482a:	53                   	push   %ebx
8010482b:	8b 55 0c             	mov    0xc(%ebp),%edx
  char *os;

  os = s;
  if(n <= 0)
8010482e:	85 c9                	test   %ecx,%ecx
80104830:	7e 26                	jle    80104858 <safestrcpy+0x38>
80104832:	8d 74 0a ff          	lea    -0x1(%edx,%ecx,1),%esi
80104836:	89 c1                	mov    %eax,%ecx
80104838:	eb 17                	jmp    80104851 <safestrcpy+0x31>
8010483a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    return os;
  while(--n > 0 && (*s++ = *t++) != 0)
80104840:	83 c2 01             	add    $0x1,%edx
80104843:	0f b6 5a ff          	movzbl -0x1(%edx),%ebx
80104847:	83 c1 01             	add    $0x1,%ecx
8010484a:	88 59 ff             	mov    %bl,-0x1(%ecx)
8010484d:	84 db                	test   %bl,%bl
8010484f:	74 04                	je     80104855 <safestrcpy+0x35>
80104851:	39 f2                	cmp    %esi,%edx
80104853:	75 eb                	jne    80104840 <safestrcpy+0x20>
    ;
  *s = 0;
80104855:	c6 01 00             	movb   $0x0,(%ecx)
  return os;
}
80104858:	5b                   	pop    %ebx
80104859:	5e                   	pop    %esi
8010485a:	5d                   	pop    %ebp
8010485b:	c3                   	ret    
8010485c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104860 <strlen>:

int
strlen(const char *s)
{
80104860:	55                   	push   %ebp
  int n;

  for(n = 0; s[n]; n++)
80104861:	31 c0                	xor    %eax,%eax
{
80104863:	89 e5                	mov    %esp,%ebp
80104865:	8b 55 08             	mov    0x8(%ebp),%edx
  for(n = 0; s[n]; n++)
80104868:	80 3a 00             	cmpb   $0x0,(%edx)
8010486b:	74 0c                	je     80104879 <strlen+0x19>
8010486d:	8d 76 00             	lea    0x0(%esi),%esi
80104870:	83 c0 01             	add    $0x1,%eax
80104873:	80 3c 02 00          	cmpb   $0x0,(%edx,%eax,1)
80104877:	75 f7                	jne    80104870 <strlen+0x10>
    ;
  return n;
}
80104879:	5d                   	pop    %ebp
8010487a:	c3                   	ret    

8010487b <swtch>:
# a struct context, and save its address in *old.
# Switch stacks to new and pop previously-saved registers.

.globl swtch
swtch:
  movl 4(%esp), %eax
8010487b:	8b 44 24 04          	mov    0x4(%esp),%eax
  movl 8(%esp), %edx
8010487f:	8b 54 24 08          	mov    0x8(%esp),%edx

  # Save old callee-saved registers
  pushl %ebp
80104883:	55                   	push   %ebp
  pushl %ebx
80104884:	53                   	push   %ebx
  pushl %esi
80104885:	56                   	push   %esi
  pushl %edi
80104886:	57                   	push   %edi

  # Switch stacks
  movl %esp, (%eax)
80104887:	89 20                	mov    %esp,(%eax)
  movl %edx, %esp
80104889:	89 d4                	mov    %edx,%esp

  # Load new callee-saved registers
  popl %edi
8010488b:	5f                   	pop    %edi
  popl %esi
8010488c:	5e                   	pop    %esi
  popl %ebx
8010488d:	5b                   	pop    %ebx
  popl %ebp
8010488e:	5d                   	pop    %ebp
  ret
8010488f:	c3                   	ret    

80104890 <fetchint>:
// to a saved program counter, and then the first argument.

// Fetch the int at addr from the current process.
int
fetchint(uint addr, int *ip)
{
80104890:	55                   	push   %ebp
80104891:	89 e5                	mov    %esp,%ebp
80104893:	53                   	push   %ebx
80104894:	83 ec 04             	sub    $0x4,%esp
80104897:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc *curproc = myproc();
8010489a:	e8 81 f0 ff ff       	call   80103920 <myproc>

  if(addr >= curproc->sz || addr+4 > curproc->sz)
8010489f:	8b 00                	mov    (%eax),%eax
801048a1:	39 d8                	cmp    %ebx,%eax
801048a3:	76 1b                	jbe    801048c0 <fetchint+0x30>
801048a5:	8d 53 04             	lea    0x4(%ebx),%edx
801048a8:	39 d0                	cmp    %edx,%eax
801048aa:	72 14                	jb     801048c0 <fetchint+0x30>
    return -1;
  *ip = *(int*)(addr);
801048ac:	8b 45 0c             	mov    0xc(%ebp),%eax
801048af:	8b 13                	mov    (%ebx),%edx
801048b1:	89 10                	mov    %edx,(%eax)
  return 0;
801048b3:	31 c0                	xor    %eax,%eax
}
801048b5:	83 c4 04             	add    $0x4,%esp
801048b8:	5b                   	pop    %ebx
801048b9:	5d                   	pop    %ebp
801048ba:	c3                   	ret    
801048bb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801048bf:	90                   	nop
    return -1;
801048c0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801048c5:	eb ee                	jmp    801048b5 <fetchint+0x25>
801048c7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801048ce:	66 90                	xchg   %ax,%ax

801048d0 <fetchstr>:
// Fetch the nul-terminated string at addr from the current process.
// Doesn't actually copy the string - just sets *pp to point at it.
// Returns length of string, not including nul.
int
fetchstr(uint addr, char **pp)
{
801048d0:	55                   	push   %ebp
801048d1:	89 e5                	mov    %esp,%ebp
801048d3:	53                   	push   %ebx
801048d4:	83 ec 04             	sub    $0x4,%esp
801048d7:	8b 5d 08             	mov    0x8(%ebp),%ebx
  char *s, *ep;
  struct proc *curproc = myproc();
801048da:	e8 41 f0 ff ff       	call   80103920 <myproc>

  if(addr >= curproc->sz)
801048df:	39 18                	cmp    %ebx,(%eax)
801048e1:	76 29                	jbe    8010490c <fetchstr+0x3c>
    return -1;
  *pp = (char*)addr;
801048e3:	8b 55 0c             	mov    0xc(%ebp),%edx
801048e6:	89 1a                	mov    %ebx,(%edx)
  ep = (char*)curproc->sz;
801048e8:	8b 10                	mov    (%eax),%edx
  for(s = *pp; s < ep; s++){
801048ea:	39 d3                	cmp    %edx,%ebx
801048ec:	73 1e                	jae    8010490c <fetchstr+0x3c>
    if(*s == 0)
801048ee:	80 3b 00             	cmpb   $0x0,(%ebx)
801048f1:	74 35                	je     80104928 <fetchstr+0x58>
801048f3:	89 d8                	mov    %ebx,%eax
801048f5:	eb 0e                	jmp    80104905 <fetchstr+0x35>
801048f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801048fe:	66 90                	xchg   %ax,%ax
80104900:	80 38 00             	cmpb   $0x0,(%eax)
80104903:	74 1b                	je     80104920 <fetchstr+0x50>
  for(s = *pp; s < ep; s++){
80104905:	83 c0 01             	add    $0x1,%eax
80104908:	39 c2                	cmp    %eax,%edx
8010490a:	77 f4                	ja     80104900 <fetchstr+0x30>
    return -1;
8010490c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
      return s - *pp;
  }
  return -1;
}
80104911:	83 c4 04             	add    $0x4,%esp
80104914:	5b                   	pop    %ebx
80104915:	5d                   	pop    %ebp
80104916:	c3                   	ret    
80104917:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010491e:	66 90                	xchg   %ax,%ax
80104920:	83 c4 04             	add    $0x4,%esp
80104923:	29 d8                	sub    %ebx,%eax
80104925:	5b                   	pop    %ebx
80104926:	5d                   	pop    %ebp
80104927:	c3                   	ret    
    if(*s == 0)
80104928:	31 c0                	xor    %eax,%eax
      return s - *pp;
8010492a:	eb e5                	jmp    80104911 <fetchstr+0x41>
8010492c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104930 <argint>:

// Fetch the nth 32-bit system call argument.
int
argint(int n, int *ip)
{
80104930:	55                   	push   %ebp
80104931:	89 e5                	mov    %esp,%ebp
80104933:	56                   	push   %esi
80104934:	53                   	push   %ebx
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80104935:	e8 e6 ef ff ff       	call   80103920 <myproc>
8010493a:	8b 55 08             	mov    0x8(%ebp),%edx
8010493d:	8b 40 18             	mov    0x18(%eax),%eax
80104940:	8b 40 44             	mov    0x44(%eax),%eax
80104943:	8d 1c 90             	lea    (%eax,%edx,4),%ebx
  struct proc *curproc = myproc();
80104946:	e8 d5 ef ff ff       	call   80103920 <myproc>
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
8010494b:	8d 73 04             	lea    0x4(%ebx),%esi
  if(addr >= curproc->sz || addr+4 > curproc->sz)
8010494e:	8b 00                	mov    (%eax),%eax
80104950:	39 c6                	cmp    %eax,%esi
80104952:	73 1c                	jae    80104970 <argint+0x40>
80104954:	8d 53 08             	lea    0x8(%ebx),%edx
80104957:	39 d0                	cmp    %edx,%eax
80104959:	72 15                	jb     80104970 <argint+0x40>
  *ip = *(int*)(addr);
8010495b:	8b 45 0c             	mov    0xc(%ebp),%eax
8010495e:	8b 53 04             	mov    0x4(%ebx),%edx
80104961:	89 10                	mov    %edx,(%eax)
  return 0;
80104963:	31 c0                	xor    %eax,%eax
}
80104965:	5b                   	pop    %ebx
80104966:	5e                   	pop    %esi
80104967:	5d                   	pop    %ebp
80104968:	c3                   	ret    
80104969:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
80104970:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  return fetchint((myproc()->tf->esp) + 4 + 4*n, ip);
80104975:	eb ee                	jmp    80104965 <argint+0x35>
80104977:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010497e:	66 90                	xchg   %ax,%ax

80104980 <argptr>:
// Fetch the nth word-sized system call argument as a pointer
// to a block of memory of size bytes.  Check that the pointer
// lies within the process address space.
int
argptr(int n, char **pp, int size)
{
80104980:	55                   	push   %ebp
80104981:	89 e5                	mov    %esp,%ebp
80104983:	56                   	push   %esi
80104984:	53                   	push   %ebx
80104985:	83 ec 10             	sub    $0x10,%esp
80104988:	8b 5d 10             	mov    0x10(%ebp),%ebx
  int i;
  struct proc *curproc = myproc();
8010498b:	e8 90 ef ff ff       	call   80103920 <myproc>
 
  if(argint(n, &i) < 0)
80104990:	83 ec 08             	sub    $0x8,%esp
  struct proc *curproc = myproc();
80104993:	89 c6                	mov    %eax,%esi
  if(argint(n, &i) < 0)
80104995:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104998:	50                   	push   %eax
80104999:	ff 75 08             	pushl  0x8(%ebp)
8010499c:	e8 8f ff ff ff       	call   80104930 <argint>
    return -1;
  if(size < 0 || (uint)i >= curproc->sz || (uint)i+size > curproc->sz)
801049a1:	83 c4 10             	add    $0x10,%esp
801049a4:	85 c0                	test   %eax,%eax
801049a6:	78 28                	js     801049d0 <argptr+0x50>
801049a8:	85 db                	test   %ebx,%ebx
801049aa:	78 24                	js     801049d0 <argptr+0x50>
801049ac:	8b 16                	mov    (%esi),%edx
801049ae:	8b 45 f4             	mov    -0xc(%ebp),%eax
801049b1:	39 c2                	cmp    %eax,%edx
801049b3:	76 1b                	jbe    801049d0 <argptr+0x50>
801049b5:	01 c3                	add    %eax,%ebx
801049b7:	39 da                	cmp    %ebx,%edx
801049b9:	72 15                	jb     801049d0 <argptr+0x50>
    return -1;
  *pp = (char*)i;
801049bb:	8b 55 0c             	mov    0xc(%ebp),%edx
801049be:	89 02                	mov    %eax,(%edx)
  return 0;
801049c0:	31 c0                	xor    %eax,%eax
}
801049c2:	8d 65 f8             	lea    -0x8(%ebp),%esp
801049c5:	5b                   	pop    %ebx
801049c6:	5e                   	pop    %esi
801049c7:	5d                   	pop    %ebp
801049c8:	c3                   	ret    
801049c9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
801049d0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801049d5:	eb eb                	jmp    801049c2 <argptr+0x42>
801049d7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801049de:	66 90                	xchg   %ax,%ax

801049e0 <argstr>:
// Check that the pointer is valid and the string is nul-terminated.
// (There is no shared writable memory, so the string can't change
// between this check and being used by the kernel.)
int
argstr(int n, char **pp)
{
801049e0:	55                   	push   %ebp
801049e1:	89 e5                	mov    %esp,%ebp
801049e3:	83 ec 20             	sub    $0x20,%esp
  int addr;
  if(argint(n, &addr) < 0)
801049e6:	8d 45 f4             	lea    -0xc(%ebp),%eax
801049e9:	50                   	push   %eax
801049ea:	ff 75 08             	pushl  0x8(%ebp)
801049ed:	e8 3e ff ff ff       	call   80104930 <argint>
801049f2:	83 c4 10             	add    $0x10,%esp
801049f5:	85 c0                	test   %eax,%eax
801049f7:	78 17                	js     80104a10 <argstr+0x30>
    return -1;
  return fetchstr(addr, pp);
801049f9:	83 ec 08             	sub    $0x8,%esp
801049fc:	ff 75 0c             	pushl  0xc(%ebp)
801049ff:	ff 75 f4             	pushl  -0xc(%ebp)
80104a02:	e8 c9 fe ff ff       	call   801048d0 <fetchstr>
80104a07:	83 c4 10             	add    $0x10,%esp
}
80104a0a:	c9                   	leave  
80104a0b:	c3                   	ret    
80104a0c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104a10:	c9                   	leave  
    return -1;
80104a11:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104a16:	c3                   	ret    
80104a17:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104a1e:	66 90                	xchg   %ax,%ax

80104a20 <syscall>:
[SYS_semup]   sys_semup,
};

void
syscall(void)
{
80104a20:	55                   	push   %ebp
80104a21:	89 e5                	mov    %esp,%ebp
80104a23:	53                   	push   %ebx
80104a24:	83 ec 04             	sub    $0x4,%esp
  int num;
  struct proc *curproc = myproc();
80104a27:	e8 f4 ee ff ff       	call   80103920 <myproc>
80104a2c:	89 c3                	mov    %eax,%ebx

  num = curproc->tf->eax;
80104a2e:	8b 40 18             	mov    0x18(%eax),%eax
80104a31:	8b 40 1c             	mov    0x1c(%eax),%eax
  if(num > 0 && num < NELEM(syscalls) && syscalls[num]) {
80104a34:	8d 50 ff             	lea    -0x1(%eax),%edx
80104a37:	83 fa 19             	cmp    $0x19,%edx
80104a3a:	77 1c                	ja     80104a58 <syscall+0x38>
80104a3c:	8b 14 85 60 7f 10 80 	mov    -0x7fef80a0(,%eax,4),%edx
80104a43:	85 d2                	test   %edx,%edx
80104a45:	74 11                	je     80104a58 <syscall+0x38>
    curproc->tf->eax = syscalls[num]();
80104a47:	ff d2                	call   *%edx
80104a49:	8b 53 18             	mov    0x18(%ebx),%edx
80104a4c:	89 42 1c             	mov    %eax,0x1c(%edx)
  } else {
    cprintf("%d %s: unknown sys call %d\n",
            curproc->pid, curproc->name, num);
    curproc->tf->eax = -1;
  }
}
80104a4f:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104a52:	c9                   	leave  
80104a53:	c3                   	ret    
80104a54:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cprintf("%d %s: unknown sys call %d\n",
80104a58:	50                   	push   %eax
            curproc->pid, curproc->name, num);
80104a59:	8d 83 ac 00 00 00    	lea    0xac(%ebx),%eax
    cprintf("%d %s: unknown sys call %d\n",
80104a5f:	50                   	push   %eax
80104a60:	ff 73 10             	pushl  0x10(%ebx)
80104a63:	68 31 7f 10 80       	push   $0x80107f31
80104a68:	e8 43 bc ff ff       	call   801006b0 <cprintf>
    curproc->tf->eax = -1;
80104a6d:	8b 43 18             	mov    0x18(%ebx),%eax
80104a70:	83 c4 10             	add    $0x10,%esp
80104a73:	c7 40 1c ff ff ff ff 	movl   $0xffffffff,0x1c(%eax)
}
80104a7a:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80104a7d:	c9                   	leave  
80104a7e:	c3                   	ret    
80104a7f:	90                   	nop

80104a80 <create>:
  return -1;
}

static struct inode*
create(char *path, short type, short major, short minor)
{
80104a80:	55                   	push   %ebp
80104a81:	89 e5                	mov    %esp,%ebp
80104a83:	57                   	push   %edi
80104a84:	56                   	push   %esi
80104a85:	53                   	push   %ebx
  uint off;
  struct inode *ip, *dp;
  char name[DIRSIZ];

  if((dp = nameiparent(path, name)) == 0)
80104a86:	8d 5d da             	lea    -0x26(%ebp),%ebx
{
80104a89:	83 ec 44             	sub    $0x44,%esp
80104a8c:	89 4d c0             	mov    %ecx,-0x40(%ebp)
80104a8f:	8b 4d 08             	mov    0x8(%ebp),%ecx
  if((dp = nameiparent(path, name)) == 0)
80104a92:	53                   	push   %ebx
80104a93:	50                   	push   %eax
{
80104a94:	89 55 c4             	mov    %edx,-0x3c(%ebp)
80104a97:	89 4d bc             	mov    %ecx,-0x44(%ebp)
  if((dp = nameiparent(path, name)) == 0)
80104a9a:	e8 71 d5 ff ff       	call   80102010 <nameiparent>
80104a9f:	83 c4 10             	add    $0x10,%esp
80104aa2:	85 c0                	test   %eax,%eax
80104aa4:	0f 84 46 01 00 00    	je     80104bf0 <create+0x170>
    return 0;
  ilock(dp);
80104aaa:	83 ec 0c             	sub    $0xc,%esp
80104aad:	89 c6                	mov    %eax,%esi
80104aaf:	50                   	push   %eax
80104ab0:	e8 9b cc ff ff       	call   80101750 <ilock>

  if((ip = dirlookup(dp, name, &off)) != 0){
80104ab5:	83 c4 0c             	add    $0xc,%esp
80104ab8:	8d 45 d4             	lea    -0x2c(%ebp),%eax
80104abb:	50                   	push   %eax
80104abc:	53                   	push   %ebx
80104abd:	56                   	push   %esi
80104abe:	e8 bd d1 ff ff       	call   80101c80 <dirlookup>
80104ac3:	83 c4 10             	add    $0x10,%esp
80104ac6:	89 c7                	mov    %eax,%edi
80104ac8:	85 c0                	test   %eax,%eax
80104aca:	74 54                	je     80104b20 <create+0xa0>
    iunlockput(dp);
80104acc:	83 ec 0c             	sub    $0xc,%esp
80104acf:	56                   	push   %esi
80104ad0:	e8 0b cf ff ff       	call   801019e0 <iunlockput>
    ilock(ip);
80104ad5:	89 3c 24             	mov    %edi,(%esp)
80104ad8:	e8 73 cc ff ff       	call   80101750 <ilock>
    if(type == T_FILE && ip->type == T_FILE)
80104add:	83 c4 10             	add    $0x10,%esp
80104ae0:	66 83 7d c4 02       	cmpw   $0x2,-0x3c(%ebp)
80104ae5:	75 19                	jne    80104b00 <create+0x80>
80104ae7:	66 83 7f 50 02       	cmpw   $0x2,0x50(%edi)
80104aec:	75 12                	jne    80104b00 <create+0x80>
    panic("create: dirlink");

  iunlockput(dp);

  return ip;
}
80104aee:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104af1:	89 f8                	mov    %edi,%eax
80104af3:	5b                   	pop    %ebx
80104af4:	5e                   	pop    %esi
80104af5:	5f                   	pop    %edi
80104af6:	5d                   	pop    %ebp
80104af7:	c3                   	ret    
80104af8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104aff:	90                   	nop
    iunlockput(ip);
80104b00:	83 ec 0c             	sub    $0xc,%esp
80104b03:	57                   	push   %edi
    return 0;
80104b04:	31 ff                	xor    %edi,%edi
    iunlockput(ip);
80104b06:	e8 d5 ce ff ff       	call   801019e0 <iunlockput>
    return 0;
80104b0b:	83 c4 10             	add    $0x10,%esp
}
80104b0e:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104b11:	89 f8                	mov    %edi,%eax
80104b13:	5b                   	pop    %ebx
80104b14:	5e                   	pop    %esi
80104b15:	5f                   	pop    %edi
80104b16:	5d                   	pop    %ebp
80104b17:	c3                   	ret    
80104b18:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104b1f:	90                   	nop
  if((ip = ialloc(dp->dev, type)) == 0)
80104b20:	0f bf 45 c4          	movswl -0x3c(%ebp),%eax
80104b24:	83 ec 08             	sub    $0x8,%esp
80104b27:	50                   	push   %eax
80104b28:	ff 36                	pushl  (%esi)
80104b2a:	e8 b1 ca ff ff       	call   801015e0 <ialloc>
80104b2f:	83 c4 10             	add    $0x10,%esp
80104b32:	89 c7                	mov    %eax,%edi
80104b34:	85 c0                	test   %eax,%eax
80104b36:	0f 84 cd 00 00 00    	je     80104c09 <create+0x189>
  ilock(ip);
80104b3c:	83 ec 0c             	sub    $0xc,%esp
80104b3f:	50                   	push   %eax
80104b40:	e8 0b cc ff ff       	call   80101750 <ilock>
  ip->major = major;
80104b45:	0f b7 45 c0          	movzwl -0x40(%ebp),%eax
80104b49:	66 89 47 52          	mov    %ax,0x52(%edi)
  ip->minor = minor;
80104b4d:	0f b7 45 bc          	movzwl -0x44(%ebp),%eax
80104b51:	66 89 47 54          	mov    %ax,0x54(%edi)
  ip->nlink = 1;
80104b55:	b8 01 00 00 00       	mov    $0x1,%eax
80104b5a:	66 89 47 56          	mov    %ax,0x56(%edi)
  iupdate(ip);
80104b5e:	89 3c 24             	mov    %edi,(%esp)
80104b61:	e8 3a cb ff ff       	call   801016a0 <iupdate>
  if(type == T_DIR){  // Create . and .. entries.
80104b66:	83 c4 10             	add    $0x10,%esp
80104b69:	66 83 7d c4 01       	cmpw   $0x1,-0x3c(%ebp)
80104b6e:	74 30                	je     80104ba0 <create+0x120>
  if(dirlink(dp, name, ip->inum) < 0)
80104b70:	83 ec 04             	sub    $0x4,%esp
80104b73:	ff 77 04             	pushl  0x4(%edi)
80104b76:	53                   	push   %ebx
80104b77:	56                   	push   %esi
80104b78:	e8 b3 d3 ff ff       	call   80101f30 <dirlink>
80104b7d:	83 c4 10             	add    $0x10,%esp
80104b80:	85 c0                	test   %eax,%eax
80104b82:	78 78                	js     80104bfc <create+0x17c>
  iunlockput(dp);
80104b84:	83 ec 0c             	sub    $0xc,%esp
80104b87:	56                   	push   %esi
80104b88:	e8 53 ce ff ff       	call   801019e0 <iunlockput>
  return ip;
80104b8d:	83 c4 10             	add    $0x10,%esp
}
80104b90:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104b93:	89 f8                	mov    %edi,%eax
80104b95:	5b                   	pop    %ebx
80104b96:	5e                   	pop    %esi
80104b97:	5f                   	pop    %edi
80104b98:	5d                   	pop    %ebp
80104b99:	c3                   	ret    
80104b9a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    iupdate(dp);
80104ba0:	83 ec 0c             	sub    $0xc,%esp
    dp->nlink++;  // for ".."
80104ba3:	66 83 46 56 01       	addw   $0x1,0x56(%esi)
    iupdate(dp);
80104ba8:	56                   	push   %esi
80104ba9:	e8 f2 ca ff ff       	call   801016a0 <iupdate>
    if(dirlink(ip, ".", ip->inum) < 0 || dirlink(ip, "..", dp->inum) < 0)
80104bae:	83 c4 0c             	add    $0xc,%esp
80104bb1:	ff 77 04             	pushl  0x4(%edi)
80104bb4:	68 e8 7f 10 80       	push   $0x80107fe8
80104bb9:	57                   	push   %edi
80104bba:	e8 71 d3 ff ff       	call   80101f30 <dirlink>
80104bbf:	83 c4 10             	add    $0x10,%esp
80104bc2:	85 c0                	test   %eax,%eax
80104bc4:	78 18                	js     80104bde <create+0x15e>
80104bc6:	83 ec 04             	sub    $0x4,%esp
80104bc9:	ff 76 04             	pushl  0x4(%esi)
80104bcc:	68 e7 7f 10 80       	push   $0x80107fe7
80104bd1:	57                   	push   %edi
80104bd2:	e8 59 d3 ff ff       	call   80101f30 <dirlink>
80104bd7:	83 c4 10             	add    $0x10,%esp
80104bda:	85 c0                	test   %eax,%eax
80104bdc:	79 92                	jns    80104b70 <create+0xf0>
      panic("create dots");
80104bde:	83 ec 0c             	sub    $0xc,%esp
80104be1:	68 db 7f 10 80       	push   $0x80107fdb
80104be6:	e8 a5 b7 ff ff       	call   80100390 <panic>
80104beb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104bef:	90                   	nop
}
80104bf0:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return 0;
80104bf3:	31 ff                	xor    %edi,%edi
}
80104bf5:	5b                   	pop    %ebx
80104bf6:	89 f8                	mov    %edi,%eax
80104bf8:	5e                   	pop    %esi
80104bf9:	5f                   	pop    %edi
80104bfa:	5d                   	pop    %ebp
80104bfb:	c3                   	ret    
    panic("create: dirlink");
80104bfc:	83 ec 0c             	sub    $0xc,%esp
80104bff:	68 ea 7f 10 80       	push   $0x80107fea
80104c04:	e8 87 b7 ff ff       	call   80100390 <panic>
    panic("create: ialloc");
80104c09:	83 ec 0c             	sub    $0xc,%esp
80104c0c:	68 cc 7f 10 80       	push   $0x80107fcc
80104c11:	e8 7a b7 ff ff       	call   80100390 <panic>
80104c16:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104c1d:	8d 76 00             	lea    0x0(%esi),%esi

80104c20 <argfd.constprop.0>:
argfd(int n, int *pfd, struct file **pf)
80104c20:	55                   	push   %ebp
80104c21:	89 e5                	mov    %esp,%ebp
80104c23:	56                   	push   %esi
80104c24:	89 d6                	mov    %edx,%esi
80104c26:	53                   	push   %ebx
80104c27:	89 c3                	mov    %eax,%ebx
  if(argint(n, &fd) < 0)
80104c29:	8d 45 f4             	lea    -0xc(%ebp),%eax
argfd(int n, int *pfd, struct file **pf)
80104c2c:	83 ec 18             	sub    $0x18,%esp
  if(argint(n, &fd) < 0)
80104c2f:	50                   	push   %eax
80104c30:	6a 00                	push   $0x0
80104c32:	e8 f9 fc ff ff       	call   80104930 <argint>
80104c37:	83 c4 10             	add    $0x10,%esp
80104c3a:	85 c0                	test   %eax,%eax
80104c3c:	78 2a                	js     80104c68 <argfd.constprop.0+0x48>
  if(fd < 0 || fd >= NOFILE || (f=myproc()->ofile[fd]) == 0)
80104c3e:	83 7d f4 0f          	cmpl   $0xf,-0xc(%ebp)
80104c42:	77 24                	ja     80104c68 <argfd.constprop.0+0x48>
80104c44:	e8 d7 ec ff ff       	call   80103920 <myproc>
80104c49:	8b 55 f4             	mov    -0xc(%ebp),%edx
80104c4c:	8b 44 90 28          	mov    0x28(%eax,%edx,4),%eax
80104c50:	85 c0                	test   %eax,%eax
80104c52:	74 14                	je     80104c68 <argfd.constprop.0+0x48>
  if(pfd)
80104c54:	85 db                	test   %ebx,%ebx
80104c56:	74 02                	je     80104c5a <argfd.constprop.0+0x3a>
    *pfd = fd;
80104c58:	89 13                	mov    %edx,(%ebx)
    *pf = f;
80104c5a:	89 06                	mov    %eax,(%esi)
  return 0;
80104c5c:	31 c0                	xor    %eax,%eax
}
80104c5e:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104c61:	5b                   	pop    %ebx
80104c62:	5e                   	pop    %esi
80104c63:	5d                   	pop    %ebp
80104c64:	c3                   	ret    
80104c65:	8d 76 00             	lea    0x0(%esi),%esi
    return -1;
80104c68:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104c6d:	eb ef                	jmp    80104c5e <argfd.constprop.0+0x3e>
80104c6f:	90                   	nop

80104c70 <sys_dup>:
{
80104c70:	55                   	push   %ebp
  if(argfd(0, 0, &f) < 0)
80104c71:	31 c0                	xor    %eax,%eax
{
80104c73:	89 e5                	mov    %esp,%ebp
80104c75:	56                   	push   %esi
80104c76:	53                   	push   %ebx
  if(argfd(0, 0, &f) < 0)
80104c77:	8d 55 f4             	lea    -0xc(%ebp),%edx
{
80104c7a:	83 ec 10             	sub    $0x10,%esp
  if(argfd(0, 0, &f) < 0)
80104c7d:	e8 9e ff ff ff       	call   80104c20 <argfd.constprop.0>
80104c82:	85 c0                	test   %eax,%eax
80104c84:	78 1a                	js     80104ca0 <sys_dup+0x30>
  if((fd=fdalloc(f)) < 0)
80104c86:	8b 75 f4             	mov    -0xc(%ebp),%esi
  for(fd = 0; fd < NOFILE; fd++){
80104c89:	31 db                	xor    %ebx,%ebx
  struct proc *curproc = myproc();
80104c8b:	e8 90 ec ff ff       	call   80103920 <myproc>
    if(curproc->ofile[fd] == 0){
80104c90:	8b 54 98 28          	mov    0x28(%eax,%ebx,4),%edx
80104c94:	85 d2                	test   %edx,%edx
80104c96:	74 18                	je     80104cb0 <sys_dup+0x40>
  for(fd = 0; fd < NOFILE; fd++){
80104c98:	83 c3 01             	add    $0x1,%ebx
80104c9b:	83 fb 10             	cmp    $0x10,%ebx
80104c9e:	75 f0                	jne    80104c90 <sys_dup+0x20>
}
80104ca0:	8d 65 f8             	lea    -0x8(%ebp),%esp
    return -1;
80104ca3:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
}
80104ca8:	89 d8                	mov    %ebx,%eax
80104caa:	5b                   	pop    %ebx
80104cab:	5e                   	pop    %esi
80104cac:	5d                   	pop    %ebp
80104cad:	c3                   	ret    
80104cae:	66 90                	xchg   %ax,%ax
      curproc->ofile[fd] = f;
80104cb0:	89 74 98 28          	mov    %esi,0x28(%eax,%ebx,4)
  filedup(f);
80104cb4:	83 ec 0c             	sub    $0xc,%esp
80104cb7:	ff 75 f4             	pushl  -0xc(%ebp)
80104cba:	e8 e1 c1 ff ff       	call   80100ea0 <filedup>
  return fd;
80104cbf:	83 c4 10             	add    $0x10,%esp
}
80104cc2:	8d 65 f8             	lea    -0x8(%ebp),%esp
80104cc5:	89 d8                	mov    %ebx,%eax
80104cc7:	5b                   	pop    %ebx
80104cc8:	5e                   	pop    %esi
80104cc9:	5d                   	pop    %ebp
80104cca:	c3                   	ret    
80104ccb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80104ccf:	90                   	nop

80104cd0 <sys_read>:
{
80104cd0:	55                   	push   %ebp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104cd1:	31 c0                	xor    %eax,%eax
{
80104cd3:	89 e5                	mov    %esp,%ebp
80104cd5:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104cd8:	8d 55 ec             	lea    -0x14(%ebp),%edx
80104cdb:	e8 40 ff ff ff       	call   80104c20 <argfd.constprop.0>
80104ce0:	85 c0                	test   %eax,%eax
80104ce2:	78 4c                	js     80104d30 <sys_read+0x60>
80104ce4:	83 ec 08             	sub    $0x8,%esp
80104ce7:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104cea:	50                   	push   %eax
80104ceb:	6a 02                	push   $0x2
80104ced:	e8 3e fc ff ff       	call   80104930 <argint>
80104cf2:	83 c4 10             	add    $0x10,%esp
80104cf5:	85 c0                	test   %eax,%eax
80104cf7:	78 37                	js     80104d30 <sys_read+0x60>
80104cf9:	83 ec 04             	sub    $0x4,%esp
80104cfc:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104cff:	ff 75 f0             	pushl  -0x10(%ebp)
80104d02:	50                   	push   %eax
80104d03:	6a 01                	push   $0x1
80104d05:	e8 76 fc ff ff       	call   80104980 <argptr>
80104d0a:	83 c4 10             	add    $0x10,%esp
80104d0d:	85 c0                	test   %eax,%eax
80104d0f:	78 1f                	js     80104d30 <sys_read+0x60>
  return fileread(f, p, n);
80104d11:	83 ec 04             	sub    $0x4,%esp
80104d14:	ff 75 f0             	pushl  -0x10(%ebp)
80104d17:	ff 75 f4             	pushl  -0xc(%ebp)
80104d1a:	ff 75 ec             	pushl  -0x14(%ebp)
80104d1d:	e8 fe c2 ff ff       	call   80101020 <fileread>
80104d22:	83 c4 10             	add    $0x10,%esp
}
80104d25:	c9                   	leave  
80104d26:	c3                   	ret    
80104d27:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104d2e:	66 90                	xchg   %ax,%ax
80104d30:	c9                   	leave  
    return -1;
80104d31:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104d36:	c3                   	ret    
80104d37:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104d3e:	66 90                	xchg   %ax,%ax

80104d40 <sys_write>:
{
80104d40:	55                   	push   %ebp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104d41:	31 c0                	xor    %eax,%eax
{
80104d43:	89 e5                	mov    %esp,%ebp
80104d45:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argint(2, &n) < 0 || argptr(1, &p, n) < 0)
80104d48:	8d 55 ec             	lea    -0x14(%ebp),%edx
80104d4b:	e8 d0 fe ff ff       	call   80104c20 <argfd.constprop.0>
80104d50:	85 c0                	test   %eax,%eax
80104d52:	78 4c                	js     80104da0 <sys_write+0x60>
80104d54:	83 ec 08             	sub    $0x8,%esp
80104d57:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104d5a:	50                   	push   %eax
80104d5b:	6a 02                	push   $0x2
80104d5d:	e8 ce fb ff ff       	call   80104930 <argint>
80104d62:	83 c4 10             	add    $0x10,%esp
80104d65:	85 c0                	test   %eax,%eax
80104d67:	78 37                	js     80104da0 <sys_write+0x60>
80104d69:	83 ec 04             	sub    $0x4,%esp
80104d6c:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104d6f:	ff 75 f0             	pushl  -0x10(%ebp)
80104d72:	50                   	push   %eax
80104d73:	6a 01                	push   $0x1
80104d75:	e8 06 fc ff ff       	call   80104980 <argptr>
80104d7a:	83 c4 10             	add    $0x10,%esp
80104d7d:	85 c0                	test   %eax,%eax
80104d7f:	78 1f                	js     80104da0 <sys_write+0x60>
  return filewrite(f, p, n);
80104d81:	83 ec 04             	sub    $0x4,%esp
80104d84:	ff 75 f0             	pushl  -0x10(%ebp)
80104d87:	ff 75 f4             	pushl  -0xc(%ebp)
80104d8a:	ff 75 ec             	pushl  -0x14(%ebp)
80104d8d:	e8 1e c3 ff ff       	call   801010b0 <filewrite>
80104d92:	83 c4 10             	add    $0x10,%esp
}
80104d95:	c9                   	leave  
80104d96:	c3                   	ret    
80104d97:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104d9e:	66 90                	xchg   %ax,%ax
80104da0:	c9                   	leave  
    return -1;
80104da1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104da6:	c3                   	ret    
80104da7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104dae:	66 90                	xchg   %ax,%ax

80104db0 <sys_close>:
{
80104db0:	55                   	push   %ebp
80104db1:	89 e5                	mov    %esp,%ebp
80104db3:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, &fd, &f) < 0)
80104db6:	8d 55 f4             	lea    -0xc(%ebp),%edx
80104db9:	8d 45 f0             	lea    -0x10(%ebp),%eax
80104dbc:	e8 5f fe ff ff       	call   80104c20 <argfd.constprop.0>
80104dc1:	85 c0                	test   %eax,%eax
80104dc3:	78 2b                	js     80104df0 <sys_close+0x40>
  myproc()->ofile[fd] = 0;
80104dc5:	e8 56 eb ff ff       	call   80103920 <myproc>
80104dca:	8b 55 f0             	mov    -0x10(%ebp),%edx
  fileclose(f);
80104dcd:	83 ec 0c             	sub    $0xc,%esp
  myproc()->ofile[fd] = 0;
80104dd0:	c7 44 90 28 00 00 00 	movl   $0x0,0x28(%eax,%edx,4)
80104dd7:	00 
  fileclose(f);
80104dd8:	ff 75 f4             	pushl  -0xc(%ebp)
80104ddb:	e8 10 c1 ff ff       	call   80100ef0 <fileclose>
  return 0;
80104de0:	83 c4 10             	add    $0x10,%esp
80104de3:	31 c0                	xor    %eax,%eax
}
80104de5:	c9                   	leave  
80104de6:	c3                   	ret    
80104de7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104dee:	66 90                	xchg   %ax,%ax
80104df0:	c9                   	leave  
    return -1;
80104df1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104df6:	c3                   	ret    
80104df7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104dfe:	66 90                	xchg   %ax,%ax

80104e00 <sys_fstat>:
{
80104e00:	55                   	push   %ebp
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80104e01:	31 c0                	xor    %eax,%eax
{
80104e03:	89 e5                	mov    %esp,%ebp
80104e05:	83 ec 18             	sub    $0x18,%esp
  if(argfd(0, 0, &f) < 0 || argptr(1, (void*)&st, sizeof(*st)) < 0)
80104e08:	8d 55 f0             	lea    -0x10(%ebp),%edx
80104e0b:	e8 10 fe ff ff       	call   80104c20 <argfd.constprop.0>
80104e10:	85 c0                	test   %eax,%eax
80104e12:	78 2c                	js     80104e40 <sys_fstat+0x40>
80104e14:	83 ec 04             	sub    $0x4,%esp
80104e17:	8d 45 f4             	lea    -0xc(%ebp),%eax
80104e1a:	6a 14                	push   $0x14
80104e1c:	50                   	push   %eax
80104e1d:	6a 01                	push   $0x1
80104e1f:	e8 5c fb ff ff       	call   80104980 <argptr>
80104e24:	83 c4 10             	add    $0x10,%esp
80104e27:	85 c0                	test   %eax,%eax
80104e29:	78 15                	js     80104e40 <sys_fstat+0x40>
  return filestat(f, st);
80104e2b:	83 ec 08             	sub    $0x8,%esp
80104e2e:	ff 75 f4             	pushl  -0xc(%ebp)
80104e31:	ff 75 f0             	pushl  -0x10(%ebp)
80104e34:	e8 97 c1 ff ff       	call   80100fd0 <filestat>
80104e39:	83 c4 10             	add    $0x10,%esp
}
80104e3c:	c9                   	leave  
80104e3d:	c3                   	ret    
80104e3e:	66 90                	xchg   %ax,%ax
80104e40:	c9                   	leave  
    return -1;
80104e41:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80104e46:	c3                   	ret    
80104e47:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104e4e:	66 90                	xchg   %ax,%ax

80104e50 <sys_link>:
{
80104e50:	55                   	push   %ebp
80104e51:	89 e5                	mov    %esp,%ebp
80104e53:	57                   	push   %edi
80104e54:	56                   	push   %esi
  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
80104e55:	8d 45 d4             	lea    -0x2c(%ebp),%eax
{
80104e58:	53                   	push   %ebx
80104e59:	83 ec 34             	sub    $0x34,%esp
  if(argstr(0, &old) < 0 || argstr(1, &new) < 0)
80104e5c:	50                   	push   %eax
80104e5d:	6a 00                	push   $0x0
80104e5f:	e8 7c fb ff ff       	call   801049e0 <argstr>
80104e64:	83 c4 10             	add    $0x10,%esp
80104e67:	85 c0                	test   %eax,%eax
80104e69:	0f 88 fb 00 00 00    	js     80104f6a <sys_link+0x11a>
80104e6f:	83 ec 08             	sub    $0x8,%esp
80104e72:	8d 45 d0             	lea    -0x30(%ebp),%eax
80104e75:	50                   	push   %eax
80104e76:	6a 01                	push   $0x1
80104e78:	e8 63 fb ff ff       	call   801049e0 <argstr>
80104e7d:	83 c4 10             	add    $0x10,%esp
80104e80:	85 c0                	test   %eax,%eax
80104e82:	0f 88 e2 00 00 00    	js     80104f6a <sys_link+0x11a>
  begin_op();
80104e88:	e8 43 de ff ff       	call   80102cd0 <begin_op>
  if((ip = namei(old)) == 0){
80104e8d:	83 ec 0c             	sub    $0xc,%esp
80104e90:	ff 75 d4             	pushl  -0x2c(%ebp)
80104e93:	e8 58 d1 ff ff       	call   80101ff0 <namei>
80104e98:	83 c4 10             	add    $0x10,%esp
80104e9b:	89 c3                	mov    %eax,%ebx
80104e9d:	85 c0                	test   %eax,%eax
80104e9f:	0f 84 e4 00 00 00    	je     80104f89 <sys_link+0x139>
  ilock(ip);
80104ea5:	83 ec 0c             	sub    $0xc,%esp
80104ea8:	50                   	push   %eax
80104ea9:	e8 a2 c8 ff ff       	call   80101750 <ilock>
  if(ip->type == T_DIR){
80104eae:	83 c4 10             	add    $0x10,%esp
80104eb1:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80104eb6:	0f 84 b5 00 00 00    	je     80104f71 <sys_link+0x121>
  iupdate(ip);
80104ebc:	83 ec 0c             	sub    $0xc,%esp
  ip->nlink++;
80104ebf:	66 83 43 56 01       	addw   $0x1,0x56(%ebx)
  if((dp = nameiparent(new, name)) == 0)
80104ec4:	8d 7d da             	lea    -0x26(%ebp),%edi
  iupdate(ip);
80104ec7:	53                   	push   %ebx
80104ec8:	e8 d3 c7 ff ff       	call   801016a0 <iupdate>
  iunlock(ip);
80104ecd:	89 1c 24             	mov    %ebx,(%esp)
80104ed0:	e8 5b c9 ff ff       	call   80101830 <iunlock>
  if((dp = nameiparent(new, name)) == 0)
80104ed5:	58                   	pop    %eax
80104ed6:	5a                   	pop    %edx
80104ed7:	57                   	push   %edi
80104ed8:	ff 75 d0             	pushl  -0x30(%ebp)
80104edb:	e8 30 d1 ff ff       	call   80102010 <nameiparent>
80104ee0:	83 c4 10             	add    $0x10,%esp
80104ee3:	89 c6                	mov    %eax,%esi
80104ee5:	85 c0                	test   %eax,%eax
80104ee7:	74 5b                	je     80104f44 <sys_link+0xf4>
  ilock(dp);
80104ee9:	83 ec 0c             	sub    $0xc,%esp
80104eec:	50                   	push   %eax
80104eed:	e8 5e c8 ff ff       	call   80101750 <ilock>
  if(dp->dev != ip->dev || dirlink(dp, name, ip->inum) < 0){
80104ef2:	83 c4 10             	add    $0x10,%esp
80104ef5:	8b 03                	mov    (%ebx),%eax
80104ef7:	39 06                	cmp    %eax,(%esi)
80104ef9:	75 3d                	jne    80104f38 <sys_link+0xe8>
80104efb:	83 ec 04             	sub    $0x4,%esp
80104efe:	ff 73 04             	pushl  0x4(%ebx)
80104f01:	57                   	push   %edi
80104f02:	56                   	push   %esi
80104f03:	e8 28 d0 ff ff       	call   80101f30 <dirlink>
80104f08:	83 c4 10             	add    $0x10,%esp
80104f0b:	85 c0                	test   %eax,%eax
80104f0d:	78 29                	js     80104f38 <sys_link+0xe8>
  iunlockput(dp);
80104f0f:	83 ec 0c             	sub    $0xc,%esp
80104f12:	56                   	push   %esi
80104f13:	e8 c8 ca ff ff       	call   801019e0 <iunlockput>
  iput(ip);
80104f18:	89 1c 24             	mov    %ebx,(%esp)
80104f1b:	e8 60 c9 ff ff       	call   80101880 <iput>
  end_op();
80104f20:	e8 1b de ff ff       	call   80102d40 <end_op>
  return 0;
80104f25:	83 c4 10             	add    $0x10,%esp
80104f28:	31 c0                	xor    %eax,%eax
}
80104f2a:	8d 65 f4             	lea    -0xc(%ebp),%esp
80104f2d:	5b                   	pop    %ebx
80104f2e:	5e                   	pop    %esi
80104f2f:	5f                   	pop    %edi
80104f30:	5d                   	pop    %ebp
80104f31:	c3                   	ret    
80104f32:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    iunlockput(dp);
80104f38:	83 ec 0c             	sub    $0xc,%esp
80104f3b:	56                   	push   %esi
80104f3c:	e8 9f ca ff ff       	call   801019e0 <iunlockput>
    goto bad;
80104f41:	83 c4 10             	add    $0x10,%esp
  ilock(ip);
80104f44:	83 ec 0c             	sub    $0xc,%esp
80104f47:	53                   	push   %ebx
80104f48:	e8 03 c8 ff ff       	call   80101750 <ilock>
  ip->nlink--;
80104f4d:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
  iupdate(ip);
80104f52:	89 1c 24             	mov    %ebx,(%esp)
80104f55:	e8 46 c7 ff ff       	call   801016a0 <iupdate>
  iunlockput(ip);
80104f5a:	89 1c 24             	mov    %ebx,(%esp)
80104f5d:	e8 7e ca ff ff       	call   801019e0 <iunlockput>
  end_op();
80104f62:	e8 d9 dd ff ff       	call   80102d40 <end_op>
  return -1;
80104f67:	83 c4 10             	add    $0x10,%esp
80104f6a:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104f6f:	eb b9                	jmp    80104f2a <sys_link+0xda>
    iunlockput(ip);
80104f71:	83 ec 0c             	sub    $0xc,%esp
80104f74:	53                   	push   %ebx
80104f75:	e8 66 ca ff ff       	call   801019e0 <iunlockput>
    end_op();
80104f7a:	e8 c1 dd ff ff       	call   80102d40 <end_op>
    return -1;
80104f7f:	83 c4 10             	add    $0x10,%esp
80104f82:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104f87:	eb a1                	jmp    80104f2a <sys_link+0xda>
    end_op();
80104f89:	e8 b2 dd ff ff       	call   80102d40 <end_op>
    return -1;
80104f8e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80104f93:	eb 95                	jmp    80104f2a <sys_link+0xda>
80104f95:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80104f9c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80104fa0 <sys_unlink>:
{
80104fa0:	55                   	push   %ebp
80104fa1:	89 e5                	mov    %esp,%ebp
80104fa3:	57                   	push   %edi
80104fa4:	56                   	push   %esi
  if(argstr(0, &path) < 0)
80104fa5:	8d 45 c0             	lea    -0x40(%ebp),%eax
{
80104fa8:	53                   	push   %ebx
80104fa9:	83 ec 54             	sub    $0x54,%esp
  if(argstr(0, &path) < 0)
80104fac:	50                   	push   %eax
80104fad:	6a 00                	push   $0x0
80104faf:	e8 2c fa ff ff       	call   801049e0 <argstr>
80104fb4:	83 c4 10             	add    $0x10,%esp
80104fb7:	85 c0                	test   %eax,%eax
80104fb9:	0f 88 91 01 00 00    	js     80105150 <sys_unlink+0x1b0>
  begin_op();
80104fbf:	e8 0c dd ff ff       	call   80102cd0 <begin_op>
  if((dp = nameiparent(path, name)) == 0){
80104fc4:	8d 5d ca             	lea    -0x36(%ebp),%ebx
80104fc7:	83 ec 08             	sub    $0x8,%esp
80104fca:	53                   	push   %ebx
80104fcb:	ff 75 c0             	pushl  -0x40(%ebp)
80104fce:	e8 3d d0 ff ff       	call   80102010 <nameiparent>
80104fd3:	83 c4 10             	add    $0x10,%esp
80104fd6:	89 c6                	mov    %eax,%esi
80104fd8:	85 c0                	test   %eax,%eax
80104fda:	0f 84 7a 01 00 00    	je     8010515a <sys_unlink+0x1ba>
  ilock(dp);
80104fe0:	83 ec 0c             	sub    $0xc,%esp
80104fe3:	50                   	push   %eax
80104fe4:	e8 67 c7 ff ff       	call   80101750 <ilock>
  if(namecmp(name, ".") == 0 || namecmp(name, "..") == 0)
80104fe9:	58                   	pop    %eax
80104fea:	5a                   	pop    %edx
80104feb:	68 e8 7f 10 80       	push   $0x80107fe8
80104ff0:	53                   	push   %ebx
80104ff1:	e8 6a cc ff ff       	call   80101c60 <namecmp>
80104ff6:	83 c4 10             	add    $0x10,%esp
80104ff9:	85 c0                	test   %eax,%eax
80104ffb:	0f 84 0f 01 00 00    	je     80105110 <sys_unlink+0x170>
80105001:	83 ec 08             	sub    $0x8,%esp
80105004:	68 e7 7f 10 80       	push   $0x80107fe7
80105009:	53                   	push   %ebx
8010500a:	e8 51 cc ff ff       	call   80101c60 <namecmp>
8010500f:	83 c4 10             	add    $0x10,%esp
80105012:	85 c0                	test   %eax,%eax
80105014:	0f 84 f6 00 00 00    	je     80105110 <sys_unlink+0x170>
  if((ip = dirlookup(dp, name, &off)) == 0)
8010501a:	83 ec 04             	sub    $0x4,%esp
8010501d:	8d 45 c4             	lea    -0x3c(%ebp),%eax
80105020:	50                   	push   %eax
80105021:	53                   	push   %ebx
80105022:	56                   	push   %esi
80105023:	e8 58 cc ff ff       	call   80101c80 <dirlookup>
80105028:	83 c4 10             	add    $0x10,%esp
8010502b:	89 c3                	mov    %eax,%ebx
8010502d:	85 c0                	test   %eax,%eax
8010502f:	0f 84 db 00 00 00    	je     80105110 <sys_unlink+0x170>
  ilock(ip);
80105035:	83 ec 0c             	sub    $0xc,%esp
80105038:	50                   	push   %eax
80105039:	e8 12 c7 ff ff       	call   80101750 <ilock>
  if(ip->nlink < 1)
8010503e:	83 c4 10             	add    $0x10,%esp
80105041:	66 83 7b 56 00       	cmpw   $0x0,0x56(%ebx)
80105046:	0f 8e 37 01 00 00    	jle    80105183 <sys_unlink+0x1e3>
  if(ip->type == T_DIR && !isdirempty(ip)){
8010504c:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80105051:	8d 7d d8             	lea    -0x28(%ebp),%edi
80105054:	74 6a                	je     801050c0 <sys_unlink+0x120>
  memset(&de, 0, sizeof(de));
80105056:	83 ec 04             	sub    $0x4,%esp
80105059:	6a 10                	push   $0x10
8010505b:	6a 00                	push   $0x0
8010505d:	57                   	push   %edi
8010505e:	e8 ed f5 ff ff       	call   80104650 <memset>
  if(writei(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
80105063:	6a 10                	push   $0x10
80105065:	ff 75 c4             	pushl  -0x3c(%ebp)
80105068:	57                   	push   %edi
80105069:	56                   	push   %esi
8010506a:	e8 c1 ca ff ff       	call   80101b30 <writei>
8010506f:	83 c4 20             	add    $0x20,%esp
80105072:	83 f8 10             	cmp    $0x10,%eax
80105075:	0f 85 fb 00 00 00    	jne    80105176 <sys_unlink+0x1d6>
  if(ip->type == T_DIR){
8010507b:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
80105080:	0f 84 aa 00 00 00    	je     80105130 <sys_unlink+0x190>
  iunlockput(dp);
80105086:	83 ec 0c             	sub    $0xc,%esp
80105089:	56                   	push   %esi
8010508a:	e8 51 c9 ff ff       	call   801019e0 <iunlockput>
  ip->nlink--;
8010508f:	66 83 6b 56 01       	subw   $0x1,0x56(%ebx)
  iupdate(ip);
80105094:	89 1c 24             	mov    %ebx,(%esp)
80105097:	e8 04 c6 ff ff       	call   801016a0 <iupdate>
  iunlockput(ip);
8010509c:	89 1c 24             	mov    %ebx,(%esp)
8010509f:	e8 3c c9 ff ff       	call   801019e0 <iunlockput>
  end_op();
801050a4:	e8 97 dc ff ff       	call   80102d40 <end_op>
  return 0;
801050a9:	83 c4 10             	add    $0x10,%esp
801050ac:	31 c0                	xor    %eax,%eax
}
801050ae:	8d 65 f4             	lea    -0xc(%ebp),%esp
801050b1:	5b                   	pop    %ebx
801050b2:	5e                   	pop    %esi
801050b3:	5f                   	pop    %edi
801050b4:	5d                   	pop    %ebp
801050b5:	c3                   	ret    
801050b6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801050bd:	8d 76 00             	lea    0x0(%esi),%esi
  for(off=2*sizeof(de); off<dp->size; off+=sizeof(de)){
801050c0:	83 7b 58 20          	cmpl   $0x20,0x58(%ebx)
801050c4:	76 90                	jbe    80105056 <sys_unlink+0xb6>
801050c6:	ba 20 00 00 00       	mov    $0x20,%edx
801050cb:	eb 0f                	jmp    801050dc <sys_unlink+0x13c>
801050cd:	8d 76 00             	lea    0x0(%esi),%esi
801050d0:	83 c2 10             	add    $0x10,%edx
801050d3:	39 53 58             	cmp    %edx,0x58(%ebx)
801050d6:	0f 86 7a ff ff ff    	jbe    80105056 <sys_unlink+0xb6>
    if(readi(dp, (char*)&de, off, sizeof(de)) != sizeof(de))
801050dc:	6a 10                	push   $0x10
801050de:	52                   	push   %edx
801050df:	57                   	push   %edi
801050e0:	53                   	push   %ebx
801050e1:	89 55 b4             	mov    %edx,-0x4c(%ebp)
801050e4:	e8 47 c9 ff ff       	call   80101a30 <readi>
801050e9:	83 c4 10             	add    $0x10,%esp
801050ec:	8b 55 b4             	mov    -0x4c(%ebp),%edx
801050ef:	83 f8 10             	cmp    $0x10,%eax
801050f2:	75 75                	jne    80105169 <sys_unlink+0x1c9>
    if(de.inum != 0)
801050f4:	66 83 7d d8 00       	cmpw   $0x0,-0x28(%ebp)
801050f9:	74 d5                	je     801050d0 <sys_unlink+0x130>
    iunlockput(ip);
801050fb:	83 ec 0c             	sub    $0xc,%esp
801050fe:	53                   	push   %ebx
801050ff:	e8 dc c8 ff ff       	call   801019e0 <iunlockput>
    goto bad;
80105104:	83 c4 10             	add    $0x10,%esp
80105107:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010510e:	66 90                	xchg   %ax,%ax
  iunlockput(dp);
80105110:	83 ec 0c             	sub    $0xc,%esp
80105113:	56                   	push   %esi
80105114:	e8 c7 c8 ff ff       	call   801019e0 <iunlockput>
  end_op();
80105119:	e8 22 dc ff ff       	call   80102d40 <end_op>
  return -1;
8010511e:	83 c4 10             	add    $0x10,%esp
80105121:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105126:	eb 86                	jmp    801050ae <sys_unlink+0x10e>
80105128:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010512f:	90                   	nop
    iupdate(dp);
80105130:	83 ec 0c             	sub    $0xc,%esp
    dp->nlink--;
80105133:	66 83 6e 56 01       	subw   $0x1,0x56(%esi)
    iupdate(dp);
80105138:	56                   	push   %esi
80105139:	e8 62 c5 ff ff       	call   801016a0 <iupdate>
8010513e:	83 c4 10             	add    $0x10,%esp
80105141:	e9 40 ff ff ff       	jmp    80105086 <sys_unlink+0xe6>
80105146:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010514d:	8d 76 00             	lea    0x0(%esi),%esi
    return -1;
80105150:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105155:	e9 54 ff ff ff       	jmp    801050ae <sys_unlink+0x10e>
    end_op();
8010515a:	e8 e1 db ff ff       	call   80102d40 <end_op>
    return -1;
8010515f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105164:	e9 45 ff ff ff       	jmp    801050ae <sys_unlink+0x10e>
      panic("isdirempty: readi");
80105169:	83 ec 0c             	sub    $0xc,%esp
8010516c:	68 0c 80 10 80       	push   $0x8010800c
80105171:	e8 1a b2 ff ff       	call   80100390 <panic>
    panic("unlink: writei");
80105176:	83 ec 0c             	sub    $0xc,%esp
80105179:	68 1e 80 10 80       	push   $0x8010801e
8010517e:	e8 0d b2 ff ff       	call   80100390 <panic>
    panic("unlink: nlink < 1");
80105183:	83 ec 0c             	sub    $0xc,%esp
80105186:	68 fa 7f 10 80       	push   $0x80107ffa
8010518b:	e8 00 b2 ff ff       	call   80100390 <panic>

80105190 <sys_open>:

int
sys_open(void)
{
80105190:	55                   	push   %ebp
80105191:	89 e5                	mov    %esp,%ebp
80105193:	57                   	push   %edi
80105194:	56                   	push   %esi
  char *path;
  int fd, omode;
  struct file *f;
  struct inode *ip;

  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
80105195:	8d 45 e0             	lea    -0x20(%ebp),%eax
{
80105198:	53                   	push   %ebx
80105199:	83 ec 24             	sub    $0x24,%esp
  if(argstr(0, &path) < 0 || argint(1, &omode) < 0)
8010519c:	50                   	push   %eax
8010519d:	6a 00                	push   $0x0
8010519f:	e8 3c f8 ff ff       	call   801049e0 <argstr>
801051a4:	83 c4 10             	add    $0x10,%esp
801051a7:	85 c0                	test   %eax,%eax
801051a9:	0f 88 8e 00 00 00    	js     8010523d <sys_open+0xad>
801051af:	83 ec 08             	sub    $0x8,%esp
801051b2:	8d 45 e4             	lea    -0x1c(%ebp),%eax
801051b5:	50                   	push   %eax
801051b6:	6a 01                	push   $0x1
801051b8:	e8 73 f7 ff ff       	call   80104930 <argint>
801051bd:	83 c4 10             	add    $0x10,%esp
801051c0:	85 c0                	test   %eax,%eax
801051c2:	78 79                	js     8010523d <sys_open+0xad>
    return -1;

  begin_op();
801051c4:	e8 07 db ff ff       	call   80102cd0 <begin_op>

  if(omode & O_CREATE){
801051c9:	f6 45 e5 02          	testb  $0x2,-0x1b(%ebp)
801051cd:	75 79                	jne    80105248 <sys_open+0xb8>
    if(ip == 0){
      end_op();
      return -1;
    }
  } else {
    if((ip = namei(path)) == 0){
801051cf:	83 ec 0c             	sub    $0xc,%esp
801051d2:	ff 75 e0             	pushl  -0x20(%ebp)
801051d5:	e8 16 ce ff ff       	call   80101ff0 <namei>
801051da:	83 c4 10             	add    $0x10,%esp
801051dd:	89 c6                	mov    %eax,%esi
801051df:	85 c0                	test   %eax,%eax
801051e1:	0f 84 7e 00 00 00    	je     80105265 <sys_open+0xd5>
      end_op();
      return -1;
    }
    ilock(ip);
801051e7:	83 ec 0c             	sub    $0xc,%esp
801051ea:	50                   	push   %eax
801051eb:	e8 60 c5 ff ff       	call   80101750 <ilock>
    if(ip->type == T_DIR && omode != O_RDONLY){
801051f0:	83 c4 10             	add    $0x10,%esp
801051f3:	66 83 7e 50 01       	cmpw   $0x1,0x50(%esi)
801051f8:	0f 84 c2 00 00 00    	je     801052c0 <sys_open+0x130>
      end_op();
      return -1;
    }
  }

  if((f = filealloc()) == 0 || (fd = fdalloc(f)) < 0){
801051fe:	e8 2d bc ff ff       	call   80100e30 <filealloc>
80105203:	89 c7                	mov    %eax,%edi
80105205:	85 c0                	test   %eax,%eax
80105207:	74 23                	je     8010522c <sys_open+0x9c>
  struct proc *curproc = myproc();
80105209:	e8 12 e7 ff ff       	call   80103920 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
8010520e:	31 db                	xor    %ebx,%ebx
    if(curproc->ofile[fd] == 0){
80105210:	8b 54 98 28          	mov    0x28(%eax,%ebx,4),%edx
80105214:	85 d2                	test   %edx,%edx
80105216:	74 60                	je     80105278 <sys_open+0xe8>
  for(fd = 0; fd < NOFILE; fd++){
80105218:	83 c3 01             	add    $0x1,%ebx
8010521b:	83 fb 10             	cmp    $0x10,%ebx
8010521e:	75 f0                	jne    80105210 <sys_open+0x80>
    if(f)
      fileclose(f);
80105220:	83 ec 0c             	sub    $0xc,%esp
80105223:	57                   	push   %edi
80105224:	e8 c7 bc ff ff       	call   80100ef0 <fileclose>
80105229:	83 c4 10             	add    $0x10,%esp
    iunlockput(ip);
8010522c:	83 ec 0c             	sub    $0xc,%esp
8010522f:	56                   	push   %esi
80105230:	e8 ab c7 ff ff       	call   801019e0 <iunlockput>
    end_op();
80105235:	e8 06 db ff ff       	call   80102d40 <end_op>
    return -1;
8010523a:	83 c4 10             	add    $0x10,%esp
8010523d:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
80105242:	eb 6d                	jmp    801052b1 <sys_open+0x121>
80105244:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    ip = create(path, T_FILE, 0, 0);
80105248:	83 ec 0c             	sub    $0xc,%esp
8010524b:	8b 45 e0             	mov    -0x20(%ebp),%eax
8010524e:	31 c9                	xor    %ecx,%ecx
80105250:	ba 02 00 00 00       	mov    $0x2,%edx
80105255:	6a 00                	push   $0x0
80105257:	e8 24 f8 ff ff       	call   80104a80 <create>
    if(ip == 0){
8010525c:	83 c4 10             	add    $0x10,%esp
    ip = create(path, T_FILE, 0, 0);
8010525f:	89 c6                	mov    %eax,%esi
    if(ip == 0){
80105261:	85 c0                	test   %eax,%eax
80105263:	75 99                	jne    801051fe <sys_open+0x6e>
      end_op();
80105265:	e8 d6 da ff ff       	call   80102d40 <end_op>
      return -1;
8010526a:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
8010526f:	eb 40                	jmp    801052b1 <sys_open+0x121>
80105271:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  }
  iunlock(ip);
80105278:	83 ec 0c             	sub    $0xc,%esp
      curproc->ofile[fd] = f;
8010527b:	89 7c 98 28          	mov    %edi,0x28(%eax,%ebx,4)
  iunlock(ip);
8010527f:	56                   	push   %esi
80105280:	e8 ab c5 ff ff       	call   80101830 <iunlock>
  end_op();
80105285:	e8 b6 da ff ff       	call   80102d40 <end_op>

  f->type = FD_INODE;
8010528a:	c7 07 02 00 00 00    	movl   $0x2,(%edi)
  f->ip = ip;
  f->off = 0;
  f->readable = !(omode & O_WRONLY);
80105290:	8b 55 e4             	mov    -0x1c(%ebp),%edx
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
80105293:	83 c4 10             	add    $0x10,%esp
  f->ip = ip;
80105296:	89 77 10             	mov    %esi,0x10(%edi)
  f->readable = !(omode & O_WRONLY);
80105299:	89 d0                	mov    %edx,%eax
  f->off = 0;
8010529b:	c7 47 14 00 00 00 00 	movl   $0x0,0x14(%edi)
  f->readable = !(omode & O_WRONLY);
801052a2:	f7 d0                	not    %eax
801052a4:	83 e0 01             	and    $0x1,%eax
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
801052a7:	83 e2 03             	and    $0x3,%edx
  f->readable = !(omode & O_WRONLY);
801052aa:	88 47 08             	mov    %al,0x8(%edi)
  f->writable = (omode & O_WRONLY) || (omode & O_RDWR);
801052ad:	0f 95 47 09          	setne  0x9(%edi)
  return fd;
}
801052b1:	8d 65 f4             	lea    -0xc(%ebp),%esp
801052b4:	89 d8                	mov    %ebx,%eax
801052b6:	5b                   	pop    %ebx
801052b7:	5e                   	pop    %esi
801052b8:	5f                   	pop    %edi
801052b9:	5d                   	pop    %ebp
801052ba:	c3                   	ret    
801052bb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801052bf:	90                   	nop
    if(ip->type == T_DIR && omode != O_RDONLY){
801052c0:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
801052c3:	85 c9                	test   %ecx,%ecx
801052c5:	0f 84 33 ff ff ff    	je     801051fe <sys_open+0x6e>
801052cb:	e9 5c ff ff ff       	jmp    8010522c <sys_open+0x9c>

801052d0 <sys_mkdir>:

int
sys_mkdir(void)
{
801052d0:	55                   	push   %ebp
801052d1:	89 e5                	mov    %esp,%ebp
801052d3:	83 ec 18             	sub    $0x18,%esp
  char *path;
  struct inode *ip;

  begin_op();
801052d6:	e8 f5 d9 ff ff       	call   80102cd0 <begin_op>
  if(argstr(0, &path) < 0 || (ip = create(path, T_DIR, 0, 0)) == 0){
801052db:	83 ec 08             	sub    $0x8,%esp
801052de:	8d 45 f4             	lea    -0xc(%ebp),%eax
801052e1:	50                   	push   %eax
801052e2:	6a 00                	push   $0x0
801052e4:	e8 f7 f6 ff ff       	call   801049e0 <argstr>
801052e9:	83 c4 10             	add    $0x10,%esp
801052ec:	85 c0                	test   %eax,%eax
801052ee:	78 30                	js     80105320 <sys_mkdir+0x50>
801052f0:	83 ec 0c             	sub    $0xc,%esp
801052f3:	8b 45 f4             	mov    -0xc(%ebp),%eax
801052f6:	31 c9                	xor    %ecx,%ecx
801052f8:	ba 01 00 00 00       	mov    $0x1,%edx
801052fd:	6a 00                	push   $0x0
801052ff:	e8 7c f7 ff ff       	call   80104a80 <create>
80105304:	83 c4 10             	add    $0x10,%esp
80105307:	85 c0                	test   %eax,%eax
80105309:	74 15                	je     80105320 <sys_mkdir+0x50>
    end_op();
    return -1;
  }
  iunlockput(ip);
8010530b:	83 ec 0c             	sub    $0xc,%esp
8010530e:	50                   	push   %eax
8010530f:	e8 cc c6 ff ff       	call   801019e0 <iunlockput>
  end_op();
80105314:	e8 27 da ff ff       	call   80102d40 <end_op>
  return 0;
80105319:	83 c4 10             	add    $0x10,%esp
8010531c:	31 c0                	xor    %eax,%eax
}
8010531e:	c9                   	leave  
8010531f:	c3                   	ret    
    end_op();
80105320:	e8 1b da ff ff       	call   80102d40 <end_op>
    return -1;
80105325:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
8010532a:	c9                   	leave  
8010532b:	c3                   	ret    
8010532c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105330 <sys_mknod>:

int
sys_mknod(void)
{
80105330:	55                   	push   %ebp
80105331:	89 e5                	mov    %esp,%ebp
80105333:	83 ec 18             	sub    $0x18,%esp
  struct inode *ip;
  char *path;
  int major, minor;

  begin_op();
80105336:	e8 95 d9 ff ff       	call   80102cd0 <begin_op>
  if((argstr(0, &path)) < 0 ||
8010533b:	83 ec 08             	sub    $0x8,%esp
8010533e:	8d 45 ec             	lea    -0x14(%ebp),%eax
80105341:	50                   	push   %eax
80105342:	6a 00                	push   $0x0
80105344:	e8 97 f6 ff ff       	call   801049e0 <argstr>
80105349:	83 c4 10             	add    $0x10,%esp
8010534c:	85 c0                	test   %eax,%eax
8010534e:	78 60                	js     801053b0 <sys_mknod+0x80>
     argint(1, &major) < 0 ||
80105350:	83 ec 08             	sub    $0x8,%esp
80105353:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105356:	50                   	push   %eax
80105357:	6a 01                	push   $0x1
80105359:	e8 d2 f5 ff ff       	call   80104930 <argint>
  if((argstr(0, &path)) < 0 ||
8010535e:	83 c4 10             	add    $0x10,%esp
80105361:	85 c0                	test   %eax,%eax
80105363:	78 4b                	js     801053b0 <sys_mknod+0x80>
     argint(2, &minor) < 0 ||
80105365:	83 ec 08             	sub    $0x8,%esp
80105368:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010536b:	50                   	push   %eax
8010536c:	6a 02                	push   $0x2
8010536e:	e8 bd f5 ff ff       	call   80104930 <argint>
     argint(1, &major) < 0 ||
80105373:	83 c4 10             	add    $0x10,%esp
80105376:	85 c0                	test   %eax,%eax
80105378:	78 36                	js     801053b0 <sys_mknod+0x80>
     (ip = create(path, T_DEV, major, minor)) == 0){
8010537a:	0f bf 45 f4          	movswl -0xc(%ebp),%eax
8010537e:	83 ec 0c             	sub    $0xc,%esp
80105381:	0f bf 4d f0          	movswl -0x10(%ebp),%ecx
80105385:	ba 03 00 00 00       	mov    $0x3,%edx
8010538a:	50                   	push   %eax
8010538b:	8b 45 ec             	mov    -0x14(%ebp),%eax
8010538e:	e8 ed f6 ff ff       	call   80104a80 <create>
     argint(2, &minor) < 0 ||
80105393:	83 c4 10             	add    $0x10,%esp
80105396:	85 c0                	test   %eax,%eax
80105398:	74 16                	je     801053b0 <sys_mknod+0x80>
    end_op();
    return -1;
  }
  iunlockput(ip);
8010539a:	83 ec 0c             	sub    $0xc,%esp
8010539d:	50                   	push   %eax
8010539e:	e8 3d c6 ff ff       	call   801019e0 <iunlockput>
  end_op();
801053a3:	e8 98 d9 ff ff       	call   80102d40 <end_op>
  return 0;
801053a8:	83 c4 10             	add    $0x10,%esp
801053ab:	31 c0                	xor    %eax,%eax
}
801053ad:	c9                   	leave  
801053ae:	c3                   	ret    
801053af:	90                   	nop
    end_op();
801053b0:	e8 8b d9 ff ff       	call   80102d40 <end_op>
    return -1;
801053b5:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801053ba:	c9                   	leave  
801053bb:	c3                   	ret    
801053bc:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

801053c0 <sys_chdir>:

int
sys_chdir(void)
{
801053c0:	55                   	push   %ebp
801053c1:	89 e5                	mov    %esp,%ebp
801053c3:	56                   	push   %esi
801053c4:	53                   	push   %ebx
801053c5:	83 ec 10             	sub    $0x10,%esp
  char *path;
  struct inode *ip;
  struct proc *curproc = myproc();
801053c8:	e8 53 e5 ff ff       	call   80103920 <myproc>
801053cd:	89 c6                	mov    %eax,%esi
  
  begin_op();
801053cf:	e8 fc d8 ff ff       	call   80102cd0 <begin_op>
  if(argstr(0, &path) < 0 || (ip = namei(path)) == 0){
801053d4:	83 ec 08             	sub    $0x8,%esp
801053d7:	8d 45 f4             	lea    -0xc(%ebp),%eax
801053da:	50                   	push   %eax
801053db:	6a 00                	push   $0x0
801053dd:	e8 fe f5 ff ff       	call   801049e0 <argstr>
801053e2:	83 c4 10             	add    $0x10,%esp
801053e5:	85 c0                	test   %eax,%eax
801053e7:	78 77                	js     80105460 <sys_chdir+0xa0>
801053e9:	83 ec 0c             	sub    $0xc,%esp
801053ec:	ff 75 f4             	pushl  -0xc(%ebp)
801053ef:	e8 fc cb ff ff       	call   80101ff0 <namei>
801053f4:	83 c4 10             	add    $0x10,%esp
801053f7:	89 c3                	mov    %eax,%ebx
801053f9:	85 c0                	test   %eax,%eax
801053fb:	74 63                	je     80105460 <sys_chdir+0xa0>
    end_op();
    return -1;
  }
  ilock(ip);
801053fd:	83 ec 0c             	sub    $0xc,%esp
80105400:	50                   	push   %eax
80105401:	e8 4a c3 ff ff       	call   80101750 <ilock>
  if(ip->type != T_DIR){
80105406:	83 c4 10             	add    $0x10,%esp
80105409:	66 83 7b 50 01       	cmpw   $0x1,0x50(%ebx)
8010540e:	75 30                	jne    80105440 <sys_chdir+0x80>
    iunlockput(ip);
    end_op();
    return -1;
  }
  iunlock(ip);
80105410:	83 ec 0c             	sub    $0xc,%esp
80105413:	53                   	push   %ebx
80105414:	e8 17 c4 ff ff       	call   80101830 <iunlock>
  iput(curproc->cwd);
80105419:	58                   	pop    %eax
8010541a:	ff b6 a4 00 00 00    	pushl  0xa4(%esi)
80105420:	e8 5b c4 ff ff       	call   80101880 <iput>
  end_op();
80105425:	e8 16 d9 ff ff       	call   80102d40 <end_op>
  curproc->cwd = ip;
8010542a:	89 9e a4 00 00 00    	mov    %ebx,0xa4(%esi)
  return 0;
80105430:	83 c4 10             	add    $0x10,%esp
80105433:	31 c0                	xor    %eax,%eax
}
80105435:	8d 65 f8             	lea    -0x8(%ebp),%esp
80105438:	5b                   	pop    %ebx
80105439:	5e                   	pop    %esi
8010543a:	5d                   	pop    %ebp
8010543b:	c3                   	ret    
8010543c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    iunlockput(ip);
80105440:	83 ec 0c             	sub    $0xc,%esp
80105443:	53                   	push   %ebx
80105444:	e8 97 c5 ff ff       	call   801019e0 <iunlockput>
    end_op();
80105449:	e8 f2 d8 ff ff       	call   80102d40 <end_op>
    return -1;
8010544e:	83 c4 10             	add    $0x10,%esp
80105451:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80105456:	eb dd                	jmp    80105435 <sys_chdir+0x75>
80105458:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010545f:	90                   	nop
    end_op();
80105460:	e8 db d8 ff ff       	call   80102d40 <end_op>
    return -1;
80105465:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
8010546a:	eb c9                	jmp    80105435 <sys_chdir+0x75>
8010546c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105470 <sys_exec>:

int
sys_exec(void)
{
80105470:	55                   	push   %ebp
80105471:	89 e5                	mov    %esp,%ebp
80105473:	57                   	push   %edi
80105474:	56                   	push   %esi
  char *path, *argv[MAXARG];
  int i;
  uint uargv, uarg;

  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80105475:	8d 85 5c ff ff ff    	lea    -0xa4(%ebp),%eax
{
8010547b:	53                   	push   %ebx
8010547c:	81 ec a4 00 00 00    	sub    $0xa4,%esp
  if(argstr(0, &path) < 0 || argint(1, (int*)&uargv) < 0){
80105482:	50                   	push   %eax
80105483:	6a 00                	push   $0x0
80105485:	e8 56 f5 ff ff       	call   801049e0 <argstr>
8010548a:	83 c4 10             	add    $0x10,%esp
8010548d:	85 c0                	test   %eax,%eax
8010548f:	0f 88 87 00 00 00    	js     8010551c <sys_exec+0xac>
80105495:	83 ec 08             	sub    $0x8,%esp
80105498:	8d 85 60 ff ff ff    	lea    -0xa0(%ebp),%eax
8010549e:	50                   	push   %eax
8010549f:	6a 01                	push   $0x1
801054a1:	e8 8a f4 ff ff       	call   80104930 <argint>
801054a6:	83 c4 10             	add    $0x10,%esp
801054a9:	85 c0                	test   %eax,%eax
801054ab:	78 6f                	js     8010551c <sys_exec+0xac>
    return -1;
  }
  memset(argv, 0, sizeof(argv));
801054ad:	83 ec 04             	sub    $0x4,%esp
801054b0:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
  for(i=0;; i++){
801054b6:	31 db                	xor    %ebx,%ebx
  memset(argv, 0, sizeof(argv));
801054b8:	68 80 00 00 00       	push   $0x80
801054bd:	8d bd 64 ff ff ff    	lea    -0x9c(%ebp),%edi
801054c3:	6a 00                	push   $0x0
801054c5:	50                   	push   %eax
801054c6:	e8 85 f1 ff ff       	call   80104650 <memset>
801054cb:	83 c4 10             	add    $0x10,%esp
801054ce:	66 90                	xchg   %ax,%ax
    if(i >= NELEM(argv))
      return -1;
    if(fetchint(uargv+4*i, (int*)&uarg) < 0)
801054d0:	8b 85 60 ff ff ff    	mov    -0xa0(%ebp),%eax
801054d6:	8d 34 9d 00 00 00 00 	lea    0x0(,%ebx,4),%esi
801054dd:	83 ec 08             	sub    $0x8,%esp
801054e0:	57                   	push   %edi
801054e1:	01 f0                	add    %esi,%eax
801054e3:	50                   	push   %eax
801054e4:	e8 a7 f3 ff ff       	call   80104890 <fetchint>
801054e9:	83 c4 10             	add    $0x10,%esp
801054ec:	85 c0                	test   %eax,%eax
801054ee:	78 2c                	js     8010551c <sys_exec+0xac>
      return -1;
    if(uarg == 0){
801054f0:	8b 85 64 ff ff ff    	mov    -0x9c(%ebp),%eax
801054f6:	85 c0                	test   %eax,%eax
801054f8:	74 36                	je     80105530 <sys_exec+0xc0>
      argv[i] = 0;
      break;
    }
    if(fetchstr(uarg, &argv[i]) < 0)
801054fa:	8d 8d 68 ff ff ff    	lea    -0x98(%ebp),%ecx
80105500:	83 ec 08             	sub    $0x8,%esp
80105503:	8d 14 31             	lea    (%ecx,%esi,1),%edx
80105506:	52                   	push   %edx
80105507:	50                   	push   %eax
80105508:	e8 c3 f3 ff ff       	call   801048d0 <fetchstr>
8010550d:	83 c4 10             	add    $0x10,%esp
80105510:	85 c0                	test   %eax,%eax
80105512:	78 08                	js     8010551c <sys_exec+0xac>
  for(i=0;; i++){
80105514:	83 c3 01             	add    $0x1,%ebx
    if(i >= NELEM(argv))
80105517:	83 fb 20             	cmp    $0x20,%ebx
8010551a:	75 b4                	jne    801054d0 <sys_exec+0x60>
      return -1;
  }
  return exec(path, argv);
}
8010551c:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return -1;
8010551f:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105524:	5b                   	pop    %ebx
80105525:	5e                   	pop    %esi
80105526:	5f                   	pop    %edi
80105527:	5d                   	pop    %ebp
80105528:	c3                   	ret    
80105529:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  return exec(path, argv);
80105530:	83 ec 08             	sub    $0x8,%esp
80105533:	8d 85 68 ff ff ff    	lea    -0x98(%ebp),%eax
      argv[i] = 0;
80105539:	c7 84 9d 68 ff ff ff 	movl   $0x0,-0x98(%ebp,%ebx,4)
80105540:	00 00 00 00 
  return exec(path, argv);
80105544:	50                   	push   %eax
80105545:	ff b5 5c ff ff ff    	pushl  -0xa4(%ebp)
8010554b:	e8 30 b5 ff ff       	call   80100a80 <exec>
80105550:	83 c4 10             	add    $0x10,%esp
}
80105553:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105556:	5b                   	pop    %ebx
80105557:	5e                   	pop    %esi
80105558:	5f                   	pop    %edi
80105559:	5d                   	pop    %ebp
8010555a:	c3                   	ret    
8010555b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010555f:	90                   	nop

80105560 <sys_pipe>:

int
sys_pipe(void)
{
80105560:	55                   	push   %ebp
80105561:	89 e5                	mov    %esp,%ebp
80105563:	57                   	push   %edi
80105564:	56                   	push   %esi
  int *fd;
  struct file *rf, *wf;
  int fd0, fd1;

  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
80105565:	8d 45 dc             	lea    -0x24(%ebp),%eax
{
80105568:	53                   	push   %ebx
80105569:	83 ec 20             	sub    $0x20,%esp
  if(argptr(0, (void*)&fd, 2*sizeof(fd[0])) < 0)
8010556c:	6a 08                	push   $0x8
8010556e:	50                   	push   %eax
8010556f:	6a 00                	push   $0x0
80105571:	e8 0a f4 ff ff       	call   80104980 <argptr>
80105576:	83 c4 10             	add    $0x10,%esp
80105579:	85 c0                	test   %eax,%eax
8010557b:	78 4a                	js     801055c7 <sys_pipe+0x67>
    return -1;
  if(pipealloc(&rf, &wf) < 0)
8010557d:	83 ec 08             	sub    $0x8,%esp
80105580:	8d 45 e4             	lea    -0x1c(%ebp),%eax
80105583:	50                   	push   %eax
80105584:	8d 45 e0             	lea    -0x20(%ebp),%eax
80105587:	50                   	push   %eax
80105588:	e8 f3 dd ff ff       	call   80103380 <pipealloc>
8010558d:	83 c4 10             	add    $0x10,%esp
80105590:	85 c0                	test   %eax,%eax
80105592:	78 33                	js     801055c7 <sys_pipe+0x67>
    return -1;
  fd0 = -1;
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
80105594:	8b 7d e0             	mov    -0x20(%ebp),%edi
  for(fd = 0; fd < NOFILE; fd++){
80105597:	31 db                	xor    %ebx,%ebx
  struct proc *curproc = myproc();
80105599:	e8 82 e3 ff ff       	call   80103920 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
8010559e:	66 90                	xchg   %ax,%ax
    if(curproc->ofile[fd] == 0){
801055a0:	8b 74 98 28          	mov    0x28(%eax,%ebx,4),%esi
801055a4:	85 f6                	test   %esi,%esi
801055a6:	74 28                	je     801055d0 <sys_pipe+0x70>
  for(fd = 0; fd < NOFILE; fd++){
801055a8:	83 c3 01             	add    $0x1,%ebx
801055ab:	83 fb 10             	cmp    $0x10,%ebx
801055ae:	75 f0                	jne    801055a0 <sys_pipe+0x40>
    if(fd0 >= 0)
      myproc()->ofile[fd0] = 0;
    fileclose(rf);
801055b0:	83 ec 0c             	sub    $0xc,%esp
801055b3:	ff 75 e0             	pushl  -0x20(%ebp)
801055b6:	e8 35 b9 ff ff       	call   80100ef0 <fileclose>
    fileclose(wf);
801055bb:	58                   	pop    %eax
801055bc:	ff 75 e4             	pushl  -0x1c(%ebp)
801055bf:	e8 2c b9 ff ff       	call   80100ef0 <fileclose>
    return -1;
801055c4:	83 c4 10             	add    $0x10,%esp
801055c7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801055cc:	eb 53                	jmp    80105621 <sys_pipe+0xc1>
801055ce:	66 90                	xchg   %ax,%ax
      curproc->ofile[fd] = f;
801055d0:	8d 73 08             	lea    0x8(%ebx),%esi
801055d3:	89 7c b0 08          	mov    %edi,0x8(%eax,%esi,4)
  if((fd0 = fdalloc(rf)) < 0 || (fd1 = fdalloc(wf)) < 0){
801055d7:	8b 7d e4             	mov    -0x1c(%ebp),%edi
  struct proc *curproc = myproc();
801055da:	e8 41 e3 ff ff       	call   80103920 <myproc>
  for(fd = 0; fd < NOFILE; fd++){
801055df:	31 d2                	xor    %edx,%edx
801055e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    if(curproc->ofile[fd] == 0){
801055e8:	8b 4c 90 28          	mov    0x28(%eax,%edx,4),%ecx
801055ec:	85 c9                	test   %ecx,%ecx
801055ee:	74 20                	je     80105610 <sys_pipe+0xb0>
  for(fd = 0; fd < NOFILE; fd++){
801055f0:	83 c2 01             	add    $0x1,%edx
801055f3:	83 fa 10             	cmp    $0x10,%edx
801055f6:	75 f0                	jne    801055e8 <sys_pipe+0x88>
      myproc()->ofile[fd0] = 0;
801055f8:	e8 23 e3 ff ff       	call   80103920 <myproc>
801055fd:	c7 44 b0 08 00 00 00 	movl   $0x0,0x8(%eax,%esi,4)
80105604:	00 
80105605:	eb a9                	jmp    801055b0 <sys_pipe+0x50>
80105607:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010560e:	66 90                	xchg   %ax,%ax
      curproc->ofile[fd] = f;
80105610:	89 7c 90 28          	mov    %edi,0x28(%eax,%edx,4)
  }
  fd[0] = fd0;
80105614:	8b 45 dc             	mov    -0x24(%ebp),%eax
80105617:	89 18                	mov    %ebx,(%eax)
  fd[1] = fd1;
80105619:	8b 45 dc             	mov    -0x24(%ebp),%eax
8010561c:	89 50 04             	mov    %edx,0x4(%eax)
  return 0;
8010561f:	31 c0                	xor    %eax,%eax
}
80105621:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105624:	5b                   	pop    %ebx
80105625:	5e                   	pop    %esi
80105626:	5f                   	pop    %edi
80105627:	5d                   	pop    %ebp
80105628:	c3                   	ret    
80105629:	66 90                	xchg   %ax,%ax
8010562b:	66 90                	xchg   %ax,%ax
8010562d:	66 90                	xchg   %ax,%ax
8010562f:	90                   	nop

80105630 <sys_fork>:
#include "proc.h"

int
sys_fork(void)
{
  return fork();
80105630:	e9 9b e4 ff ff       	jmp    80103ad0 <fork>
80105635:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010563c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105640 <sys_exit>:
}

int
sys_exit(void)
{
80105640:	55                   	push   %ebp
80105641:	89 e5                	mov    %esp,%ebp
80105643:	83 ec 08             	sub    $0x8,%esp
  exit();
80105646:	e8 15 e7 ff ff       	call   80103d60 <exit>
  return 0;  // not reached
}
8010564b:	31 c0                	xor    %eax,%eax
8010564d:	c9                   	leave  
8010564e:	c3                   	ret    
8010564f:	90                   	nop

80105650 <sys_wait>:

int
sys_wait(void)
{
  return wait();
80105650:	e9 9b e9 ff ff       	jmp    80103ff0 <wait>
80105655:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010565c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80105660 <sys_kill>:
}

int
sys_kill(void)
{
80105660:	55                   	push   %ebp
80105661:	89 e5                	mov    %esp,%ebp
80105663:	83 ec 20             	sub    $0x20,%esp
  int pid;

  if(argint(0, &pid) < 0)
80105666:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105669:	50                   	push   %eax
8010566a:	6a 00                	push   $0x0
8010566c:	e8 bf f2 ff ff       	call   80104930 <argint>
80105671:	83 c4 10             	add    $0x10,%esp
80105674:	85 c0                	test   %eax,%eax
80105676:	78 18                	js     80105690 <sys_kill+0x30>
    return -1;
  return kill(pid);
80105678:	83 ec 0c             	sub    $0xc,%esp
8010567b:	ff 75 f4             	pushl  -0xc(%ebp)
8010567e:	e8 cd ea ff ff       	call   80104150 <kill>
80105683:	83 c4 10             	add    $0x10,%esp
}
80105686:	c9                   	leave  
80105687:	c3                   	ret    
80105688:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010568f:	90                   	nop
80105690:	c9                   	leave  
    return -1;
80105691:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105696:	c3                   	ret    
80105697:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010569e:	66 90                	xchg   %ax,%ax

801056a0 <sys_getpid>:

int
sys_getpid(void)
{
801056a0:	55                   	push   %ebp
801056a1:	89 e5                	mov    %esp,%ebp
801056a3:	83 ec 08             	sub    $0x8,%esp
  return myproc()->pid;
801056a6:	e8 75 e2 ff ff       	call   80103920 <myproc>
801056ab:	8b 40 10             	mov    0x10(%eax),%eax
}
801056ae:	c9                   	leave  
801056af:	c3                   	ret    

801056b0 <sys_sbrk>:

int
sys_sbrk(void)
{
801056b0:	55                   	push   %ebp
801056b1:	89 e5                	mov    %esp,%ebp
801056b3:	53                   	push   %ebx
  int addr;
  int n;

  if(argint(0, &n) < 0)
801056b4:	8d 45 f4             	lea    -0xc(%ebp),%eax
{
801056b7:	83 ec 1c             	sub    $0x1c,%esp
  if(argint(0, &n) < 0)
801056ba:	50                   	push   %eax
801056bb:	6a 00                	push   $0x0
801056bd:	e8 6e f2 ff ff       	call   80104930 <argint>
801056c2:	83 c4 10             	add    $0x10,%esp
801056c5:	85 c0                	test   %eax,%eax
801056c7:	78 27                	js     801056f0 <sys_sbrk+0x40>
    return -1;
  addr = myproc()->sz;
801056c9:	e8 52 e2 ff ff       	call   80103920 <myproc>
  if(growproc(n) < 0)
801056ce:	83 ec 0c             	sub    $0xc,%esp
  addr = myproc()->sz;
801056d1:	8b 18                	mov    (%eax),%ebx
  if(growproc(n) < 0)
801056d3:	ff 75 f4             	pushl  -0xc(%ebp)
801056d6:	e8 75 e3 ff ff       	call   80103a50 <growproc>
801056db:	83 c4 10             	add    $0x10,%esp
801056de:	85 c0                	test   %eax,%eax
801056e0:	78 0e                	js     801056f0 <sys_sbrk+0x40>
    return -1;
  return addr;
}
801056e2:	89 d8                	mov    %ebx,%eax
801056e4:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801056e7:	c9                   	leave  
801056e8:	c3                   	ret    
801056e9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    return -1;
801056f0:	bb ff ff ff ff       	mov    $0xffffffff,%ebx
801056f5:	eb eb                	jmp    801056e2 <sys_sbrk+0x32>
801056f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801056fe:	66 90                	xchg   %ax,%ax

80105700 <sys_shmget>:

int
sys_shmget(void)
{
80105700:	55                   	push   %ebp
80105701:	89 e5                	mov    %esp,%ebp
80105703:	83 ec 20             	sub    $0x20,%esp
	int key;

	if (argint(0, &key) < 0)
80105706:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105709:	50                   	push   %eax
8010570a:	6a 00                	push   $0x0
8010570c:	e8 1f f2 ff ff       	call   80104930 <argint>
80105711:	83 c4 10             	add    $0x10,%esp
80105714:	85 c0                	test   %eax,%eax
80105716:	78 18                	js     80105730 <sys_shmget+0x30>
		return -1;
	return shmget(key);
80105718:	83 ec 0c             	sub    $0xc,%esp
8010571b:	ff 75 f4             	pushl  -0xc(%ebp)
8010571e:	e8 dd 1e 00 00       	call   80107600 <shmget>
80105723:	83 c4 10             	add    $0x10,%esp
}
80105726:	c9                   	leave  
80105727:	c3                   	ret    
80105728:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010572f:	90                   	nop
80105730:	c9                   	leave  
		return -1;
80105731:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105736:	c3                   	ret    
80105737:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010573e:	66 90                	xchg   %ax,%ax

80105740 <sys_sleep>:

int
sys_sleep(void)
{
80105740:	55                   	push   %ebp
80105741:	89 e5                	mov    %esp,%ebp
80105743:	53                   	push   %ebx
  int n;
  uint ticks0;

  if(argint(0, &n) < 0)
80105744:	8d 45 f4             	lea    -0xc(%ebp),%eax
{
80105747:	83 ec 1c             	sub    $0x1c,%esp
  if(argint(0, &n) < 0)
8010574a:	50                   	push   %eax
8010574b:	6a 00                	push   $0x0
8010574d:	e8 de f1 ff ff       	call   80104930 <argint>
80105752:	83 c4 10             	add    $0x10,%esp
80105755:	85 c0                	test   %eax,%eax
80105757:	0f 88 8a 00 00 00    	js     801057e7 <sys_sleep+0xa7>
    return -1;
  acquire(&tickslock);
8010575d:	83 ec 0c             	sub    $0xc,%esp
80105760:	68 20 6d 11 80       	push   $0x80116d20
80105765:	e8 d6 ed ff ff       	call   80104540 <acquire>
  ticks0 = ticks;
  while(ticks - ticks0 < n){
8010576a:	8b 55 f4             	mov    -0xc(%ebp),%edx
  ticks0 = ticks;
8010576d:	8b 1d 60 75 11 80    	mov    0x80117560,%ebx
  while(ticks - ticks0 < n){
80105773:	83 c4 10             	add    $0x10,%esp
80105776:	85 d2                	test   %edx,%edx
80105778:	75 27                	jne    801057a1 <sys_sleep+0x61>
8010577a:	eb 54                	jmp    801057d0 <sys_sleep+0x90>
8010577c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
80105780:	83 ec 08             	sub    $0x8,%esp
80105783:	68 20 6d 11 80       	push   $0x80116d20
80105788:	68 60 75 11 80       	push   $0x80117560
8010578d:	e8 9e e7 ff ff       	call   80103f30 <sleep>
  while(ticks - ticks0 < n){
80105792:	a1 60 75 11 80       	mov    0x80117560,%eax
80105797:	83 c4 10             	add    $0x10,%esp
8010579a:	29 d8                	sub    %ebx,%eax
8010579c:	3b 45 f4             	cmp    -0xc(%ebp),%eax
8010579f:	73 2f                	jae    801057d0 <sys_sleep+0x90>
    if(myproc()->killed){
801057a1:	e8 7a e1 ff ff       	call   80103920 <myproc>
801057a6:	8b 40 24             	mov    0x24(%eax),%eax
801057a9:	85 c0                	test   %eax,%eax
801057ab:	74 d3                	je     80105780 <sys_sleep+0x40>
      release(&tickslock);
801057ad:	83 ec 0c             	sub    $0xc,%esp
801057b0:	68 20 6d 11 80       	push   $0x80116d20
801057b5:	e8 46 ee ff ff       	call   80104600 <release>
      return -1;
801057ba:	83 c4 10             	add    $0x10,%esp
801057bd:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
  }
  release(&tickslock);
  return 0;
}
801057c2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801057c5:	c9                   	leave  
801057c6:	c3                   	ret    
801057c7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801057ce:	66 90                	xchg   %ax,%ax
  release(&tickslock);
801057d0:	83 ec 0c             	sub    $0xc,%esp
801057d3:	68 20 6d 11 80       	push   $0x80116d20
801057d8:	e8 23 ee ff ff       	call   80104600 <release>
  return 0;
801057dd:	83 c4 10             	add    $0x10,%esp
801057e0:	31 c0                	xor    %eax,%eax
}
801057e2:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801057e5:	c9                   	leave  
801057e6:	c3                   	ret    
    return -1;
801057e7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801057ec:	eb f4                	jmp    801057e2 <sys_sleep+0xa2>
801057ee:	66 90                	xchg   %ax,%ax

801057f0 <sys_uptime>:

// return how many clock tick interrupts have occurred
// since start.
int
sys_uptime(void)
{
801057f0:	55                   	push   %ebp
801057f1:	89 e5                	mov    %esp,%ebp
801057f3:	53                   	push   %ebx
801057f4:	83 ec 10             	sub    $0x10,%esp
  uint xticks;

  acquire(&tickslock);
801057f7:	68 20 6d 11 80       	push   $0x80116d20
801057fc:	e8 3f ed ff ff       	call   80104540 <acquire>
  xticks = ticks;
80105801:	8b 1d 60 75 11 80    	mov    0x80117560,%ebx
  release(&tickslock);
80105807:	c7 04 24 20 6d 11 80 	movl   $0x80116d20,(%esp)
8010580e:	e8 ed ed ff ff       	call   80104600 <release>
  return xticks;
}
80105813:	89 d8                	mov    %ebx,%eax
80105815:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80105818:	c9                   	leave  
80105819:	c3                   	ret    
8010581a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80105820 <sys_semget>:
//////////////////
int
sys_semget(void)
{
80105820:	55                   	push   %ebp
80105821:	89 e5                	mov    %esp,%ebp
80105823:	83 ec 20             	sub    $0x20,%esp
  int key;
  int init_value;
  if (argint(0, &key) < 0)
80105826:	8d 45 f0             	lea    -0x10(%ebp),%eax
80105829:	50                   	push   %eax
8010582a:	6a 00                	push   $0x0
8010582c:	e8 ff f0 ff ff       	call   80104930 <argint>
80105831:	83 c4 10             	add    $0x10,%esp
80105834:	85 c0                	test   %eax,%eax
80105836:	78 28                	js     80105860 <sys_semget+0x40>
    return -1;
  if (argint(1, &init_value) < 0)
80105838:	83 ec 08             	sub    $0x8,%esp
8010583b:	8d 45 f4             	lea    -0xc(%ebp),%eax
8010583e:	50                   	push   %eax
8010583f:	6a 01                	push   $0x1
80105841:	e8 ea f0 ff ff       	call   80104930 <argint>
80105846:	83 c4 10             	add    $0x10,%esp
80105849:	85 c0                	test   %eax,%eax
8010584b:	78 13                	js     80105860 <sys_semget+0x40>
    return -1;

  return semget(key, init_value);
8010584d:	83 ec 08             	sub    $0x8,%esp
80105850:	ff 75 f4             	pushl  -0xc(%ebp)
80105853:	ff 75 f0             	pushl  -0x10(%ebp)
80105856:	e8 f5 1a 00 00       	call   80107350 <semget>
8010585b:	83 c4 10             	add    $0x10,%esp
}
8010585e:	c9                   	leave  
8010585f:	c3                   	ret    
80105860:	c9                   	leave  
    return -1;
80105861:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105866:	c3                   	ret    
80105867:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010586e:	66 90                	xchg   %ax,%ax

80105870 <sys_semclose>:

int
sys_semclose(void)
{
80105870:	55                   	push   %ebp
80105871:	89 e5                	mov    %esp,%ebp
80105873:	83 ec 20             	sub    $0x20,%esp
  int semid;

  if (argint(0, &semid) < 0)
80105876:	8d 45 f4             	lea    -0xc(%ebp),%eax
80105879:	50                   	push   %eax
8010587a:	6a 00                	push   $0x0
8010587c:	e8 af f0 ff ff       	call   80104930 <argint>
80105881:	83 c4 10             	add    $0x10,%esp
80105884:	85 c0                	test   %eax,%eax
80105886:	78 18                	js     801058a0 <sys_semclose+0x30>
    return -1;
  return semclose(semid);
80105888:	83 ec 0c             	sub    $0xc,%esp
8010588b:	ff 75 f4             	pushl  -0xc(%ebp)
8010588e:	e8 dd 1c 00 00       	call   80107570 <semclose>
80105893:	83 c4 10             	add    $0x10,%esp
}
80105896:	c9                   	leave  
80105897:	c3                   	ret    
80105898:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010589f:	90                   	nop
801058a0:	c9                   	leave  
    return -1;
801058a1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801058a6:	c3                   	ret    
801058a7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801058ae:	66 90                	xchg   %ax,%ax

801058b0 <sys_semdown>:

int
sys_semdown(void)
{
801058b0:	55                   	push   %ebp
801058b1:	89 e5                	mov    %esp,%ebp
801058b3:	83 ec 20             	sub    $0x20,%esp
  int semid;

  if (argint(0, &semid) < 0)
801058b6:	8d 45 f4             	lea    -0xc(%ebp),%eax
801058b9:	50                   	push   %eax
801058ba:	6a 00                	push   $0x0
801058bc:	e8 6f f0 ff ff       	call   80104930 <argint>
801058c1:	83 c4 10             	add    $0x10,%esp
801058c4:	85 c0                	test   %eax,%eax
801058c6:	78 18                	js     801058e0 <sys_semdown+0x30>
    return -1;
  return semdown(semid);
801058c8:	83 ec 0c             	sub    $0xc,%esp
801058cb:	ff 75 f4             	pushl  -0xc(%ebp)
801058ce:	e8 8d 1b 00 00       	call   80107460 <semdown>
801058d3:	83 c4 10             	add    $0x10,%esp
}
801058d6:	c9                   	leave  
801058d7:	c3                   	ret    
801058d8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801058df:	90                   	nop
801058e0:	c9                   	leave  
    return -1;
801058e1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801058e6:	c3                   	ret    
801058e7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801058ee:	66 90                	xchg   %ax,%ax

801058f0 <sys_semup>:

int
sys_semup(void)
{
801058f0:	55                   	push   %ebp
801058f1:	89 e5                	mov    %esp,%ebp
801058f3:	83 ec 20             	sub    $0x20,%esp
  int key;

  if (argint(0, &key) < 0)
801058f6:	8d 45 f4             	lea    -0xc(%ebp),%eax
801058f9:	50                   	push   %eax
801058fa:	6a 00                	push   $0x0
801058fc:	e8 2f f0 ff ff       	call   80104930 <argint>
80105901:	83 c4 10             	add    $0x10,%esp
80105904:	85 c0                	test   %eax,%eax
80105906:	78 18                	js     80105920 <sys_semup+0x30>
    return -1;
  return semup(key);
80105908:	83 ec 0c             	sub    $0xc,%esp
8010590b:	ff 75 f4             	pushl  -0xc(%ebp)
8010590e:	e8 ed 1b 00 00       	call   80107500 <semup>
80105913:	83 c4 10             	add    $0x10,%esp
}
80105916:	c9                   	leave  
80105917:	c3                   	ret    
80105918:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010591f:	90                   	nop
80105920:	c9                   	leave  
    return -1;
80105921:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105926:	c3                   	ret    

80105927 <alltraps>:

  # vectors.S sends all traps here.
.globl alltraps
alltraps:
  # Build trap frame.
  pushl %ds
80105927:	1e                   	push   %ds
  pushl %es
80105928:	06                   	push   %es
  pushl %fs
80105929:	0f a0                	push   %fs
  pushl %gs
8010592b:	0f a8                	push   %gs
  pushal
8010592d:	60                   	pusha  
  
  # Set up data segments.
  movw $(SEG_KDATA<<3), %ax
8010592e:	66 b8 10 00          	mov    $0x10,%ax
  movw %ax, %ds
80105932:	8e d8                	mov    %eax,%ds
  movw %ax, %es
80105934:	8e c0                	mov    %eax,%es

  # Call trap(tf), where tf=%esp
  pushl %esp
80105936:	54                   	push   %esp
  call trap
80105937:	e8 c4 00 00 00       	call   80105a00 <trap>
  addl $4, %esp
8010593c:	83 c4 04             	add    $0x4,%esp

8010593f <trapret>:

  # Return falls through to trapret...
.globl trapret
trapret:
  popal
8010593f:	61                   	popa   
  popl %gs
80105940:	0f a9                	pop    %gs
  popl %fs
80105942:	0f a1                	pop    %fs
  popl %es
80105944:	07                   	pop    %es
  popl %ds
80105945:	1f                   	pop    %ds
  addl $0x8, %esp  # trapno and errcode
80105946:	83 c4 08             	add    $0x8,%esp
  iret
80105949:	cf                   	iret   
8010594a:	66 90                	xchg   %ax,%ax
8010594c:	66 90                	xchg   %ax,%ax
8010594e:	66 90                	xchg   %ax,%ax

80105950 <tvinit>:
struct spinlock tickslock;
uint ticks;

void
tvinit(void)
{
80105950:	55                   	push   %ebp
  int i;

  for(i = 0; i < 256; i++)
80105951:	31 c0                	xor    %eax,%eax
{
80105953:	89 e5                	mov    %esp,%ebp
80105955:	83 ec 08             	sub    $0x8,%esp
80105958:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010595f:	90                   	nop
    SETGATE(idt[i], 0, SEG_KCODE<<3, vectors[i], 0);
80105960:	8b 14 85 08 b0 10 80 	mov    -0x7fef4ff8(,%eax,4),%edx
80105967:	c7 04 c5 62 6d 11 80 	movl   $0x8e000008,-0x7fee929e(,%eax,8)
8010596e:	08 00 00 8e 
80105972:	66 89 14 c5 60 6d 11 	mov    %dx,-0x7fee92a0(,%eax,8)
80105979:	80 
8010597a:	c1 ea 10             	shr    $0x10,%edx
8010597d:	66 89 14 c5 66 6d 11 	mov    %dx,-0x7fee929a(,%eax,8)
80105984:	80 
  for(i = 0; i < 256; i++)
80105985:	83 c0 01             	add    $0x1,%eax
80105988:	3d 00 01 00 00       	cmp    $0x100,%eax
8010598d:	75 d1                	jne    80105960 <tvinit+0x10>
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);

  initlock(&tickslock, "time");
8010598f:	83 ec 08             	sub    $0x8,%esp
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
80105992:	a1 08 b1 10 80       	mov    0x8010b108,%eax
80105997:	c7 05 62 6f 11 80 08 	movl   $0xef000008,0x80116f62
8010599e:	00 00 ef 
  initlock(&tickslock, "time");
801059a1:	68 2d 80 10 80       	push   $0x8010802d
801059a6:	68 20 6d 11 80       	push   $0x80116d20
  SETGATE(idt[T_SYSCALL], 1, SEG_KCODE<<3, vectors[T_SYSCALL], DPL_USER);
801059ab:	66 a3 60 6f 11 80    	mov    %ax,0x80116f60
801059b1:	c1 e8 10             	shr    $0x10,%eax
801059b4:	66 a3 66 6f 11 80    	mov    %ax,0x80116f66
  initlock(&tickslock, "time");
801059ba:	e8 21 ea ff ff       	call   801043e0 <initlock>
}
801059bf:	83 c4 10             	add    $0x10,%esp
801059c2:	c9                   	leave  
801059c3:	c3                   	ret    
801059c4:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801059cb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801059cf:	90                   	nop

801059d0 <idtinit>:

void
idtinit(void)
{
801059d0:	55                   	push   %ebp
  pd[0] = size-1;
801059d1:	b8 ff 07 00 00       	mov    $0x7ff,%eax
801059d6:	89 e5                	mov    %esp,%ebp
801059d8:	83 ec 10             	sub    $0x10,%esp
801059db:	66 89 45 fa          	mov    %ax,-0x6(%ebp)
  pd[1] = (uint)p;
801059df:	b8 60 6d 11 80       	mov    $0x80116d60,%eax
801059e4:	66 89 45 fc          	mov    %ax,-0x4(%ebp)
  pd[2] = (uint)p >> 16;
801059e8:	c1 e8 10             	shr    $0x10,%eax
801059eb:	66 89 45 fe          	mov    %ax,-0x2(%ebp)
  asm volatile("lidt (%0)" : : "r" (pd));
801059ef:	8d 45 fa             	lea    -0x6(%ebp),%eax
801059f2:	0f 01 18             	lidtl  (%eax)
  lidt(idt, sizeof(idt));
}
801059f5:	c9                   	leave  
801059f6:	c3                   	ret    
801059f7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801059fe:	66 90                	xchg   %ax,%ax

80105a00 <trap>:

//PAGEBREAK: 41
void
trap(struct trapframe *tf)
{
80105a00:	55                   	push   %ebp
80105a01:	89 e5                	mov    %esp,%ebp
80105a03:	57                   	push   %edi
80105a04:	56                   	push   %esi
80105a05:	53                   	push   %ebx
80105a06:	83 ec 1c             	sub    $0x1c,%esp
80105a09:	8b 7d 08             	mov    0x8(%ebp),%edi
  if(tf->trapno == T_SYSCALL){
80105a0c:	8b 47 30             	mov    0x30(%edi),%eax
80105a0f:	83 f8 40             	cmp    $0x40,%eax
80105a12:	0f 84 60 02 00 00    	je     80105c78 <trap+0x278>
    if(myproc()->killed)
      exit();
    return;
  }

  switch(tf->trapno){
80105a18:	83 e8 0e             	sub    $0xe,%eax
80105a1b:	83 f8 31             	cmp    $0x31,%eax
80105a1e:	0f 87 cc 01 00 00    	ja     80105bf0 <trap+0x1f0>
80105a24:	ff 24 85 f4 80 10 80 	jmp    *-0x7fef7f0c(,%eax,4)
80105a2b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105a2f:	90                   	nop
            cpuid(), tf->cs, tf->eip);
    lapiceoi();
    break;

  case T_PGFLT:
    if (myproc() != 0){ // en user
80105a30:	e8 eb de ff ff       	call   80103920 <myproc>
80105a35:	85 c0                	test   %eax,%eax
80105a37:	0f 84 b3 01 00 00    	je     80105bf0 <trap+0x1f0>
        int init_size = myproc()->sz - (PGSIZE*NPAG);
80105a3d:	e8 de de ff ff       	call   80103920 <myproc>
80105a42:	8b 18                	mov    (%eax),%ebx
80105a44:	81 eb 00 40 00 00    	sub    $0x4000,%ebx

static inline uint
rcr2(void)
{
  uint val;
  asm volatile("movl %%cr2,%0" : "=r" (val));
80105a4a:	0f 20 d6             	mov    %cr2,%esi
        int pg_fault = rcr2(); 
        if (pg_fault >  myproc() -> sz || pg_fault < init_size){      
80105a4d:	e8 ce de ff ff       	call   80103920 <myproc>
80105a52:	39 30                	cmp    %esi,(%eax)
80105a54:	0f 82 56 02 00 00    	jb     80105cb0 <trap+0x2b0>
80105a5a:	39 f3                	cmp    %esi,%ebx
80105a5c:	0f 8f 4e 02 00 00    	jg     80105cb0 <trap+0x2b0>
80105a62:	0f 20 d3             	mov    %cr2,%ebx
80105a65:	0f 20 d6             	mov    %cr2,%esi
	      cprintf("*Stack Overflow\n");
	      myproc()->killed = 1;
        }else{
          if (allocuvm(myproc()->pgdir, PGROUNDDOWN(rcr2()), PGROUNDUP(rcr2())) != 0){
80105a68:	81 c3 ff 0f 00 00    	add    $0xfff,%ebx
80105a6e:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
80105a74:	e8 a7 de ff ff       	call   80103920 <myproc>
80105a79:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
80105a7f:	83 ec 04             	sub    $0x4,%esp
80105a82:	53                   	push   %ebx
80105a83:	56                   	push   %esi
80105a84:	ff 70 04             	pushl  0x4(%eax)
80105a87:	e8 04 14 00 00       	call   80106e90 <allocuvm>
80105a8c:	83 c4 10             	add    $0x10,%esp
80105a8f:	85 c0                	test   %eax,%eax
80105a91:	0f 84 19 02 00 00    	je     80105cb0 <trap+0x2b0>
          	cprintf("Nueva pagina\n");
80105a97:	83 ec 0c             	sub    $0xc,%esp
80105a9a:	68 43 80 10 80       	push   $0x80108043
80105a9f:	e8 0c ac ff ff       	call   801006b0 <cprintf>
80105aa4:	83 c4 10             	add    $0x10,%esp
80105aa7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105aae:	66 90                	xchg   %ax,%ax
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running
  // until it gets to the regular system call return.)
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105ab0:	e8 6b de ff ff       	call   80103920 <myproc>
80105ab5:	85 c0                	test   %eax,%eax
80105ab7:	74 24                	je     80105add <trap+0xdd>
80105ab9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105ac0:	e8 5b de ff ff       	call   80103920 <myproc>
80105ac5:	8b 50 24             	mov    0x24(%eax),%edx
80105ac8:	85 d2                	test   %edx,%edx
80105aca:	74 11                	je     80105add <trap+0xdd>
80105acc:	0f b7 47 3c          	movzwl 0x3c(%edi),%eax
80105ad0:	83 e0 03             	and    $0x3,%eax
80105ad3:	66 83 f8 03          	cmp    $0x3,%ax
80105ad7:	0f 84 fb 01 00 00    	je     80105cd8 <trap+0x2d8>
    exit();

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if(myproc() && myproc()->state == RUNNING && tf->trapno == T_IRQ0+IRQ_TIMER) { 
80105add:	e8 3e de ff ff       	call   80103920 <myproc>
80105ae2:	85 c0                	test   %eax,%eax
80105ae4:	74 0b                	je     80105af1 <trap+0xf1>
80105ae6:	e8 35 de ff ff       	call   80103920 <myproc>
80105aeb:	83 78 0c 04          	cmpl   $0x4,0xc(%eax)
80105aef:	74 2f                	je     80105b20 <trap+0x120>
  	  else
		myproc()->ticks++;
  }

  // Check if the process has been killed since we yielded
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105af1:	e8 2a de ff ff       	call   80103920 <myproc>
80105af6:	85 c0                	test   %eax,%eax
80105af8:	74 1d                	je     80105b17 <trap+0x117>
80105afa:	e8 21 de ff ff       	call   80103920 <myproc>
80105aff:	8b 40 24             	mov    0x24(%eax),%eax
80105b02:	85 c0                	test   %eax,%eax
80105b04:	74 11                	je     80105b17 <trap+0x117>
80105b06:	0f b7 47 3c          	movzwl 0x3c(%edi),%eax
80105b0a:	83 e0 03             	and    $0x3,%eax
80105b0d:	66 83 f8 03          	cmp    $0x3,%ax
80105b11:	0f 84 8a 01 00 00    	je     80105ca1 <trap+0x2a1>
    exit();
}
80105b17:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105b1a:	5b                   	pop    %ebx
80105b1b:	5e                   	pop    %esi
80105b1c:	5f                   	pop    %edi
80105b1d:	5d                   	pop    %ebp
80105b1e:	c3                   	ret    
80105b1f:	90                   	nop
  if(myproc() && myproc()->state == RUNNING && tf->trapno == T_IRQ0+IRQ_TIMER) { 
80105b20:	83 7f 30 20          	cmpl   $0x20,0x30(%edi)
80105b24:	75 cb                	jne    80105af1 <trap+0xf1>
	  if (myproc()->ticks > QUANTUM) {
80105b26:	e8 f5 dd ff ff       	call   80103920 <myproc>
80105b2b:	83 b8 a8 00 00 00 03 	cmpl   $0x3,0xa8(%eax)
80105b32:	0f 8e f0 01 00 00    	jle    80105d28 <trap+0x328>
		  yield();
80105b38:	e8 a3 e3 ff ff       	call   80103ee0 <yield>
80105b3d:	eb b2                	jmp    80105af1 <trap+0xf1>
80105b3f:	90                   	nop
    if(cpuid() == 0){
80105b40:	e8 bb dd ff ff       	call   80103900 <cpuid>
80105b45:	85 c0                	test   %eax,%eax
80105b47:	0f 84 a3 01 00 00    	je     80105cf0 <trap+0x2f0>
    lapiceoi();
80105b4d:	e8 2e cd ff ff       	call   80102880 <lapiceoi>
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105b52:	e8 c9 dd ff ff       	call   80103920 <myproc>
80105b57:	85 c0                	test   %eax,%eax
80105b59:	0f 85 61 ff ff ff    	jne    80105ac0 <trap+0xc0>
80105b5f:	e9 79 ff ff ff       	jmp    80105add <trap+0xdd>
80105b64:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    uartintr();
80105b68:	e8 73 03 00 00       	call   80105ee0 <uartintr>
    lapiceoi();
80105b6d:	e8 0e cd ff ff       	call   80102880 <lapiceoi>
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105b72:	e8 a9 dd ff ff       	call   80103920 <myproc>
80105b77:	85 c0                	test   %eax,%eax
80105b79:	0f 85 41 ff ff ff    	jne    80105ac0 <trap+0xc0>
80105b7f:	e9 59 ff ff ff       	jmp    80105add <trap+0xdd>
80105b84:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    cprintf("cpu%d: spurious interrupt at %x:%x\n",
80105b88:	8b 77 38             	mov    0x38(%edi),%esi
80105b8b:	0f b7 5f 3c          	movzwl 0x3c(%edi),%ebx
80105b8f:	e8 6c dd ff ff       	call   80103900 <cpuid>
80105b94:	56                   	push   %esi
80105b95:	53                   	push   %ebx
80105b96:	50                   	push   %eax
80105b97:	68 58 80 10 80       	push   $0x80108058
80105b9c:	e8 0f ab ff ff       	call   801006b0 <cprintf>
    lapiceoi();
80105ba1:	e8 da cc ff ff       	call   80102880 <lapiceoi>
    break;
80105ba6:	83 c4 10             	add    $0x10,%esp
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105ba9:	e8 72 dd ff ff       	call   80103920 <myproc>
80105bae:	85 c0                	test   %eax,%eax
80105bb0:	0f 85 0a ff ff ff    	jne    80105ac0 <trap+0xc0>
80105bb6:	e9 22 ff ff ff       	jmp    80105add <trap+0xdd>
80105bbb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105bbf:	90                   	nop
    ideintr();
80105bc0:	e8 cb c5 ff ff       	call   80102190 <ideintr>
80105bc5:	eb 86                	jmp    80105b4d <trap+0x14d>
80105bc7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105bce:	66 90                	xchg   %ax,%ax
    kbdintr();
80105bd0:	e8 6b cb ff ff       	call   80102740 <kbdintr>
    lapiceoi();
80105bd5:	e8 a6 cc ff ff       	call   80102880 <lapiceoi>
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105bda:	e8 41 dd ff ff       	call   80103920 <myproc>
80105bdf:	85 c0                	test   %eax,%eax
80105be1:	0f 85 d9 fe ff ff    	jne    80105ac0 <trap+0xc0>
80105be7:	e9 f1 fe ff ff       	jmp    80105add <trap+0xdd>
80105bec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    if(myproc() == 0 || (tf->cs&3) == 0){
80105bf0:	e8 2b dd ff ff       	call   80103920 <myproc>
80105bf5:	8b 5f 38             	mov    0x38(%edi),%ebx
80105bf8:	85 c0                	test   %eax,%eax
80105bfa:	0f 84 39 01 00 00    	je     80105d39 <trap+0x339>
80105c00:	f6 47 3c 03          	testb  $0x3,0x3c(%edi)
80105c04:	0f 84 2f 01 00 00    	je     80105d39 <trap+0x339>
80105c0a:	0f 20 d1             	mov    %cr2,%ecx
80105c0d:	89 4d d8             	mov    %ecx,-0x28(%ebp)
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105c10:	e8 eb dc ff ff       	call   80103900 <cpuid>
80105c15:	8b 77 30             	mov    0x30(%edi),%esi
80105c18:	89 45 dc             	mov    %eax,-0x24(%ebp)
80105c1b:	8b 47 34             	mov    0x34(%edi),%eax
80105c1e:	89 45 e4             	mov    %eax,-0x1c(%ebp)
            myproc()->pid, myproc()->name, tf->trapno,
80105c21:	e8 fa dc ff ff       	call   80103920 <myproc>
80105c26:	89 45 e0             	mov    %eax,-0x20(%ebp)
80105c29:	e8 f2 dc ff ff       	call   80103920 <myproc>
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105c2e:	8b 4d d8             	mov    -0x28(%ebp),%ecx
80105c31:	8b 55 dc             	mov    -0x24(%ebp),%edx
80105c34:	51                   	push   %ecx
80105c35:	53                   	push   %ebx
80105c36:	52                   	push   %edx
            myproc()->pid, myproc()->name, tf->trapno,
80105c37:	8b 55 e0             	mov    -0x20(%ebp),%edx
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105c3a:	ff 75 e4             	pushl  -0x1c(%ebp)
            myproc()->pid, myproc()->name, tf->trapno,
80105c3d:	81 c2 ac 00 00 00    	add    $0xac,%edx
    cprintf("pid %d %s: trap %d err %d on cpu %d "
80105c43:	56                   	push   %esi
80105c44:	52                   	push   %edx
80105c45:	ff 70 10             	pushl  0x10(%eax)
80105c48:	68 b0 80 10 80       	push   $0x801080b0
80105c4d:	e8 5e aa ff ff       	call   801006b0 <cprintf>
    myproc()->killed = 1;
80105c52:	83 c4 20             	add    $0x20,%esp
80105c55:	e8 c6 dc ff ff       	call   80103920 <myproc>
80105c5a:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
  if(myproc() && myproc()->killed && (tf->cs&3) == DPL_USER)
80105c61:	e8 ba dc ff ff       	call   80103920 <myproc>
80105c66:	85 c0                	test   %eax,%eax
80105c68:	0f 85 52 fe ff ff    	jne    80105ac0 <trap+0xc0>
80105c6e:	e9 6a fe ff ff       	jmp    80105add <trap+0xdd>
80105c73:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105c77:	90                   	nop
    if(myproc()->killed)
80105c78:	e8 a3 dc ff ff       	call   80103920 <myproc>
80105c7d:	8b 58 24             	mov    0x24(%eax),%ebx
80105c80:	85 db                	test   %ebx,%ebx
80105c82:	75 64                	jne    80105ce8 <trap+0x2e8>
    myproc()->tf = tf;
80105c84:	e8 97 dc ff ff       	call   80103920 <myproc>
80105c89:	89 78 18             	mov    %edi,0x18(%eax)
    syscall();
80105c8c:	e8 8f ed ff ff       	call   80104a20 <syscall>
    if(myproc()->killed)
80105c91:	e8 8a dc ff ff       	call   80103920 <myproc>
80105c96:	8b 48 24             	mov    0x24(%eax),%ecx
80105c99:	85 c9                	test   %ecx,%ecx
80105c9b:	0f 84 76 fe ff ff    	je     80105b17 <trap+0x117>
}
80105ca1:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105ca4:	5b                   	pop    %ebx
80105ca5:	5e                   	pop    %esi
80105ca6:	5f                   	pop    %edi
80105ca7:	5d                   	pop    %ebp
      exit();
80105ca8:	e9 b3 e0 ff ff       	jmp    80103d60 <exit>
80105cad:	8d 76 00             	lea    0x0(%esi),%esi
		  	cprintf("*Stack Overflow\n");
80105cb0:	83 ec 0c             	sub    $0xc,%esp
80105cb3:	68 32 80 10 80       	push   $0x80108032
80105cb8:	e8 f3 a9 ff ff       	call   801006b0 <cprintf>
      		myproc()->killed = 1;          	
80105cbd:	e8 5e dc ff ff       	call   80103920 <myproc>
80105cc2:	83 c4 10             	add    $0x10,%esp
80105cc5:	c7 40 24 01 00 00 00 	movl   $0x1,0x24(%eax)
80105ccc:	e9 df fd ff ff       	jmp    80105ab0 <trap+0xb0>
80105cd1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
    exit();
80105cd8:	e8 83 e0 ff ff       	call   80103d60 <exit>
80105cdd:	e9 fb fd ff ff       	jmp    80105add <trap+0xdd>
80105ce2:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      exit();
80105ce8:	e8 73 e0 ff ff       	call   80103d60 <exit>
80105ced:	eb 95                	jmp    80105c84 <trap+0x284>
80105cef:	90                   	nop
      acquire(&tickslock);
80105cf0:	83 ec 0c             	sub    $0xc,%esp
80105cf3:	68 20 6d 11 80       	push   $0x80116d20
80105cf8:	e8 43 e8 ff ff       	call   80104540 <acquire>
      wakeup(&ticks);
80105cfd:	c7 04 24 60 75 11 80 	movl   $0x80117560,(%esp)
      ticks++;
80105d04:	83 05 60 75 11 80 01 	addl   $0x1,0x80117560
      wakeup(&ticks);
80105d0b:	e8 e0 e3 ff ff       	call   801040f0 <wakeup>
      release(&tickslock);
80105d10:	c7 04 24 20 6d 11 80 	movl   $0x80116d20,(%esp)
80105d17:	e8 e4 e8 ff ff       	call   80104600 <release>
80105d1c:	83 c4 10             	add    $0x10,%esp
    lapiceoi();
80105d1f:	e9 29 fe ff ff       	jmp    80105b4d <trap+0x14d>
80105d24:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
		myproc()->ticks++;
80105d28:	e8 f3 db ff ff       	call   80103920 <myproc>
80105d2d:	83 80 a8 00 00 00 01 	addl   $0x1,0xa8(%eax)
80105d34:	e9 b8 fd ff ff       	jmp    80105af1 <trap+0xf1>
80105d39:	0f 20 d6             	mov    %cr2,%esi
      cprintf("unexpected trap %d from cpu %d eip %x (cr2=0x%x)\n",
80105d3c:	e8 bf db ff ff       	call   80103900 <cpuid>
80105d41:	83 ec 0c             	sub    $0xc,%esp
80105d44:	56                   	push   %esi
80105d45:	53                   	push   %ebx
80105d46:	50                   	push   %eax
80105d47:	ff 77 30             	pushl  0x30(%edi)
80105d4a:	68 7c 80 10 80       	push   $0x8010807c
80105d4f:	e8 5c a9 ff ff       	call   801006b0 <cprintf>
      panic("trap");
80105d54:	83 c4 14             	add    $0x14,%esp
80105d57:	68 51 80 10 80       	push   $0x80108051
80105d5c:	e8 2f a6 ff ff       	call   80100390 <panic>
80105d61:	66 90                	xchg   %ax,%ax
80105d63:	66 90                	xchg   %ax,%ax
80105d65:	66 90                	xchg   %ax,%ax
80105d67:	66 90                	xchg   %ax,%ax
80105d69:	66 90                	xchg   %ax,%ax
80105d6b:	66 90                	xchg   %ax,%ax
80105d6d:	66 90                	xchg   %ax,%ax
80105d6f:	90                   	nop

80105d70 <uartgetc>:
}

static int
uartgetc(void)
{
  if(!uart)
80105d70:	a1 bc b5 10 80       	mov    0x8010b5bc,%eax
80105d75:	85 c0                	test   %eax,%eax
80105d77:	74 17                	je     80105d90 <uartgetc+0x20>
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80105d79:	ba fd 03 00 00       	mov    $0x3fd,%edx
80105d7e:	ec                   	in     (%dx),%al
    return -1;
  if(!(inb(COM1+5) & 0x01))
80105d7f:	a8 01                	test   $0x1,%al
80105d81:	74 0d                	je     80105d90 <uartgetc+0x20>
80105d83:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105d88:	ec                   	in     (%dx),%al
    return -1;
  return inb(COM1+0);
80105d89:	0f b6 c0             	movzbl %al,%eax
80105d8c:	c3                   	ret    
80105d8d:	8d 76 00             	lea    0x0(%esi),%esi
    return -1;
80105d90:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80105d95:	c3                   	ret    
80105d96:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105d9d:	8d 76 00             	lea    0x0(%esi),%esi

80105da0 <uartputc.part.0>:
uartputc(int c)
80105da0:	55                   	push   %ebp
80105da1:	89 e5                	mov    %esp,%ebp
80105da3:	57                   	push   %edi
80105da4:	89 c7                	mov    %eax,%edi
80105da6:	56                   	push   %esi
80105da7:	be fd 03 00 00       	mov    $0x3fd,%esi
80105dac:	53                   	push   %ebx
80105dad:	bb 80 00 00 00       	mov    $0x80,%ebx
80105db2:	83 ec 0c             	sub    $0xc,%esp
80105db5:	eb 1b                	jmp    80105dd2 <uartputc.part.0+0x32>
80105db7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105dbe:	66 90                	xchg   %ax,%ax
    microdelay(10);
80105dc0:	83 ec 0c             	sub    $0xc,%esp
80105dc3:	6a 0a                	push   $0xa
80105dc5:	e8 d6 ca ff ff       	call   801028a0 <microdelay>
  for(i = 0; i < 128 && !(inb(COM1+5) & 0x20); i++)
80105dca:	83 c4 10             	add    $0x10,%esp
80105dcd:	83 eb 01             	sub    $0x1,%ebx
80105dd0:	74 07                	je     80105dd9 <uartputc.part.0+0x39>
80105dd2:	89 f2                	mov    %esi,%edx
80105dd4:	ec                   	in     (%dx),%al
80105dd5:	a8 20                	test   $0x20,%al
80105dd7:	74 e7                	je     80105dc0 <uartputc.part.0+0x20>
  asm volatile("out %0,%1" : : "a" (data), "d" (port));
80105dd9:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105dde:	89 f8                	mov    %edi,%eax
80105de0:	ee                   	out    %al,(%dx)
}
80105de1:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105de4:	5b                   	pop    %ebx
80105de5:	5e                   	pop    %esi
80105de6:	5f                   	pop    %edi
80105de7:	5d                   	pop    %ebp
80105de8:	c3                   	ret    
80105de9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105df0 <uartinit>:
{
80105df0:	55                   	push   %ebp
80105df1:	31 c9                	xor    %ecx,%ecx
80105df3:	89 c8                	mov    %ecx,%eax
80105df5:	89 e5                	mov    %esp,%ebp
80105df7:	57                   	push   %edi
80105df8:	56                   	push   %esi
80105df9:	53                   	push   %ebx
80105dfa:	bb fa 03 00 00       	mov    $0x3fa,%ebx
80105dff:	89 da                	mov    %ebx,%edx
80105e01:	83 ec 0c             	sub    $0xc,%esp
80105e04:	ee                   	out    %al,(%dx)
80105e05:	bf fb 03 00 00       	mov    $0x3fb,%edi
80105e0a:	b8 80 ff ff ff       	mov    $0xffffff80,%eax
80105e0f:	89 fa                	mov    %edi,%edx
80105e11:	ee                   	out    %al,(%dx)
80105e12:	b8 0c 00 00 00       	mov    $0xc,%eax
80105e17:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105e1c:	ee                   	out    %al,(%dx)
80105e1d:	be f9 03 00 00       	mov    $0x3f9,%esi
80105e22:	89 c8                	mov    %ecx,%eax
80105e24:	89 f2                	mov    %esi,%edx
80105e26:	ee                   	out    %al,(%dx)
80105e27:	b8 03 00 00 00       	mov    $0x3,%eax
80105e2c:	89 fa                	mov    %edi,%edx
80105e2e:	ee                   	out    %al,(%dx)
80105e2f:	ba fc 03 00 00       	mov    $0x3fc,%edx
80105e34:	89 c8                	mov    %ecx,%eax
80105e36:	ee                   	out    %al,(%dx)
80105e37:	b8 01 00 00 00       	mov    $0x1,%eax
80105e3c:	89 f2                	mov    %esi,%edx
80105e3e:	ee                   	out    %al,(%dx)
  asm volatile("in %1,%0" : "=a" (data) : "d" (port));
80105e3f:	ba fd 03 00 00       	mov    $0x3fd,%edx
80105e44:	ec                   	in     (%dx),%al
  if(inb(COM1+5) == 0xFF)
80105e45:	3c ff                	cmp    $0xff,%al
80105e47:	74 56                	je     80105e9f <uartinit+0xaf>
  uart = 1;
80105e49:	c7 05 bc b5 10 80 01 	movl   $0x1,0x8010b5bc
80105e50:	00 00 00 
80105e53:	89 da                	mov    %ebx,%edx
80105e55:	ec                   	in     (%dx),%al
80105e56:	ba f8 03 00 00       	mov    $0x3f8,%edx
80105e5b:	ec                   	in     (%dx),%al
  ioapicenable(IRQ_COM1, 0);
80105e5c:	83 ec 08             	sub    $0x8,%esp
80105e5f:	be 76 00 00 00       	mov    $0x76,%esi
  for(p="xv6...\n"; *p; p++)
80105e64:	bb bc 81 10 80       	mov    $0x801081bc,%ebx
  ioapicenable(IRQ_COM1, 0);
80105e69:	6a 00                	push   $0x0
80105e6b:	6a 04                	push   $0x4
80105e6d:	e8 6e c5 ff ff       	call   801023e0 <ioapicenable>
80105e72:	83 c4 10             	add    $0x10,%esp
  for(p="xv6...\n"; *p; p++)
80105e75:	b8 78 00 00 00       	mov    $0x78,%eax
80105e7a:	eb 08                	jmp    80105e84 <uartinit+0x94>
80105e7c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80105e80:	0f b6 73 01          	movzbl 0x1(%ebx),%esi
  if(!uart)
80105e84:	8b 15 bc b5 10 80    	mov    0x8010b5bc,%edx
80105e8a:	85 d2                	test   %edx,%edx
80105e8c:	74 08                	je     80105e96 <uartinit+0xa6>
    uartputc(*p);
80105e8e:	0f be c0             	movsbl %al,%eax
80105e91:	e8 0a ff ff ff       	call   80105da0 <uartputc.part.0>
  for(p="xv6...\n"; *p; p++)
80105e96:	89 f0                	mov    %esi,%eax
80105e98:	83 c3 01             	add    $0x1,%ebx
80105e9b:	84 c0                	test   %al,%al
80105e9d:	75 e1                	jne    80105e80 <uartinit+0x90>
}
80105e9f:	8d 65 f4             	lea    -0xc(%ebp),%esp
80105ea2:	5b                   	pop    %ebx
80105ea3:	5e                   	pop    %esi
80105ea4:	5f                   	pop    %edi
80105ea5:	5d                   	pop    %ebp
80105ea6:	c3                   	ret    
80105ea7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105eae:	66 90                	xchg   %ax,%ax

80105eb0 <uartputc>:
{
80105eb0:	55                   	push   %ebp
  if(!uart)
80105eb1:	8b 15 bc b5 10 80    	mov    0x8010b5bc,%edx
{
80105eb7:	89 e5                	mov    %esp,%ebp
80105eb9:	8b 45 08             	mov    0x8(%ebp),%eax
  if(!uart)
80105ebc:	85 d2                	test   %edx,%edx
80105ebe:	74 10                	je     80105ed0 <uartputc+0x20>
}
80105ec0:	5d                   	pop    %ebp
80105ec1:	e9 da fe ff ff       	jmp    80105da0 <uartputc.part.0>
80105ec6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105ecd:	8d 76 00             	lea    0x0(%esi),%esi
80105ed0:	5d                   	pop    %ebp
80105ed1:	c3                   	ret    
80105ed2:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80105ed9:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80105ee0 <uartintr>:

void
uartintr(void)
{
80105ee0:	55                   	push   %ebp
80105ee1:	89 e5                	mov    %esp,%ebp
80105ee3:	83 ec 14             	sub    $0x14,%esp
  consoleintr(uartgetc);
80105ee6:	68 70 5d 10 80       	push   $0x80105d70
80105eeb:	e8 70 a9 ff ff       	call   80100860 <consoleintr>
}
80105ef0:	83 c4 10             	add    $0x10,%esp
80105ef3:	c9                   	leave  
80105ef4:	c3                   	ret    

80105ef5 <vector0>:
# generated by vectors.pl - do not edit
# handlers
.globl alltraps
.globl vector0
vector0:
  pushl $0
80105ef5:	6a 00                	push   $0x0
  pushl $0
80105ef7:	6a 00                	push   $0x0
  jmp alltraps
80105ef9:	e9 29 fa ff ff       	jmp    80105927 <alltraps>

80105efe <vector1>:
.globl vector1
vector1:
  pushl $0
80105efe:	6a 00                	push   $0x0
  pushl $1
80105f00:	6a 01                	push   $0x1
  jmp alltraps
80105f02:	e9 20 fa ff ff       	jmp    80105927 <alltraps>

80105f07 <vector2>:
.globl vector2
vector2:
  pushl $0
80105f07:	6a 00                	push   $0x0
  pushl $2
80105f09:	6a 02                	push   $0x2
  jmp alltraps
80105f0b:	e9 17 fa ff ff       	jmp    80105927 <alltraps>

80105f10 <vector3>:
.globl vector3
vector3:
  pushl $0
80105f10:	6a 00                	push   $0x0
  pushl $3
80105f12:	6a 03                	push   $0x3
  jmp alltraps
80105f14:	e9 0e fa ff ff       	jmp    80105927 <alltraps>

80105f19 <vector4>:
.globl vector4
vector4:
  pushl $0
80105f19:	6a 00                	push   $0x0
  pushl $4
80105f1b:	6a 04                	push   $0x4
  jmp alltraps
80105f1d:	e9 05 fa ff ff       	jmp    80105927 <alltraps>

80105f22 <vector5>:
.globl vector5
vector5:
  pushl $0
80105f22:	6a 00                	push   $0x0
  pushl $5
80105f24:	6a 05                	push   $0x5
  jmp alltraps
80105f26:	e9 fc f9 ff ff       	jmp    80105927 <alltraps>

80105f2b <vector6>:
.globl vector6
vector6:
  pushl $0
80105f2b:	6a 00                	push   $0x0
  pushl $6
80105f2d:	6a 06                	push   $0x6
  jmp alltraps
80105f2f:	e9 f3 f9 ff ff       	jmp    80105927 <alltraps>

80105f34 <vector7>:
.globl vector7
vector7:
  pushl $0
80105f34:	6a 00                	push   $0x0
  pushl $7
80105f36:	6a 07                	push   $0x7
  jmp alltraps
80105f38:	e9 ea f9 ff ff       	jmp    80105927 <alltraps>

80105f3d <vector8>:
.globl vector8
vector8:
  pushl $8
80105f3d:	6a 08                	push   $0x8
  jmp alltraps
80105f3f:	e9 e3 f9 ff ff       	jmp    80105927 <alltraps>

80105f44 <vector9>:
.globl vector9
vector9:
  pushl $0
80105f44:	6a 00                	push   $0x0
  pushl $9
80105f46:	6a 09                	push   $0x9
  jmp alltraps
80105f48:	e9 da f9 ff ff       	jmp    80105927 <alltraps>

80105f4d <vector10>:
.globl vector10
vector10:
  pushl $10
80105f4d:	6a 0a                	push   $0xa
  jmp alltraps
80105f4f:	e9 d3 f9 ff ff       	jmp    80105927 <alltraps>

80105f54 <vector11>:
.globl vector11
vector11:
  pushl $11
80105f54:	6a 0b                	push   $0xb
  jmp alltraps
80105f56:	e9 cc f9 ff ff       	jmp    80105927 <alltraps>

80105f5b <vector12>:
.globl vector12
vector12:
  pushl $12
80105f5b:	6a 0c                	push   $0xc
  jmp alltraps
80105f5d:	e9 c5 f9 ff ff       	jmp    80105927 <alltraps>

80105f62 <vector13>:
.globl vector13
vector13:
  pushl $13
80105f62:	6a 0d                	push   $0xd
  jmp alltraps
80105f64:	e9 be f9 ff ff       	jmp    80105927 <alltraps>

80105f69 <vector14>:
.globl vector14
vector14:
  pushl $14
80105f69:	6a 0e                	push   $0xe
  jmp alltraps
80105f6b:	e9 b7 f9 ff ff       	jmp    80105927 <alltraps>

80105f70 <vector15>:
.globl vector15
vector15:
  pushl $0
80105f70:	6a 00                	push   $0x0
  pushl $15
80105f72:	6a 0f                	push   $0xf
  jmp alltraps
80105f74:	e9 ae f9 ff ff       	jmp    80105927 <alltraps>

80105f79 <vector16>:
.globl vector16
vector16:
  pushl $0
80105f79:	6a 00                	push   $0x0
  pushl $16
80105f7b:	6a 10                	push   $0x10
  jmp alltraps
80105f7d:	e9 a5 f9 ff ff       	jmp    80105927 <alltraps>

80105f82 <vector17>:
.globl vector17
vector17:
  pushl $17
80105f82:	6a 11                	push   $0x11
  jmp alltraps
80105f84:	e9 9e f9 ff ff       	jmp    80105927 <alltraps>

80105f89 <vector18>:
.globl vector18
vector18:
  pushl $0
80105f89:	6a 00                	push   $0x0
  pushl $18
80105f8b:	6a 12                	push   $0x12
  jmp alltraps
80105f8d:	e9 95 f9 ff ff       	jmp    80105927 <alltraps>

80105f92 <vector19>:
.globl vector19
vector19:
  pushl $0
80105f92:	6a 00                	push   $0x0
  pushl $19
80105f94:	6a 13                	push   $0x13
  jmp alltraps
80105f96:	e9 8c f9 ff ff       	jmp    80105927 <alltraps>

80105f9b <vector20>:
.globl vector20
vector20:
  pushl $0
80105f9b:	6a 00                	push   $0x0
  pushl $20
80105f9d:	6a 14                	push   $0x14
  jmp alltraps
80105f9f:	e9 83 f9 ff ff       	jmp    80105927 <alltraps>

80105fa4 <vector21>:
.globl vector21
vector21:
  pushl $0
80105fa4:	6a 00                	push   $0x0
  pushl $21
80105fa6:	6a 15                	push   $0x15
  jmp alltraps
80105fa8:	e9 7a f9 ff ff       	jmp    80105927 <alltraps>

80105fad <vector22>:
.globl vector22
vector22:
  pushl $0
80105fad:	6a 00                	push   $0x0
  pushl $22
80105faf:	6a 16                	push   $0x16
  jmp alltraps
80105fb1:	e9 71 f9 ff ff       	jmp    80105927 <alltraps>

80105fb6 <vector23>:
.globl vector23
vector23:
  pushl $0
80105fb6:	6a 00                	push   $0x0
  pushl $23
80105fb8:	6a 17                	push   $0x17
  jmp alltraps
80105fba:	e9 68 f9 ff ff       	jmp    80105927 <alltraps>

80105fbf <vector24>:
.globl vector24
vector24:
  pushl $0
80105fbf:	6a 00                	push   $0x0
  pushl $24
80105fc1:	6a 18                	push   $0x18
  jmp alltraps
80105fc3:	e9 5f f9 ff ff       	jmp    80105927 <alltraps>

80105fc8 <vector25>:
.globl vector25
vector25:
  pushl $0
80105fc8:	6a 00                	push   $0x0
  pushl $25
80105fca:	6a 19                	push   $0x19
  jmp alltraps
80105fcc:	e9 56 f9 ff ff       	jmp    80105927 <alltraps>

80105fd1 <vector26>:
.globl vector26
vector26:
  pushl $0
80105fd1:	6a 00                	push   $0x0
  pushl $26
80105fd3:	6a 1a                	push   $0x1a
  jmp alltraps
80105fd5:	e9 4d f9 ff ff       	jmp    80105927 <alltraps>

80105fda <vector27>:
.globl vector27
vector27:
  pushl $0
80105fda:	6a 00                	push   $0x0
  pushl $27
80105fdc:	6a 1b                	push   $0x1b
  jmp alltraps
80105fde:	e9 44 f9 ff ff       	jmp    80105927 <alltraps>

80105fe3 <vector28>:
.globl vector28
vector28:
  pushl $0
80105fe3:	6a 00                	push   $0x0
  pushl $28
80105fe5:	6a 1c                	push   $0x1c
  jmp alltraps
80105fe7:	e9 3b f9 ff ff       	jmp    80105927 <alltraps>

80105fec <vector29>:
.globl vector29
vector29:
  pushl $0
80105fec:	6a 00                	push   $0x0
  pushl $29
80105fee:	6a 1d                	push   $0x1d
  jmp alltraps
80105ff0:	e9 32 f9 ff ff       	jmp    80105927 <alltraps>

80105ff5 <vector30>:
.globl vector30
vector30:
  pushl $0
80105ff5:	6a 00                	push   $0x0
  pushl $30
80105ff7:	6a 1e                	push   $0x1e
  jmp alltraps
80105ff9:	e9 29 f9 ff ff       	jmp    80105927 <alltraps>

80105ffe <vector31>:
.globl vector31
vector31:
  pushl $0
80105ffe:	6a 00                	push   $0x0
  pushl $31
80106000:	6a 1f                	push   $0x1f
  jmp alltraps
80106002:	e9 20 f9 ff ff       	jmp    80105927 <alltraps>

80106007 <vector32>:
.globl vector32
vector32:
  pushl $0
80106007:	6a 00                	push   $0x0
  pushl $32
80106009:	6a 20                	push   $0x20
  jmp alltraps
8010600b:	e9 17 f9 ff ff       	jmp    80105927 <alltraps>

80106010 <vector33>:
.globl vector33
vector33:
  pushl $0
80106010:	6a 00                	push   $0x0
  pushl $33
80106012:	6a 21                	push   $0x21
  jmp alltraps
80106014:	e9 0e f9 ff ff       	jmp    80105927 <alltraps>

80106019 <vector34>:
.globl vector34
vector34:
  pushl $0
80106019:	6a 00                	push   $0x0
  pushl $34
8010601b:	6a 22                	push   $0x22
  jmp alltraps
8010601d:	e9 05 f9 ff ff       	jmp    80105927 <alltraps>

80106022 <vector35>:
.globl vector35
vector35:
  pushl $0
80106022:	6a 00                	push   $0x0
  pushl $35
80106024:	6a 23                	push   $0x23
  jmp alltraps
80106026:	e9 fc f8 ff ff       	jmp    80105927 <alltraps>

8010602b <vector36>:
.globl vector36
vector36:
  pushl $0
8010602b:	6a 00                	push   $0x0
  pushl $36
8010602d:	6a 24                	push   $0x24
  jmp alltraps
8010602f:	e9 f3 f8 ff ff       	jmp    80105927 <alltraps>

80106034 <vector37>:
.globl vector37
vector37:
  pushl $0
80106034:	6a 00                	push   $0x0
  pushl $37
80106036:	6a 25                	push   $0x25
  jmp alltraps
80106038:	e9 ea f8 ff ff       	jmp    80105927 <alltraps>

8010603d <vector38>:
.globl vector38
vector38:
  pushl $0
8010603d:	6a 00                	push   $0x0
  pushl $38
8010603f:	6a 26                	push   $0x26
  jmp alltraps
80106041:	e9 e1 f8 ff ff       	jmp    80105927 <alltraps>

80106046 <vector39>:
.globl vector39
vector39:
  pushl $0
80106046:	6a 00                	push   $0x0
  pushl $39
80106048:	6a 27                	push   $0x27
  jmp alltraps
8010604a:	e9 d8 f8 ff ff       	jmp    80105927 <alltraps>

8010604f <vector40>:
.globl vector40
vector40:
  pushl $0
8010604f:	6a 00                	push   $0x0
  pushl $40
80106051:	6a 28                	push   $0x28
  jmp alltraps
80106053:	e9 cf f8 ff ff       	jmp    80105927 <alltraps>

80106058 <vector41>:
.globl vector41
vector41:
  pushl $0
80106058:	6a 00                	push   $0x0
  pushl $41
8010605a:	6a 29                	push   $0x29
  jmp alltraps
8010605c:	e9 c6 f8 ff ff       	jmp    80105927 <alltraps>

80106061 <vector42>:
.globl vector42
vector42:
  pushl $0
80106061:	6a 00                	push   $0x0
  pushl $42
80106063:	6a 2a                	push   $0x2a
  jmp alltraps
80106065:	e9 bd f8 ff ff       	jmp    80105927 <alltraps>

8010606a <vector43>:
.globl vector43
vector43:
  pushl $0
8010606a:	6a 00                	push   $0x0
  pushl $43
8010606c:	6a 2b                	push   $0x2b
  jmp alltraps
8010606e:	e9 b4 f8 ff ff       	jmp    80105927 <alltraps>

80106073 <vector44>:
.globl vector44
vector44:
  pushl $0
80106073:	6a 00                	push   $0x0
  pushl $44
80106075:	6a 2c                	push   $0x2c
  jmp alltraps
80106077:	e9 ab f8 ff ff       	jmp    80105927 <alltraps>

8010607c <vector45>:
.globl vector45
vector45:
  pushl $0
8010607c:	6a 00                	push   $0x0
  pushl $45
8010607e:	6a 2d                	push   $0x2d
  jmp alltraps
80106080:	e9 a2 f8 ff ff       	jmp    80105927 <alltraps>

80106085 <vector46>:
.globl vector46
vector46:
  pushl $0
80106085:	6a 00                	push   $0x0
  pushl $46
80106087:	6a 2e                	push   $0x2e
  jmp alltraps
80106089:	e9 99 f8 ff ff       	jmp    80105927 <alltraps>

8010608e <vector47>:
.globl vector47
vector47:
  pushl $0
8010608e:	6a 00                	push   $0x0
  pushl $47
80106090:	6a 2f                	push   $0x2f
  jmp alltraps
80106092:	e9 90 f8 ff ff       	jmp    80105927 <alltraps>

80106097 <vector48>:
.globl vector48
vector48:
  pushl $0
80106097:	6a 00                	push   $0x0
  pushl $48
80106099:	6a 30                	push   $0x30
  jmp alltraps
8010609b:	e9 87 f8 ff ff       	jmp    80105927 <alltraps>

801060a0 <vector49>:
.globl vector49
vector49:
  pushl $0
801060a0:	6a 00                	push   $0x0
  pushl $49
801060a2:	6a 31                	push   $0x31
  jmp alltraps
801060a4:	e9 7e f8 ff ff       	jmp    80105927 <alltraps>

801060a9 <vector50>:
.globl vector50
vector50:
  pushl $0
801060a9:	6a 00                	push   $0x0
  pushl $50
801060ab:	6a 32                	push   $0x32
  jmp alltraps
801060ad:	e9 75 f8 ff ff       	jmp    80105927 <alltraps>

801060b2 <vector51>:
.globl vector51
vector51:
  pushl $0
801060b2:	6a 00                	push   $0x0
  pushl $51
801060b4:	6a 33                	push   $0x33
  jmp alltraps
801060b6:	e9 6c f8 ff ff       	jmp    80105927 <alltraps>

801060bb <vector52>:
.globl vector52
vector52:
  pushl $0
801060bb:	6a 00                	push   $0x0
  pushl $52
801060bd:	6a 34                	push   $0x34
  jmp alltraps
801060bf:	e9 63 f8 ff ff       	jmp    80105927 <alltraps>

801060c4 <vector53>:
.globl vector53
vector53:
  pushl $0
801060c4:	6a 00                	push   $0x0
  pushl $53
801060c6:	6a 35                	push   $0x35
  jmp alltraps
801060c8:	e9 5a f8 ff ff       	jmp    80105927 <alltraps>

801060cd <vector54>:
.globl vector54
vector54:
  pushl $0
801060cd:	6a 00                	push   $0x0
  pushl $54
801060cf:	6a 36                	push   $0x36
  jmp alltraps
801060d1:	e9 51 f8 ff ff       	jmp    80105927 <alltraps>

801060d6 <vector55>:
.globl vector55
vector55:
  pushl $0
801060d6:	6a 00                	push   $0x0
  pushl $55
801060d8:	6a 37                	push   $0x37
  jmp alltraps
801060da:	e9 48 f8 ff ff       	jmp    80105927 <alltraps>

801060df <vector56>:
.globl vector56
vector56:
  pushl $0
801060df:	6a 00                	push   $0x0
  pushl $56
801060e1:	6a 38                	push   $0x38
  jmp alltraps
801060e3:	e9 3f f8 ff ff       	jmp    80105927 <alltraps>

801060e8 <vector57>:
.globl vector57
vector57:
  pushl $0
801060e8:	6a 00                	push   $0x0
  pushl $57
801060ea:	6a 39                	push   $0x39
  jmp alltraps
801060ec:	e9 36 f8 ff ff       	jmp    80105927 <alltraps>

801060f1 <vector58>:
.globl vector58
vector58:
  pushl $0
801060f1:	6a 00                	push   $0x0
  pushl $58
801060f3:	6a 3a                	push   $0x3a
  jmp alltraps
801060f5:	e9 2d f8 ff ff       	jmp    80105927 <alltraps>

801060fa <vector59>:
.globl vector59
vector59:
  pushl $0
801060fa:	6a 00                	push   $0x0
  pushl $59
801060fc:	6a 3b                	push   $0x3b
  jmp alltraps
801060fe:	e9 24 f8 ff ff       	jmp    80105927 <alltraps>

80106103 <vector60>:
.globl vector60
vector60:
  pushl $0
80106103:	6a 00                	push   $0x0
  pushl $60
80106105:	6a 3c                	push   $0x3c
  jmp alltraps
80106107:	e9 1b f8 ff ff       	jmp    80105927 <alltraps>

8010610c <vector61>:
.globl vector61
vector61:
  pushl $0
8010610c:	6a 00                	push   $0x0
  pushl $61
8010610e:	6a 3d                	push   $0x3d
  jmp alltraps
80106110:	e9 12 f8 ff ff       	jmp    80105927 <alltraps>

80106115 <vector62>:
.globl vector62
vector62:
  pushl $0
80106115:	6a 00                	push   $0x0
  pushl $62
80106117:	6a 3e                	push   $0x3e
  jmp alltraps
80106119:	e9 09 f8 ff ff       	jmp    80105927 <alltraps>

8010611e <vector63>:
.globl vector63
vector63:
  pushl $0
8010611e:	6a 00                	push   $0x0
  pushl $63
80106120:	6a 3f                	push   $0x3f
  jmp alltraps
80106122:	e9 00 f8 ff ff       	jmp    80105927 <alltraps>

80106127 <vector64>:
.globl vector64
vector64:
  pushl $0
80106127:	6a 00                	push   $0x0
  pushl $64
80106129:	6a 40                	push   $0x40
  jmp alltraps
8010612b:	e9 f7 f7 ff ff       	jmp    80105927 <alltraps>

80106130 <vector65>:
.globl vector65
vector65:
  pushl $0
80106130:	6a 00                	push   $0x0
  pushl $65
80106132:	6a 41                	push   $0x41
  jmp alltraps
80106134:	e9 ee f7 ff ff       	jmp    80105927 <alltraps>

80106139 <vector66>:
.globl vector66
vector66:
  pushl $0
80106139:	6a 00                	push   $0x0
  pushl $66
8010613b:	6a 42                	push   $0x42
  jmp alltraps
8010613d:	e9 e5 f7 ff ff       	jmp    80105927 <alltraps>

80106142 <vector67>:
.globl vector67
vector67:
  pushl $0
80106142:	6a 00                	push   $0x0
  pushl $67
80106144:	6a 43                	push   $0x43
  jmp alltraps
80106146:	e9 dc f7 ff ff       	jmp    80105927 <alltraps>

8010614b <vector68>:
.globl vector68
vector68:
  pushl $0
8010614b:	6a 00                	push   $0x0
  pushl $68
8010614d:	6a 44                	push   $0x44
  jmp alltraps
8010614f:	e9 d3 f7 ff ff       	jmp    80105927 <alltraps>

80106154 <vector69>:
.globl vector69
vector69:
  pushl $0
80106154:	6a 00                	push   $0x0
  pushl $69
80106156:	6a 45                	push   $0x45
  jmp alltraps
80106158:	e9 ca f7 ff ff       	jmp    80105927 <alltraps>

8010615d <vector70>:
.globl vector70
vector70:
  pushl $0
8010615d:	6a 00                	push   $0x0
  pushl $70
8010615f:	6a 46                	push   $0x46
  jmp alltraps
80106161:	e9 c1 f7 ff ff       	jmp    80105927 <alltraps>

80106166 <vector71>:
.globl vector71
vector71:
  pushl $0
80106166:	6a 00                	push   $0x0
  pushl $71
80106168:	6a 47                	push   $0x47
  jmp alltraps
8010616a:	e9 b8 f7 ff ff       	jmp    80105927 <alltraps>

8010616f <vector72>:
.globl vector72
vector72:
  pushl $0
8010616f:	6a 00                	push   $0x0
  pushl $72
80106171:	6a 48                	push   $0x48
  jmp alltraps
80106173:	e9 af f7 ff ff       	jmp    80105927 <alltraps>

80106178 <vector73>:
.globl vector73
vector73:
  pushl $0
80106178:	6a 00                	push   $0x0
  pushl $73
8010617a:	6a 49                	push   $0x49
  jmp alltraps
8010617c:	e9 a6 f7 ff ff       	jmp    80105927 <alltraps>

80106181 <vector74>:
.globl vector74
vector74:
  pushl $0
80106181:	6a 00                	push   $0x0
  pushl $74
80106183:	6a 4a                	push   $0x4a
  jmp alltraps
80106185:	e9 9d f7 ff ff       	jmp    80105927 <alltraps>

8010618a <vector75>:
.globl vector75
vector75:
  pushl $0
8010618a:	6a 00                	push   $0x0
  pushl $75
8010618c:	6a 4b                	push   $0x4b
  jmp alltraps
8010618e:	e9 94 f7 ff ff       	jmp    80105927 <alltraps>

80106193 <vector76>:
.globl vector76
vector76:
  pushl $0
80106193:	6a 00                	push   $0x0
  pushl $76
80106195:	6a 4c                	push   $0x4c
  jmp alltraps
80106197:	e9 8b f7 ff ff       	jmp    80105927 <alltraps>

8010619c <vector77>:
.globl vector77
vector77:
  pushl $0
8010619c:	6a 00                	push   $0x0
  pushl $77
8010619e:	6a 4d                	push   $0x4d
  jmp alltraps
801061a0:	e9 82 f7 ff ff       	jmp    80105927 <alltraps>

801061a5 <vector78>:
.globl vector78
vector78:
  pushl $0
801061a5:	6a 00                	push   $0x0
  pushl $78
801061a7:	6a 4e                	push   $0x4e
  jmp alltraps
801061a9:	e9 79 f7 ff ff       	jmp    80105927 <alltraps>

801061ae <vector79>:
.globl vector79
vector79:
  pushl $0
801061ae:	6a 00                	push   $0x0
  pushl $79
801061b0:	6a 4f                	push   $0x4f
  jmp alltraps
801061b2:	e9 70 f7 ff ff       	jmp    80105927 <alltraps>

801061b7 <vector80>:
.globl vector80
vector80:
  pushl $0
801061b7:	6a 00                	push   $0x0
  pushl $80
801061b9:	6a 50                	push   $0x50
  jmp alltraps
801061bb:	e9 67 f7 ff ff       	jmp    80105927 <alltraps>

801061c0 <vector81>:
.globl vector81
vector81:
  pushl $0
801061c0:	6a 00                	push   $0x0
  pushl $81
801061c2:	6a 51                	push   $0x51
  jmp alltraps
801061c4:	e9 5e f7 ff ff       	jmp    80105927 <alltraps>

801061c9 <vector82>:
.globl vector82
vector82:
  pushl $0
801061c9:	6a 00                	push   $0x0
  pushl $82
801061cb:	6a 52                	push   $0x52
  jmp alltraps
801061cd:	e9 55 f7 ff ff       	jmp    80105927 <alltraps>

801061d2 <vector83>:
.globl vector83
vector83:
  pushl $0
801061d2:	6a 00                	push   $0x0
  pushl $83
801061d4:	6a 53                	push   $0x53
  jmp alltraps
801061d6:	e9 4c f7 ff ff       	jmp    80105927 <alltraps>

801061db <vector84>:
.globl vector84
vector84:
  pushl $0
801061db:	6a 00                	push   $0x0
  pushl $84
801061dd:	6a 54                	push   $0x54
  jmp alltraps
801061df:	e9 43 f7 ff ff       	jmp    80105927 <alltraps>

801061e4 <vector85>:
.globl vector85
vector85:
  pushl $0
801061e4:	6a 00                	push   $0x0
  pushl $85
801061e6:	6a 55                	push   $0x55
  jmp alltraps
801061e8:	e9 3a f7 ff ff       	jmp    80105927 <alltraps>

801061ed <vector86>:
.globl vector86
vector86:
  pushl $0
801061ed:	6a 00                	push   $0x0
  pushl $86
801061ef:	6a 56                	push   $0x56
  jmp alltraps
801061f1:	e9 31 f7 ff ff       	jmp    80105927 <alltraps>

801061f6 <vector87>:
.globl vector87
vector87:
  pushl $0
801061f6:	6a 00                	push   $0x0
  pushl $87
801061f8:	6a 57                	push   $0x57
  jmp alltraps
801061fa:	e9 28 f7 ff ff       	jmp    80105927 <alltraps>

801061ff <vector88>:
.globl vector88
vector88:
  pushl $0
801061ff:	6a 00                	push   $0x0
  pushl $88
80106201:	6a 58                	push   $0x58
  jmp alltraps
80106203:	e9 1f f7 ff ff       	jmp    80105927 <alltraps>

80106208 <vector89>:
.globl vector89
vector89:
  pushl $0
80106208:	6a 00                	push   $0x0
  pushl $89
8010620a:	6a 59                	push   $0x59
  jmp alltraps
8010620c:	e9 16 f7 ff ff       	jmp    80105927 <alltraps>

80106211 <vector90>:
.globl vector90
vector90:
  pushl $0
80106211:	6a 00                	push   $0x0
  pushl $90
80106213:	6a 5a                	push   $0x5a
  jmp alltraps
80106215:	e9 0d f7 ff ff       	jmp    80105927 <alltraps>

8010621a <vector91>:
.globl vector91
vector91:
  pushl $0
8010621a:	6a 00                	push   $0x0
  pushl $91
8010621c:	6a 5b                	push   $0x5b
  jmp alltraps
8010621e:	e9 04 f7 ff ff       	jmp    80105927 <alltraps>

80106223 <vector92>:
.globl vector92
vector92:
  pushl $0
80106223:	6a 00                	push   $0x0
  pushl $92
80106225:	6a 5c                	push   $0x5c
  jmp alltraps
80106227:	e9 fb f6 ff ff       	jmp    80105927 <alltraps>

8010622c <vector93>:
.globl vector93
vector93:
  pushl $0
8010622c:	6a 00                	push   $0x0
  pushl $93
8010622e:	6a 5d                	push   $0x5d
  jmp alltraps
80106230:	e9 f2 f6 ff ff       	jmp    80105927 <alltraps>

80106235 <vector94>:
.globl vector94
vector94:
  pushl $0
80106235:	6a 00                	push   $0x0
  pushl $94
80106237:	6a 5e                	push   $0x5e
  jmp alltraps
80106239:	e9 e9 f6 ff ff       	jmp    80105927 <alltraps>

8010623e <vector95>:
.globl vector95
vector95:
  pushl $0
8010623e:	6a 00                	push   $0x0
  pushl $95
80106240:	6a 5f                	push   $0x5f
  jmp alltraps
80106242:	e9 e0 f6 ff ff       	jmp    80105927 <alltraps>

80106247 <vector96>:
.globl vector96
vector96:
  pushl $0
80106247:	6a 00                	push   $0x0
  pushl $96
80106249:	6a 60                	push   $0x60
  jmp alltraps
8010624b:	e9 d7 f6 ff ff       	jmp    80105927 <alltraps>

80106250 <vector97>:
.globl vector97
vector97:
  pushl $0
80106250:	6a 00                	push   $0x0
  pushl $97
80106252:	6a 61                	push   $0x61
  jmp alltraps
80106254:	e9 ce f6 ff ff       	jmp    80105927 <alltraps>

80106259 <vector98>:
.globl vector98
vector98:
  pushl $0
80106259:	6a 00                	push   $0x0
  pushl $98
8010625b:	6a 62                	push   $0x62
  jmp alltraps
8010625d:	e9 c5 f6 ff ff       	jmp    80105927 <alltraps>

80106262 <vector99>:
.globl vector99
vector99:
  pushl $0
80106262:	6a 00                	push   $0x0
  pushl $99
80106264:	6a 63                	push   $0x63
  jmp alltraps
80106266:	e9 bc f6 ff ff       	jmp    80105927 <alltraps>

8010626b <vector100>:
.globl vector100
vector100:
  pushl $0
8010626b:	6a 00                	push   $0x0
  pushl $100
8010626d:	6a 64                	push   $0x64
  jmp alltraps
8010626f:	e9 b3 f6 ff ff       	jmp    80105927 <alltraps>

80106274 <vector101>:
.globl vector101
vector101:
  pushl $0
80106274:	6a 00                	push   $0x0
  pushl $101
80106276:	6a 65                	push   $0x65
  jmp alltraps
80106278:	e9 aa f6 ff ff       	jmp    80105927 <alltraps>

8010627d <vector102>:
.globl vector102
vector102:
  pushl $0
8010627d:	6a 00                	push   $0x0
  pushl $102
8010627f:	6a 66                	push   $0x66
  jmp alltraps
80106281:	e9 a1 f6 ff ff       	jmp    80105927 <alltraps>

80106286 <vector103>:
.globl vector103
vector103:
  pushl $0
80106286:	6a 00                	push   $0x0
  pushl $103
80106288:	6a 67                	push   $0x67
  jmp alltraps
8010628a:	e9 98 f6 ff ff       	jmp    80105927 <alltraps>

8010628f <vector104>:
.globl vector104
vector104:
  pushl $0
8010628f:	6a 00                	push   $0x0
  pushl $104
80106291:	6a 68                	push   $0x68
  jmp alltraps
80106293:	e9 8f f6 ff ff       	jmp    80105927 <alltraps>

80106298 <vector105>:
.globl vector105
vector105:
  pushl $0
80106298:	6a 00                	push   $0x0
  pushl $105
8010629a:	6a 69                	push   $0x69
  jmp alltraps
8010629c:	e9 86 f6 ff ff       	jmp    80105927 <alltraps>

801062a1 <vector106>:
.globl vector106
vector106:
  pushl $0
801062a1:	6a 00                	push   $0x0
  pushl $106
801062a3:	6a 6a                	push   $0x6a
  jmp alltraps
801062a5:	e9 7d f6 ff ff       	jmp    80105927 <alltraps>

801062aa <vector107>:
.globl vector107
vector107:
  pushl $0
801062aa:	6a 00                	push   $0x0
  pushl $107
801062ac:	6a 6b                	push   $0x6b
  jmp alltraps
801062ae:	e9 74 f6 ff ff       	jmp    80105927 <alltraps>

801062b3 <vector108>:
.globl vector108
vector108:
  pushl $0
801062b3:	6a 00                	push   $0x0
  pushl $108
801062b5:	6a 6c                	push   $0x6c
  jmp alltraps
801062b7:	e9 6b f6 ff ff       	jmp    80105927 <alltraps>

801062bc <vector109>:
.globl vector109
vector109:
  pushl $0
801062bc:	6a 00                	push   $0x0
  pushl $109
801062be:	6a 6d                	push   $0x6d
  jmp alltraps
801062c0:	e9 62 f6 ff ff       	jmp    80105927 <alltraps>

801062c5 <vector110>:
.globl vector110
vector110:
  pushl $0
801062c5:	6a 00                	push   $0x0
  pushl $110
801062c7:	6a 6e                	push   $0x6e
  jmp alltraps
801062c9:	e9 59 f6 ff ff       	jmp    80105927 <alltraps>

801062ce <vector111>:
.globl vector111
vector111:
  pushl $0
801062ce:	6a 00                	push   $0x0
  pushl $111
801062d0:	6a 6f                	push   $0x6f
  jmp alltraps
801062d2:	e9 50 f6 ff ff       	jmp    80105927 <alltraps>

801062d7 <vector112>:
.globl vector112
vector112:
  pushl $0
801062d7:	6a 00                	push   $0x0
  pushl $112
801062d9:	6a 70                	push   $0x70
  jmp alltraps
801062db:	e9 47 f6 ff ff       	jmp    80105927 <alltraps>

801062e0 <vector113>:
.globl vector113
vector113:
  pushl $0
801062e0:	6a 00                	push   $0x0
  pushl $113
801062e2:	6a 71                	push   $0x71
  jmp alltraps
801062e4:	e9 3e f6 ff ff       	jmp    80105927 <alltraps>

801062e9 <vector114>:
.globl vector114
vector114:
  pushl $0
801062e9:	6a 00                	push   $0x0
  pushl $114
801062eb:	6a 72                	push   $0x72
  jmp alltraps
801062ed:	e9 35 f6 ff ff       	jmp    80105927 <alltraps>

801062f2 <vector115>:
.globl vector115
vector115:
  pushl $0
801062f2:	6a 00                	push   $0x0
  pushl $115
801062f4:	6a 73                	push   $0x73
  jmp alltraps
801062f6:	e9 2c f6 ff ff       	jmp    80105927 <alltraps>

801062fb <vector116>:
.globl vector116
vector116:
  pushl $0
801062fb:	6a 00                	push   $0x0
  pushl $116
801062fd:	6a 74                	push   $0x74
  jmp alltraps
801062ff:	e9 23 f6 ff ff       	jmp    80105927 <alltraps>

80106304 <vector117>:
.globl vector117
vector117:
  pushl $0
80106304:	6a 00                	push   $0x0
  pushl $117
80106306:	6a 75                	push   $0x75
  jmp alltraps
80106308:	e9 1a f6 ff ff       	jmp    80105927 <alltraps>

8010630d <vector118>:
.globl vector118
vector118:
  pushl $0
8010630d:	6a 00                	push   $0x0
  pushl $118
8010630f:	6a 76                	push   $0x76
  jmp alltraps
80106311:	e9 11 f6 ff ff       	jmp    80105927 <alltraps>

80106316 <vector119>:
.globl vector119
vector119:
  pushl $0
80106316:	6a 00                	push   $0x0
  pushl $119
80106318:	6a 77                	push   $0x77
  jmp alltraps
8010631a:	e9 08 f6 ff ff       	jmp    80105927 <alltraps>

8010631f <vector120>:
.globl vector120
vector120:
  pushl $0
8010631f:	6a 00                	push   $0x0
  pushl $120
80106321:	6a 78                	push   $0x78
  jmp alltraps
80106323:	e9 ff f5 ff ff       	jmp    80105927 <alltraps>

80106328 <vector121>:
.globl vector121
vector121:
  pushl $0
80106328:	6a 00                	push   $0x0
  pushl $121
8010632a:	6a 79                	push   $0x79
  jmp alltraps
8010632c:	e9 f6 f5 ff ff       	jmp    80105927 <alltraps>

80106331 <vector122>:
.globl vector122
vector122:
  pushl $0
80106331:	6a 00                	push   $0x0
  pushl $122
80106333:	6a 7a                	push   $0x7a
  jmp alltraps
80106335:	e9 ed f5 ff ff       	jmp    80105927 <alltraps>

8010633a <vector123>:
.globl vector123
vector123:
  pushl $0
8010633a:	6a 00                	push   $0x0
  pushl $123
8010633c:	6a 7b                	push   $0x7b
  jmp alltraps
8010633e:	e9 e4 f5 ff ff       	jmp    80105927 <alltraps>

80106343 <vector124>:
.globl vector124
vector124:
  pushl $0
80106343:	6a 00                	push   $0x0
  pushl $124
80106345:	6a 7c                	push   $0x7c
  jmp alltraps
80106347:	e9 db f5 ff ff       	jmp    80105927 <alltraps>

8010634c <vector125>:
.globl vector125
vector125:
  pushl $0
8010634c:	6a 00                	push   $0x0
  pushl $125
8010634e:	6a 7d                	push   $0x7d
  jmp alltraps
80106350:	e9 d2 f5 ff ff       	jmp    80105927 <alltraps>

80106355 <vector126>:
.globl vector126
vector126:
  pushl $0
80106355:	6a 00                	push   $0x0
  pushl $126
80106357:	6a 7e                	push   $0x7e
  jmp alltraps
80106359:	e9 c9 f5 ff ff       	jmp    80105927 <alltraps>

8010635e <vector127>:
.globl vector127
vector127:
  pushl $0
8010635e:	6a 00                	push   $0x0
  pushl $127
80106360:	6a 7f                	push   $0x7f
  jmp alltraps
80106362:	e9 c0 f5 ff ff       	jmp    80105927 <alltraps>

80106367 <vector128>:
.globl vector128
vector128:
  pushl $0
80106367:	6a 00                	push   $0x0
  pushl $128
80106369:	68 80 00 00 00       	push   $0x80
  jmp alltraps
8010636e:	e9 b4 f5 ff ff       	jmp    80105927 <alltraps>

80106373 <vector129>:
.globl vector129
vector129:
  pushl $0
80106373:	6a 00                	push   $0x0
  pushl $129
80106375:	68 81 00 00 00       	push   $0x81
  jmp alltraps
8010637a:	e9 a8 f5 ff ff       	jmp    80105927 <alltraps>

8010637f <vector130>:
.globl vector130
vector130:
  pushl $0
8010637f:	6a 00                	push   $0x0
  pushl $130
80106381:	68 82 00 00 00       	push   $0x82
  jmp alltraps
80106386:	e9 9c f5 ff ff       	jmp    80105927 <alltraps>

8010638b <vector131>:
.globl vector131
vector131:
  pushl $0
8010638b:	6a 00                	push   $0x0
  pushl $131
8010638d:	68 83 00 00 00       	push   $0x83
  jmp alltraps
80106392:	e9 90 f5 ff ff       	jmp    80105927 <alltraps>

80106397 <vector132>:
.globl vector132
vector132:
  pushl $0
80106397:	6a 00                	push   $0x0
  pushl $132
80106399:	68 84 00 00 00       	push   $0x84
  jmp alltraps
8010639e:	e9 84 f5 ff ff       	jmp    80105927 <alltraps>

801063a3 <vector133>:
.globl vector133
vector133:
  pushl $0
801063a3:	6a 00                	push   $0x0
  pushl $133
801063a5:	68 85 00 00 00       	push   $0x85
  jmp alltraps
801063aa:	e9 78 f5 ff ff       	jmp    80105927 <alltraps>

801063af <vector134>:
.globl vector134
vector134:
  pushl $0
801063af:	6a 00                	push   $0x0
  pushl $134
801063b1:	68 86 00 00 00       	push   $0x86
  jmp alltraps
801063b6:	e9 6c f5 ff ff       	jmp    80105927 <alltraps>

801063bb <vector135>:
.globl vector135
vector135:
  pushl $0
801063bb:	6a 00                	push   $0x0
  pushl $135
801063bd:	68 87 00 00 00       	push   $0x87
  jmp alltraps
801063c2:	e9 60 f5 ff ff       	jmp    80105927 <alltraps>

801063c7 <vector136>:
.globl vector136
vector136:
  pushl $0
801063c7:	6a 00                	push   $0x0
  pushl $136
801063c9:	68 88 00 00 00       	push   $0x88
  jmp alltraps
801063ce:	e9 54 f5 ff ff       	jmp    80105927 <alltraps>

801063d3 <vector137>:
.globl vector137
vector137:
  pushl $0
801063d3:	6a 00                	push   $0x0
  pushl $137
801063d5:	68 89 00 00 00       	push   $0x89
  jmp alltraps
801063da:	e9 48 f5 ff ff       	jmp    80105927 <alltraps>

801063df <vector138>:
.globl vector138
vector138:
  pushl $0
801063df:	6a 00                	push   $0x0
  pushl $138
801063e1:	68 8a 00 00 00       	push   $0x8a
  jmp alltraps
801063e6:	e9 3c f5 ff ff       	jmp    80105927 <alltraps>

801063eb <vector139>:
.globl vector139
vector139:
  pushl $0
801063eb:	6a 00                	push   $0x0
  pushl $139
801063ed:	68 8b 00 00 00       	push   $0x8b
  jmp alltraps
801063f2:	e9 30 f5 ff ff       	jmp    80105927 <alltraps>

801063f7 <vector140>:
.globl vector140
vector140:
  pushl $0
801063f7:	6a 00                	push   $0x0
  pushl $140
801063f9:	68 8c 00 00 00       	push   $0x8c
  jmp alltraps
801063fe:	e9 24 f5 ff ff       	jmp    80105927 <alltraps>

80106403 <vector141>:
.globl vector141
vector141:
  pushl $0
80106403:	6a 00                	push   $0x0
  pushl $141
80106405:	68 8d 00 00 00       	push   $0x8d
  jmp alltraps
8010640a:	e9 18 f5 ff ff       	jmp    80105927 <alltraps>

8010640f <vector142>:
.globl vector142
vector142:
  pushl $0
8010640f:	6a 00                	push   $0x0
  pushl $142
80106411:	68 8e 00 00 00       	push   $0x8e
  jmp alltraps
80106416:	e9 0c f5 ff ff       	jmp    80105927 <alltraps>

8010641b <vector143>:
.globl vector143
vector143:
  pushl $0
8010641b:	6a 00                	push   $0x0
  pushl $143
8010641d:	68 8f 00 00 00       	push   $0x8f
  jmp alltraps
80106422:	e9 00 f5 ff ff       	jmp    80105927 <alltraps>

80106427 <vector144>:
.globl vector144
vector144:
  pushl $0
80106427:	6a 00                	push   $0x0
  pushl $144
80106429:	68 90 00 00 00       	push   $0x90
  jmp alltraps
8010642e:	e9 f4 f4 ff ff       	jmp    80105927 <alltraps>

80106433 <vector145>:
.globl vector145
vector145:
  pushl $0
80106433:	6a 00                	push   $0x0
  pushl $145
80106435:	68 91 00 00 00       	push   $0x91
  jmp alltraps
8010643a:	e9 e8 f4 ff ff       	jmp    80105927 <alltraps>

8010643f <vector146>:
.globl vector146
vector146:
  pushl $0
8010643f:	6a 00                	push   $0x0
  pushl $146
80106441:	68 92 00 00 00       	push   $0x92
  jmp alltraps
80106446:	e9 dc f4 ff ff       	jmp    80105927 <alltraps>

8010644b <vector147>:
.globl vector147
vector147:
  pushl $0
8010644b:	6a 00                	push   $0x0
  pushl $147
8010644d:	68 93 00 00 00       	push   $0x93
  jmp alltraps
80106452:	e9 d0 f4 ff ff       	jmp    80105927 <alltraps>

80106457 <vector148>:
.globl vector148
vector148:
  pushl $0
80106457:	6a 00                	push   $0x0
  pushl $148
80106459:	68 94 00 00 00       	push   $0x94
  jmp alltraps
8010645e:	e9 c4 f4 ff ff       	jmp    80105927 <alltraps>

80106463 <vector149>:
.globl vector149
vector149:
  pushl $0
80106463:	6a 00                	push   $0x0
  pushl $149
80106465:	68 95 00 00 00       	push   $0x95
  jmp alltraps
8010646a:	e9 b8 f4 ff ff       	jmp    80105927 <alltraps>

8010646f <vector150>:
.globl vector150
vector150:
  pushl $0
8010646f:	6a 00                	push   $0x0
  pushl $150
80106471:	68 96 00 00 00       	push   $0x96
  jmp alltraps
80106476:	e9 ac f4 ff ff       	jmp    80105927 <alltraps>

8010647b <vector151>:
.globl vector151
vector151:
  pushl $0
8010647b:	6a 00                	push   $0x0
  pushl $151
8010647d:	68 97 00 00 00       	push   $0x97
  jmp alltraps
80106482:	e9 a0 f4 ff ff       	jmp    80105927 <alltraps>

80106487 <vector152>:
.globl vector152
vector152:
  pushl $0
80106487:	6a 00                	push   $0x0
  pushl $152
80106489:	68 98 00 00 00       	push   $0x98
  jmp alltraps
8010648e:	e9 94 f4 ff ff       	jmp    80105927 <alltraps>

80106493 <vector153>:
.globl vector153
vector153:
  pushl $0
80106493:	6a 00                	push   $0x0
  pushl $153
80106495:	68 99 00 00 00       	push   $0x99
  jmp alltraps
8010649a:	e9 88 f4 ff ff       	jmp    80105927 <alltraps>

8010649f <vector154>:
.globl vector154
vector154:
  pushl $0
8010649f:	6a 00                	push   $0x0
  pushl $154
801064a1:	68 9a 00 00 00       	push   $0x9a
  jmp alltraps
801064a6:	e9 7c f4 ff ff       	jmp    80105927 <alltraps>

801064ab <vector155>:
.globl vector155
vector155:
  pushl $0
801064ab:	6a 00                	push   $0x0
  pushl $155
801064ad:	68 9b 00 00 00       	push   $0x9b
  jmp alltraps
801064b2:	e9 70 f4 ff ff       	jmp    80105927 <alltraps>

801064b7 <vector156>:
.globl vector156
vector156:
  pushl $0
801064b7:	6a 00                	push   $0x0
  pushl $156
801064b9:	68 9c 00 00 00       	push   $0x9c
  jmp alltraps
801064be:	e9 64 f4 ff ff       	jmp    80105927 <alltraps>

801064c3 <vector157>:
.globl vector157
vector157:
  pushl $0
801064c3:	6a 00                	push   $0x0
  pushl $157
801064c5:	68 9d 00 00 00       	push   $0x9d
  jmp alltraps
801064ca:	e9 58 f4 ff ff       	jmp    80105927 <alltraps>

801064cf <vector158>:
.globl vector158
vector158:
  pushl $0
801064cf:	6a 00                	push   $0x0
  pushl $158
801064d1:	68 9e 00 00 00       	push   $0x9e
  jmp alltraps
801064d6:	e9 4c f4 ff ff       	jmp    80105927 <alltraps>

801064db <vector159>:
.globl vector159
vector159:
  pushl $0
801064db:	6a 00                	push   $0x0
  pushl $159
801064dd:	68 9f 00 00 00       	push   $0x9f
  jmp alltraps
801064e2:	e9 40 f4 ff ff       	jmp    80105927 <alltraps>

801064e7 <vector160>:
.globl vector160
vector160:
  pushl $0
801064e7:	6a 00                	push   $0x0
  pushl $160
801064e9:	68 a0 00 00 00       	push   $0xa0
  jmp alltraps
801064ee:	e9 34 f4 ff ff       	jmp    80105927 <alltraps>

801064f3 <vector161>:
.globl vector161
vector161:
  pushl $0
801064f3:	6a 00                	push   $0x0
  pushl $161
801064f5:	68 a1 00 00 00       	push   $0xa1
  jmp alltraps
801064fa:	e9 28 f4 ff ff       	jmp    80105927 <alltraps>

801064ff <vector162>:
.globl vector162
vector162:
  pushl $0
801064ff:	6a 00                	push   $0x0
  pushl $162
80106501:	68 a2 00 00 00       	push   $0xa2
  jmp alltraps
80106506:	e9 1c f4 ff ff       	jmp    80105927 <alltraps>

8010650b <vector163>:
.globl vector163
vector163:
  pushl $0
8010650b:	6a 00                	push   $0x0
  pushl $163
8010650d:	68 a3 00 00 00       	push   $0xa3
  jmp alltraps
80106512:	e9 10 f4 ff ff       	jmp    80105927 <alltraps>

80106517 <vector164>:
.globl vector164
vector164:
  pushl $0
80106517:	6a 00                	push   $0x0
  pushl $164
80106519:	68 a4 00 00 00       	push   $0xa4
  jmp alltraps
8010651e:	e9 04 f4 ff ff       	jmp    80105927 <alltraps>

80106523 <vector165>:
.globl vector165
vector165:
  pushl $0
80106523:	6a 00                	push   $0x0
  pushl $165
80106525:	68 a5 00 00 00       	push   $0xa5
  jmp alltraps
8010652a:	e9 f8 f3 ff ff       	jmp    80105927 <alltraps>

8010652f <vector166>:
.globl vector166
vector166:
  pushl $0
8010652f:	6a 00                	push   $0x0
  pushl $166
80106531:	68 a6 00 00 00       	push   $0xa6
  jmp alltraps
80106536:	e9 ec f3 ff ff       	jmp    80105927 <alltraps>

8010653b <vector167>:
.globl vector167
vector167:
  pushl $0
8010653b:	6a 00                	push   $0x0
  pushl $167
8010653d:	68 a7 00 00 00       	push   $0xa7
  jmp alltraps
80106542:	e9 e0 f3 ff ff       	jmp    80105927 <alltraps>

80106547 <vector168>:
.globl vector168
vector168:
  pushl $0
80106547:	6a 00                	push   $0x0
  pushl $168
80106549:	68 a8 00 00 00       	push   $0xa8
  jmp alltraps
8010654e:	e9 d4 f3 ff ff       	jmp    80105927 <alltraps>

80106553 <vector169>:
.globl vector169
vector169:
  pushl $0
80106553:	6a 00                	push   $0x0
  pushl $169
80106555:	68 a9 00 00 00       	push   $0xa9
  jmp alltraps
8010655a:	e9 c8 f3 ff ff       	jmp    80105927 <alltraps>

8010655f <vector170>:
.globl vector170
vector170:
  pushl $0
8010655f:	6a 00                	push   $0x0
  pushl $170
80106561:	68 aa 00 00 00       	push   $0xaa
  jmp alltraps
80106566:	e9 bc f3 ff ff       	jmp    80105927 <alltraps>

8010656b <vector171>:
.globl vector171
vector171:
  pushl $0
8010656b:	6a 00                	push   $0x0
  pushl $171
8010656d:	68 ab 00 00 00       	push   $0xab
  jmp alltraps
80106572:	e9 b0 f3 ff ff       	jmp    80105927 <alltraps>

80106577 <vector172>:
.globl vector172
vector172:
  pushl $0
80106577:	6a 00                	push   $0x0
  pushl $172
80106579:	68 ac 00 00 00       	push   $0xac
  jmp alltraps
8010657e:	e9 a4 f3 ff ff       	jmp    80105927 <alltraps>

80106583 <vector173>:
.globl vector173
vector173:
  pushl $0
80106583:	6a 00                	push   $0x0
  pushl $173
80106585:	68 ad 00 00 00       	push   $0xad
  jmp alltraps
8010658a:	e9 98 f3 ff ff       	jmp    80105927 <alltraps>

8010658f <vector174>:
.globl vector174
vector174:
  pushl $0
8010658f:	6a 00                	push   $0x0
  pushl $174
80106591:	68 ae 00 00 00       	push   $0xae
  jmp alltraps
80106596:	e9 8c f3 ff ff       	jmp    80105927 <alltraps>

8010659b <vector175>:
.globl vector175
vector175:
  pushl $0
8010659b:	6a 00                	push   $0x0
  pushl $175
8010659d:	68 af 00 00 00       	push   $0xaf
  jmp alltraps
801065a2:	e9 80 f3 ff ff       	jmp    80105927 <alltraps>

801065a7 <vector176>:
.globl vector176
vector176:
  pushl $0
801065a7:	6a 00                	push   $0x0
  pushl $176
801065a9:	68 b0 00 00 00       	push   $0xb0
  jmp alltraps
801065ae:	e9 74 f3 ff ff       	jmp    80105927 <alltraps>

801065b3 <vector177>:
.globl vector177
vector177:
  pushl $0
801065b3:	6a 00                	push   $0x0
  pushl $177
801065b5:	68 b1 00 00 00       	push   $0xb1
  jmp alltraps
801065ba:	e9 68 f3 ff ff       	jmp    80105927 <alltraps>

801065bf <vector178>:
.globl vector178
vector178:
  pushl $0
801065bf:	6a 00                	push   $0x0
  pushl $178
801065c1:	68 b2 00 00 00       	push   $0xb2
  jmp alltraps
801065c6:	e9 5c f3 ff ff       	jmp    80105927 <alltraps>

801065cb <vector179>:
.globl vector179
vector179:
  pushl $0
801065cb:	6a 00                	push   $0x0
  pushl $179
801065cd:	68 b3 00 00 00       	push   $0xb3
  jmp alltraps
801065d2:	e9 50 f3 ff ff       	jmp    80105927 <alltraps>

801065d7 <vector180>:
.globl vector180
vector180:
  pushl $0
801065d7:	6a 00                	push   $0x0
  pushl $180
801065d9:	68 b4 00 00 00       	push   $0xb4
  jmp alltraps
801065de:	e9 44 f3 ff ff       	jmp    80105927 <alltraps>

801065e3 <vector181>:
.globl vector181
vector181:
  pushl $0
801065e3:	6a 00                	push   $0x0
  pushl $181
801065e5:	68 b5 00 00 00       	push   $0xb5
  jmp alltraps
801065ea:	e9 38 f3 ff ff       	jmp    80105927 <alltraps>

801065ef <vector182>:
.globl vector182
vector182:
  pushl $0
801065ef:	6a 00                	push   $0x0
  pushl $182
801065f1:	68 b6 00 00 00       	push   $0xb6
  jmp alltraps
801065f6:	e9 2c f3 ff ff       	jmp    80105927 <alltraps>

801065fb <vector183>:
.globl vector183
vector183:
  pushl $0
801065fb:	6a 00                	push   $0x0
  pushl $183
801065fd:	68 b7 00 00 00       	push   $0xb7
  jmp alltraps
80106602:	e9 20 f3 ff ff       	jmp    80105927 <alltraps>

80106607 <vector184>:
.globl vector184
vector184:
  pushl $0
80106607:	6a 00                	push   $0x0
  pushl $184
80106609:	68 b8 00 00 00       	push   $0xb8
  jmp alltraps
8010660e:	e9 14 f3 ff ff       	jmp    80105927 <alltraps>

80106613 <vector185>:
.globl vector185
vector185:
  pushl $0
80106613:	6a 00                	push   $0x0
  pushl $185
80106615:	68 b9 00 00 00       	push   $0xb9
  jmp alltraps
8010661a:	e9 08 f3 ff ff       	jmp    80105927 <alltraps>

8010661f <vector186>:
.globl vector186
vector186:
  pushl $0
8010661f:	6a 00                	push   $0x0
  pushl $186
80106621:	68 ba 00 00 00       	push   $0xba
  jmp alltraps
80106626:	e9 fc f2 ff ff       	jmp    80105927 <alltraps>

8010662b <vector187>:
.globl vector187
vector187:
  pushl $0
8010662b:	6a 00                	push   $0x0
  pushl $187
8010662d:	68 bb 00 00 00       	push   $0xbb
  jmp alltraps
80106632:	e9 f0 f2 ff ff       	jmp    80105927 <alltraps>

80106637 <vector188>:
.globl vector188
vector188:
  pushl $0
80106637:	6a 00                	push   $0x0
  pushl $188
80106639:	68 bc 00 00 00       	push   $0xbc
  jmp alltraps
8010663e:	e9 e4 f2 ff ff       	jmp    80105927 <alltraps>

80106643 <vector189>:
.globl vector189
vector189:
  pushl $0
80106643:	6a 00                	push   $0x0
  pushl $189
80106645:	68 bd 00 00 00       	push   $0xbd
  jmp alltraps
8010664a:	e9 d8 f2 ff ff       	jmp    80105927 <alltraps>

8010664f <vector190>:
.globl vector190
vector190:
  pushl $0
8010664f:	6a 00                	push   $0x0
  pushl $190
80106651:	68 be 00 00 00       	push   $0xbe
  jmp alltraps
80106656:	e9 cc f2 ff ff       	jmp    80105927 <alltraps>

8010665b <vector191>:
.globl vector191
vector191:
  pushl $0
8010665b:	6a 00                	push   $0x0
  pushl $191
8010665d:	68 bf 00 00 00       	push   $0xbf
  jmp alltraps
80106662:	e9 c0 f2 ff ff       	jmp    80105927 <alltraps>

80106667 <vector192>:
.globl vector192
vector192:
  pushl $0
80106667:	6a 00                	push   $0x0
  pushl $192
80106669:	68 c0 00 00 00       	push   $0xc0
  jmp alltraps
8010666e:	e9 b4 f2 ff ff       	jmp    80105927 <alltraps>

80106673 <vector193>:
.globl vector193
vector193:
  pushl $0
80106673:	6a 00                	push   $0x0
  pushl $193
80106675:	68 c1 00 00 00       	push   $0xc1
  jmp alltraps
8010667a:	e9 a8 f2 ff ff       	jmp    80105927 <alltraps>

8010667f <vector194>:
.globl vector194
vector194:
  pushl $0
8010667f:	6a 00                	push   $0x0
  pushl $194
80106681:	68 c2 00 00 00       	push   $0xc2
  jmp alltraps
80106686:	e9 9c f2 ff ff       	jmp    80105927 <alltraps>

8010668b <vector195>:
.globl vector195
vector195:
  pushl $0
8010668b:	6a 00                	push   $0x0
  pushl $195
8010668d:	68 c3 00 00 00       	push   $0xc3
  jmp alltraps
80106692:	e9 90 f2 ff ff       	jmp    80105927 <alltraps>

80106697 <vector196>:
.globl vector196
vector196:
  pushl $0
80106697:	6a 00                	push   $0x0
  pushl $196
80106699:	68 c4 00 00 00       	push   $0xc4
  jmp alltraps
8010669e:	e9 84 f2 ff ff       	jmp    80105927 <alltraps>

801066a3 <vector197>:
.globl vector197
vector197:
  pushl $0
801066a3:	6a 00                	push   $0x0
  pushl $197
801066a5:	68 c5 00 00 00       	push   $0xc5
  jmp alltraps
801066aa:	e9 78 f2 ff ff       	jmp    80105927 <alltraps>

801066af <vector198>:
.globl vector198
vector198:
  pushl $0
801066af:	6a 00                	push   $0x0
  pushl $198
801066b1:	68 c6 00 00 00       	push   $0xc6
  jmp alltraps
801066b6:	e9 6c f2 ff ff       	jmp    80105927 <alltraps>

801066bb <vector199>:
.globl vector199
vector199:
  pushl $0
801066bb:	6a 00                	push   $0x0
  pushl $199
801066bd:	68 c7 00 00 00       	push   $0xc7
  jmp alltraps
801066c2:	e9 60 f2 ff ff       	jmp    80105927 <alltraps>

801066c7 <vector200>:
.globl vector200
vector200:
  pushl $0
801066c7:	6a 00                	push   $0x0
  pushl $200
801066c9:	68 c8 00 00 00       	push   $0xc8
  jmp alltraps
801066ce:	e9 54 f2 ff ff       	jmp    80105927 <alltraps>

801066d3 <vector201>:
.globl vector201
vector201:
  pushl $0
801066d3:	6a 00                	push   $0x0
  pushl $201
801066d5:	68 c9 00 00 00       	push   $0xc9
  jmp alltraps
801066da:	e9 48 f2 ff ff       	jmp    80105927 <alltraps>

801066df <vector202>:
.globl vector202
vector202:
  pushl $0
801066df:	6a 00                	push   $0x0
  pushl $202
801066e1:	68 ca 00 00 00       	push   $0xca
  jmp alltraps
801066e6:	e9 3c f2 ff ff       	jmp    80105927 <alltraps>

801066eb <vector203>:
.globl vector203
vector203:
  pushl $0
801066eb:	6a 00                	push   $0x0
  pushl $203
801066ed:	68 cb 00 00 00       	push   $0xcb
  jmp alltraps
801066f2:	e9 30 f2 ff ff       	jmp    80105927 <alltraps>

801066f7 <vector204>:
.globl vector204
vector204:
  pushl $0
801066f7:	6a 00                	push   $0x0
  pushl $204
801066f9:	68 cc 00 00 00       	push   $0xcc
  jmp alltraps
801066fe:	e9 24 f2 ff ff       	jmp    80105927 <alltraps>

80106703 <vector205>:
.globl vector205
vector205:
  pushl $0
80106703:	6a 00                	push   $0x0
  pushl $205
80106705:	68 cd 00 00 00       	push   $0xcd
  jmp alltraps
8010670a:	e9 18 f2 ff ff       	jmp    80105927 <alltraps>

8010670f <vector206>:
.globl vector206
vector206:
  pushl $0
8010670f:	6a 00                	push   $0x0
  pushl $206
80106711:	68 ce 00 00 00       	push   $0xce
  jmp alltraps
80106716:	e9 0c f2 ff ff       	jmp    80105927 <alltraps>

8010671b <vector207>:
.globl vector207
vector207:
  pushl $0
8010671b:	6a 00                	push   $0x0
  pushl $207
8010671d:	68 cf 00 00 00       	push   $0xcf
  jmp alltraps
80106722:	e9 00 f2 ff ff       	jmp    80105927 <alltraps>

80106727 <vector208>:
.globl vector208
vector208:
  pushl $0
80106727:	6a 00                	push   $0x0
  pushl $208
80106729:	68 d0 00 00 00       	push   $0xd0
  jmp alltraps
8010672e:	e9 f4 f1 ff ff       	jmp    80105927 <alltraps>

80106733 <vector209>:
.globl vector209
vector209:
  pushl $0
80106733:	6a 00                	push   $0x0
  pushl $209
80106735:	68 d1 00 00 00       	push   $0xd1
  jmp alltraps
8010673a:	e9 e8 f1 ff ff       	jmp    80105927 <alltraps>

8010673f <vector210>:
.globl vector210
vector210:
  pushl $0
8010673f:	6a 00                	push   $0x0
  pushl $210
80106741:	68 d2 00 00 00       	push   $0xd2
  jmp alltraps
80106746:	e9 dc f1 ff ff       	jmp    80105927 <alltraps>

8010674b <vector211>:
.globl vector211
vector211:
  pushl $0
8010674b:	6a 00                	push   $0x0
  pushl $211
8010674d:	68 d3 00 00 00       	push   $0xd3
  jmp alltraps
80106752:	e9 d0 f1 ff ff       	jmp    80105927 <alltraps>

80106757 <vector212>:
.globl vector212
vector212:
  pushl $0
80106757:	6a 00                	push   $0x0
  pushl $212
80106759:	68 d4 00 00 00       	push   $0xd4
  jmp alltraps
8010675e:	e9 c4 f1 ff ff       	jmp    80105927 <alltraps>

80106763 <vector213>:
.globl vector213
vector213:
  pushl $0
80106763:	6a 00                	push   $0x0
  pushl $213
80106765:	68 d5 00 00 00       	push   $0xd5
  jmp alltraps
8010676a:	e9 b8 f1 ff ff       	jmp    80105927 <alltraps>

8010676f <vector214>:
.globl vector214
vector214:
  pushl $0
8010676f:	6a 00                	push   $0x0
  pushl $214
80106771:	68 d6 00 00 00       	push   $0xd6
  jmp alltraps
80106776:	e9 ac f1 ff ff       	jmp    80105927 <alltraps>

8010677b <vector215>:
.globl vector215
vector215:
  pushl $0
8010677b:	6a 00                	push   $0x0
  pushl $215
8010677d:	68 d7 00 00 00       	push   $0xd7
  jmp alltraps
80106782:	e9 a0 f1 ff ff       	jmp    80105927 <alltraps>

80106787 <vector216>:
.globl vector216
vector216:
  pushl $0
80106787:	6a 00                	push   $0x0
  pushl $216
80106789:	68 d8 00 00 00       	push   $0xd8
  jmp alltraps
8010678e:	e9 94 f1 ff ff       	jmp    80105927 <alltraps>

80106793 <vector217>:
.globl vector217
vector217:
  pushl $0
80106793:	6a 00                	push   $0x0
  pushl $217
80106795:	68 d9 00 00 00       	push   $0xd9
  jmp alltraps
8010679a:	e9 88 f1 ff ff       	jmp    80105927 <alltraps>

8010679f <vector218>:
.globl vector218
vector218:
  pushl $0
8010679f:	6a 00                	push   $0x0
  pushl $218
801067a1:	68 da 00 00 00       	push   $0xda
  jmp alltraps
801067a6:	e9 7c f1 ff ff       	jmp    80105927 <alltraps>

801067ab <vector219>:
.globl vector219
vector219:
  pushl $0
801067ab:	6a 00                	push   $0x0
  pushl $219
801067ad:	68 db 00 00 00       	push   $0xdb
  jmp alltraps
801067b2:	e9 70 f1 ff ff       	jmp    80105927 <alltraps>

801067b7 <vector220>:
.globl vector220
vector220:
  pushl $0
801067b7:	6a 00                	push   $0x0
  pushl $220
801067b9:	68 dc 00 00 00       	push   $0xdc
  jmp alltraps
801067be:	e9 64 f1 ff ff       	jmp    80105927 <alltraps>

801067c3 <vector221>:
.globl vector221
vector221:
  pushl $0
801067c3:	6a 00                	push   $0x0
  pushl $221
801067c5:	68 dd 00 00 00       	push   $0xdd
  jmp alltraps
801067ca:	e9 58 f1 ff ff       	jmp    80105927 <alltraps>

801067cf <vector222>:
.globl vector222
vector222:
  pushl $0
801067cf:	6a 00                	push   $0x0
  pushl $222
801067d1:	68 de 00 00 00       	push   $0xde
  jmp alltraps
801067d6:	e9 4c f1 ff ff       	jmp    80105927 <alltraps>

801067db <vector223>:
.globl vector223
vector223:
  pushl $0
801067db:	6a 00                	push   $0x0
  pushl $223
801067dd:	68 df 00 00 00       	push   $0xdf
  jmp alltraps
801067e2:	e9 40 f1 ff ff       	jmp    80105927 <alltraps>

801067e7 <vector224>:
.globl vector224
vector224:
  pushl $0
801067e7:	6a 00                	push   $0x0
  pushl $224
801067e9:	68 e0 00 00 00       	push   $0xe0
  jmp alltraps
801067ee:	e9 34 f1 ff ff       	jmp    80105927 <alltraps>

801067f3 <vector225>:
.globl vector225
vector225:
  pushl $0
801067f3:	6a 00                	push   $0x0
  pushl $225
801067f5:	68 e1 00 00 00       	push   $0xe1
  jmp alltraps
801067fa:	e9 28 f1 ff ff       	jmp    80105927 <alltraps>

801067ff <vector226>:
.globl vector226
vector226:
  pushl $0
801067ff:	6a 00                	push   $0x0
  pushl $226
80106801:	68 e2 00 00 00       	push   $0xe2
  jmp alltraps
80106806:	e9 1c f1 ff ff       	jmp    80105927 <alltraps>

8010680b <vector227>:
.globl vector227
vector227:
  pushl $0
8010680b:	6a 00                	push   $0x0
  pushl $227
8010680d:	68 e3 00 00 00       	push   $0xe3
  jmp alltraps
80106812:	e9 10 f1 ff ff       	jmp    80105927 <alltraps>

80106817 <vector228>:
.globl vector228
vector228:
  pushl $0
80106817:	6a 00                	push   $0x0
  pushl $228
80106819:	68 e4 00 00 00       	push   $0xe4
  jmp alltraps
8010681e:	e9 04 f1 ff ff       	jmp    80105927 <alltraps>

80106823 <vector229>:
.globl vector229
vector229:
  pushl $0
80106823:	6a 00                	push   $0x0
  pushl $229
80106825:	68 e5 00 00 00       	push   $0xe5
  jmp alltraps
8010682a:	e9 f8 f0 ff ff       	jmp    80105927 <alltraps>

8010682f <vector230>:
.globl vector230
vector230:
  pushl $0
8010682f:	6a 00                	push   $0x0
  pushl $230
80106831:	68 e6 00 00 00       	push   $0xe6
  jmp alltraps
80106836:	e9 ec f0 ff ff       	jmp    80105927 <alltraps>

8010683b <vector231>:
.globl vector231
vector231:
  pushl $0
8010683b:	6a 00                	push   $0x0
  pushl $231
8010683d:	68 e7 00 00 00       	push   $0xe7
  jmp alltraps
80106842:	e9 e0 f0 ff ff       	jmp    80105927 <alltraps>

80106847 <vector232>:
.globl vector232
vector232:
  pushl $0
80106847:	6a 00                	push   $0x0
  pushl $232
80106849:	68 e8 00 00 00       	push   $0xe8
  jmp alltraps
8010684e:	e9 d4 f0 ff ff       	jmp    80105927 <alltraps>

80106853 <vector233>:
.globl vector233
vector233:
  pushl $0
80106853:	6a 00                	push   $0x0
  pushl $233
80106855:	68 e9 00 00 00       	push   $0xe9
  jmp alltraps
8010685a:	e9 c8 f0 ff ff       	jmp    80105927 <alltraps>

8010685f <vector234>:
.globl vector234
vector234:
  pushl $0
8010685f:	6a 00                	push   $0x0
  pushl $234
80106861:	68 ea 00 00 00       	push   $0xea
  jmp alltraps
80106866:	e9 bc f0 ff ff       	jmp    80105927 <alltraps>

8010686b <vector235>:
.globl vector235
vector235:
  pushl $0
8010686b:	6a 00                	push   $0x0
  pushl $235
8010686d:	68 eb 00 00 00       	push   $0xeb
  jmp alltraps
80106872:	e9 b0 f0 ff ff       	jmp    80105927 <alltraps>

80106877 <vector236>:
.globl vector236
vector236:
  pushl $0
80106877:	6a 00                	push   $0x0
  pushl $236
80106879:	68 ec 00 00 00       	push   $0xec
  jmp alltraps
8010687e:	e9 a4 f0 ff ff       	jmp    80105927 <alltraps>

80106883 <vector237>:
.globl vector237
vector237:
  pushl $0
80106883:	6a 00                	push   $0x0
  pushl $237
80106885:	68 ed 00 00 00       	push   $0xed
  jmp alltraps
8010688a:	e9 98 f0 ff ff       	jmp    80105927 <alltraps>

8010688f <vector238>:
.globl vector238
vector238:
  pushl $0
8010688f:	6a 00                	push   $0x0
  pushl $238
80106891:	68 ee 00 00 00       	push   $0xee
  jmp alltraps
80106896:	e9 8c f0 ff ff       	jmp    80105927 <alltraps>

8010689b <vector239>:
.globl vector239
vector239:
  pushl $0
8010689b:	6a 00                	push   $0x0
  pushl $239
8010689d:	68 ef 00 00 00       	push   $0xef
  jmp alltraps
801068a2:	e9 80 f0 ff ff       	jmp    80105927 <alltraps>

801068a7 <vector240>:
.globl vector240
vector240:
  pushl $0
801068a7:	6a 00                	push   $0x0
  pushl $240
801068a9:	68 f0 00 00 00       	push   $0xf0
  jmp alltraps
801068ae:	e9 74 f0 ff ff       	jmp    80105927 <alltraps>

801068b3 <vector241>:
.globl vector241
vector241:
  pushl $0
801068b3:	6a 00                	push   $0x0
  pushl $241
801068b5:	68 f1 00 00 00       	push   $0xf1
  jmp alltraps
801068ba:	e9 68 f0 ff ff       	jmp    80105927 <alltraps>

801068bf <vector242>:
.globl vector242
vector242:
  pushl $0
801068bf:	6a 00                	push   $0x0
  pushl $242
801068c1:	68 f2 00 00 00       	push   $0xf2
  jmp alltraps
801068c6:	e9 5c f0 ff ff       	jmp    80105927 <alltraps>

801068cb <vector243>:
.globl vector243
vector243:
  pushl $0
801068cb:	6a 00                	push   $0x0
  pushl $243
801068cd:	68 f3 00 00 00       	push   $0xf3
  jmp alltraps
801068d2:	e9 50 f0 ff ff       	jmp    80105927 <alltraps>

801068d7 <vector244>:
.globl vector244
vector244:
  pushl $0
801068d7:	6a 00                	push   $0x0
  pushl $244
801068d9:	68 f4 00 00 00       	push   $0xf4
  jmp alltraps
801068de:	e9 44 f0 ff ff       	jmp    80105927 <alltraps>

801068e3 <vector245>:
.globl vector245
vector245:
  pushl $0
801068e3:	6a 00                	push   $0x0
  pushl $245
801068e5:	68 f5 00 00 00       	push   $0xf5
  jmp alltraps
801068ea:	e9 38 f0 ff ff       	jmp    80105927 <alltraps>

801068ef <vector246>:
.globl vector246
vector246:
  pushl $0
801068ef:	6a 00                	push   $0x0
  pushl $246
801068f1:	68 f6 00 00 00       	push   $0xf6
  jmp alltraps
801068f6:	e9 2c f0 ff ff       	jmp    80105927 <alltraps>

801068fb <vector247>:
.globl vector247
vector247:
  pushl $0
801068fb:	6a 00                	push   $0x0
  pushl $247
801068fd:	68 f7 00 00 00       	push   $0xf7
  jmp alltraps
80106902:	e9 20 f0 ff ff       	jmp    80105927 <alltraps>

80106907 <vector248>:
.globl vector248
vector248:
  pushl $0
80106907:	6a 00                	push   $0x0
  pushl $248
80106909:	68 f8 00 00 00       	push   $0xf8
  jmp alltraps
8010690e:	e9 14 f0 ff ff       	jmp    80105927 <alltraps>

80106913 <vector249>:
.globl vector249
vector249:
  pushl $0
80106913:	6a 00                	push   $0x0
  pushl $249
80106915:	68 f9 00 00 00       	push   $0xf9
  jmp alltraps
8010691a:	e9 08 f0 ff ff       	jmp    80105927 <alltraps>

8010691f <vector250>:
.globl vector250
vector250:
  pushl $0
8010691f:	6a 00                	push   $0x0
  pushl $250
80106921:	68 fa 00 00 00       	push   $0xfa
  jmp alltraps
80106926:	e9 fc ef ff ff       	jmp    80105927 <alltraps>

8010692b <vector251>:
.globl vector251
vector251:
  pushl $0
8010692b:	6a 00                	push   $0x0
  pushl $251
8010692d:	68 fb 00 00 00       	push   $0xfb
  jmp alltraps
80106932:	e9 f0 ef ff ff       	jmp    80105927 <alltraps>

80106937 <vector252>:
.globl vector252
vector252:
  pushl $0
80106937:	6a 00                	push   $0x0
  pushl $252
80106939:	68 fc 00 00 00       	push   $0xfc
  jmp alltraps
8010693e:	e9 e4 ef ff ff       	jmp    80105927 <alltraps>

80106943 <vector253>:
.globl vector253
vector253:
  pushl $0
80106943:	6a 00                	push   $0x0
  pushl $253
80106945:	68 fd 00 00 00       	push   $0xfd
  jmp alltraps
8010694a:	e9 d8 ef ff ff       	jmp    80105927 <alltraps>

8010694f <vector254>:
.globl vector254
vector254:
  pushl $0
8010694f:	6a 00                	push   $0x0
  pushl $254
80106951:	68 fe 00 00 00       	push   $0xfe
  jmp alltraps
80106956:	e9 cc ef ff ff       	jmp    80105927 <alltraps>

8010695b <vector255>:
.globl vector255
vector255:
  pushl $0
8010695b:	6a 00                	push   $0x0
  pushl $255
8010695d:	68 ff 00 00 00       	push   $0xff
  jmp alltraps
80106962:	e9 c0 ef ff ff       	jmp    80105927 <alltraps>
80106967:	66 90                	xchg   %ax,%ax
80106969:	66 90                	xchg   %ax,%ax
8010696b:	66 90                	xchg   %ax,%ax
8010696d:	66 90                	xchg   %ax,%ax
8010696f:	90                   	nop

80106970 <walkpgdir>:
// Return the address of the PTE in page table pgdir
// that corresponds to virtual address va.  If alloc!=0,
// create any required page table pages.
static pte_t *
walkpgdir(pde_t *pgdir, const void *va, int alloc)
{
80106970:	55                   	push   %ebp
80106971:	89 e5                	mov    %esp,%ebp
80106973:	57                   	push   %edi
80106974:	56                   	push   %esi
80106975:	89 d6                	mov    %edx,%esi
  pde_t *pde;
  pte_t *pgtab;

  pde = &pgdir[PDX(va)];
80106977:	c1 ea 16             	shr    $0x16,%edx
{
8010697a:	53                   	push   %ebx
  pde = &pgdir[PDX(va)];
8010697b:	8d 3c 90             	lea    (%eax,%edx,4),%edi
{
8010697e:	83 ec 0c             	sub    $0xc,%esp
  if(*pde & PTE_P){
80106981:	8b 07                	mov    (%edi),%eax
80106983:	a8 01                	test   $0x1,%al
80106985:	74 29                	je     801069b0 <walkpgdir+0x40>
    pgtab = (pte_t*)P2V(PTE_ADDR(*pde));
80106987:	25 00 f0 ff ff       	and    $0xfffff000,%eax
8010698c:	8d 98 00 00 00 80    	lea    -0x80000000(%eax),%ebx
    // The permissions here are overly generous, but they can
    // be further restricted by the permissions in the page table
    // entries, if necessary.
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
  }
  return &pgtab[PTX(va)];
80106992:	c1 ee 0a             	shr    $0xa,%esi
}
80106995:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return &pgtab[PTX(va)];
80106998:	89 f2                	mov    %esi,%edx
8010699a:	81 e2 fc 0f 00 00    	and    $0xffc,%edx
801069a0:	8d 04 13             	lea    (%ebx,%edx,1),%eax
}
801069a3:	5b                   	pop    %ebx
801069a4:	5e                   	pop    %esi
801069a5:	5f                   	pop    %edi
801069a6:	5d                   	pop    %ebp
801069a7:	c3                   	ret    
801069a8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801069af:	90                   	nop
    if(!alloc || (pgtab = (pte_t*)kalloc()) == 0)
801069b0:	85 c9                	test   %ecx,%ecx
801069b2:	74 2c                	je     801069e0 <walkpgdir+0x70>
801069b4:	e8 27 bc ff ff       	call   801025e0 <kalloc>
801069b9:	89 c3                	mov    %eax,%ebx
801069bb:	85 c0                	test   %eax,%eax
801069bd:	74 21                	je     801069e0 <walkpgdir+0x70>
    memset(pgtab, 0, PGSIZE);
801069bf:	83 ec 04             	sub    $0x4,%esp
801069c2:	68 00 10 00 00       	push   $0x1000
801069c7:	6a 00                	push   $0x0
801069c9:	50                   	push   %eax
801069ca:	e8 81 dc ff ff       	call   80104650 <memset>
    *pde = V2P(pgtab) | PTE_P | PTE_W | PTE_U;
801069cf:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
801069d5:	83 c4 10             	add    $0x10,%esp
801069d8:	83 c8 07             	or     $0x7,%eax
801069db:	89 07                	mov    %eax,(%edi)
801069dd:	eb b3                	jmp    80106992 <walkpgdir+0x22>
801069df:	90                   	nop
}
801069e0:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return 0;
801069e3:	31 c0                	xor    %eax,%eax
}
801069e5:	5b                   	pop    %ebx
801069e6:	5e                   	pop    %esi
801069e7:	5f                   	pop    %edi
801069e8:	5d                   	pop    %ebp
801069e9:	c3                   	ret    
801069ea:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

801069f0 <deallocuvm.part.0>:
// Deallocate user pages to bring the process size from oldsz to
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
int
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
801069f0:	55                   	push   %ebp
801069f1:	89 e5                	mov    %esp,%ebp
801069f3:	57                   	push   %edi
801069f4:	89 c7                	mov    %eax,%edi
801069f6:	56                   	push   %esi
801069f7:	53                   	push   %ebx
  uint a, pa;

  if(newsz >= oldsz)
    return oldsz;

  a = PGROUNDUP(newsz);
801069f8:	8d 99 ff 0f 00 00    	lea    0xfff(%ecx),%ebx
801069fe:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
deallocuvm(pde_t *pgdir, uint oldsz, uint newsz)
80106a04:	83 ec 1c             	sub    $0x1c,%esp
80106a07:	89 4d e0             	mov    %ecx,-0x20(%ebp)
  for(; a  < oldsz; a += PGSIZE){
80106a0a:	39 d3                	cmp    %edx,%ebx
80106a0c:	73 5a                	jae    80106a68 <deallocuvm.part.0+0x78>
80106a0e:	89 d6                	mov    %edx,%esi
80106a10:	eb 10                	jmp    80106a22 <deallocuvm.part.0+0x32>
80106a12:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
80106a18:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80106a1e:	39 de                	cmp    %ebx,%esi
80106a20:	76 46                	jbe    80106a68 <deallocuvm.part.0+0x78>
    pte = walkpgdir(pgdir, (char*)a, 0);
80106a22:	31 c9                	xor    %ecx,%ecx
80106a24:	89 da                	mov    %ebx,%edx
80106a26:	89 f8                	mov    %edi,%eax
80106a28:	e8 43 ff ff ff       	call   80106970 <walkpgdir>
    if(!pte)
80106a2d:	85 c0                	test   %eax,%eax
80106a2f:	74 47                	je     80106a78 <deallocuvm.part.0+0x88>
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
    else if((*pte & PTE_P) != 0){
80106a31:	8b 10                	mov    (%eax),%edx
80106a33:	f6 c2 01             	test   $0x1,%dl
80106a36:	74 e0                	je     80106a18 <deallocuvm.part.0+0x28>
      pa = PTE_ADDR(*pte);
      if(pa == 0)
80106a38:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
80106a3e:	74 46                	je     80106a86 <deallocuvm.part.0+0x96>
        panic("kfree");
      char *v = P2V(pa);
      kfree(v);
80106a40:	83 ec 0c             	sub    $0xc,%esp
      char *v = P2V(pa);
80106a43:	81 c2 00 00 00 80    	add    $0x80000000,%edx
80106a49:	89 45 e4             	mov    %eax,-0x1c(%ebp)
      kfree(v);
80106a4c:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80106a52:	52                   	push   %edx
80106a53:	e8 c8 b9 ff ff       	call   80102420 <kfree>
      *pte = 0;
80106a58:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80106a5b:	83 c4 10             	add    $0x10,%esp
80106a5e:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
  for(; a  < oldsz; a += PGSIZE){
80106a64:	39 de                	cmp    %ebx,%esi
80106a66:	77 ba                	ja     80106a22 <deallocuvm.part.0+0x32>
    }
  }
  return newsz;
}
80106a68:	8b 45 e0             	mov    -0x20(%ebp),%eax
80106a6b:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106a6e:	5b                   	pop    %ebx
80106a6f:	5e                   	pop    %esi
80106a70:	5f                   	pop    %edi
80106a71:	5d                   	pop    %ebp
80106a72:	c3                   	ret    
80106a73:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80106a77:	90                   	nop
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
80106a78:	81 e3 00 00 c0 ff    	and    $0xffc00000,%ebx
80106a7e:	81 c3 00 00 40 00    	add    $0x400000,%ebx
80106a84:	eb 98                	jmp    80106a1e <deallocuvm.part.0+0x2e>
        panic("kfree");
80106a86:	83 ec 0c             	sub    $0xc,%esp
80106a89:	68 ee 7a 10 80       	push   $0x80107aee
80106a8e:	e8 fd 98 ff ff       	call   80100390 <panic>
80106a93:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106a9a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80106aa0 <seginit>:
{
80106aa0:	55                   	push   %ebp
80106aa1:	89 e5                	mov    %esp,%ebp
80106aa3:	83 ec 18             	sub    $0x18,%esp
  c = &cpus[cpuid()];
80106aa6:	e8 55 ce ff ff       	call   80103900 <cpuid>
  pd[0] = size-1;
80106aab:	ba 2f 00 00 00       	mov    $0x2f,%edx
80106ab0:	69 c0 b0 00 00 00    	imul   $0xb0,%eax,%eax
80106ab6:	66 89 55 f2          	mov    %dx,-0xe(%ebp)
  c->gdt[SEG_KCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, 0);
80106aba:	c7 80 b8 38 11 80 ff 	movl   $0xffff,-0x7feec748(%eax)
80106ac1:	ff 00 00 
80106ac4:	c7 80 bc 38 11 80 00 	movl   $0xcf9a00,-0x7feec744(%eax)
80106acb:	9a cf 00 
  c->gdt[SEG_KDATA] = SEG(STA_W, 0, 0xffffffff, 0);
80106ace:	c7 80 c0 38 11 80 ff 	movl   $0xffff,-0x7feec740(%eax)
80106ad5:	ff 00 00 
80106ad8:	c7 80 c4 38 11 80 00 	movl   $0xcf9200,-0x7feec73c(%eax)
80106adf:	92 cf 00 
  c->gdt[SEG_UCODE] = SEG(STA_X|STA_R, 0, 0xffffffff, DPL_USER);
80106ae2:	c7 80 c8 38 11 80 ff 	movl   $0xffff,-0x7feec738(%eax)
80106ae9:	ff 00 00 
80106aec:	c7 80 cc 38 11 80 00 	movl   $0xcffa00,-0x7feec734(%eax)
80106af3:	fa cf 00 
  c->gdt[SEG_UDATA] = SEG(STA_W, 0, 0xffffffff, DPL_USER);
80106af6:	c7 80 d0 38 11 80 ff 	movl   $0xffff,-0x7feec730(%eax)
80106afd:	ff 00 00 
80106b00:	c7 80 d4 38 11 80 00 	movl   $0xcff200,-0x7feec72c(%eax)
80106b07:	f2 cf 00 
  lgdt(c->gdt, sizeof(c->gdt));
80106b0a:	05 b0 38 11 80       	add    $0x801138b0,%eax
  pd[1] = (uint)p;
80106b0f:	66 89 45 f4          	mov    %ax,-0xc(%ebp)
  pd[2] = (uint)p >> 16;
80106b13:	c1 e8 10             	shr    $0x10,%eax
80106b16:	66 89 45 f6          	mov    %ax,-0xa(%ebp)
  asm volatile("lgdt (%0)" : : "r" (pd));
80106b1a:	8d 45 f2             	lea    -0xe(%ebp),%eax
80106b1d:	0f 01 10             	lgdtl  (%eax)
}
80106b20:	c9                   	leave  
80106b21:	c3                   	ret    
80106b22:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106b29:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80106b30 <mappages>:
{
80106b30:	55                   	push   %ebp
80106b31:	89 e5                	mov    %esp,%ebp
80106b33:	57                   	push   %edi
80106b34:	56                   	push   %esi
80106b35:	53                   	push   %ebx
80106b36:	83 ec 1c             	sub    $0x1c,%esp
80106b39:	8b 45 0c             	mov    0xc(%ebp),%eax
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
80106b3c:	8b 55 10             	mov    0x10(%ebp),%edx
80106b3f:	8b 7d 14             	mov    0x14(%ebp),%edi
  a = (char*)PGROUNDDOWN((uint)va);
80106b42:	89 c6                	mov    %eax,%esi
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
80106b44:	8d 44 10 ff          	lea    -0x1(%eax,%edx,1),%eax
  a = (char*)PGROUNDDOWN((uint)va);
80106b48:	81 e6 00 f0 ff ff    	and    $0xfffff000,%esi
  last = (char*)PGROUNDDOWN(((uint)va) + size - 1);
80106b4e:	25 00 f0 ff ff       	and    $0xfffff000,%eax
80106b53:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80106b56:	29 f7                	sub    %esi,%edi
80106b58:	eb 19                	jmp    80106b73 <mappages+0x43>
80106b5a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
    *pte = pa | perm | PTE_P;
80106b60:	0b 5d 18             	or     0x18(%ebp),%ebx
80106b63:	83 cb 01             	or     $0x1,%ebx
80106b66:	89 18                	mov    %ebx,(%eax)
    if(a == last)
80106b68:	3b 75 e4             	cmp    -0x1c(%ebp),%esi
80106b6b:	74 33                	je     80106ba0 <mappages+0x70>
    a += PGSIZE;
80106b6d:	81 c6 00 10 00 00    	add    $0x1000,%esi
    if((pte = walkpgdir(pgdir, a, 1)) == 0)
80106b73:	8b 45 08             	mov    0x8(%ebp),%eax
80106b76:	b9 01 00 00 00       	mov    $0x1,%ecx
80106b7b:	89 f2                	mov    %esi,%edx
80106b7d:	8d 1c 3e             	lea    (%esi,%edi,1),%ebx
80106b80:	e8 eb fd ff ff       	call   80106970 <walkpgdir>
80106b85:	85 c0                	test   %eax,%eax
80106b87:	75 d7                	jne    80106b60 <mappages+0x30>
}
80106b89:	83 c4 1c             	add    $0x1c,%esp
      return -1;
80106b8c:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106b91:	5b                   	pop    %ebx
80106b92:	5e                   	pop    %esi
80106b93:	5f                   	pop    %edi
80106b94:	5d                   	pop    %ebp
80106b95:	c3                   	ret    
80106b96:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106b9d:	8d 76 00             	lea    0x0(%esi),%esi
80106ba0:	83 c4 1c             	add    $0x1c,%esp
  return 0;
80106ba3:	31 c0                	xor    %eax,%eax
}
80106ba5:	5b                   	pop    %ebx
80106ba6:	5e                   	pop    %esi
80106ba7:	5f                   	pop    %edi
80106ba8:	5d                   	pop    %ebp
80106ba9:	c3                   	ret    
80106baa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80106bb0 <unmappages>:
{
80106bb0:	55                   	push   %ebp
80106bb1:	89 e5                	mov    %esp,%ebp
80106bb3:	57                   	push   %edi
80106bb4:	56                   	push   %esi
80106bb5:	53                   	push   %ebx
80106bb6:	83 ec 0c             	sub    $0xc,%esp
80106bb9:	8b 75 0c             	mov    0xc(%ebp),%esi
80106bbc:	8b 7d 08             	mov    0x8(%ebp),%edi
  for(a = PGROUNDUP(va); a < va + size; a += PGSIZE){
80106bbf:	8d 9e ff 0f 00 00    	lea    0xfff(%esi),%ebx
80106bc5:	03 75 10             	add    0x10(%ebp),%esi
80106bc8:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
80106bce:	66 90                	xchg   %ax,%ax
80106bd0:	39 f3                	cmp    %esi,%ebx
80106bd2:	73 2e                	jae    80106c02 <unmappages+0x52>
    pte = walkpgdir(pgdir, (char*)a, 0);
80106bd4:	31 c9                	xor    %ecx,%ecx
80106bd6:	89 da                	mov    %ebx,%edx
80106bd8:	89 f8                	mov    %edi,%eax
80106bda:	e8 91 fd ff ff       	call   80106970 <walkpgdir>
    if(!pte)
80106bdf:	85 c0                	test   %eax,%eax
80106be1:	74 2d                	je     80106c10 <unmappages+0x60>
    else if((*pte & PTE_P) != 0){
80106be3:	8b 10                	mov    (%eax),%edx
80106be5:	f6 c2 01             	test   $0x1,%dl
80106be8:	74 0e                	je     80106bf8 <unmappages+0x48>
      if(pa == 0)
80106bea:	81 e2 00 f0 ff ff    	and    $0xfffff000,%edx
80106bf0:	74 2c                	je     80106c1e <unmappages+0x6e>
      *pte = 0;
80106bf2:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
80106bf8:	81 c3 00 10 00 00    	add    $0x1000,%ebx
  for(a = PGROUNDUP(va); a < va + size; a += PGSIZE){
80106bfe:	39 f3                	cmp    %esi,%ebx
80106c00:	72 d2                	jb     80106bd4 <unmappages+0x24>
}
80106c02:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106c05:	5b                   	pop    %ebx
80106c06:	5e                   	pop    %esi
80106c07:	5f                   	pop    %edi
80106c08:	5d                   	pop    %ebp
80106c09:	c3                   	ret    
80106c0a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
      a = PGADDR(PDX(a) + 1, 0, 0) - PGSIZE;
80106c10:	81 e3 00 00 c0 ff    	and    $0xffc00000,%ebx
80106c16:	81 c3 00 00 40 00    	add    $0x400000,%ebx
80106c1c:	eb b2                	jmp    80106bd0 <unmappages+0x20>
        panic("kfree");
80106c1e:	83 ec 0c             	sub    $0xc,%esp
80106c21:	68 ee 7a 10 80       	push   $0x80107aee
80106c26:	e8 65 97 ff ff       	call   80100390 <panic>
80106c2b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80106c2f:	90                   	nop

80106c30 <switchkvm>:
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80106c30:	a1 64 75 11 80       	mov    0x80117564,%eax
80106c35:	05 00 00 00 80       	add    $0x80000000,%eax
}

static inline void
lcr3(uint val)
{
  asm volatile("movl %0,%%cr3" : : "r" (val));
80106c3a:	0f 22 d8             	mov    %eax,%cr3
}
80106c3d:	c3                   	ret    
80106c3e:	66 90                	xchg   %ax,%ax

80106c40 <switchuvm>:
{
80106c40:	55                   	push   %ebp
80106c41:	89 e5                	mov    %esp,%ebp
80106c43:	57                   	push   %edi
80106c44:	56                   	push   %esi
80106c45:	53                   	push   %ebx
80106c46:	83 ec 1c             	sub    $0x1c,%esp
80106c49:	8b 5d 08             	mov    0x8(%ebp),%ebx
  if(p == 0)
80106c4c:	85 db                	test   %ebx,%ebx
80106c4e:	0f 84 cb 00 00 00    	je     80106d1f <switchuvm+0xdf>
  if(p->kstack == 0)
80106c54:	8b 43 08             	mov    0x8(%ebx),%eax
80106c57:	85 c0                	test   %eax,%eax
80106c59:	0f 84 da 00 00 00    	je     80106d39 <switchuvm+0xf9>
  if(p->pgdir == 0)
80106c5f:	8b 43 04             	mov    0x4(%ebx),%eax
80106c62:	85 c0                	test   %eax,%eax
80106c64:	0f 84 c2 00 00 00    	je     80106d2c <switchuvm+0xec>
  pushcli();
80106c6a:	e8 e1 d7 ff ff       	call   80104450 <pushcli>
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
80106c6f:	e8 0c cc ff ff       	call   80103880 <mycpu>
80106c74:	89 c6                	mov    %eax,%esi
80106c76:	e8 05 cc ff ff       	call   80103880 <mycpu>
80106c7b:	89 c7                	mov    %eax,%edi
80106c7d:	e8 fe cb ff ff       	call   80103880 <mycpu>
80106c82:	83 c7 08             	add    $0x8,%edi
80106c85:	89 45 e4             	mov    %eax,-0x1c(%ebp)
80106c88:	e8 f3 cb ff ff       	call   80103880 <mycpu>
80106c8d:	8b 4d e4             	mov    -0x1c(%ebp),%ecx
80106c90:	ba 67 00 00 00       	mov    $0x67,%edx
80106c95:	66 89 be 9a 00 00 00 	mov    %di,0x9a(%esi)
80106c9c:	83 c0 08             	add    $0x8,%eax
80106c9f:	66 89 96 98 00 00 00 	mov    %dx,0x98(%esi)
  mycpu()->ts.iomb = (ushort) 0xFFFF;
80106ca6:	bf ff ff ff ff       	mov    $0xffffffff,%edi
  mycpu()->gdt[SEG_TSS] = SEG16(STS_T32A, &mycpu()->ts,
80106cab:	83 c1 08             	add    $0x8,%ecx
80106cae:	c1 e8 18             	shr    $0x18,%eax
80106cb1:	c1 e9 10             	shr    $0x10,%ecx
80106cb4:	88 86 9f 00 00 00    	mov    %al,0x9f(%esi)
80106cba:	88 8e 9c 00 00 00    	mov    %cl,0x9c(%esi)
80106cc0:	b9 99 40 00 00       	mov    $0x4099,%ecx
80106cc5:	66 89 8e 9d 00 00 00 	mov    %cx,0x9d(%esi)
  mycpu()->ts.ss0 = SEG_KDATA << 3;
80106ccc:	be 10 00 00 00       	mov    $0x10,%esi
  mycpu()->gdt[SEG_TSS].s = 0;
80106cd1:	e8 aa cb ff ff       	call   80103880 <mycpu>
80106cd6:	80 a0 9d 00 00 00 ef 	andb   $0xef,0x9d(%eax)
  mycpu()->ts.ss0 = SEG_KDATA << 3;
80106cdd:	e8 9e cb ff ff       	call   80103880 <mycpu>
80106ce2:	66 89 70 10          	mov    %si,0x10(%eax)
  mycpu()->ts.esp0 = (uint)p->kstack + KSTACKSIZE;
80106ce6:	8b 73 08             	mov    0x8(%ebx),%esi
80106ce9:	81 c6 00 10 00 00    	add    $0x1000,%esi
80106cef:	e8 8c cb ff ff       	call   80103880 <mycpu>
80106cf4:	89 70 0c             	mov    %esi,0xc(%eax)
  mycpu()->ts.iomb = (ushort) 0xFFFF;
80106cf7:	e8 84 cb ff ff       	call   80103880 <mycpu>
80106cfc:	66 89 78 6e          	mov    %di,0x6e(%eax)
  asm volatile("ltr %0" : : "r" (sel));
80106d00:	b8 28 00 00 00       	mov    $0x28,%eax
80106d05:	0f 00 d8             	ltr    %ax
  lcr3(V2P(p->pgdir));  // switch to process's address space
80106d08:	8b 43 04             	mov    0x4(%ebx),%eax
80106d0b:	05 00 00 00 80       	add    $0x80000000,%eax
  asm volatile("movl %0,%%cr3" : : "r" (val));
80106d10:	0f 22 d8             	mov    %eax,%cr3
}
80106d13:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106d16:	5b                   	pop    %ebx
80106d17:	5e                   	pop    %esi
80106d18:	5f                   	pop    %edi
80106d19:	5d                   	pop    %ebp
  popcli();
80106d1a:	e9 81 d7 ff ff       	jmp    801044a0 <popcli>
    panic("switchuvm: no process");
80106d1f:	83 ec 0c             	sub    $0xc,%esp
80106d22:	68 c4 81 10 80       	push   $0x801081c4
80106d27:	e8 64 96 ff ff       	call   80100390 <panic>
    panic("switchuvm: no pgdir");
80106d2c:	83 ec 0c             	sub    $0xc,%esp
80106d2f:	68 ef 81 10 80       	push   $0x801081ef
80106d34:	e8 57 96 ff ff       	call   80100390 <panic>
    panic("switchuvm: no kstack");
80106d39:	83 ec 0c             	sub    $0xc,%esp
80106d3c:	68 da 81 10 80       	push   $0x801081da
80106d41:	e8 4a 96 ff ff       	call   80100390 <panic>
80106d46:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106d4d:	8d 76 00             	lea    0x0(%esi),%esi

80106d50 <inituvm>:
{
80106d50:	55                   	push   %ebp
80106d51:	89 e5                	mov    %esp,%ebp
80106d53:	57                   	push   %edi
80106d54:	56                   	push   %esi
80106d55:	53                   	push   %ebx
80106d56:	83 ec 1c             	sub    $0x1c,%esp
80106d59:	8b 75 10             	mov    0x10(%ebp),%esi
80106d5c:	8b 55 08             	mov    0x8(%ebp),%edx
80106d5f:	8b 7d 0c             	mov    0xc(%ebp),%edi
  if(sz >= PGSIZE)
80106d62:	81 fe ff 0f 00 00    	cmp    $0xfff,%esi
80106d68:	77 50                	ja     80106dba <inituvm+0x6a>
80106d6a:	89 55 e4             	mov    %edx,-0x1c(%ebp)
  mem = kalloc();
80106d6d:	e8 6e b8 ff ff       	call   801025e0 <kalloc>
  memset(mem, 0, PGSIZE);
80106d72:	83 ec 04             	sub    $0x4,%esp
80106d75:	68 00 10 00 00       	push   $0x1000
  mem = kalloc();
80106d7a:	89 c3                	mov    %eax,%ebx
  memset(mem, 0, PGSIZE);
80106d7c:	6a 00                	push   $0x0
80106d7e:	50                   	push   %eax
80106d7f:	e8 cc d8 ff ff       	call   80104650 <memset>
  mappages(pgdir, 0, PGSIZE, V2P(mem), PTE_W|PTE_U);
80106d84:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80106d87:	8d 83 00 00 00 80    	lea    -0x80000000(%ebx),%eax
80106d8d:	c7 04 24 06 00 00 00 	movl   $0x6,(%esp)
80106d94:	50                   	push   %eax
80106d95:	68 00 10 00 00       	push   $0x1000
80106d9a:	6a 00                	push   $0x0
80106d9c:	52                   	push   %edx
80106d9d:	e8 8e fd ff ff       	call   80106b30 <mappages>
  memmove(mem, init, sz);
80106da2:	89 75 10             	mov    %esi,0x10(%ebp)
80106da5:	83 c4 20             	add    $0x20,%esp
80106da8:	89 7d 0c             	mov    %edi,0xc(%ebp)
80106dab:	89 5d 08             	mov    %ebx,0x8(%ebp)
}
80106dae:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106db1:	5b                   	pop    %ebx
80106db2:	5e                   	pop    %esi
80106db3:	5f                   	pop    %edi
80106db4:	5d                   	pop    %ebp
  memmove(mem, init, sz);
80106db5:	e9 36 d9 ff ff       	jmp    801046f0 <memmove>
    panic("inituvm: more than a page");
80106dba:	83 ec 0c             	sub    $0xc,%esp
80106dbd:	68 03 82 10 80       	push   $0x80108203
80106dc2:	e8 c9 95 ff ff       	call   80100390 <panic>
80106dc7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106dce:	66 90                	xchg   %ax,%ax

80106dd0 <loaduvm>:
{
80106dd0:	55                   	push   %ebp
80106dd1:	89 e5                	mov    %esp,%ebp
80106dd3:	57                   	push   %edi
80106dd4:	56                   	push   %esi
80106dd5:	53                   	push   %ebx
80106dd6:	83 ec 1c             	sub    $0x1c,%esp
80106dd9:	8b 45 0c             	mov    0xc(%ebp),%eax
80106ddc:	8b 75 18             	mov    0x18(%ebp),%esi
  if((uint) addr % PGSIZE != 0)
80106ddf:	a9 ff 0f 00 00       	test   $0xfff,%eax
80106de4:	0f 85 8d 00 00 00    	jne    80106e77 <loaduvm+0xa7>
80106dea:	01 f0                	add    %esi,%eax
  for(i = 0; i < sz; i += PGSIZE){
80106dec:	89 f3                	mov    %esi,%ebx
80106dee:	89 45 e4             	mov    %eax,-0x1c(%ebp)
    if(readi(ip, P2V(pa), offset+i, n) != n)
80106df1:	8b 45 14             	mov    0x14(%ebp),%eax
80106df4:	01 f0                	add    %esi,%eax
80106df6:	89 45 e0             	mov    %eax,-0x20(%ebp)
  for(i = 0; i < sz; i += PGSIZE){
80106df9:	85 f6                	test   %esi,%esi
80106dfb:	75 11                	jne    80106e0e <loaduvm+0x3e>
80106dfd:	eb 61                	jmp    80106e60 <loaduvm+0x90>
80106dff:	90                   	nop
80106e00:	81 eb 00 10 00 00    	sub    $0x1000,%ebx
80106e06:	89 f0                	mov    %esi,%eax
80106e08:	29 d8                	sub    %ebx,%eax
80106e0a:	39 c6                	cmp    %eax,%esi
80106e0c:	76 52                	jbe    80106e60 <loaduvm+0x90>
    if((pte = walkpgdir(pgdir, addr+i, 0)) == 0)
80106e0e:	8b 55 e4             	mov    -0x1c(%ebp),%edx
80106e11:	8b 45 08             	mov    0x8(%ebp),%eax
80106e14:	31 c9                	xor    %ecx,%ecx
80106e16:	29 da                	sub    %ebx,%edx
80106e18:	e8 53 fb ff ff       	call   80106970 <walkpgdir>
80106e1d:	85 c0                	test   %eax,%eax
80106e1f:	74 49                	je     80106e6a <loaduvm+0x9a>
    pa = PTE_ADDR(*pte);
80106e21:	8b 00                	mov    (%eax),%eax
    if(readi(ip, P2V(pa), offset+i, n) != n)
80106e23:	8b 4d e0             	mov    -0x20(%ebp),%ecx
    if(sz - i < PGSIZE)
80106e26:	bf 00 10 00 00       	mov    $0x1000,%edi
    pa = PTE_ADDR(*pte);
80106e2b:	25 00 f0 ff ff       	and    $0xfffff000,%eax
    if(sz - i < PGSIZE)
80106e30:	81 fb ff 0f 00 00    	cmp    $0xfff,%ebx
80106e36:	0f 46 fb             	cmovbe %ebx,%edi
    if(readi(ip, P2V(pa), offset+i, n) != n)
80106e39:	29 d9                	sub    %ebx,%ecx
80106e3b:	05 00 00 00 80       	add    $0x80000000,%eax
80106e40:	57                   	push   %edi
80106e41:	51                   	push   %ecx
80106e42:	50                   	push   %eax
80106e43:	ff 75 10             	pushl  0x10(%ebp)
80106e46:	e8 e5 ab ff ff       	call   80101a30 <readi>
80106e4b:	83 c4 10             	add    $0x10,%esp
80106e4e:	39 f8                	cmp    %edi,%eax
80106e50:	74 ae                	je     80106e00 <loaduvm+0x30>
}
80106e52:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
80106e55:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
80106e5a:	5b                   	pop    %ebx
80106e5b:	5e                   	pop    %esi
80106e5c:	5f                   	pop    %edi
80106e5d:	5d                   	pop    %ebp
80106e5e:	c3                   	ret    
80106e5f:	90                   	nop
80106e60:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80106e63:	31 c0                	xor    %eax,%eax
}
80106e65:	5b                   	pop    %ebx
80106e66:	5e                   	pop    %esi
80106e67:	5f                   	pop    %edi
80106e68:	5d                   	pop    %ebp
80106e69:	c3                   	ret    
      panic("loaduvm: address should exist");
80106e6a:	83 ec 0c             	sub    $0xc,%esp
80106e6d:	68 1d 82 10 80       	push   $0x8010821d
80106e72:	e8 19 95 ff ff       	call   80100390 <panic>
    panic("loaduvm: addr must be page aligned");
80106e77:	83 ec 0c             	sub    $0xc,%esp
80106e7a:	68 c0 82 10 80       	push   $0x801082c0
80106e7f:	e8 0c 95 ff ff       	call   80100390 <panic>
80106e84:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106e8b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80106e8f:	90                   	nop

80106e90 <allocuvm>:
{
80106e90:	55                   	push   %ebp
80106e91:	89 e5                	mov    %esp,%ebp
80106e93:	57                   	push   %edi
80106e94:	56                   	push   %esi
80106e95:	53                   	push   %ebx
80106e96:	83 ec 1c             	sub    $0x1c,%esp
  if(newsz >= KERNBASE)
80106e99:	8b 7d 10             	mov    0x10(%ebp),%edi
80106e9c:	85 ff                	test   %edi,%edi
80106e9e:	0f 88 bc 00 00 00    	js     80106f60 <allocuvm+0xd0>
  if(newsz < oldsz)
80106ea4:	3b 7d 0c             	cmp    0xc(%ebp),%edi
80106ea7:	0f 82 a3 00 00 00    	jb     80106f50 <allocuvm+0xc0>
  a = PGROUNDUP(oldsz);
80106ead:	8b 45 0c             	mov    0xc(%ebp),%eax
80106eb0:	8d 98 ff 0f 00 00    	lea    0xfff(%eax),%ebx
80106eb6:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
  for(; a < newsz; a += PGSIZE){
80106ebc:	39 5d 10             	cmp    %ebx,0x10(%ebp)
80106ebf:	0f 86 8e 00 00 00    	jbe    80106f53 <allocuvm+0xc3>
80106ec5:	89 7d e4             	mov    %edi,-0x1c(%ebp)
80106ec8:	8b 7d 08             	mov    0x8(%ebp),%edi
80106ecb:	eb 43                	jmp    80106f10 <allocuvm+0x80>
80106ecd:	8d 76 00             	lea    0x0(%esi),%esi
    memset(mem, 0, PGSIZE);
80106ed0:	83 ec 04             	sub    $0x4,%esp
80106ed3:	68 00 10 00 00       	push   $0x1000
80106ed8:	6a 00                	push   $0x0
80106eda:	50                   	push   %eax
80106edb:	e8 70 d7 ff ff       	call   80104650 <memset>
    if(mappages(pgdir, (char*)a, PGSIZE, V2P(mem), PTE_W|PTE_U) < 0){
80106ee0:	8d 86 00 00 00 80    	lea    -0x80000000(%esi),%eax
80106ee6:	c7 04 24 06 00 00 00 	movl   $0x6,(%esp)
80106eed:	50                   	push   %eax
80106eee:	68 00 10 00 00       	push   $0x1000
80106ef3:	53                   	push   %ebx
80106ef4:	57                   	push   %edi
80106ef5:	e8 36 fc ff ff       	call   80106b30 <mappages>
80106efa:	83 c4 20             	add    $0x20,%esp
80106efd:	85 c0                	test   %eax,%eax
80106eff:	78 6f                	js     80106f70 <allocuvm+0xe0>
  for(; a < newsz; a += PGSIZE){
80106f01:	81 c3 00 10 00 00    	add    $0x1000,%ebx
80106f07:	39 5d 10             	cmp    %ebx,0x10(%ebp)
80106f0a:	0f 86 a0 00 00 00    	jbe    80106fb0 <allocuvm+0x120>
    mem = kalloc();
80106f10:	e8 cb b6 ff ff       	call   801025e0 <kalloc>
80106f15:	89 c6                	mov    %eax,%esi
    if(mem == 0){
80106f17:	85 c0                	test   %eax,%eax
80106f19:	75 b5                	jne    80106ed0 <allocuvm+0x40>
      cprintf("allocuvm out of memory\n");
80106f1b:	83 ec 0c             	sub    $0xc,%esp
80106f1e:	68 3b 82 10 80       	push   $0x8010823b
80106f23:	e8 88 97 ff ff       	call   801006b0 <cprintf>
  if(newsz >= oldsz)
80106f28:	83 c4 10             	add    $0x10,%esp
80106f2b:	8b 45 0c             	mov    0xc(%ebp),%eax
80106f2e:	39 45 10             	cmp    %eax,0x10(%ebp)
80106f31:	74 2d                	je     80106f60 <allocuvm+0xd0>
80106f33:	89 c1                	mov    %eax,%ecx
80106f35:	8b 55 10             	mov    0x10(%ebp),%edx
80106f38:	8b 45 08             	mov    0x8(%ebp),%eax
      return 0;
80106f3b:	31 ff                	xor    %edi,%edi
80106f3d:	e8 ae fa ff ff       	call   801069f0 <deallocuvm.part.0>
}
80106f42:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106f45:	89 f8                	mov    %edi,%eax
80106f47:	5b                   	pop    %ebx
80106f48:	5e                   	pop    %esi
80106f49:	5f                   	pop    %edi
80106f4a:	5d                   	pop    %ebp
80106f4b:	c3                   	ret    
80106f4c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
    return oldsz;
80106f50:	8b 7d 0c             	mov    0xc(%ebp),%edi
}
80106f53:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106f56:	89 f8                	mov    %edi,%eax
80106f58:	5b                   	pop    %ebx
80106f59:	5e                   	pop    %esi
80106f5a:	5f                   	pop    %edi
80106f5b:	5d                   	pop    %ebp
80106f5c:	c3                   	ret    
80106f5d:	8d 76 00             	lea    0x0(%esi),%esi
80106f60:	8d 65 f4             	lea    -0xc(%ebp),%esp
    return 0;
80106f63:	31 ff                	xor    %edi,%edi
}
80106f65:	5b                   	pop    %ebx
80106f66:	89 f8                	mov    %edi,%eax
80106f68:	5e                   	pop    %esi
80106f69:	5f                   	pop    %edi
80106f6a:	5d                   	pop    %ebp
80106f6b:	c3                   	ret    
80106f6c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
      cprintf("allocuvm out of memory (2)\n");
80106f70:	83 ec 0c             	sub    $0xc,%esp
80106f73:	68 53 82 10 80       	push   $0x80108253
80106f78:	e8 33 97 ff ff       	call   801006b0 <cprintf>
  if(newsz >= oldsz)
80106f7d:	83 c4 10             	add    $0x10,%esp
80106f80:	8b 45 0c             	mov    0xc(%ebp),%eax
80106f83:	39 45 10             	cmp    %eax,0x10(%ebp)
80106f86:	74 0d                	je     80106f95 <allocuvm+0x105>
80106f88:	89 c1                	mov    %eax,%ecx
80106f8a:	8b 55 10             	mov    0x10(%ebp),%edx
80106f8d:	8b 45 08             	mov    0x8(%ebp),%eax
80106f90:	e8 5b fa ff ff       	call   801069f0 <deallocuvm.part.0>
      kfree(mem);
80106f95:	83 ec 0c             	sub    $0xc,%esp
      return 0;
80106f98:	31 ff                	xor    %edi,%edi
      kfree(mem);
80106f9a:	56                   	push   %esi
80106f9b:	e8 80 b4 ff ff       	call   80102420 <kfree>
      return 0;
80106fa0:	83 c4 10             	add    $0x10,%esp
}
80106fa3:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106fa6:	89 f8                	mov    %edi,%eax
80106fa8:	5b                   	pop    %ebx
80106fa9:	5e                   	pop    %esi
80106faa:	5f                   	pop    %edi
80106fab:	5d                   	pop    %ebp
80106fac:	c3                   	ret    
80106fad:	8d 76 00             	lea    0x0(%esi),%esi
80106fb0:	8b 7d e4             	mov    -0x1c(%ebp),%edi
80106fb3:	8d 65 f4             	lea    -0xc(%ebp),%esp
80106fb6:	5b                   	pop    %ebx
80106fb7:	5e                   	pop    %esi
80106fb8:	89 f8                	mov    %edi,%eax
80106fba:	5f                   	pop    %edi
80106fbb:	5d                   	pop    %ebp
80106fbc:	c3                   	ret    
80106fbd:	8d 76 00             	lea    0x0(%esi),%esi

80106fc0 <deallocuvm>:
{
80106fc0:	55                   	push   %ebp
80106fc1:	89 e5                	mov    %esp,%ebp
80106fc3:	8b 55 0c             	mov    0xc(%ebp),%edx
80106fc6:	8b 4d 10             	mov    0x10(%ebp),%ecx
80106fc9:	8b 45 08             	mov    0x8(%ebp),%eax
  if(newsz >= oldsz)
80106fcc:	39 d1                	cmp    %edx,%ecx
80106fce:	73 10                	jae    80106fe0 <deallocuvm+0x20>
}
80106fd0:	5d                   	pop    %ebp
80106fd1:	e9 1a fa ff ff       	jmp    801069f0 <deallocuvm.part.0>
80106fd6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106fdd:	8d 76 00             	lea    0x0(%esi),%esi
80106fe0:	89 d0                	mov    %edx,%eax
80106fe2:	5d                   	pop    %ebp
80106fe3:	c3                   	ret    
80106fe4:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80106feb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
80106fef:	90                   	nop

80106ff0 <freevm>:

// Free a page table and all the physical memory pages
// in the user part.
void
freevm(pde_t *pgdir)
{
80106ff0:	55                   	push   %ebp
80106ff1:	89 e5                	mov    %esp,%ebp
80106ff3:	57                   	push   %edi
80106ff4:	56                   	push   %esi
80106ff5:	53                   	push   %ebx
80106ff6:	83 ec 0c             	sub    $0xc,%esp
80106ff9:	8b 75 08             	mov    0x8(%ebp),%esi
  uint i;

  if(pgdir == 0)
80106ffc:	85 f6                	test   %esi,%esi
80106ffe:	74 59                	je     80107059 <freevm+0x69>
  if(newsz >= oldsz)
80107000:	31 c9                	xor    %ecx,%ecx
80107002:	ba 00 00 00 80       	mov    $0x80000000,%edx
80107007:	89 f0                	mov    %esi,%eax
80107009:	89 f3                	mov    %esi,%ebx
8010700b:	e8 e0 f9 ff ff       	call   801069f0 <deallocuvm.part.0>
freevm(pde_t *pgdir)
80107010:	8d be 00 10 00 00    	lea    0x1000(%esi),%edi
80107016:	eb 0f                	jmp    80107027 <freevm+0x37>
80107018:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010701f:	90                   	nop
80107020:	83 c3 04             	add    $0x4,%ebx
    panic("freevm: no pgdir");
  deallocuvm(pgdir, KERNBASE, 0);
  for(i = 0; i < NPDENTRIES; i++){
80107023:	39 df                	cmp    %ebx,%edi
80107025:	74 23                	je     8010704a <freevm+0x5a>
    if(pgdir[i] & PTE_P){
80107027:	8b 03                	mov    (%ebx),%eax
80107029:	a8 01                	test   $0x1,%al
8010702b:	74 f3                	je     80107020 <freevm+0x30>
      char * v = P2V(PTE_ADDR(pgdir[i]));
8010702d:	25 00 f0 ff ff       	and    $0xfffff000,%eax
      kfree(v);
80107032:	83 ec 0c             	sub    $0xc,%esp
80107035:	83 c3 04             	add    $0x4,%ebx
      char * v = P2V(PTE_ADDR(pgdir[i]));
80107038:	05 00 00 00 80       	add    $0x80000000,%eax
      kfree(v);
8010703d:	50                   	push   %eax
8010703e:	e8 dd b3 ff ff       	call   80102420 <kfree>
80107043:	83 c4 10             	add    $0x10,%esp
  for(i = 0; i < NPDENTRIES; i++){
80107046:	39 df                	cmp    %ebx,%edi
80107048:	75 dd                	jne    80107027 <freevm+0x37>
    }
  }
  kfree((char*)pgdir);
8010704a:	89 75 08             	mov    %esi,0x8(%ebp)
}
8010704d:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107050:	5b                   	pop    %ebx
80107051:	5e                   	pop    %esi
80107052:	5f                   	pop    %edi
80107053:	5d                   	pop    %ebp
  kfree((char*)pgdir);
80107054:	e9 c7 b3 ff ff       	jmp    80102420 <kfree>
    panic("freevm: no pgdir");
80107059:	83 ec 0c             	sub    $0xc,%esp
8010705c:	68 6f 82 10 80       	push   $0x8010826f
80107061:	e8 2a 93 ff ff       	call   80100390 <panic>
80107066:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010706d:	8d 76 00             	lea    0x0(%esi),%esi

80107070 <setupkvm>:
{
80107070:	55                   	push   %ebp
80107071:	89 e5                	mov    %esp,%ebp
80107073:	56                   	push   %esi
80107074:	53                   	push   %ebx
  if((pgdir = (pde_t*)kalloc()) == 0)
80107075:	e8 66 b5 ff ff       	call   801025e0 <kalloc>
8010707a:	89 c6                	mov    %eax,%esi
8010707c:	85 c0                	test   %eax,%eax
8010707e:	74 42                	je     801070c2 <setupkvm+0x52>
  memset(pgdir, 0, PGSIZE);
80107080:	83 ec 04             	sub    $0x4,%esp
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
80107083:	bb 20 b4 10 80       	mov    $0x8010b420,%ebx
  memset(pgdir, 0, PGSIZE);
80107088:	68 00 10 00 00       	push   $0x1000
8010708d:	6a 00                	push   $0x0
8010708f:	50                   	push   %eax
80107090:	e8 bb d5 ff ff       	call   80104650 <memset>
80107095:	83 c4 10             	add    $0x10,%esp
                (uint)k->phys_start, k->perm) < 0) {
80107098:	8b 43 04             	mov    0x4(%ebx),%eax
    if(mappages(pgdir, k->virt, k->phys_end - k->phys_start,
8010709b:	8b 53 08             	mov    0x8(%ebx),%edx
8010709e:	83 ec 0c             	sub    $0xc,%esp
801070a1:	ff 73 0c             	pushl  0xc(%ebx)
801070a4:	29 c2                	sub    %eax,%edx
801070a6:	50                   	push   %eax
801070a7:	52                   	push   %edx
801070a8:	ff 33                	pushl  (%ebx)
801070aa:	56                   	push   %esi
801070ab:	e8 80 fa ff ff       	call   80106b30 <mappages>
801070b0:	83 c4 20             	add    $0x20,%esp
801070b3:	85 c0                	test   %eax,%eax
801070b5:	78 19                	js     801070d0 <setupkvm+0x60>
  for(k = kmap; k < &kmap[NELEM(kmap)]; k++)
801070b7:	83 c3 10             	add    $0x10,%ebx
801070ba:	81 fb 60 b4 10 80    	cmp    $0x8010b460,%ebx
801070c0:	75 d6                	jne    80107098 <setupkvm+0x28>
}
801070c2:	8d 65 f8             	lea    -0x8(%ebp),%esp
801070c5:	89 f0                	mov    %esi,%eax
801070c7:	5b                   	pop    %ebx
801070c8:	5e                   	pop    %esi
801070c9:	5d                   	pop    %ebp
801070ca:	c3                   	ret    
801070cb:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801070cf:	90                   	nop
      freevm(pgdir);
801070d0:	83 ec 0c             	sub    $0xc,%esp
801070d3:	56                   	push   %esi
      return 0;
801070d4:	31 f6                	xor    %esi,%esi
      freevm(pgdir);
801070d6:	e8 15 ff ff ff       	call   80106ff0 <freevm>
      return 0;
801070db:	83 c4 10             	add    $0x10,%esp
}
801070de:	8d 65 f8             	lea    -0x8(%ebp),%esp
801070e1:	89 f0                	mov    %esi,%eax
801070e3:	5b                   	pop    %ebx
801070e4:	5e                   	pop    %esi
801070e5:	5d                   	pop    %ebp
801070e6:	c3                   	ret    
801070e7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801070ee:	66 90                	xchg   %ax,%ax

801070f0 <kvmalloc>:
{
801070f0:	55                   	push   %ebp
801070f1:	89 e5                	mov    %esp,%ebp
801070f3:	83 ec 08             	sub    $0x8,%esp
  kpgdir = setupkvm();
801070f6:	e8 75 ff ff ff       	call   80107070 <setupkvm>
801070fb:	a3 64 75 11 80       	mov    %eax,0x80117564
  lcr3(V2P(kpgdir));   // switch to the kernel page table
80107100:	05 00 00 00 80       	add    $0x80000000,%eax
80107105:	0f 22 d8             	mov    %eax,%cr3
}
80107108:	c9                   	leave  
80107109:	c3                   	ret    
8010710a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80107110 <clearpteu>:

// Clear PTE_U on a page. Used to create an inaccessible
// page beneath the user stack.
void
clearpteu(pde_t *pgdir, char *uva)
{
80107110:	55                   	push   %ebp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80107111:	31 c9                	xor    %ecx,%ecx
{
80107113:	89 e5                	mov    %esp,%ebp
80107115:	83 ec 08             	sub    $0x8,%esp
  pte = walkpgdir(pgdir, uva, 0);
80107118:	8b 55 0c             	mov    0xc(%ebp),%edx
8010711b:	8b 45 08             	mov    0x8(%ebp),%eax
8010711e:	e8 4d f8 ff ff       	call   80106970 <walkpgdir>
  if(pte == 0)
80107123:	85 c0                	test   %eax,%eax
80107125:	74 05                	je     8010712c <clearpteu+0x1c>
    panic("clearpteu");
  *pte &= ~PTE_U;
80107127:	83 20 fb             	andl   $0xfffffffb,(%eax)
}
8010712a:	c9                   	leave  
8010712b:	c3                   	ret    
    panic("clearpteu");
8010712c:	83 ec 0c             	sub    $0xc,%esp
8010712f:	68 80 82 10 80       	push   $0x80108280
80107134:	e8 57 92 ff ff       	call   80100390 <panic>
80107139:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80107140 <copyuvm>:

// Given a parent process's page table, create a copy
// of it for a child.
pde_t*
copyuvm(pde_t *pgdir, uint sz)
{
80107140:	55                   	push   %ebp
80107141:	89 e5                	mov    %esp,%ebp
80107143:	57                   	push   %edi
80107144:	56                   	push   %esi
80107145:	53                   	push   %ebx
80107146:	83 ec 1c             	sub    $0x1c,%esp
  pde_t *d;
  pte_t *pte;
  uint pa, i, flags;
  char *mem;

  if((d = setupkvm()) == 0)
80107149:	e8 22 ff ff ff       	call   80107070 <setupkvm>
8010714e:	89 45 e0             	mov    %eax,-0x20(%ebp)
80107151:	85 c0                	test   %eax,%eax
80107153:	0f 84 a2 00 00 00    	je     801071fb <copyuvm+0xbb>
    return 0;
  for(i = 0; i < sz; i += PGSIZE){
80107159:	8b 55 0c             	mov    0xc(%ebp),%edx
8010715c:	85 d2                	test   %edx,%edx
8010715e:	0f 84 97 00 00 00    	je     801071fb <copyuvm+0xbb>
80107164:	31 f6                	xor    %esi,%esi
80107166:	eb 48                	jmp    801071b0 <copyuvm+0x70>
80107168:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010716f:	90                   	nop
      panic("copyuvm: page not present");
    pa = PTE_ADDR(*pte);
    flags = PTE_FLAGS(*pte);
    if((mem = kalloc()) == 0)
      goto bad;
    memmove(mem, (char*)P2V(pa), PGSIZE);
80107170:	83 ec 04             	sub    $0x4,%esp
80107173:	81 c3 00 00 00 80    	add    $0x80000000,%ebx
80107179:	68 00 10 00 00       	push   $0x1000
8010717e:	53                   	push   %ebx
8010717f:	50                   	push   %eax
80107180:	e8 6b d5 ff ff       	call   801046f0 <memmove>
    if(mappages(d, (void*)i, PGSIZE, V2P(mem), flags) < 0) {
80107185:	58                   	pop    %eax
80107186:	8d 87 00 00 00 80    	lea    -0x80000000(%edi),%eax
8010718c:	ff 75 e4             	pushl  -0x1c(%ebp)
8010718f:	50                   	push   %eax
80107190:	68 00 10 00 00       	push   $0x1000
80107195:	56                   	push   %esi
80107196:	ff 75 e0             	pushl  -0x20(%ebp)
80107199:	e8 92 f9 ff ff       	call   80106b30 <mappages>
8010719e:	83 c4 20             	add    $0x20,%esp
801071a1:	85 c0                	test   %eax,%eax
801071a3:	78 6b                	js     80107210 <copyuvm+0xd0>
  for(i = 0; i < sz; i += PGSIZE){
801071a5:	81 c6 00 10 00 00    	add    $0x1000,%esi
801071ab:	39 75 0c             	cmp    %esi,0xc(%ebp)
801071ae:	76 4b                	jbe    801071fb <copyuvm+0xbb>
    if((pte = walkpgdir(pgdir, (void *) i, 0)) == 0)
801071b0:	8b 45 08             	mov    0x8(%ebp),%eax
801071b3:	31 c9                	xor    %ecx,%ecx
801071b5:	89 f2                	mov    %esi,%edx
801071b7:	e8 b4 f7 ff ff       	call   80106970 <walkpgdir>
801071bc:	85 c0                	test   %eax,%eax
801071be:	74 6b                	je     8010722b <copyuvm+0xeb>
    if(!(*pte & PTE_P))
801071c0:	8b 38                	mov    (%eax),%edi
801071c2:	f7 c7 01 00 00 00    	test   $0x1,%edi
801071c8:	74 54                	je     8010721e <copyuvm+0xde>
    pa = PTE_ADDR(*pte);
801071ca:	89 fb                	mov    %edi,%ebx
    flags = PTE_FLAGS(*pte);
801071cc:	81 e7 ff 0f 00 00    	and    $0xfff,%edi
801071d2:	89 7d e4             	mov    %edi,-0x1c(%ebp)
    pa = PTE_ADDR(*pte);
801071d5:	81 e3 00 f0 ff ff    	and    $0xfffff000,%ebx
    if((mem = kalloc()) == 0)
801071db:	e8 00 b4 ff ff       	call   801025e0 <kalloc>
801071e0:	89 c7                	mov    %eax,%edi
801071e2:	85 c0                	test   %eax,%eax
801071e4:	75 8a                	jne    80107170 <copyuvm+0x30>
    }
  }
  return d;

bad:
  freevm(d);
801071e6:	83 ec 0c             	sub    $0xc,%esp
801071e9:	ff 75 e0             	pushl  -0x20(%ebp)
801071ec:	e8 ff fd ff ff       	call   80106ff0 <freevm>
  return 0;
801071f1:	c7 45 e0 00 00 00 00 	movl   $0x0,-0x20(%ebp)
801071f8:	83 c4 10             	add    $0x10,%esp
}
801071fb:	8b 45 e0             	mov    -0x20(%ebp),%eax
801071fe:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107201:	5b                   	pop    %ebx
80107202:	5e                   	pop    %esi
80107203:	5f                   	pop    %edi
80107204:	5d                   	pop    %ebp
80107205:	c3                   	ret    
80107206:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010720d:	8d 76 00             	lea    0x0(%esi),%esi
      kfree(mem);
80107210:	83 ec 0c             	sub    $0xc,%esp
80107213:	57                   	push   %edi
80107214:	e8 07 b2 ff ff       	call   80102420 <kfree>
      goto bad;
80107219:	83 c4 10             	add    $0x10,%esp
8010721c:	eb c8                	jmp    801071e6 <copyuvm+0xa6>
      panic("copyuvm: page not present");
8010721e:	83 ec 0c             	sub    $0xc,%esp
80107221:	68 a4 82 10 80       	push   $0x801082a4
80107226:	e8 65 91 ff ff       	call   80100390 <panic>
      panic("copyuvm: pte should exist");
8010722b:	83 ec 0c             	sub    $0xc,%esp
8010722e:	68 8a 82 10 80       	push   $0x8010828a
80107233:	e8 58 91 ff ff       	call   80100390 <panic>
80107238:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010723f:	90                   	nop

80107240 <uva2ka>:

//PAGEBREAK!
// Map user virtual address to kernel address.
char*
uva2ka(pde_t *pgdir, char *uva)
{
80107240:	55                   	push   %ebp
  pte_t *pte;

  pte = walkpgdir(pgdir, uva, 0);
80107241:	31 c9                	xor    %ecx,%ecx
{
80107243:	89 e5                	mov    %esp,%ebp
80107245:	83 ec 08             	sub    $0x8,%esp
  pte = walkpgdir(pgdir, uva, 0);
80107248:	8b 55 0c             	mov    0xc(%ebp),%edx
8010724b:	8b 45 08             	mov    0x8(%ebp),%eax
8010724e:	e8 1d f7 ff ff       	call   80106970 <walkpgdir>
  if((*pte & PTE_P) == 0)
80107253:	8b 00                	mov    (%eax),%eax
    return 0;
  if((*pte & PTE_U) == 0)
    return 0;
  return (char*)P2V(PTE_ADDR(*pte));
}
80107255:	c9                   	leave  
  if((*pte & PTE_U) == 0)
80107256:	89 c2                	mov    %eax,%edx
  return (char*)P2V(PTE_ADDR(*pte));
80107258:	25 00 f0 ff ff       	and    $0xfffff000,%eax
  if((*pte & PTE_U) == 0)
8010725d:	83 e2 05             	and    $0x5,%edx
  return (char*)P2V(PTE_ADDR(*pte));
80107260:	05 00 00 00 80       	add    $0x80000000,%eax
80107265:	83 fa 05             	cmp    $0x5,%edx
80107268:	ba 00 00 00 00       	mov    $0x0,%edx
8010726d:	0f 45 c2             	cmovne %edx,%eax
}
80107270:	c3                   	ret    
80107271:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107278:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010727f:	90                   	nop

80107280 <copyout>:
// Copy len bytes from p to user address va in page table pgdir.
// Most useful when pgdir is not the current page table.
// uva2ka ensures this only works for PTE_U pages.
int
copyout(pde_t *pgdir, uint va, void *p, uint len)
{
80107280:	55                   	push   %ebp
80107281:	89 e5                	mov    %esp,%ebp
80107283:	57                   	push   %edi
80107284:	56                   	push   %esi
80107285:	53                   	push   %ebx
80107286:	83 ec 0c             	sub    $0xc,%esp
80107289:	8b 75 14             	mov    0x14(%ebp),%esi
8010728c:	8b 55 0c             	mov    0xc(%ebp),%edx
  char *buf, *pa0;
  uint n, va0;

  buf = (char*)p;
  while(len > 0){
8010728f:	85 f6                	test   %esi,%esi
80107291:	75 38                	jne    801072cb <copyout+0x4b>
80107293:	eb 6b                	jmp    80107300 <copyout+0x80>
80107295:	8d 76 00             	lea    0x0(%esi),%esi
    va0 = (uint)PGROUNDDOWN(va);
    pa0 = uva2ka(pgdir, (char*)va0);
    if(pa0 == 0)
      return -1;
    n = PGSIZE - (va - va0);
80107298:	8b 55 0c             	mov    0xc(%ebp),%edx
8010729b:	89 fb                	mov    %edi,%ebx
8010729d:	29 d3                	sub    %edx,%ebx
8010729f:	81 c3 00 10 00 00    	add    $0x1000,%ebx
    if(n > len)
801072a5:	39 f3                	cmp    %esi,%ebx
801072a7:	0f 47 de             	cmova  %esi,%ebx
      n = len;
    memmove(pa0 + (va - va0), buf, n);
801072aa:	29 fa                	sub    %edi,%edx
801072ac:	83 ec 04             	sub    $0x4,%esp
801072af:	01 c2                	add    %eax,%edx
801072b1:	53                   	push   %ebx
801072b2:	ff 75 10             	pushl  0x10(%ebp)
801072b5:	52                   	push   %edx
801072b6:	e8 35 d4 ff ff       	call   801046f0 <memmove>
    len -= n;
    buf += n;
801072bb:	01 5d 10             	add    %ebx,0x10(%ebp)
    va = va0 + PGSIZE;
801072be:	8d 97 00 10 00 00    	lea    0x1000(%edi),%edx
  while(len > 0){
801072c4:	83 c4 10             	add    $0x10,%esp
801072c7:	29 de                	sub    %ebx,%esi
801072c9:	74 35                	je     80107300 <copyout+0x80>
    va0 = (uint)PGROUNDDOWN(va);
801072cb:	89 d7                	mov    %edx,%edi
    pa0 = uva2ka(pgdir, (char*)va0);
801072cd:	83 ec 08             	sub    $0x8,%esp
    va0 = (uint)PGROUNDDOWN(va);
801072d0:	89 55 0c             	mov    %edx,0xc(%ebp)
801072d3:	81 e7 00 f0 ff ff    	and    $0xfffff000,%edi
    pa0 = uva2ka(pgdir, (char*)va0);
801072d9:	57                   	push   %edi
801072da:	ff 75 08             	pushl  0x8(%ebp)
801072dd:	e8 5e ff ff ff       	call   80107240 <uva2ka>
    if(pa0 == 0)
801072e2:	83 c4 10             	add    $0x10,%esp
801072e5:	85 c0                	test   %eax,%eax
801072e7:	75 af                	jne    80107298 <copyout+0x18>
  }
  return 0;
}
801072e9:	8d 65 f4             	lea    -0xc(%ebp),%esp
      return -1;
801072ec:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
}
801072f1:	5b                   	pop    %ebx
801072f2:	5e                   	pop    %esi
801072f3:	5f                   	pop    %edi
801072f4:	5d                   	pop    %ebp
801072f5:	c3                   	ret    
801072f6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801072fd:	8d 76 00             	lea    0x0(%esi),%esi
80107300:	8d 65 f4             	lea    -0xc(%ebp),%esp
  return 0;
80107303:	31 c0                	xor    %eax,%eax
}
80107305:	5b                   	pop    %ebx
80107306:	5e                   	pop    %esi
80107307:	5f                   	pop    %edi
80107308:	5d                   	pop    %ebp
80107309:	c3                   	ret    
8010730a:	66 90                	xchg   %ax,%ax
8010730c:	66 90                	xchg   %ax,%ax
8010730e:	66 90                	xchg   %ax,%ax

80107310 <seminit>:
struct {
	struct spinlock lock;
	struct sem sems[MAXNSEM];
} semtable;

void seminit(void){
80107310:	55                   	push   %ebp
80107311:	89 e5                	mov    %esp,%ebp
80107313:	83 ec 10             	sub    $0x10,%esp
  initlock(&semtable.lock, "semtable");
80107316:	68 e3 82 10 80       	push   $0x801082e3
8010731b:	68 80 75 11 80       	push   $0x80117580
80107320:	e8 bb d0 ff ff       	call   801043e0 <initlock>
  int i;
  for(i=0; i < MAXNSEM; i++) semtable.sems[i].refcounter = 0;
80107325:	b8 b4 75 11 80       	mov    $0x801175b4,%eax
8010732a:	83 c4 10             	add    $0x10,%esp
8010732d:	8d 76 00             	lea    0x0(%esi),%esi
80107330:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
80107336:	83 c0 0c             	add    $0xc,%eax
80107339:	3d a4 76 11 80       	cmp    $0x801176a4,%eax
8010733e:	75 f0                	jne    80107330 <seminit+0x20>
}
80107340:	c9                   	leave  
80107341:	c3                   	ret    
80107342:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107349:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi

80107350 <semget>:

int semget(int key, int init_value){
80107350:	55                   	push   %ebp
80107351:	89 e5                	mov    %esp,%ebp
80107353:	57                   	push   %edi
80107354:	56                   	push   %esi
	struct proc* p = myproc();
	int slot, i;

	for(slot=0; slot < NSEM && p->psem[slot] != 0; slot++); //find free place
80107355:	31 f6                	xor    %esi,%esi
int semget(int key, int init_value){
80107357:	53                   	push   %ebx
80107358:	83 ec 1c             	sub    $0x1c,%esp
8010735b:	8b 7d 08             	mov    0x8(%ebp),%edi
	struct proc* p = myproc();
8010735e:	e8 bd c5 ff ff       	call   80103920 <myproc>
	for(slot=0; slot < NSEM && p->psem[slot] != 0; slot++); //find free place
80107363:	8b 5c b0 68          	mov    0x68(%eax,%esi,4),%ebx
80107367:	85 db                	test   %ebx,%ebx
80107369:	74 1d                	je     80107388 <semget+0x38>
8010736b:	83 c6 01             	add    $0x1,%esi
8010736e:	83 fe 05             	cmp    $0x5,%esi
80107371:	75 f0                	jne    80107363 <semget+0x13>
	if (slot != -1)	{
		p->psem[slot] = &semtable.sems[i];
	}
	
	return slot;
}
80107373:	8d 65 f4             	lea    -0xc(%ebp),%esp
			slot = -1;
80107376:	be ff ff ff ff       	mov    $0xffffffff,%esi
}
8010737b:	5b                   	pop    %ebx
8010737c:	89 f0                	mov    %esi,%eax
8010737e:	5e                   	pop    %esi
8010737f:	5f                   	pop    %edi
80107380:	5d                   	pop    %ebp
80107381:	c3                   	ret    
80107382:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
	acquire(&semtable.lock);
80107388:	83 ec 0c             	sub    $0xc,%esp
8010738b:	89 45 e4             	mov    %eax,-0x1c(%ebp)
	for (i=0; i < MAXNSEM && key != semtable.sems[i].key; i++); //find existing key
8010738e:	31 db                	xor    %ebx,%ebx
	acquire(&semtable.lock);
80107390:	68 80 75 11 80       	push   $0x80117580
80107395:	e8 a6 d1 ff ff       	call   80104540 <acquire>
	for (i=0; i < MAXNSEM && key != semtable.sems[i].key; i++); //find existing key
8010739a:	ba bc 75 11 80       	mov    $0x801175bc,%edx
8010739f:	83 c4 10             	add    $0x10,%esp
801073a2:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801073a5:	eb 14                	jmp    801073bb <semget+0x6b>
801073a7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801073ae:	66 90                	xchg   %ax,%ax
801073b0:	83 c3 01             	add    $0x1,%ebx
801073b3:	83 c2 0c             	add    $0xc,%edx
801073b6:	83 fb 14             	cmp    $0x14,%ebx
801073b9:	74 45                	je     80107400 <semget+0xb0>
801073bb:	39 3a                	cmp    %edi,(%edx)
801073bd:	75 f1                	jne    801073b0 <semget+0x60>
		(semtable.sems[i]).refcounter++; 
801073bf:	8d 14 5b             	lea    (%ebx,%ebx,2),%edx
801073c2:	83 04 95 b4 75 11 80 	addl   $0x1,-0x7fee8a4c(,%edx,4)
801073c9:	01 
	release(&semtable.lock);
801073ca:	83 ec 0c             	sub    $0xc,%esp
801073cd:	89 45 e4             	mov    %eax,-0x1c(%ebp)
801073d0:	68 80 75 11 80       	push   $0x80117580
801073d5:	e8 26 d2 ff ff       	call   80104600 <release>
		p->psem[slot] = &semtable.sems[i];
801073da:	8b 45 e4             	mov    -0x1c(%ebp),%eax
801073dd:	8d 14 5b             	lea    (%ebx,%ebx,2),%edx
801073e0:	83 c4 10             	add    $0x10,%esp
801073e3:	8d 14 95 b4 75 11 80 	lea    -0x7fee8a4c(,%edx,4),%edx
801073ea:	89 54 b0 68          	mov    %edx,0x68(%eax,%esi,4)
}
801073ee:	8d 65 f4             	lea    -0xc(%ebp),%esp
801073f1:	89 f0                	mov    %esi,%eax
801073f3:	5b                   	pop    %ebx
801073f4:	5e                   	pop    %esi
801073f5:	5f                   	pop    %edi
801073f6:	5d                   	pop    %ebp
801073f7:	c3                   	ret    
801073f8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801073ff:	90                   	nop
80107400:	ba b4 75 11 80       	mov    $0x801175b4,%edx
		for (i=0; i < MAXNSEM && semtable.sems[i].refcounter; i++); //find existing key
80107405:	31 db                	xor    %ebx,%ebx
80107407:	eb 12                	jmp    8010741b <semget+0xcb>
80107409:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
80107410:	83 c3 01             	add    $0x1,%ebx
80107413:	83 c2 0c             	add    $0xc,%edx
80107416:	83 fb 14             	cmp    $0x14,%ebx
80107419:	74 25                	je     80107440 <semget+0xf0>
8010741b:	8b 0a                	mov    (%edx),%ecx
8010741d:	85 c9                	test   %ecx,%ecx
8010741f:	75 ef                	jne    80107410 <semget+0xc0>
			semtable.sems[i].refcounter++;
80107421:	8d 14 5b             	lea    (%ebx,%ebx,2),%edx
			semtable.sems[i].value = init_value;
80107424:	8b 4d 0c             	mov    0xc(%ebp),%ecx
			semtable.sems[i].refcounter++;
80107427:	8d 14 95 80 75 11 80 	lea    -0x7fee8a80(,%edx,4),%edx
8010742e:	c7 42 34 01 00 00 00 	movl   $0x1,0x34(%edx)
			semtable.sems[i].key = key;
80107435:	89 7a 3c             	mov    %edi,0x3c(%edx)
			semtable.sems[i].value = init_value;
80107438:	89 4a 38             	mov    %ecx,0x38(%edx)
8010743b:	eb 8d                	jmp    801073ca <semget+0x7a>
8010743d:	8d 76 00             	lea    0x0(%esi),%esi
	release(&semtable.lock);
80107440:	83 ec 0c             	sub    $0xc,%esp
80107443:	68 80 75 11 80       	push   $0x80117580
80107448:	e8 b3 d1 ff ff       	call   80104600 <release>
8010744d:	83 c4 10             	add    $0x10,%esp
80107450:	e9 1e ff ff ff       	jmp    80107373 <semget+0x23>
80107455:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010745c:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi

80107460 <semdown>:

int semdown(int semid){
80107460:	55                   	push   %ebp
80107461:	89 e5                	mov    %esp,%ebp
80107463:	57                   	push   %edi
80107464:	56                   	push   %esi
80107465:	53                   	push   %ebx
80107466:	83 ec 0c             	sub    $0xc,%esp
80107469:	8b 7d 08             	mov    0x8(%ebp),%edi

	struct proc* p = myproc();
8010746c:	e8 af c4 ff ff       	call   80103920 <myproc>
	if (semid < 0 || semid >= NSEM || !p->psem[semid]){
80107471:	83 ff 04             	cmp    $0x4,%edi
80107474:	77 79                	ja     801074ef <semdown+0x8f>
80107476:	8b 44 b8 68          	mov    0x68(%eax,%edi,4),%eax
8010747a:	85 c0                	test   %eax,%eax
8010747c:	74 71                	je     801074ef <semdown+0x8f>
		return -1;
	}
	acquire(&semtable.lock);
8010747e:	83 ec 0c             	sub    $0xc,%esp
	struct sem *s=&semtable.sems[semid];
80107481:	8d 1c 7f             	lea    (%edi,%edi,2),%ebx
	acquire(&semtable.lock);
80107484:	68 80 75 11 80       	push   $0x80117580
	struct sem *s=&semtable.sems[semid];
80107489:	c1 e3 02             	shl    $0x2,%ebx
8010748c:	8d b3 b4 75 11 80    	lea    -0x7fee8a4c(%ebx),%esi
	for(;;){
		if (s->value > 0){
80107492:	81 c3 80 75 11 80    	add    $0x80117580,%ebx
	acquire(&semtable.lock);
80107498:	e8 a3 d0 ff ff       	call   80104540 <acquire>
		if (s->value > 0){
8010749d:	8b 43 38             	mov    0x38(%ebx),%eax
801074a0:	83 c4 10             	add    $0x10,%esp
801074a3:	85 c0                	test   %eax,%eax
801074a5:	7f 21                	jg     801074c8 <semdown+0x68>
801074a7:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801074ae:	66 90                	xchg   %ax,%ax
			 s->value--;
			 release(&semtable.lock);
			 break;
		}else
		  sleep(s,&semtable.lock);
801074b0:	83 ec 08             	sub    $0x8,%esp
801074b3:	68 80 75 11 80       	push   $0x80117580
801074b8:	56                   	push   %esi
801074b9:	e8 72 ca ff ff       	call   80103f30 <sleep>
		if (s->value > 0){
801074be:	8b 43 38             	mov    0x38(%ebx),%eax
801074c1:	83 c4 10             	add    $0x10,%esp
801074c4:	85 c0                	test   %eax,%eax
801074c6:	7e e8                	jle    801074b0 <semdown+0x50>
			 release(&semtable.lock);
801074c8:	83 ec 0c             	sub    $0xc,%esp
			 s->value--;
801074cb:	83 e8 01             	sub    $0x1,%eax
801074ce:	8d 14 7f             	lea    (%edi,%edi,2),%edx
			 release(&semtable.lock);
801074d1:	68 80 75 11 80       	push   $0x80117580
			 s->value--;
801074d6:	89 04 95 b8 75 11 80 	mov    %eax,-0x7fee8a48(,%edx,4)
			 release(&semtable.lock);
801074dd:	e8 1e d1 ff ff       	call   80104600 <release>
	}
	return 0;
801074e2:	83 c4 10             	add    $0x10,%esp
801074e5:	31 c0                	xor    %eax,%eax

}
801074e7:	8d 65 f4             	lea    -0xc(%ebp),%esp
801074ea:	5b                   	pop    %ebx
801074eb:	5e                   	pop    %esi
801074ec:	5f                   	pop    %edi
801074ed:	5d                   	pop    %ebp
801074ee:	c3                   	ret    
		return -1;
801074ef:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801074f4:	eb f1                	jmp    801074e7 <semdown+0x87>
801074f6:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801074fd:	8d 76 00             	lea    0x0(%esi),%esi

80107500 <semup>:

int semup(int semid){
80107500:	55                   	push   %ebp
80107501:	89 e5                	mov    %esp,%ebp
80107503:	53                   	push   %ebx
80107504:	83 ec 04             	sub    $0x4,%esp
80107507:	8b 5d 08             	mov    0x8(%ebp),%ebx
	struct proc* p = myproc();
8010750a:	e8 11 c4 ff ff       	call   80103920 <myproc>
  	if (semid < 0 || semid >= NSEM || !p->psem[semid]){
8010750f:	83 fb 04             	cmp    $0x4,%ebx
80107512:	77 4c                	ja     80107560 <semup+0x60>
80107514:	8b 44 98 68          	mov    0x68(%eax,%ebx,4),%eax
80107518:	85 c0                	test   %eax,%eax
8010751a:	74 44                	je     80107560 <semup+0x60>
  		return -1;
  	}
	acquire(&semtable.lock);
8010751c:	83 ec 0c             	sub    $0xc,%esp
	struct sem * s= &semtable.sems[semid];	
	s->value++;
8010751f:	8d 1c 5b             	lea    (%ebx,%ebx,2),%ebx
	acquire(&semtable.lock);
80107522:	68 80 75 11 80       	push   $0x80117580
	s->value++;
80107527:	c1 e3 02             	shl    $0x2,%ebx
	acquire(&semtable.lock);
8010752a:	e8 11 d0 ff ff       	call   80104540 <acquire>
	s->value++;
8010752f:	83 83 b8 75 11 80 01 	addl   $0x1,-0x7fee8a48(%ebx)
	struct sem * s= &semtable.sems[semid];	
80107536:	81 c3 b4 75 11 80    	add    $0x801175b4,%ebx
	release(&semtable.lock);
8010753c:	c7 04 24 80 75 11 80 	movl   $0x80117580,(%esp)
80107543:	e8 b8 d0 ff ff       	call   80104600 <release>
	wakeup(s);
80107548:	89 1c 24             	mov    %ebx,(%esp)
8010754b:	e8 a0 cb ff ff       	call   801040f0 <wakeup>
	return 0;
80107550:	83 c4 10             	add    $0x10,%esp
80107553:	31 c0                	xor    %eax,%eax

}
80107555:	8b 5d fc             	mov    -0x4(%ebp),%ebx
80107558:	c9                   	leave  
80107559:	c3                   	ret    
8010755a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
  		return -1;
80107560:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
80107565:	eb ee                	jmp    80107555 <semup+0x55>
80107567:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
8010756e:	66 90                	xchg   %ax,%ax

80107570 <semclose>:

int semclose(int semid){
80107570:	55                   	push   %ebp
80107571:	89 e5                	mov    %esp,%ebp
80107573:	53                   	push   %ebx
80107574:	83 ec 04             	sub    $0x4,%esp
80107577:	8b 5d 08             	mov    0x8(%ebp),%ebx
  struct proc* p = myproc();
8010757a:	e8 a1 c3 ff ff       	call   80103920 <myproc>
  if (semid < 0 || semid >= NSEM || !p->psem[semid]){
8010757f:	83 fb 04             	cmp    $0x4,%ebx
80107582:	77 54                	ja     801075d8 <semclose+0x68>
80107584:	8b 44 98 68          	mov    0x68(%eax,%ebx,4),%eax
80107588:	85 c0                	test   %eax,%eax
8010758a:	74 4c                	je     801075d8 <semclose+0x68>
  	return -1;
  }
  
  acquire(&semtable.lock);
8010758c:	83 ec 0c             	sub    $0xc,%esp
8010758f:	68 80 75 11 80       	push   $0x80117580
80107594:	e8 a7 cf ff ff       	call   80104540 <acquire>
  if (--semtable.sems[semid].refcounter <= 0){
80107599:	8d 04 5b             	lea    (%ebx,%ebx,2),%eax
8010759c:	83 c4 10             	add    $0x10,%esp
8010759f:	8d 14 85 80 75 11 80 	lea    -0x7fee8a80(,%eax,4),%edx
801075a6:	8b 42 34             	mov    0x34(%edx),%eax
801075a9:	83 e8 01             	sub    $0x1,%eax
801075ac:	89 42 34             	mov    %eax,0x34(%edx)
801075af:	85 c0                	test   %eax,%eax
801075b1:	7f 07                	jg     801075ba <semclose+0x4a>
	semtable.sems[semid].key = 0;
801075b3:	c7 42 3c 00 00 00 00 	movl   $0x0,0x3c(%edx)
  }
  release(&semtable.lock);
801075ba:	83 ec 0c             	sub    $0xc,%esp
801075bd:	68 80 75 11 80       	push   $0x80117580
801075c2:	e8 39 d0 ff ff       	call   80104600 <release>
 
  return 0;
801075c7:	83 c4 10             	add    $0x10,%esp
801075ca:	31 c0                	xor    %eax,%eax
}
801075cc:	8b 5d fc             	mov    -0x4(%ebp),%ebx
801075cf:	c9                   	leave  
801075d0:	c3                   	ret    
801075d1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
  	return -1;
801075d8:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
801075dd:	eb ed                	jmp    801075cc <semclose+0x5c>
801075df:	90                   	nop

801075e0 <shminit>:
	struct spinlock lock;
	struct shmblock blocks[NSHM];
} shmtable;

void shminit(void)
{
801075e0:	55                   	push   %ebp
801075e1:	89 e5                	mov    %esp,%ebp
801075e3:	83 ec 10             	sub    $0x10,%esp
  initlock(&shmtable.lock, "shmtable");
801075e6:	68 ec 82 10 80       	push   $0x801082ec
801075eb:	68 c0 b5 10 80       	push   $0x8010b5c0
801075f0:	e8 eb cd ff ff       	call   801043e0 <initlock>
}
801075f5:	83 c4 10             	add    $0x10,%esp
801075f8:	c9                   	leave  
801075f9:	c3                   	ret    
801075fa:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi

80107600 <shmget>:
	return max;
}

// return a virtual address of shared memory block (1 page)
int shmget(int key)
{
80107600:	55                   	push   %ebp
80107601:	89 e5                	mov    %esp,%ebp
80107603:	57                   	push   %edi
80107604:	56                   	push   %esi
80107605:	53                   	push   %ebx
80107606:	83 ec 1c             	sub    $0x1c,%esp
	int i, slot;
	struct proc* p = myproc();
80107609:	e8 12 c3 ff ff       	call   80103920 <myproc>
8010760e:	8b 55 08             	mov    0x8(%ebp),%edx
80107611:	89 c6                	mov    %eax,%esi
	uint max = p->sz;
80107613:	8b 00                	mov    (%eax),%eax
		if (p->shm[slot].va > max)
80107615:	39 86 80 00 00 00    	cmp    %eax,0x80(%esi)
8010761b:	0f 43 86 80 00 00 00 	cmovae 0x80(%esi),%eax
80107622:	39 86 88 00 00 00    	cmp    %eax,0x88(%esi)
80107628:	0f 43 86 88 00 00 00 	cmovae 0x88(%esi),%eax
8010762f:	39 86 90 00 00 00    	cmp    %eax,0x90(%esi)
80107635:	0f 43 86 90 00 00 00 	cmovae 0x90(%esi),%eax
8010763c:	39 86 98 00 00 00    	cmp    %eax,0x98(%esi)
80107642:	0f 43 86 98 00 00 00 	cmovae 0x98(%esi),%eax
80107649:	39 86 a0 00 00 00    	cmp    %eax,0xa0(%esi)
8010764f:	0f 43 86 a0 00 00 00 	cmovae 0xa0(%esi),%eax
	uint va = maxva(p);

	// search for free slot in p->shm array
	for(slot=0; slot<NOSHM && p->shm[slot].block; slot++)
80107656:	31 ff                	xor    %edi,%edi
80107658:	89 45 e4             	mov    %eax,-0x1c(%ebp)
8010765b:	8b 5c fe 7c          	mov    0x7c(%esi,%edi,8),%ebx
8010765f:	85 db                	test   %ebx,%ebx
80107661:	74 1d                	je     80107680 <shmget+0x80>
80107663:	83 c7 01             	add    $0x1,%edi
80107666:	83 ff 05             	cmp    $0x5,%edi
80107669:	75 f0                	jne    8010765b <shmget+0x5b>
		p->shm[slot].block = &(shmtable.blocks[i]);
	}	
	release(&shmtable.lock);
	cprintf("shmem: va=%d\n", va);
	return (int)va;
}
8010766b:	8d 65 f4             	lea    -0xc(%ebp),%esp
		return -1;
8010766e:	ba ff ff ff ff       	mov    $0xffffffff,%edx
}
80107673:	5b                   	pop    %ebx
80107674:	89 d0                	mov    %edx,%eax
80107676:	5e                   	pop    %esi
80107677:	5f                   	pop    %edi
80107678:	5d                   	pop    %ebp
80107679:	c3                   	ret    
8010767a:	8d b6 00 00 00 00    	lea    0x0(%esi),%esi
	acquire(&shmtable.lock);
80107680:	83 ec 0c             	sub    $0xc,%esp
80107683:	89 55 08             	mov    %edx,0x8(%ebp)
	for (i=0; i<NSHM && key != shmtable.blocks[i].key; i++)
80107686:	31 db                	xor    %ebx,%ebx
	acquire(&shmtable.lock);
80107688:	68 c0 b5 10 80       	push   $0x8010b5c0
8010768d:	e8 ae ce ff ff       	call   80104540 <acquire>
	for (i=0; i<NSHM && key != shmtable.blocks[i].key; i++)
80107692:	8b 55 08             	mov    0x8(%ebp),%edx
80107695:	b8 f4 b5 10 80       	mov    $0x8010b5f4,%eax
8010769a:	83 c4 10             	add    $0x10,%esp
8010769d:	8d 76 00             	lea    0x0(%esi),%esi
801076a0:	39 10                	cmp    %edx,(%eax)
801076a2:	0f 84 88 00 00 00    	je     80107730 <shmget+0x130>
801076a8:	83 c3 01             	add    $0x1,%ebx
801076ab:	83 c0 0c             	add    $0xc,%eax
801076ae:	83 fb 0a             	cmp    $0xa,%ebx
801076b1:	75 ed                	jne    801076a0 <shmget+0xa0>
		cprintf("Creating new shmem block...\n");
801076b3:	83 ec 0c             	sub    $0xc,%esp
801076b6:	89 55 08             	mov    %edx,0x8(%ebp)
		for (i=0; i<NSHM && shmtable.blocks[i].refcount != 0; i++) 
801076b9:	31 db                	xor    %ebx,%ebx
		cprintf("Creating new shmem block...\n");
801076bb:	68 03 83 10 80       	push   $0x80108303
801076c0:	e8 eb 8f ff ff       	call   801006b0 <cprintf>
		for (i=0; i<NSHM && shmtable.blocks[i].refcount != 0; i++) 
801076c5:	8b 55 08             	mov    0x8(%ebp),%edx
801076c8:	b8 f8 b5 10 80       	mov    $0x8010b5f8,%eax
801076cd:	83 c4 10             	add    $0x10,%esp
801076d0:	8b 08                	mov    (%eax),%ecx
801076d2:	85 c9                	test   %ecx,%ecx
801076d4:	74 1a                	je     801076f0 <shmget+0xf0>
801076d6:	83 c3 01             	add    $0x1,%ebx
801076d9:	83 c0 0c             	add    $0xc,%eax
801076dc:	83 fb 0a             	cmp    $0xa,%ebx
801076df:	75 ef                	jne    801076d0 <shmget+0xd0>
801076e1:	31 d2                	xor    %edx,%edx
			va = 0;	// no free shared memory descriptors
801076e3:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%ebp)
801076ea:	eb 50                	jmp    8010773c <shmget+0x13c>
801076ec:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
801076f0:	89 55 08             	mov    %edx,0x8(%ebp)
			char * frame = kalloc();
801076f3:	e8 e8 ae ff ff       	call   801025e0 <kalloc>
			shmtable.blocks[i].pa = frame;
801076f8:	8d 0c 5b             	lea    (%ebx,%ebx,2),%ecx
			memset(frame, 0, PGSIZE);
801076fb:	83 ec 04             	sub    $0x4,%esp
			shmtable.blocks[i].pa = frame;
801076fe:	8d 0c 8d c0 b5 10 80 	lea    -0x7fef4a40(,%ecx,4),%ecx
80107705:	89 41 3c             	mov    %eax,0x3c(%ecx)
			memset(frame, 0, PGSIZE);
80107708:	68 00 10 00 00       	push   $0x1000
8010770d:	6a 00                	push   $0x0
8010770f:	50                   	push   %eax
			shmtable.blocks[i].pa = frame;
80107710:	89 4d e0             	mov    %ecx,-0x20(%ebp)
			memset(frame, 0, PGSIZE);
80107713:	e8 38 cf ff ff       	call   80104650 <memset>
			if (shmtable.blocks[i].pa == 0)
80107718:	8b 4d e0             	mov    -0x20(%ebp),%ecx
8010771b:	83 c4 10             	add    $0x10,%esp
8010771e:	8b 55 08             	mov    0x8(%ebp),%edx
80107721:	8b 41 3c             	mov    0x3c(%ecx),%eax
80107724:	85 c0                	test   %eax,%eax
80107726:	74 b9                	je     801076e1 <shmget+0xe1>
				shmtable.blocks[i].key = key;
80107728:	89 51 34             	mov    %edx,0x34(%ecx)
8010772b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010772f:	90                   	nop
	if (va) {
80107730:	8b 45 e4             	mov    -0x1c(%ebp),%eax
80107733:	89 c2                	mov    %eax,%edx
80107735:	85 c0                	test   %eax,%eax
80107737:	75 37                	jne    80107770 <shmget+0x170>
80107739:	8b 55 e4             	mov    -0x1c(%ebp),%edx
	release(&shmtable.lock);
8010773c:	83 ec 0c             	sub    $0xc,%esp
8010773f:	89 55 e0             	mov    %edx,-0x20(%ebp)
80107742:	68 c0 b5 10 80       	push   $0x8010b5c0
80107747:	e8 b4 ce ff ff       	call   80104600 <release>
	cprintf("shmem: va=%d\n", va);
8010774c:	5a                   	pop    %edx
8010774d:	59                   	pop    %ecx
8010774e:	ff 75 e4             	pushl  -0x1c(%ebp)
80107751:	68 f5 82 10 80       	push   $0x801082f5
80107756:	e8 55 8f ff ff       	call   801006b0 <cprintf>
	return (int)va;
8010775b:	8b 55 e0             	mov    -0x20(%ebp),%edx
8010775e:	83 c4 10             	add    $0x10,%esp
}
80107761:	8d 65 f4             	lea    -0xc(%ebp),%esp
80107764:	5b                   	pop    %ebx
80107765:	5e                   	pop    %esi
80107766:	89 d0                	mov    %edx,%eax
80107768:	5f                   	pop    %edi
80107769:	5d                   	pop    %ebp
8010776a:	c3                   	ret    
8010776b:	8d 74 26 00          	lea    0x0(%esi,%eiz,1),%esi
8010776f:	90                   	nop
		cprintf("shmem: Mapping va=%p to pa=%p\n", va, V2P(shmtable.blocks[i].pa));
80107770:	8d 0c 5b             	lea    (%ebx,%ebx,2),%ecx
80107773:	89 45 dc             	mov    %eax,-0x24(%ebp)
80107776:	83 ec 04             	sub    $0x4,%esp
80107779:	c1 e1 02             	shl    $0x2,%ecx
8010777c:	8b 81 fc b5 10 80    	mov    -0x7fef4a04(%ecx),%eax
80107782:	8d 99 c0 b5 10 80    	lea    -0x7fef4a40(%ecx),%ebx
80107788:	89 4d e0             	mov    %ecx,-0x20(%ebp)
8010778b:	05 00 00 00 80       	add    $0x80000000,%eax
80107790:	50                   	push   %eax
80107791:	52                   	push   %edx
80107792:	68 20 83 10 80       	push   $0x80108320
80107797:	e8 14 8f ff ff       	call   801006b0 <cprintf>
		shmtable.blocks[i].refcount++;
8010779c:	83 43 38 01          	addl   $0x1,0x38(%ebx)
		mappages(p->pgdir, (void*)va, PGSIZE, V2P(shmtable.blocks[i].pa), PTE_W|PTE_U);
801077a0:	c7 04 24 06 00 00 00 	movl   $0x6,(%esp)
801077a7:	8b 43 3c             	mov    0x3c(%ebx),%eax
801077aa:	8b 5d e4             	mov    -0x1c(%ebp),%ebx
801077ad:	05 00 00 00 80       	add    $0x80000000,%eax
801077b2:	50                   	push   %eax
801077b3:	68 00 10 00 00       	push   $0x1000
801077b8:	53                   	push   %ebx
801077b9:	ff 76 04             	pushl  0x4(%esi)
801077bc:	e8 6f f3 ff ff       	call   80106b30 <mappages>
		p->shm[slot].block = &(shmtable.blocks[i]);
801077c1:	8b 4d e0             	mov    -0x20(%ebp),%ecx
801077c4:	8d 04 fe             	lea    (%esi,%edi,8),%eax
801077c7:	83 c4 20             	add    $0x20,%esp
		p->shm[slot].va = va;
801077ca:	89 98 80 00 00 00    	mov    %ebx,0x80(%eax)
		p->shm[slot].block = &(shmtable.blocks[i]);
801077d0:	8b 55 dc             	mov    -0x24(%ebp),%edx
801077d3:	81 c1 f4 b5 10 80    	add    $0x8010b5f4,%ecx
801077d9:	89 48 7c             	mov    %ecx,0x7c(%eax)
801077dc:	e9 5b ff ff ff       	jmp    8010773c <shmget+0x13c>
801077e1:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801077e8:	8d b4 26 00 00 00 00 	lea    0x0(%esi,%eiz,1),%esi
801077ef:	90                   	nop

801077f0 <shmfree>:

void shmfree(struct proc *p, int i)
{
801077f0:	55                   	push   %ebp
801077f1:	89 e5                	mov    %esp,%ebp
801077f3:	53                   	push   %ebx
801077f4:	83 ec 04             	sub    $0x4,%esp
801077f7:	8b 55 08             	mov    0x8(%ebp),%edx
801077fa:	8b 45 0c             	mov    0xc(%ebp),%eax
801077fd:	8d 1c c2             	lea    (%edx,%eax,8),%ebx
	if (--p->shm[i].block->refcount == 0) {
80107800:	8b 4b 7c             	mov    0x7c(%ebx),%ecx
80107803:	83 69 04 01          	subl   $0x1,0x4(%ecx)
80107807:	75 47                	jne    80107850 <shmfree+0x60>
	  deallocuvm(p->pgdir, p->shm[i].va + PGSIZE, p->shm[i].va);
80107809:	8b 83 80 00 00 00    	mov    0x80(%ebx),%eax
8010780f:	83 ec 04             	sub    $0x4,%esp
80107812:	50                   	push   %eax
80107813:	05 00 10 00 00       	add    $0x1000,%eax
80107818:	50                   	push   %eax
80107819:	ff 72 04             	pushl  0x4(%edx)
8010781c:	e8 9f f7 ff ff       	call   80106fc0 <deallocuvm>
	  p->shm[i].block->key = 0;
80107821:	8b 43 7c             	mov    0x7c(%ebx),%eax
80107824:	83 c4 10             	add    $0x10,%esp
80107827:	c7 00 00 00 00 00    	movl   $0x0,(%eax)
	  p->shm[i].block->pa = 0;
8010782d:	8b 43 7c             	mov    0x7c(%ebx),%eax
80107830:	c7 40 08 00 00 00 00 	movl   $0x0,0x8(%eax)
	}
	else
	  unmappages(p->pgdir, p->shm[i].va, PGSIZE);
	p->shm[i].block = 0;
80107837:	c7 43 7c 00 00 00 00 	movl   $0x0,0x7c(%ebx)
	p->shm[i].va = 0;
8010783e:	c7 83 80 00 00 00 00 	movl   $0x0,0x80(%ebx)
80107845:	00 00 00 
}
80107848:	8b 5d fc             	mov    -0x4(%ebp),%ebx
8010784b:	c9                   	leave  
8010784c:	c3                   	ret    
8010784d:	8d 76 00             	lea    0x0(%esi),%esi
	  unmappages(p->pgdir, p->shm[i].va, PGSIZE);
80107850:	83 ec 04             	sub    $0x4,%esp
80107853:	68 00 10 00 00       	push   $0x1000
80107858:	ff b3 80 00 00 00    	pushl  0x80(%ebx)
8010785e:	ff 72 04             	pushl  0x4(%edx)
80107861:	e8 4a f3 ff ff       	call   80106bb0 <unmappages>
80107866:	83 c4 10             	add    $0x10,%esp
80107869:	eb cc                	jmp    80107837 <shmfree+0x47>
