#include "types.h"
#include "user.h"
#include "fcntl.h"

int main(int argc, char const *argv[]){
  int fd,n;
  char buffer[512];
  char head[] = "123";
  char tail[] = "    456";
  char file[] = "trash";
  fd = open(file, O_CREATE | O_RDWR);

  printf(1,"Deberia escribir '123456'.\n");

  write(fd, tail, sizeof(tail));
  seek(fd, 0);
  write(fd, head, sizeof(head));
  printf(1,"Resultado:\n");

  seek(fd, 0);

  while((n = read(fd, buffer, sizeof(buffer))) > 0){
    write(1, buffer, n);
  }

  write(1,"\n",1);
  exit();
}
