#include "types.h"
#include "date.h"
#include "x86.h"
#include "defs.h"

int 
sys_gettime(void){

  struct rtcdate * i;

  if(argptr(0, (void*)&i, sizeof(*i)) < 0 ){
    return -1;
  }
  
  cmostime(i);

  return 0;
}