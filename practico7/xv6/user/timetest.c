#include "types.h"
#include "user.h"
#include "../date.h"


int main(int argc, char const *argv[]){
  printf(1, "Time test\n");
  
  struct rtcdate * t = malloc(sizeof(struct rtcdate));
  gettime(t);
 
  printf(1, "└─ Seconds: %d \n", t->second);
  printf(1, "└─ Minutes: %d \n", t->minute);
  printf(1, "└─ Hours: %d \n", t->hour);
  printf(1, "└─ Days: %d \n", t->day);
  printf(1, "└─ Months: %d \n", t->month);
  printf(1, "└─ Years: %d \n", t->year);

  exit();
}
