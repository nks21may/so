#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

int main(int argc, char *argv[]){

  if (argc != 3){
      printf(2, "Usage: cp <source><destination>\n");
      exit();
  }

  char* source = argv[1];
  char* destination = argv[2];


  int fileSource = open(source, O_RDONLY);
  if (fileSource < 0) {
      printf(2, "cp: cannot open source %s\n", source);
      exit();
  }

  int fileDestination = open(destination, O_CREATE|O_WRONLY);
  if (fileDestination  < 0) {
      printf(2, "cp: cannot open destination %s\n", destination);
      exit();
  }

  char boofer[512];
  int r = 0;
  int w = 0;  
  while ((r = read(fileSource, boofer, sizeof(boofer))) > 0) {
    w = write(fileDestination, boofer, r);
    if (w != r || w < 0)
      break;
  }


  if (r < 0 || w < 0){
    printf(2, "cp: error copying %s to %s. (r:%d, w:%d)\n", source, destination, r, w);
  }

  close(fileSource);
  close(fileDestination);

  exit();
}